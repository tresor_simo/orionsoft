package dev.soft.assurance.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Garanti.
 */
@Entity
@Table(name = "garanti")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "garanti")
public class Garanti extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "nom")
    private String nom;

    @Column(name = "capitaux")
    private String capitaux;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public Garanti nom(String nom) {
        this.nom = nom;
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getCapitaux() {
        return capitaux;
    }

    public Garanti capitaux(String capitaux) {
        this.capitaux = capitaux;
        return this;
    }

    public void setCapitaux(String capitaux) {
        this.capitaux = capitaux;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Garanti garanti = (Garanti) o;
        if(garanti.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, garanti.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Garanti{" +
            "id=" + id +
            ", nom='" + nom + "'" +
            ", capitaux='" + capitaux + "'" +
            '}';
    }
}
