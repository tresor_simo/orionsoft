package dev.soft.assurance.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A CarteRose.
 */
@Entity
@Table(name = "carte_rose")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "carterose")
public class CarteRose extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "nom_assure")
    private String nomAssure;

    @Column(name = "immatriculation")
    private String immatriculation;

    @Column(name = "marque")
    private String marque;

    @Column(name = "assureur")
    private String assureur;

    @Column(name = "bureau_emetteur")
    private String bureauEmetteur;

    @Column(name = "police")
    private String police;

    @Column(name = "date_effet")
    private LocalDate dateEffet;

    @Column(name = "date_fin")
    private LocalDate dateFin;

    @Column(name = "category_vehicule")
    private String categoryVehicule;

    @Column(name = "numero_chassis")
    private String numeroChassis;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNomAssure() {
        return nomAssure;
    }

    public CarteRose nomAssure(String nomAssure) {
        this.nomAssure = nomAssure;
        return this;
    }

    public void setNomAssure(String nomAssure) {
        this.nomAssure = nomAssure;
    }

    public String getImmatriculation() {
        return immatriculation;
    }

    public CarteRose immatriculation(String immatriculation) {
        this.immatriculation = immatriculation;
        return this;
    }

    public void setImmatriculation(String immatriculation) {
        this.immatriculation = immatriculation;
    }

    public String getMarque() {
        return marque;
    }

    public CarteRose marque(String marque) {
        this.marque = marque;
        return this;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }

    public String getAssureur() {
        return assureur;
    }

    public CarteRose assureur(String assureur) {
        this.assureur = assureur;
        return this;
    }

    public void setAssureur(String assureur) {
        this.assureur = assureur;
    }

    public String getBureauEmetteur() {
        return bureauEmetteur;
    }

    public CarteRose bureauEmetteur(String bureauEmetteur) {
        this.bureauEmetteur = bureauEmetteur;
        return this;
    }

    public void setBureauEmetteur(String bureauEmetteur) {
        this.bureauEmetteur = bureauEmetteur;
    }

    public String getPolice() {
        return police;
    }

    public CarteRose police(String police) {
        this.police = police;
        return this;
    }

    public void setPolice(String police) {
        this.police = police;
    }

    public LocalDate getDateEffet() {
        return dateEffet;
    }

    public CarteRose dateEffet(LocalDate dateEffet) {
        this.dateEffet = dateEffet;
        return this;
    }

    public void setDateEffet(LocalDate dateEffet) {
        this.dateEffet = dateEffet;
    }

    public LocalDate getDateFin() {
        return dateFin;
    }

    public CarteRose dateFin(LocalDate dateFin) {
        this.dateFin = dateFin;
        return this;
    }

    public void setDateFin(LocalDate dateFin) {
        this.dateFin = dateFin;
    }

    public String getCategoryVehicule() {
        return categoryVehicule;
    }

    public CarteRose categoryVehicule(String categoryVehicule) {
        this.categoryVehicule = categoryVehicule;
        return this;
    }

    public void setCategoryVehicule(String categoryVehicule) {
        this.categoryVehicule = categoryVehicule;
    }

    public String getNumeroChassis() {
        return numeroChassis;
    }

    public CarteRose numeroChassis(String numeroChassis) {
        this.numeroChassis = numeroChassis;
        return this;
    }

    public void setNumeroChassis(String numeroChassis) {
        this.numeroChassis = numeroChassis;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CarteRose carteRose = (CarteRose) o;
        if(carteRose.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, carteRose.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "CarteRose{" +
            "id=" + id +
            ", nomAssure='" + nomAssure + "'" +
            ", immatriculation='" + immatriculation + "'" +
            ", marque='" + marque + "'" +
            ", assureur='" + assureur + "'" +
            ", bureauEmetteur='" + bureauEmetteur + "'" +
            ", police='" + police + "'" +
            ", dateEffet='" + dateEffet + "'" +
            ", dateFin='" + dateFin + "'" +
            ", categoryVehicule='" + categoryVehicule + "'" +
            ", numeroChassis='" + numeroChassis + "'" +
            '}';
    }
}
