package dev.soft.assurance.domain.enumeration;

/**
 * The VILLE enumeration.
 */
public enum VILLE {
    YAOUNDE,DOUALA,BERTOUA
}
