package dev.soft.assurance.domain.enumeration;

/**
 * The STATUS_CONTRAT enumeration.
 */
public enum STATUS_CONTRAT {
    NOUVEAU,RENOUVELE
}
