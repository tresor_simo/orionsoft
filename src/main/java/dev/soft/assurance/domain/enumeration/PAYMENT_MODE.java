package dev.soft.assurance.domain.enumeration;

/**
 * The PAYMENT_MODE enumeration.
 */
public enum PAYMENT_MODE {
    ESPECE,CHEQUE,CARTE
}
