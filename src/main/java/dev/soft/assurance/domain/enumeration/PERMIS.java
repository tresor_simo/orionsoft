package dev.soft.assurance.domain.enumeration;

/**
 * The PERMIS enumeration.
 */
public enum PERMIS {
    A,B,C,D,E,F
}
