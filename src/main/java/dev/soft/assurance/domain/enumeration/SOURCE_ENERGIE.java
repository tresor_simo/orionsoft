package dev.soft.assurance.domain.enumeration;

/**
 * The SOURCE_ENERGIE enumeration.
 */
public enum SOURCE_ENERGIE {
    DIESEL,ESSENCE
}
