package dev.soft.assurance.domain.enumeration;

/**
 * The NATURE_PAIEMENT enumeration.
 */
public enum NATURE_PAIEMENT {
    SOLDE,ACOMPTE
}
