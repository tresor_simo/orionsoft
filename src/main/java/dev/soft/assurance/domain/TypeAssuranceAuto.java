package dev.soft.assurance.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A TypeAssuranceAuto.
 */
@Entity
@Table(name = "type_assurance_auto")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "typeassuranceauto")
public class TypeAssuranceAuto extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "nom")
    private String nom;

    @Column(name = "nom_pro")
    private String nomPro;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public TypeAssuranceAuto nom(String nom) {
        this.nom = nom;
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getNomPro() {
        return nomPro;
    }

    public TypeAssuranceAuto nomPro(String nomPro) {
        this.nomPro = nomPro;
        return this;
    }

    public void setNomPro(String nomPro) {
        this.nomPro = nomPro;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TypeAssuranceAuto typeAssuranceAuto = (TypeAssuranceAuto) o;
        if(typeAssuranceAuto.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, typeAssuranceAuto.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "TypeAssuranceAuto{" +
            "id=" + id +
            ", nom='" + nom + "'" +
            ", nomPro='" + nomPro + "'" +
            '}';
    }
}
