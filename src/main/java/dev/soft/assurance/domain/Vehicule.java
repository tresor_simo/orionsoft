package dev.soft.assurance.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

import dev.soft.assurance.domain.enumeration.SOURCE_ENERGIE;

/**
 * A Vehicule.
 */
@Entity
@Table(name = "vehicule")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "vehicule")
public class Vehicule extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "immatriculation")
    private String immatriculation;

    @Column(name = "marque")
    private String marque;

    @Column(name = "puissance", precision=10, scale=2)
    private BigDecimal puissance;

    @Enumerated(EnumType.STRING)
    @Column(name = "source")
    private SOURCE_ENERGIE source;

    @Column(name = "numero_chassi")
    private String numeroChassi;

    @Column(name = "date_prem_mise_circulation")
    private LocalDate datePremMiseCirculation;

    @Column(name = "nbre_place", precision=10, scale=2)
    private BigDecimal nbrePlace;

    @Column(name = "poids", precision=10, scale=2)
    private BigDecimal poids;

    @Column(name = "valeur_accessoire", precision=10, scale=2)
    private BigDecimal valeurAccessoire;

    @Column(name = "valeur_venal", precision=10, scale=2)
    private BigDecimal valeurVenal;

    @Column(name = "date_derniere_vt")
    private LocalDate dateDerniereVT;

    @Column(name = "valeur_neuve", precision=10, scale=2)
    private BigDecimal valeurNeuve;

    @Column(name = "categorie")
    private String categorie;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImmatriculation() {
        return immatriculation;
    }

    public Vehicule immatriculation(String immatriculation) {
        this.immatriculation = immatriculation;
        return this;
    }

    public void setImmatriculation(String immatriculation) {
        this.immatriculation = immatriculation;
    }

    public String getMarque() {
        return marque;
    }

    public Vehicule marque(String marque) {
        this.marque = marque;
        return this;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }

    public BigDecimal getPuissance() {
        return puissance;
    }

    public Vehicule puissance(BigDecimal puissance) {
        this.puissance = puissance;
        return this;
    }

    public void setPuissance(BigDecimal puissance) {
        this.puissance = puissance;
    }

    public SOURCE_ENERGIE getSource() {
        return source;
    }

    public Vehicule source(SOURCE_ENERGIE source) {
        this.source = source;
        return this;
    }

    public void setSource(SOURCE_ENERGIE source) {
        this.source = source;
    }

    public String getNumeroChassi() {
        return numeroChassi;
    }

    public Vehicule numeroChassi(String numeroChassi) {
        this.numeroChassi = numeroChassi;
        return this;
    }

    public void setNumeroChassi(String numeroChassi) {
        this.numeroChassi = numeroChassi;
    }

    public LocalDate getDatePremMiseCirculation() {
        return datePremMiseCirculation;
    }

    public Vehicule datePremMiseCirculation(LocalDate datePremMiseCirculation) {
        this.datePremMiseCirculation = datePremMiseCirculation;
        return this;
    }

    public void setDatePremMiseCirculation(LocalDate datePremMiseCirculation) {
        this.datePremMiseCirculation = datePremMiseCirculation;
    }

    public BigDecimal getNbrePlace() {
        return nbrePlace;
    }

    public Vehicule nbrePlace(BigDecimal nbrePlace) {
        this.nbrePlace = nbrePlace;
        return this;
    }

    public void setNbrePlace(BigDecimal nbrePlace) {
        this.nbrePlace = nbrePlace;
    }

    public BigDecimal getPoids() {
        return poids;
    }

    public Vehicule poids(BigDecimal poids) {
        this.poids = poids;
        return this;
    }

    public void setPoids(BigDecimal poids) {
        this.poids = poids;
    }

    public BigDecimal getValeurAccessoire() {
        return valeurAccessoire;
    }

    public Vehicule valeurAccessoire(BigDecimal valeurAccessoire) {
        this.valeurAccessoire = valeurAccessoire;
        return this;
    }

    public void setValeurAccessoire(BigDecimal valeurAccessoire) {
        this.valeurAccessoire = valeurAccessoire;
    }

    public BigDecimal getValeurVenal() {
        return valeurVenal;
    }

    public Vehicule valeurVenal(BigDecimal valeurVenal) {
        this.valeurVenal = valeurVenal;
        return this;
    }

    public void setValeurVenal(BigDecimal valeurVenal) {
        this.valeurVenal = valeurVenal;
    }

    public LocalDate getDateDerniereVT() {
        return dateDerniereVT;
    }

    public Vehicule dateDerniereVT(LocalDate dateDerniereVT) {
        this.dateDerniereVT = dateDerniereVT;
        return this;
    }

    public void setDateDerniereVT(LocalDate dateDerniereVT) {
        this.dateDerniereVT = dateDerniereVT;
    }

    public BigDecimal getValeurNeuve() {
        return valeurNeuve;
    }

    public Vehicule valeurNeuve(BigDecimal valeurNeuve) {
        this.valeurNeuve = valeurNeuve;
        return this;
    }

    public void setValeurNeuve(BigDecimal valeurNeuve) {
        this.valeurNeuve = valeurNeuve;
    }

    public String getCategorie() {
        return categorie;
    }

    public Vehicule categorie(String categorie) {
        this.categorie = categorie;
        return this;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Vehicule vehicule = (Vehicule) o;
        if(vehicule.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, vehicule.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Vehicule{" +
            "id=" + id +
            ", immatriculation='" + immatriculation + "'" +
            ", marque='" + marque + "'" +
            ", puissance='" + puissance + "'" +
            ", source='" + source + "'" +
            ", numeroChassi='" + numeroChassi + "'" +
            ", datePremMiseCirculation='" + datePremMiseCirculation + "'" +
            ", nbrePlace='" + nbrePlace + "'" +
            ", poids='" + poids + "'" +
            ", valeurAccessoire='" + valeurAccessoire + "'" +
            ", valeurVenal='" + valeurVenal + "'" +
            ", dateDerniereVT='" + dateDerniereVT + "'" +
            ", valeurNeuve='" + valeurNeuve + "'" +
            ", categorie='" + categorie + "'" +
            '}';
    }
}
