package dev.soft.assurance.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * A Flotte.
 */
@Entity
@Table(name = "flotte")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "flotte")
public class Flotte extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "numero_police")
    private String numeroPolice;

    @Column(name = "nbre")
    private Integer nbre;

    @Column(name = "accessoire", precision=10, scale=2)
    private BigDecimal accessoire;

    @Column(name = "etat")
    private Boolean etat;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumeroPolice() {
        return numeroPolice;
    }

    public Flotte numeroPolice(String numeroPolice) {
        this.numeroPolice = numeroPolice;
        return this;
    }

    public void setNumeroPolice(String numeroPolice) {
        this.numeroPolice = numeroPolice;
    }

    public Integer getNbre() {
        return nbre;
    }

    public Flotte nbre(Integer nbre) {
        this.nbre = nbre;
        return this;
    }

    public void setNbre(Integer nbre) {
        this.nbre = nbre;
    }

    public BigDecimal getAccessoire() {
        return accessoire;
    }

    public Flotte accessoire(BigDecimal accessoire) {
        this.accessoire = accessoire;
        return this;
    }

    public void setAccessoire(BigDecimal accessoire) {
        this.accessoire = accessoire;
    }

    public Boolean isEtat() {
        return etat;
    }

    public Flotte etat(Boolean etat) {
        this.etat = etat;
        return this;
    }

    public void setEtat(Boolean etat) {
        this.etat = etat;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Flotte flotte = (Flotte) o;
        if(flotte.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, flotte.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Flotte{" +
            "id=" + id +
            ", numeroPolice='" + numeroPolice + "'" +
            ", nbre='" + nbre + "'" +
            ", accessoire='" + accessoire + "'" +
            ", etat='" + etat + "'" +
            '}';
    }
}
