package dev.soft.assurance.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

import dev.soft.assurance.domain.enumeration.PERMIS;

import dev.soft.assurance.domain.enumeration.STATUS_CONTRAT;

/**
 * A Contrat.
 */
@Entity
@Table(name = "contrat")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "contrat")
public class Contrat extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "numero_police")
    private String numeroPolice;

    @Column(name = "date_effet")
    private LocalDate dateEffet;

    @Column(name = "duree", precision=10, scale=2)
    private BigDecimal duree;

    @Column(name = "souscripteur")
    private String souscripteur;

    @Column(name = "conducteur")
    private String conducteur;

    @Column(name = "statut_socio")
    private String statutSocio;

    @Enumerated(EnumType.STRING)
    @Column(name = "categorie_permis")
    private PERMIS categoriePermis;

    @Column(name = "prime_ttc", precision=10, scale=2)
    private BigDecimal primeTTC;

    @Column(name = "acompte", precision=10, scale=2)
    private BigDecimal acompte;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private STATUS_CONTRAT status;

    @Column(name = "date_fin")
    private LocalDate dateFin;

    @Column(name = "prime_base", precision=10, scale=2)
    private BigDecimal primeBase;

    @Column(name = "pourcentage", precision=10, scale=2)
    private BigDecimal pourcentage;

    @Column(name = "accessoire", precision=10, scale=2)
    private BigDecimal accessoire;

    @Column(name = "cout_police", precision=10, scale=2)
    private BigDecimal coutPolice;

    @Column(name = "asac", precision=10, scale=2)
    private BigDecimal asac;

    @Column(name = "carte_rose", precision=10, scale=2)
    private BigDecimal carteRose;

    @Column(name = "is_dommage_collision")
    private Boolean isDommageCollision;

    @Column(name = "pourcent_dommage_collision", precision=10, scale=2)
    private BigDecimal pourcentDommageCollision;

    @Column(name = "is_dommage_accident")
    private Boolean isDommageAccident;

    @Column(name = "pourcent_dommage_accident", precision=10, scale=2)
    private BigDecimal pourcentDommageAccident;

    @Column(name = "is_vol_incendie")
    private Boolean isVolIncendie;

    @Column(name = "pourcent_vol_incendie", precision=10, scale=2)
    private BigDecimal pourcentVolIncendie;

    @Column(name = "is_vol_electronic")
    private Boolean isVolElectronic;

    @Column(name = "pourcent_vol_electronic", precision=10, scale=2)
    private BigDecimal pourcentVolElectronic;

    @Column(name = "annuel_vol_electronic", precision=10, scale=2)
    private BigDecimal annuelVolElectronic;

    @Column(name = "is_avance_secour")
    private Boolean isAvanceSecour;

    @Column(name = "pourcent_avance_secour", precision=10, scale=2)
    private BigDecimal pourcentAvanceSecour;

    @Column(name = "annuel_avance_secour", precision=10, scale=2)
    private BigDecimal annuelAvanceSecour;

    @Column(name = "is_assistance")
    private Boolean isAssistance;

    @Column(name = "pourcent_assistance", precision=10, scale=2)
    private BigDecimal pourcentAssistance;

    @Column(name = "is_brise_glace")
    private Boolean isBriseGlace;

    @Column(name = "pourcent_brise_glace", precision=10, scale=2)
    private BigDecimal pourcentBriseGlace;

    @Column(name = "is_recour_defence")
    private Boolean isRecourDefence;

    @Column(name = "pourcent_recour_defense", precision=10, scale=2)
    private BigDecimal pourcentRecourDefense;

    @Column(name = "is_ind_acc")
    private Boolean isIndAcc;

    @Column(name = "annuel_ind_acc", precision=10, scale=2)
    private BigDecimal annuelIndAcc;

    @Column(name = "is_ipt_invalid")
    private Boolean isIptInvalid;

    @Column(name = "is_ipt_fmph")
    private Boolean isIptFMPH;

    @Column(name = "ipt_deces_capitaux", precision=10, scale=2)
    private BigDecimal iptDecesCapitaux;

    @Column(name = "ipt_invalid_capitaux", precision=10, scale=2)
    private BigDecimal iptInvalidCapitaux;

    @Column(name = "ipt_fmph_capitaux", precision=10, scale=2)
    private BigDecimal iptFMPHCapitaux;

    @Column(name = "ipt_deces_annuel", precision=10, scale=2)
    private BigDecimal iptDecesAnnuel;

    @Column(name = "ipt_invalid_annuel", precision=10, scale=2)
    private BigDecimal iptInvalidAnnuel;

    @Column(name = "ipt_fmph_annuel", precision=10, scale=2)
    private BigDecimal iptFMPHAnnuel;

    @Column(name = "bonus", precision=10, scale=2)
    private BigDecimal bonus;

    @Column(name = "surplus", precision=10, scale=2)
    private BigDecimal surplus;

    @Column(name = "numero_carte_rose")
    private String numeroCarteRose;

    @Column(name = "numero_attestation")
    private String numeroAttestation;

    @Column(name = "avenant", precision=10, scale=2)
    private BigDecimal avenant;

    @Column(name = "id_prev", precision=10, scale=2)
    private BigDecimal idPrev;

    @Column(name = "id_next", precision=10, scale=2)
    private BigDecimal idNext;

    @Column(name = "nette", precision=10, scale=2)
    private BigDecimal nette;

    @Column(name = "tva", precision=10, scale=2)
    private BigDecimal tva;

    @Column(name = "etat")
    private Boolean etat;

    @ManyToOne
    private Agent agent;

    @ManyToOne
    private Assureur assureur;

    @ManyToOne
    private Vehicule vehicule;

    @ManyToOne
    private Zone zone;

    @ManyToOne
    private Assure assure;

    @ManyToOne
    private TypeAssuranceAuto typeAssuranceAuto;

    @ManyToOne
    private Agence agence;

    @ManyToOne
    private Flotte flotte;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumeroPolice() {
        return numeroPolice;
    }

    public Contrat numeroPolice(String numeroPolice) {
        this.numeroPolice = numeroPolice;
        return this;
    }

    public void setNumeroPolice(String numeroPolice) {
        this.numeroPolice = numeroPolice;
    }

    public LocalDate getDateEffet() {
        return dateEffet;
    }

    public Contrat dateEffet(LocalDate dateEffet) {
        this.dateEffet = dateEffet;
        return this;
    }

    public void setDateEffet(LocalDate dateEffet) {
        this.dateEffet = dateEffet;
    }

    public BigDecimal getDuree() {
        return duree;
    }

    public Contrat duree(BigDecimal duree) {
        this.duree = duree;
        return this;
    }

    public void setDuree(BigDecimal duree) {
        this.duree = duree;
    }

    public String getSouscripteur() {
        return souscripteur;
    }

    public Contrat souscripteur(String souscripteur) {
        this.souscripteur = souscripteur;
        return this;
    }

    public void setSouscripteur(String souscripteur) {
        this.souscripteur = souscripteur;
    }

    public String getConducteur() {
        return conducteur;
    }

    public Contrat conducteur(String conducteur) {
        this.conducteur = conducteur;
        return this;
    }

    public void setConducteur(String conducteur) {
        this.conducteur = conducteur;
    }

    public String getStatutSocio() {
        return statutSocio;
    }

    public Contrat statutSocio(String statutSocio) {
        this.statutSocio = statutSocio;
        return this;
    }

    public void setStatutSocio(String statutSocio) {
        this.statutSocio = statutSocio;
    }

    public PERMIS getCategoriePermis() {
        return categoriePermis;
    }

    public Contrat categoriePermis(PERMIS categoriePermis) {
        this.categoriePermis = categoriePermis;
        return this;
    }

    public void setCategoriePermis(PERMIS categoriePermis) {
        this.categoriePermis = categoriePermis;
    }

    public BigDecimal getPrimeTTC() {
        return primeTTC;
    }

    public Contrat primeTTC(BigDecimal primeTTC) {
        this.primeTTC = primeTTC;
        return this;
    }

    public void setPrimeTTC(BigDecimal primeTTC) {
        this.primeTTC = primeTTC;
    }

    public BigDecimal getAcompte() {
        return acompte;
    }

    public Contrat acompte(BigDecimal acompte) {
        this.acompte = acompte;
        return this;
    }

    public void setAcompte(BigDecimal acompte) {
        this.acompte = acompte;
    }

    public STATUS_CONTRAT getStatus() {
        return status;
    }

    public Contrat status(STATUS_CONTRAT status) {
        this.status = status;
        return this;
    }

    public void setStatus(STATUS_CONTRAT status) {
        this.status = status;
    }

    public LocalDate getDateFin() {
        return dateFin;
    }

    public Contrat dateFin(LocalDate dateFin) {
        this.dateFin = dateFin;
        return this;
    }

    public void setDateFin(LocalDate dateFin) {
        this.dateFin = dateFin;
    }

    public BigDecimal getPrimeBase() {
        return primeBase;
    }

    public Contrat primeBase(BigDecimal primeBase) {
        this.primeBase = primeBase;
        return this;
    }

    public void setPrimeBase(BigDecimal primeBase) {
        this.primeBase = primeBase;
    }

    public BigDecimal getPourcentage() {
        return pourcentage;
    }

    public Contrat pourcentage(BigDecimal pourcentage) {
        this.pourcentage = pourcentage;
        return this;
    }

    public void setPourcentage(BigDecimal pourcentage) {
        this.pourcentage = pourcentage;
    }

    public BigDecimal getAccessoire() {
        return accessoire;
    }

    public Contrat accessoire(BigDecimal accessoire) {
        this.accessoire = accessoire;
        return this;
    }

    public void setAccessoire(BigDecimal accessoire) {
        this.accessoire = accessoire;
    }

    public BigDecimal getCoutPolice() {
        return coutPolice;
    }

    public Contrat coutPolice(BigDecimal coutPolice) {
        this.coutPolice = coutPolice;
        return this;
    }

    public void setCoutPolice(BigDecimal coutPolice) {
        this.coutPolice = coutPolice;
    }

    public BigDecimal getAsac() {
        return asac;
    }

    public Contrat asac(BigDecimal asac) {
        this.asac = asac;
        return this;
    }

    public void setAsac(BigDecimal asac) {
        this.asac = asac;
    }

    public BigDecimal getCarteRose() {
        return carteRose;
    }

    public Contrat carteRose(BigDecimal carteRose) {
        this.carteRose = carteRose;
        return this;
    }

    public void setCarteRose(BigDecimal carteRose) {
        this.carteRose = carteRose;
    }

    public Boolean isIsDommageCollision() {
        return isDommageCollision;
    }

    public Contrat isDommageCollision(Boolean isDommageCollision) {
        this.isDommageCollision = isDommageCollision;
        return this;
    }

    public void setIsDommageCollision(Boolean isDommageCollision) {
        this.isDommageCollision = isDommageCollision;
    }

    public BigDecimal getPourcentDommageCollision() {
        return pourcentDommageCollision;
    }

    public Contrat pourcentDommageCollision(BigDecimal pourcentDommageCollision) {
        this.pourcentDommageCollision = pourcentDommageCollision;
        return this;
    }

    public void setPourcentDommageCollision(BigDecimal pourcentDommageCollision) {
        this.pourcentDommageCollision = pourcentDommageCollision;
    }

    public Boolean isIsDommageAccident() {
        return isDommageAccident;
    }

    public Contrat isDommageAccident(Boolean isDommageAccident) {
        this.isDommageAccident = isDommageAccident;
        return this;
    }

    public void setIsDommageAccident(Boolean isDommageAccident) {
        this.isDommageAccident = isDommageAccident;
    }

    public BigDecimal getPourcentDommageAccident() {
        return pourcentDommageAccident;
    }

    public Contrat pourcentDommageAccident(BigDecimal pourcentDommageAccident) {
        this.pourcentDommageAccident = pourcentDommageAccident;
        return this;
    }

    public void setPourcentDommageAccident(BigDecimal pourcentDommageAccident) {
        this.pourcentDommageAccident = pourcentDommageAccident;
    }

    public Boolean isIsVolIncendie() {
        return isVolIncendie;
    }

    public Contrat isVolIncendie(Boolean isVolIncendie) {
        this.isVolIncendie = isVolIncendie;
        return this;
    }

    public void setIsVolIncendie(Boolean isVolIncendie) {
        this.isVolIncendie = isVolIncendie;
    }

    public BigDecimal getPourcentVolIncendie() {
        return pourcentVolIncendie;
    }

    public Contrat pourcentVolIncendie(BigDecimal pourcentVolIncendie) {
        this.pourcentVolIncendie = pourcentVolIncendie;
        return this;
    }

    public void setPourcentVolIncendie(BigDecimal pourcentVolIncendie) {
        this.pourcentVolIncendie = pourcentVolIncendie;
    }

    public Boolean isIsVolElectronic() {
        return isVolElectronic;
    }

    public Contrat isVolElectronic(Boolean isVolElectronic) {
        this.isVolElectronic = isVolElectronic;
        return this;
    }

    public void setIsVolElectronic(Boolean isVolElectronic) {
        this.isVolElectronic = isVolElectronic;
    }

    public BigDecimal getPourcentVolElectronic() {
        return pourcentVolElectronic;
    }

    public Contrat pourcentVolElectronic(BigDecimal pourcentVolElectronic) {
        this.pourcentVolElectronic = pourcentVolElectronic;
        return this;
    }

    public void setPourcentVolElectronic(BigDecimal pourcentVolElectronic) {
        this.pourcentVolElectronic = pourcentVolElectronic;
    }

    public BigDecimal getAnnuelVolElectronic() {
        return annuelVolElectronic;
    }

    public Contrat annuelVolElectronic(BigDecimal annuelVolElectronic) {
        this.annuelVolElectronic = annuelVolElectronic;
        return this;
    }

    public void setAnnuelVolElectronic(BigDecimal annuelVolElectronic) {
        this.annuelVolElectronic = annuelVolElectronic;
    }

    public Boolean isIsAvanceSecour() {
        return isAvanceSecour;
    }

    public Contrat isAvanceSecour(Boolean isAvanceSecour) {
        this.isAvanceSecour = isAvanceSecour;
        return this;
    }

    public void setIsAvanceSecour(Boolean isAvanceSecour) {
        this.isAvanceSecour = isAvanceSecour;
    }

    public BigDecimal getPourcentAvanceSecour() {
        return pourcentAvanceSecour;
    }

    public Contrat pourcentAvanceSecour(BigDecimal pourcentAvanceSecour) {
        this.pourcentAvanceSecour = pourcentAvanceSecour;
        return this;
    }

    public void setPourcentAvanceSecour(BigDecimal pourcentAvanceSecour) {
        this.pourcentAvanceSecour = pourcentAvanceSecour;
    }

    public BigDecimal getAnnuelAvanceSecour() {
        return annuelAvanceSecour;
    }

    public Contrat annuelAvanceSecour(BigDecimal annuelAvanceSecour) {
        this.annuelAvanceSecour = annuelAvanceSecour;
        return this;
    }

    public void setAnnuelAvanceSecour(BigDecimal annuelAvanceSecour) {
        this.annuelAvanceSecour = annuelAvanceSecour;
    }

    public Boolean isIsAssistance() {
        return isAssistance;
    }

    public Contrat isAssistance(Boolean isAssistance) {
        this.isAssistance = isAssistance;
        return this;
    }

    public void setIsAssistance(Boolean isAssistance) {
        this.isAssistance = isAssistance;
    }

    public BigDecimal getPourcentAssistance() {
        return pourcentAssistance;
    }

    public Contrat pourcentAssistance(BigDecimal pourcentAssistance) {
        this.pourcentAssistance = pourcentAssistance;
        return this;
    }

    public void setPourcentAssistance(BigDecimal pourcentAssistance) {
        this.pourcentAssistance = pourcentAssistance;
    }

    public Boolean isIsBriseGlace() {
        return isBriseGlace;
    }

    public Contrat isBriseGlace(Boolean isBriseGlace) {
        this.isBriseGlace = isBriseGlace;
        return this;
    }

    public void setIsBriseGlace(Boolean isBriseGlace) {
        this.isBriseGlace = isBriseGlace;
    }

    public BigDecimal getPourcentBriseGlace() {
        return pourcentBriseGlace;
    }

    public Contrat pourcentBriseGlace(BigDecimal pourcentBriseGlace) {
        this.pourcentBriseGlace = pourcentBriseGlace;
        return this;
    }

    public void setPourcentBriseGlace(BigDecimal pourcentBriseGlace) {
        this.pourcentBriseGlace = pourcentBriseGlace;
    }

    public Boolean isIsRecourDefence() {
        return isRecourDefence;
    }

    public Contrat isRecourDefence(Boolean isRecourDefence) {
        this.isRecourDefence = isRecourDefence;
        return this;
    }

    public void setIsRecourDefence(Boolean isRecourDefence) {
        this.isRecourDefence = isRecourDefence;
    }

    public BigDecimal getPourcentRecourDefense() {
        return pourcentRecourDefense;
    }

    public Contrat pourcentRecourDefense(BigDecimal pourcentRecourDefense) {
        this.pourcentRecourDefense = pourcentRecourDefense;
        return this;
    }

    public void setPourcentRecourDefense(BigDecimal pourcentRecourDefense) {
        this.pourcentRecourDefense = pourcentRecourDefense;
    }

    public Boolean isIsIndAcc() {
        return isIndAcc;
    }

    public Contrat isIndAcc(Boolean isIndAcc) {
        this.isIndAcc = isIndAcc;
        return this;
    }

    public void setIsIndAcc(Boolean isIndAcc) {
        this.isIndAcc = isIndAcc;
    }

    public BigDecimal getAnnuelIndAcc() {
        return annuelIndAcc;
    }

    public Contrat annuelIndAcc(BigDecimal annuelIndAcc) {
        this.annuelIndAcc = annuelIndAcc;
        return this;
    }

    public void setAnnuelIndAcc(BigDecimal annuelIndAcc) {
        this.annuelIndAcc = annuelIndAcc;
    }

    public Boolean isIsIptInvalid() {
        return isIptInvalid;
    }

    public Contrat isIptInvalid(Boolean isIptInvalid) {
        this.isIptInvalid = isIptInvalid;
        return this;
    }

    public void setIsIptInvalid(Boolean isIptInvalid) {
        this.isIptInvalid = isIptInvalid;
    }

    public Boolean isIsIptFMPH() {
        return isIptFMPH;
    }

    public Contrat isIptFMPH(Boolean isIptFMPH) {
        this.isIptFMPH = isIptFMPH;
        return this;
    }

    public void setIsIptFMPH(Boolean isIptFMPH) {
        this.isIptFMPH = isIptFMPH;
    }

    public BigDecimal getIptDecesCapitaux() {
        return iptDecesCapitaux;
    }

    public Contrat iptDecesCapitaux(BigDecimal iptDecesCapitaux) {
        this.iptDecesCapitaux = iptDecesCapitaux;
        return this;
    }

    public void setIptDecesCapitaux(BigDecimal iptDecesCapitaux) {
        this.iptDecesCapitaux = iptDecesCapitaux;
    }

    public BigDecimal getIptInvalidCapitaux() {
        return iptInvalidCapitaux;
    }

    public Contrat iptInvalidCapitaux(BigDecimal iptInvalidCapitaux) {
        this.iptInvalidCapitaux = iptInvalidCapitaux;
        return this;
    }

    public void setIptInvalidCapitaux(BigDecimal iptInvalidCapitaux) {
        this.iptInvalidCapitaux = iptInvalidCapitaux;
    }

    public BigDecimal getIptFMPHCapitaux() {
        return iptFMPHCapitaux;
    }

    public Contrat iptFMPHCapitaux(BigDecimal iptFMPHCapitaux) {
        this.iptFMPHCapitaux = iptFMPHCapitaux;
        return this;
    }

    public void setIptFMPHCapitaux(BigDecimal iptFMPHCapitaux) {
        this.iptFMPHCapitaux = iptFMPHCapitaux;
    }

    public BigDecimal getIptDecesAnnuel() {
        return iptDecesAnnuel;
    }

    public Contrat iptDecesAnnuel(BigDecimal iptDecesAnnuel) {
        this.iptDecesAnnuel = iptDecesAnnuel;
        return this;
    }

    public void setIptDecesAnnuel(BigDecimal iptDecesAnnuel) {
        this.iptDecesAnnuel = iptDecesAnnuel;
    }

    public BigDecimal getIptInvalidAnnuel() {
        return iptInvalidAnnuel;
    }

    public Contrat iptInvalidAnnuel(BigDecimal iptInvalidAnnuel) {
        this.iptInvalidAnnuel = iptInvalidAnnuel;
        return this;
    }

    public void setIptInvalidAnnuel(BigDecimal iptInvalidAnnuel) {
        this.iptInvalidAnnuel = iptInvalidAnnuel;
    }

    public BigDecimal getIptFMPHAnnuel() {
        return iptFMPHAnnuel;
    }

    public Contrat iptFMPHAnnuel(BigDecimal iptFMPHAnnuel) {
        this.iptFMPHAnnuel = iptFMPHAnnuel;
        return this;
    }

    public void setIptFMPHAnnuel(BigDecimal iptFMPHAnnuel) {
        this.iptFMPHAnnuel = iptFMPHAnnuel;
    }

    public BigDecimal getBonus() {
        return bonus;
    }

    public Contrat bonus(BigDecimal bonus) {
        this.bonus = bonus;
        return this;
    }

    public void setBonus(BigDecimal bonus) {
        this.bonus = bonus;
    }

    public BigDecimal getSurplus() {
        return surplus;
    }

    public Contrat surplus(BigDecimal surplus) {
        this.surplus = surplus;
        return this;
    }

    public void setSurplus(BigDecimal surplus) {
        this.surplus = surplus;
    }

    public String getNumeroCarteRose() {
        return numeroCarteRose;
    }

    public Contrat numeroCarteRose(String numeroCarteRose) {
        this.numeroCarteRose = numeroCarteRose;
        return this;
    }

    public void setNumeroCarteRose(String numeroCarteRose) {
        this.numeroCarteRose = numeroCarteRose;
    }

    public String getNumeroAttestation() {
        return numeroAttestation;
    }

    public Contrat numeroAttestation(String numeroAttestation) {
        this.numeroAttestation = numeroAttestation;
        return this;
    }

    public void setNumeroAttestation(String numeroAttestation) {
        this.numeroAttestation = numeroAttestation;
    }

    public BigDecimal getAvenant() {
        return avenant;
    }

    public Contrat avenant(BigDecimal avenant) {
        this.avenant = avenant;
        return this;
    }

    public void setAvenant(BigDecimal avenant) {
        this.avenant = avenant;
    }

    public BigDecimal getIdPrev() {
        return idPrev;
    }

    public Contrat idPrev(BigDecimal idPrev) {
        this.idPrev = idPrev;
        return this;
    }

    public void setIdPrev(BigDecimal idPrev) {
        this.idPrev = idPrev;
    }

    public BigDecimal getIdNext() {
        return idNext;
    }

    public Contrat idNext(BigDecimal idNext) {
        this.idNext = idNext;
        return this;
    }

    public void setIdNext(BigDecimal idNext) {
        this.idNext = idNext;
    }

    public BigDecimal getNette() {
        return nette;
    }

    public Contrat nette(BigDecimal nette) {
        this.nette = nette;
        return this;
    }

    public void setNette(BigDecimal nette) {
        this.nette = nette;
    }

    public BigDecimal getTva() {
        return tva;
    }

    public Contrat tva(BigDecimal tva) {
        this.tva = tva;
        return this;
    }

    public void setTva(BigDecimal tva) {
        this.tva = tva;
    }

    public Boolean isEtat() {
        return etat;
    }

    public Contrat etat(Boolean etat) {
        this.etat = etat;
        return this;
    }

    public void setEtat(Boolean etat) {
        this.etat = etat;
    }

    public Agent getAgent() {
        return agent;
    }

    public Contrat agent(Agent agent) {
        this.agent = agent;
        return this;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public Assureur getAssureur() {
        return assureur;
    }

    public Contrat assureur(Assureur assureur) {
        this.assureur = assureur;
        return this;
    }

    public void setAssureur(Assureur assureur) {
        this.assureur = assureur;
    }

    public Vehicule getVehicule() {
        return vehicule;
    }

    public Contrat vehicule(Vehicule vehicule) {
        this.vehicule = vehicule;
        return this;
    }

    public void setVehicule(Vehicule vehicule) {
        this.vehicule = vehicule;
    }

    public Zone getZone() {
        return zone;
    }

    public Contrat zone(Zone zone) {
        this.zone = zone;
        return this;
    }

    public void setZone(Zone zone) {
        this.zone = zone;
    }

    public Assure getAssure() {
        return assure;
    }

    public Contrat assure(Assure assure) {
        this.assure = assure;
        return this;
    }

    public void setAssure(Assure assure) {
        this.assure = assure;
    }

    public TypeAssuranceAuto getTypeAssuranceAuto() {
        return typeAssuranceAuto;
    }

    public Contrat typeAssuranceAuto(TypeAssuranceAuto typeAssuranceAuto) {
        this.typeAssuranceAuto = typeAssuranceAuto;
        return this;
    }

    public void setTypeAssuranceAuto(TypeAssuranceAuto typeAssuranceAuto) {
        this.typeAssuranceAuto = typeAssuranceAuto;
    }

    public Agence getAgence() {
        return agence;
    }

    public Contrat agence(Agence agence) {
        this.agence = agence;
        return this;
    }

    public void setAgence(Agence agence) {
        this.agence = agence;
    }

    public Flotte getFlotte() {
        return flotte;
    }

    public Contrat flotte(Flotte flotte) {
        this.flotte = flotte;
        return this;
    }

    public void setFlotte(Flotte flotte) {
        this.flotte = flotte;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Contrat contrat = (Contrat) o;
        if(contrat.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, contrat.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Contrat{" +
            "id=" + id +
            ", numeroPolice='" + numeroPolice + "'" +
            ", dateEffet='" + dateEffet + "'" +
            ", duree='" + duree + "'" +
            ", souscripteur='" + souscripteur + "'" +
            ", conducteur='" + conducteur + "'" +
            ", statutSocio='" + statutSocio + "'" +
            ", categoriePermis='" + categoriePermis + "'" +
            ", primeTTC='" + primeTTC + "'" +
            ", acompte='" + acompte + "'" +
            ", status='" + status + "'" +
            ", dateFin='" + dateFin + "'" +
            ", primeBase='" + primeBase + "'" +
            ", pourcentage='" + pourcentage + "'" +
            ", accessoire='" + accessoire + "'" +
            ", coutPolice='" + coutPolice + "'" +
            ", asac='" + asac + "'" +
            ", carteRose='" + carteRose + "'" +
            ", isDommageCollision='" + isDommageCollision + "'" +
            ", pourcentDommageCollision='" + pourcentDommageCollision + "'" +
            ", isDommageAccident='" + isDommageAccident + "'" +
            ", pourcentDommageAccident='" + pourcentDommageAccident + "'" +
            ", isVolIncendie='" + isVolIncendie + "'" +
            ", pourcentVolIncendie='" + pourcentVolIncendie + "'" +
            ", isVolElectronic='" + isVolElectronic + "'" +
            ", pourcentVolElectronic='" + pourcentVolElectronic + "'" +
            ", annuelVolElectronic='" + annuelVolElectronic + "'" +
            ", isAvanceSecour='" + isAvanceSecour + "'" +
            ", pourcentAvanceSecour='" + pourcentAvanceSecour + "'" +
            ", annuelAvanceSecour='" + annuelAvanceSecour + "'" +
            ", isAssistance='" + isAssistance + "'" +
            ", pourcentAssistance='" + pourcentAssistance + "'" +
            ", isBriseGlace='" + isBriseGlace + "'" +
            ", pourcentBriseGlace='" + pourcentBriseGlace + "'" +
            ", isRecourDefence='" + isRecourDefence + "'" +
            ", pourcentRecourDefense='" + pourcentRecourDefense + "'" +
            ", isIndAcc='" + isIndAcc + "'" +
            ", annuelIndAcc='" + annuelIndAcc + "'" +
            ", isIptInvalid='" + isIptInvalid + "'" +
            ", isIptFMPH='" + isIptFMPH + "'" +
            ", iptDecesCapitaux='" + iptDecesCapitaux + "'" +
            ", iptInvalidCapitaux='" + iptInvalidCapitaux + "'" +
            ", iptFMPHCapitaux='" + iptFMPHCapitaux + "'" +
            ", iptDecesAnnuel='" + iptDecesAnnuel + "'" +
            ", iptInvalidAnnuel='" + iptInvalidAnnuel + "'" +
            ", iptFMPHAnnuel='" + iptFMPHAnnuel + "'" +
            ", bonus='" + bonus + "'" +
            ", surplus='" + surplus + "'" +
            ", numeroCarteRose='" + numeroCarteRose + "'" +
            ", numeroAttestation='" + numeroAttestation + "'" +
            ", avenant='" + avenant + "'" +
            ", idPrev='" + idPrev + "'" +
            ", idNext='" + idNext + "'" +
            ", nette='" + nette + "'" +
            ", tva='" + tva + "'" +
            ", etat='" + etat + "'" +
            '}';
    }
}
