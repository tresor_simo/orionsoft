package dev.soft.assurance.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * A GarantiContrat.
 */
@Entity
@Table(name = "garanti_contrat")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "garanticontrat")
public class GarantiContrat extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "annuel", precision=10, scale=2)
    private BigDecimal annuel;

    @Column(name = "pourcentage", precision=10, scale=2)
    private BigDecimal pourcentage;

    @Column(name = "nette", precision=10, scale=2)
    private BigDecimal nette;

    @Column(name = "bonus", precision=10, scale=2)
    private BigDecimal bonus;

    @Column(name = "surplus", precision=10, scale=2)
    private BigDecimal surplus;

    @Column(name = "nettefinal", precision=10, scale=2)
    private BigDecimal nettefinal;

    @ManyToOne
    private Contrat contrat;

    @ManyToOne
    private Garanti garanti;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getAnnuel() {
        return annuel;
    }

    public GarantiContrat annuel(BigDecimal annuel) {
        this.annuel = annuel;
        return this;
    }

    public void setAnnuel(BigDecimal annuel) {
        this.annuel = annuel;
    }

    public BigDecimal getPourcentage() {
        return pourcentage;
    }

    public GarantiContrat pourcentage(BigDecimal pourcentage) {
        this.pourcentage = pourcentage;
        return this;
    }

    public void setPourcentage(BigDecimal pourcentage) {
        this.pourcentage = pourcentage;
    }

    public BigDecimal getNette() {
        return nette;
    }

    public GarantiContrat nette(BigDecimal nette) {
        this.nette = nette;
        return this;
    }

    public void setNette(BigDecimal nette) {
        this.nette = nette;
    }

    public BigDecimal getBonus() {
        return bonus;
    }

    public GarantiContrat bonus(BigDecimal bonus) {
        this.bonus = bonus;
        return this;
    }

    public void setBonus(BigDecimal bonus) {
        this.bonus = bonus;
    }

    public BigDecimal getSurplus() {
        return surplus;
    }

    public GarantiContrat surplus(BigDecimal surplus) {
        this.surplus = surplus;
        return this;
    }

    public void setSurplus(BigDecimal surplus) {
        this.surplus = surplus;
    }

    public BigDecimal getNettefinal() {
        return nettefinal;
    }

    public GarantiContrat nettefinal(BigDecimal nettefinal) {
        this.nettefinal = nettefinal;
        return this;
    }

    public void setNettefinal(BigDecimal nettefinal) {
        this.nettefinal = nettefinal;
    }

    public Contrat getContrat() {
        return contrat;
    }

    public GarantiContrat contrat(Contrat contrat) {
        this.contrat = contrat;
        return this;
    }

    public void setContrat(Contrat contrat) {
        this.contrat = contrat;
    }

    public Garanti getGaranti() {
        return garanti;
    }

    public GarantiContrat garanti(Garanti garanti) {
        this.garanti = garanti;
        return this;
    }

    public void setGaranti(Garanti garanti) {
        this.garanti = garanti;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        GarantiContrat garantiContrat = (GarantiContrat) o;
        if(garantiContrat.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, garantiContrat.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "GarantiContrat{" +
            "id=" + id +
            ", annuel='" + annuel + "'" +
            ", pourcentage='" + pourcentage + "'" +
            ", nette='" + nette + "'" +
            ", bonus='" + bonus + "'" +
            ", surplus='" + surplus + "'" +
            ", nettefinal='" + nettefinal + "'" +
            '}';
    }
}
