package dev.soft.assurance.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A Vignette.
 */
@Entity
@Table(name = "vignette")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "vignette")
public class Vignette extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "date_effet")
    private LocalDate dateEffet;

    @Column(name = "date_fin")
    private LocalDate dateFin;

    @Column(name = "montant", precision=10, scale=2)
    private BigDecimal montant;

    @Column(name = "etat")
    private Boolean etat;

    @ManyToOne
    private Vehicule vehicule;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDateEffet() {
        return dateEffet;
    }

    public Vignette dateEffet(LocalDate dateEffet) {
        this.dateEffet = dateEffet;
        return this;
    }

    public void setDateEffet(LocalDate dateEffet) {
        this.dateEffet = dateEffet;
    }

    public LocalDate getDateFin() {
        return dateFin;
    }

    public Vignette dateFin(LocalDate dateFin) {
        this.dateFin = dateFin;
        return this;
    }

    public void setDateFin(LocalDate dateFin) {
        this.dateFin = dateFin;
    }

    public BigDecimal getMontant() {
        return montant;
    }

    public Vignette montant(BigDecimal montant) {
        this.montant = montant;
        return this;
    }

    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }

    public Boolean isEtat() {
        return etat;
    }

    public Vignette etat(Boolean etat) {
        this.etat = etat;
        return this;
    }

    public void setEtat(Boolean etat) {
        this.etat = etat;
    }

    public Vehicule getVehicule() {
        return vehicule;
    }

    public Vignette vehicule(Vehicule vehicule) {
        this.vehicule = vehicule;
        return this;
    }

    public void setVehicule(Vehicule vehicule) {
        this.vehicule = vehicule;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Vignette vignette = (Vignette) o;
        if(vignette.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, vignette.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Vignette{" +
            "id=" + id +
            ", dateEffet='" + dateEffet + "'" +
            ", dateFin='" + dateFin + "'" +
            ", montant='" + montant + "'" +
            ", etat='" + etat + "'" +
            '}';
    }
}
