package dev.soft.assurance.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

import dev.soft.assurance.domain.enumeration.VILLE;

/**
 * A Assure.
 */
@Entity
@Table(name = "assure")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "assure")
public class Assure extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "nom")
    private String nom;

    @Column(name = "number")
    private String number;

    @Enumerated(EnumType.STRING)
    @Column(name = "ville")
    private VILLE ville;

    @Column(name = "adress")
    private String adress;

    @Column(name = "profession")
    private String profession;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public Assure nom(String nom) {
        this.nom = nom;
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getNumber() {
        return number;
    }

    public Assure number(String number) {
        this.number = number;
        return this;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public VILLE getVille() {
        return ville;
    }

    public Assure ville(VILLE ville) {
        this.ville = ville;
        return this;
    }

    public void setVille(VILLE ville) {
        this.ville = ville;
    }

    public String getAdress() {
        return adress;
    }

    public Assure adress(String adress) {
        this.adress = adress;
        return this;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public String getProfession() {
        return profession;
    }

    public Assure profession(String profession) {
        this.profession = profession;
        return this;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Assure assure = (Assure) o;
        if(assure.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, assure.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Assure{" +
            "id=" + id +
            ", nom='" + nom + "'" +
            ", number='" + number + "'" +
            ", ville='" + ville + "'" +
            ", adress='" + adress + "'" +
            ", profession='" + profession + "'" +
            '}';
    }
}
