package dev.soft.assurance.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Objects;

import dev.soft.assurance.domain.enumeration.PAYMENT_MODE;

import dev.soft.assurance.domain.enumeration.NATURE_PAIEMENT;

/**
 * A Paiement.
 */
@Entity
@Table(name = "paiement")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "paiement")
public class Paiement extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "date_paiement")
    private ZonedDateTime datePaiement;

    @Column(name = "somme", precision=10, scale=2)
    private BigDecimal somme;

    @Enumerated(EnumType.STRING)
    @Column(name = "mode_paiement")
    private PAYMENT_MODE modePaiement;

    @Enumerated(EnumType.STRING)
    @Column(name = "nature_paiement")
    private NATURE_PAIEMENT naturePaiement;

    @Column(name = "pc")
    private String pc;

    @Column(name = "numero_avenant")
    private String numeroAvenant;

    @Column(name = "etat")
    private Boolean etat;

    @ManyToOne
    private UserAssurance userAssurance;

    @ManyToOne
    private Contrat contrat;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getDatePaiement() {
        return datePaiement;
    }

    public Paiement datePaiement(ZonedDateTime datePaiement) {
        this.datePaiement = datePaiement;
        return this;
    }

    public void setDatePaiement(ZonedDateTime datePaiement) {
        this.datePaiement = datePaiement;
    }

    public BigDecimal getSomme() {
        return somme;
    }

    public Paiement somme(BigDecimal somme) {
        this.somme = somme;
        return this;
    }

    public void setSomme(BigDecimal somme) {
        this.somme = somme;
    }

    public PAYMENT_MODE getModePaiement() {
        return modePaiement;
    }

    public Paiement modePaiement(PAYMENT_MODE modePaiement) {
        this.modePaiement = modePaiement;
        return this;
    }

    public void setModePaiement(PAYMENT_MODE modePaiement) {
        this.modePaiement = modePaiement;
    }

    public NATURE_PAIEMENT getNaturePaiement() {
        return naturePaiement;
    }

    public Paiement naturePaiement(NATURE_PAIEMENT naturePaiement) {
        this.naturePaiement = naturePaiement;
        return this;
    }

    public void setNaturePaiement(NATURE_PAIEMENT naturePaiement) {
        this.naturePaiement = naturePaiement;
    }

    public String getPc() {
        return pc;
    }

    public Paiement pc(String pc) {
        this.pc = pc;
        return this;
    }

    public void setPc(String pc) {
        this.pc = pc;
    }

    public String getNumeroAvenant() {
        return numeroAvenant;
    }

    public Paiement numeroAvenant(String numeroAvenant) {
        this.numeroAvenant = numeroAvenant;
        return this;
    }

    public void setNumeroAvenant(String numeroAvenant) {
        this.numeroAvenant = numeroAvenant;
    }

    public Boolean isEtat() {
        return etat;
    }

    public Paiement etat(Boolean etat) {
        this.etat = etat;
        return this;
    }

    public void setEtat(Boolean etat) {
        this.etat = etat;
    }

    public UserAssurance getUserAssurance() {
        return userAssurance;
    }

    public Paiement userAssurance(UserAssurance userAssurance) {
        this.userAssurance = userAssurance;
        return this;
    }

    public void setUserAssurance(UserAssurance userAssurance) {
        this.userAssurance = userAssurance;
    }

    public Contrat getContrat() {
        return contrat;
    }

    public Paiement contrat(Contrat contrat) {
        this.contrat = contrat;
        return this;
    }

    public void setContrat(Contrat contrat) {
        this.contrat = contrat;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Paiement paiement = (Paiement) o;
        if(paiement.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, paiement.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Paiement{" +
            "id=" + id +
            ", datePaiement='" + datePaiement + "'" +
            ", somme='" + somme + "'" +
            ", modePaiement='" + modePaiement + "'" +
            ", naturePaiement='" + naturePaiement + "'" +
            ", pc='" + pc + "'" +
            ", numeroAvenant='" + numeroAvenant + "'" +
            ", etat='" + etat + "'" +
            '}';
    }
}
