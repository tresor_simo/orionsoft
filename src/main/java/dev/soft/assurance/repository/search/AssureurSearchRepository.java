package dev.soft.assurance.repository.search;

import dev.soft.assurance.domain.Assureur;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Assureur entity.
 */
public interface AssureurSearchRepository extends ElasticsearchRepository<Assureur, Long> {
}
