package dev.soft.assurance.repository.search;

import dev.soft.assurance.domain.CarteRose;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the CarteRose entity.
 */
public interface CarteRoseSearchRepository extends ElasticsearchRepository<CarteRose, Long> {
}
