package dev.soft.assurance.repository.search;

import dev.soft.assurance.domain.UserAssurance;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the UserAssurance entity.
 */
public interface UserAssuranceSearchRepository extends ElasticsearchRepository<UserAssurance, Long> {
}
