package dev.soft.assurance.repository.search;

import dev.soft.assurance.domain.Contrat;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Contrat entity.
 */
public interface ContratSearchRepository extends ElasticsearchRepository<Contrat, Long> {
}
