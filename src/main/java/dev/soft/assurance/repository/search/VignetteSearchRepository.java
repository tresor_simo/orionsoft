package dev.soft.assurance.repository.search;

import dev.soft.assurance.domain.Vignette;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Vignette entity.
 */
public interface VignetteSearchRepository extends ElasticsearchRepository<Vignette, Long> {
}
