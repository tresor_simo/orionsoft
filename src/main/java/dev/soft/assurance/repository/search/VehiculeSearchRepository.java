package dev.soft.assurance.repository.search;

import dev.soft.assurance.domain.Vehicule;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Vehicule entity.
 */
public interface VehiculeSearchRepository extends ElasticsearchRepository<Vehicule, Long> {
}
