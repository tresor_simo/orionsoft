package dev.soft.assurance.repository.search;

import dev.soft.assurance.domain.NumeroAgence;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the NumeroAgence entity.
 */
public interface NumeroAgenceSearchRepository extends ElasticsearchRepository<NumeroAgence, Long> {
}
