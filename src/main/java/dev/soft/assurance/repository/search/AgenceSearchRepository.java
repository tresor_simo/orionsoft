package dev.soft.assurance.repository.search;

import dev.soft.assurance.domain.Agence;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Agence entity.
 */
public interface AgenceSearchRepository extends ElasticsearchRepository<Agence, Long> {
}
