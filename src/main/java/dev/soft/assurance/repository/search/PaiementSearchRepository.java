package dev.soft.assurance.repository.search;

import dev.soft.assurance.domain.Paiement;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Paiement entity.
 */
public interface PaiementSearchRepository extends ElasticsearchRepository<Paiement, Long> {
}
