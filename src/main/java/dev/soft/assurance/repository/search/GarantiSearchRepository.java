package dev.soft.assurance.repository.search;

import dev.soft.assurance.domain.Garanti;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Garanti entity.
 */
public interface GarantiSearchRepository extends ElasticsearchRepository<Garanti, Long> {
}
