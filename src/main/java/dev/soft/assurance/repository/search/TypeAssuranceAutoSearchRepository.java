package dev.soft.assurance.repository.search;

import dev.soft.assurance.domain.TypeAssuranceAuto;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the TypeAssuranceAuto entity.
 */
public interface TypeAssuranceAutoSearchRepository extends ElasticsearchRepository<TypeAssuranceAuto, Long> {
}
