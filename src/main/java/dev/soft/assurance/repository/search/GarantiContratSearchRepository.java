package dev.soft.assurance.repository.search;

import dev.soft.assurance.domain.GarantiContrat;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the GarantiContrat entity.
 */
public interface GarantiContratSearchRepository extends ElasticsearchRepository<GarantiContrat, Long> {
}
