package dev.soft.assurance.repository.search;

import dev.soft.assurance.domain.Assure;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Assure entity.
 */
public interface AssureSearchRepository extends ElasticsearchRepository<Assure, Long> {
}
