package dev.soft.assurance.repository.search;

import dev.soft.assurance.domain.Flotte;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Flotte entity.
 */
public interface FlotteSearchRepository extends ElasticsearchRepository<Flotte, Long> {
}
