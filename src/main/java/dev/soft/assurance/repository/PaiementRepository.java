package dev.soft.assurance.repository;

import dev.soft.assurance.domain.Paiement;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Paiement entity.
 */
@SuppressWarnings("unused")
public interface PaiementRepository extends JpaRepository<Paiement,Long> {
   List <Paiement> findByContratId(Long contrat_id);
}
