package dev.soft.assurance.repository;

import dev.soft.assurance.domain.Agent;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Agent entity.
 */
@SuppressWarnings("unused")
public interface AgentRepository extends JpaRepository<Agent,Long> {

}
