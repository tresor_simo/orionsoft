package dev.soft.assurance.repository;

import dev.soft.assurance.domain.GarantiContrat;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the GarantiContrat entity.
 */
@SuppressWarnings("unused")
public interface GarantiContratRepository extends JpaRepository<GarantiContrat,Long> {

}
