package dev.soft.assurance.repository;

import dev.soft.assurance.domain.UserAssurance;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the UserAssurance entity.
 */
@SuppressWarnings("unused")
public interface UserAssuranceRepository extends JpaRepository<UserAssurance,Long> {

    @Query("select userAssurance from UserAssurance userAssurance where userAssurance.user.login = ?#{principal.username}")
    List<UserAssurance> findByUserIsCurrentUser();

}
