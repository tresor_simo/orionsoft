package dev.soft.assurance.repository;

import dev.soft.assurance.domain.Vignette;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Vignette entity.
 */
@SuppressWarnings("unused")
public interface VignetteRepository extends JpaRepository<Vignette,Long> {
  List <Vignette> findByVehiculeId(Long vehicule_id);
}
