package dev.soft.assurance.repository;

import dev.soft.assurance.domain.NumeroAgence;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the NumeroAgence entity.
 */
@SuppressWarnings("unused")
public interface NumeroAgenceRepository extends JpaRepository<NumeroAgence,Long> {

}
