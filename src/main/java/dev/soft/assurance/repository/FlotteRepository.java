package dev.soft.assurance.repository;

import dev.soft.assurance.domain.Flotte;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Flotte entity.
 */
@SuppressWarnings("unused")
public interface FlotteRepository extends JpaRepository<Flotte,Long> {

}
