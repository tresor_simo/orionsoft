package dev.soft.assurance.repository;

import dev.soft.assurance.domain.Contrat;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Contrat entity.
 */
@SuppressWarnings("unused")
public interface ContratRepository extends JpaRepository<Contrat,Long> {

}
