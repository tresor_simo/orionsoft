package dev.soft.assurance.repository;

import dev.soft.assurance.domain.Agence;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Agence entity.
 */
@SuppressWarnings("unused")
public interface AgenceRepository extends JpaRepository<Agence,Long> {

}
