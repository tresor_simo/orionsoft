package dev.soft.assurance.repository;

import dev.soft.assurance.domain.Assure;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Assure entity.
 */
@SuppressWarnings("unused")
public interface AssureRepository extends JpaRepository<Assure,Long> {

}
