package dev.soft.assurance.repository;

import dev.soft.assurance.domain.Garanti;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Garanti entity.
 */
@SuppressWarnings("unused")
public interface GarantiRepository extends JpaRepository<Garanti,Long> {

}
