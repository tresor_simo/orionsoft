package dev.soft.assurance.repository;

import dev.soft.assurance.domain.TypeAssuranceAuto;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the TypeAssuranceAuto entity.
 */
@SuppressWarnings("unused")
public interface TypeAssuranceAutoRepository extends JpaRepository<TypeAssuranceAuto,Long> {

}
