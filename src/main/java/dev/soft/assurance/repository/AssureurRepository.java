package dev.soft.assurance.repository;

import dev.soft.assurance.domain.Assureur;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Assureur entity.
 */
@SuppressWarnings("unused")
public interface AssureurRepository extends JpaRepository<Assureur,Long> {

}
