package dev.soft.assurance.repository;

import dev.soft.assurance.domain.CarteRose;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the CarteRose entity.
 */
@SuppressWarnings("unused")
public interface CarteRoseRepository extends JpaRepository<CarteRose,Long> {

}
