package dev.soft.assurance.repository;

import dev.soft.assurance.domain.Setting;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Setting entity.
 */
@SuppressWarnings("unused")
public interface SettingRepository extends JpaRepository<Setting,Long> {

}
