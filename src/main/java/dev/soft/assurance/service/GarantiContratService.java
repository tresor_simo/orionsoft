package dev.soft.assurance.service;

import dev.soft.assurance.domain.GarantiContrat;
import dev.soft.assurance.repository.GarantiContratRepository;
import dev.soft.assurance.repository.search.GarantiContratSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing GarantiContrat.
 */
@Service
@Transactional
public class GarantiContratService {

    private final Logger log = LoggerFactory.getLogger(GarantiContratService.class);
    
    @Inject
    private GarantiContratRepository garantiContratRepository;

    @Inject
    private GarantiContratSearchRepository garantiContratSearchRepository;

    /**
     * Save a garantiContrat.
     *
     * @param garantiContrat the entity to save
     * @return the persisted entity
     */
    public GarantiContrat save(GarantiContrat garantiContrat) {
        log.debug("Request to save GarantiContrat : {}", garantiContrat);
        GarantiContrat result = garantiContratRepository.save(garantiContrat);
        garantiContratSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the garantiContrats.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<GarantiContrat> findAll(Pageable pageable) {
        log.debug("Request to get all GarantiContrats");
        Page<GarantiContrat> result = garantiContratRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get one garantiContrat by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public GarantiContrat findOne(Long id) {
        log.debug("Request to get GarantiContrat : {}", id);
        GarantiContrat garantiContrat = garantiContratRepository.findOne(id);
        return garantiContrat;
    }

    /**
     *  Delete the  garantiContrat by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete GarantiContrat : {}", id);
        garantiContratRepository.delete(id);
        garantiContratSearchRepository.delete(id);
    }

    /**
     * Search for the garantiContrat corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<GarantiContrat> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of GarantiContrats for query {}", query);
        Page<GarantiContrat> result = garantiContratSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
