package dev.soft.assurance.service;

import dev.soft.assurance.domain.Setting;
import dev.soft.assurance.repository.SettingRepository;
import dev.soft.assurance.repository.search.SettingSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Setting.
 */
@Service
@Transactional
public class SettingService {

    private final Logger log = LoggerFactory.getLogger(SettingService.class);
    
    @Inject
    private SettingRepository settingRepository;

    @Inject
    private SettingSearchRepository settingSearchRepository;

    /**
     * Save a setting.
     *
     * @param setting the entity to save
     * @return the persisted entity
     */
    public Setting save(Setting setting) {
        log.debug("Request to save Setting : {}", setting);
        Setting result = settingRepository.save(setting);
        settingSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the settings.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<Setting> findAll(Pageable pageable) {
        log.debug("Request to get all Settings");
        Page<Setting> result = settingRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get one setting by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public Setting findOne(Long id) {
        log.debug("Request to get Setting : {}", id);
        Setting setting = settingRepository.findOne(id);
        return setting;
    }

    /**
     *  Delete the  setting by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Setting : {}", id);
        settingRepository.delete(id);
        settingSearchRepository.delete(id);
    }

    /**
     * Search for the setting corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Setting> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Settings for query {}", query);
        Page<Setting> result = settingSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
