package dev.soft.assurance.service;

import dev.soft.assurance.domain.Assure;
import dev.soft.assurance.repository.AssureRepository;
import dev.soft.assurance.repository.search.AssureSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Assure.
 */
@Service
@Transactional
public class AssureService {

    private final Logger log = LoggerFactory.getLogger(AssureService.class);
    
    @Inject
    private AssureRepository assureRepository;

    @Inject
    private AssureSearchRepository assureSearchRepository;

    /**
     * Save a assure.
     *
     * @param assure the entity to save
     * @return the persisted entity
     */
    public Assure save(Assure assure) {
        log.debug("Request to save Assure : {}", assure);
        Assure result = assureRepository.save(assure);
        assureSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the assures.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<Assure> findAll(Pageable pageable) {
        log.debug("Request to get all Assures");
        Page<Assure> result = assureRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get one assure by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public Assure findOne(Long id) {
        log.debug("Request to get Assure : {}", id);
        Assure assure = assureRepository.findOne(id);
        return assure;
    }

    /**
     *  Delete the  assure by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Assure : {}", id);
        assureRepository.delete(id);
        assureSearchRepository.delete(id);
    }

    /**
     * Search for the assure corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Assure> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Assures for query {}", query);
        Page<Assure> result = assureSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
