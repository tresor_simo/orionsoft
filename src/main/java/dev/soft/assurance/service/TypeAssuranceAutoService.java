package dev.soft.assurance.service;

import dev.soft.assurance.domain.TypeAssuranceAuto;
import dev.soft.assurance.repository.TypeAssuranceAutoRepository;
import dev.soft.assurance.repository.search.TypeAssuranceAutoSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing TypeAssuranceAuto.
 */
@Service
@Transactional
public class TypeAssuranceAutoService {

    private final Logger log = LoggerFactory.getLogger(TypeAssuranceAutoService.class);
    
    @Inject
    private TypeAssuranceAutoRepository typeAssuranceAutoRepository;

    @Inject
    private TypeAssuranceAutoSearchRepository typeAssuranceAutoSearchRepository;

    /**
     * Save a typeAssuranceAuto.
     *
     * @param typeAssuranceAuto the entity to save
     * @return the persisted entity
     */
    public TypeAssuranceAuto save(TypeAssuranceAuto typeAssuranceAuto) {
        log.debug("Request to save TypeAssuranceAuto : {}", typeAssuranceAuto);
        TypeAssuranceAuto result = typeAssuranceAutoRepository.save(typeAssuranceAuto);
        typeAssuranceAutoSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the typeAssuranceAutos.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<TypeAssuranceAuto> findAll(Pageable pageable) {
        log.debug("Request to get all TypeAssuranceAutos");
        Page<TypeAssuranceAuto> result = typeAssuranceAutoRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get one typeAssuranceAuto by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public TypeAssuranceAuto findOne(Long id) {
        log.debug("Request to get TypeAssuranceAuto : {}", id);
        TypeAssuranceAuto typeAssuranceAuto = typeAssuranceAutoRepository.findOne(id);
        return typeAssuranceAuto;
    }

    /**
     *  Delete the  typeAssuranceAuto by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete TypeAssuranceAuto : {}", id);
        typeAssuranceAutoRepository.delete(id);
        typeAssuranceAutoSearchRepository.delete(id);
    }

    /**
     * Search for the typeAssuranceAuto corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<TypeAssuranceAuto> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of TypeAssuranceAutos for query {}", query);
        Page<TypeAssuranceAuto> result = typeAssuranceAutoSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
