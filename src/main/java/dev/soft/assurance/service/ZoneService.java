package dev.soft.assurance.service;

import dev.soft.assurance.domain.Zone;
import dev.soft.assurance.repository.ZoneRepository;
import dev.soft.assurance.repository.search.ZoneSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Zone.
 */
@Service
@Transactional
public class ZoneService {

    private final Logger log = LoggerFactory.getLogger(ZoneService.class);
    
    @Inject
    private ZoneRepository zoneRepository;

    @Inject
    private ZoneSearchRepository zoneSearchRepository;

    /**
     * Save a zone.
     *
     * @param zone the entity to save
     * @return the persisted entity
     */
    public Zone save(Zone zone) {
        log.debug("Request to save Zone : {}", zone);
        Zone result = zoneRepository.save(zone);
        zoneSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the zones.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<Zone> findAll(Pageable pageable) {
        log.debug("Request to get all Zones");
        Page<Zone> result = zoneRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get one zone by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public Zone findOne(Long id) {
        log.debug("Request to get Zone : {}", id);
        Zone zone = zoneRepository.findOne(id);
        return zone;
    }

    /**
     *  Delete the  zone by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Zone : {}", id);
        zoneRepository.delete(id);
        zoneSearchRepository.delete(id);
    }

    /**
     * Search for the zone corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Zone> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Zones for query {}", query);
        Page<Zone> result = zoneSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
