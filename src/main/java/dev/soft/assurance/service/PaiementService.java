package dev.soft.assurance.service;

import dev.soft.assurance.domain.Paiement;
import dev.soft.assurance.repository.PaiementRepository;
import dev.soft.assurance.repository.search.PaiementSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Paiement.
 */
@Service
@Transactional
public class PaiementService {

    private final Logger log = LoggerFactory.getLogger(PaiementService.class);

    @Inject
    private PaiementRepository paiementRepository;

    @Inject
    private PaiementSearchRepository paiementSearchRepository;

    /**
     * Save a paiement.
     *
     * @param paiement the entity to save
     * @return the persisted entity
     */
    public Paiement save(Paiement paiement) {
        log.debug("Request to save Paiement : {}", paiement);
        paiement.setEtat(false);
        Paiement result = paiementRepository.save(paiement);
        paiementSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the paiements.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Paiement> findAll(Pageable pageable) {
        log.debug("Request to get all Paiements");
        Page<Paiement> result = paiementRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get one paiement by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public Paiement findOne(Long id) {
        log.debug("Request to get Paiement : {}", id);
        Paiement paiement = paiementRepository.findOne(id);
        return paiement;
    }

    /**
     *  Delete the  paiement by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Paiement : {}", id);
        paiementRepository.delete(id);
        paiementSearchRepository.delete(id);
    }

    /**
     * Search for the paiement corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Paiement> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Paiements for query {}", query);
        Page<Paiement> result = paiementSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }

    public List<Paiement> findByContrat(Long contrat_id){
        List <Paiement> paiements = paiementRepository.findByContratId(contrat_id);
        return paiements;
    }
}
