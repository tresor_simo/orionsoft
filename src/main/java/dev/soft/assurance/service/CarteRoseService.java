package dev.soft.assurance.service;

import dev.soft.assurance.domain.CarteRose;
import dev.soft.assurance.repository.CarteRoseRepository;
import dev.soft.assurance.repository.search.CarteRoseSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing CarteRose.
 */
@Service
@Transactional
public class CarteRoseService {

    private final Logger log = LoggerFactory.getLogger(CarteRoseService.class);
    
    @Inject
    private CarteRoseRepository carteRoseRepository;

    @Inject
    private CarteRoseSearchRepository carteRoseSearchRepository;

    /**
     * Save a carteRose.
     *
     * @param carteRose the entity to save
     * @return the persisted entity
     */
    public CarteRose save(CarteRose carteRose) {
        log.debug("Request to save CarteRose : {}", carteRose);
        CarteRose result = carteRoseRepository.save(carteRose);
        carteRoseSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the carteRoses.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<CarteRose> findAll(Pageable pageable) {
        log.debug("Request to get all CarteRoses");
        Page<CarteRose> result = carteRoseRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get one carteRose by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public CarteRose findOne(Long id) {
        log.debug("Request to get CarteRose : {}", id);
        CarteRose carteRose = carteRoseRepository.findOne(id);
        return carteRose;
    }

    /**
     *  Delete the  carteRose by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete CarteRose : {}", id);
        carteRoseRepository.delete(id);
        carteRoseSearchRepository.delete(id);
    }

    /**
     * Search for the carteRose corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<CarteRose> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of CarteRoses for query {}", query);
        Page<CarteRose> result = carteRoseSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
