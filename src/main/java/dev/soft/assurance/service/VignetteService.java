package dev.soft.assurance.service;

import dev.soft.assurance.domain.Vignette;
import dev.soft.assurance.repository.VignetteRepository;
import dev.soft.assurance.repository.search.VignetteSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Vignette.
 */
@Service
@Transactional
public class VignetteService {

    private final Logger log = LoggerFactory.getLogger(VignetteService.class);

    @Inject
    private VignetteRepository vignetteRepository;

    @Inject
    private VignetteSearchRepository vignetteSearchRepository;

    /**
     * Save a vignette.
     *
     * @param vignette the entity to save
     * @return the persisted entity
     */
    public Vignette save(Vignette vignette) {
        log.debug("Request to save Vignette : {}", vignette);
        Vignette result = vignetteRepository.save(vignette);
        vignetteSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the vignettes.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Vignette> findAll(Pageable pageable) {
        log.debug("Request to get all Vignettes");
        Page<Vignette> result = vignetteRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get one vignette by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public Vignette findOne(Long id) {
        log.debug("Request to get Vignette : {}", id);
        Vignette vignette = vignetteRepository.findOne(id);
        return vignette;
    }

    /**
     *  Delete the  vignette by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Vignette : {}", id);
        vignetteRepository.delete(id);
        vignetteSearchRepository.delete(id);
    }

    /**
     * Search for the vignette corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Vignette> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Vignettes for query {}", query);
        Page<Vignette> result = vignetteSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }

    public List<Vignette> findByVehicule(Long vehicule_id){
        List <Vignette> vignettes = vignetteRepository.findByVehiculeId(vehicule_id);
        return vignettes;
    }
}
