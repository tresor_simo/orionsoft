package dev.soft.assurance.service;

import dev.soft.assurance.domain.Vehicule;
import dev.soft.assurance.repository.VehiculeRepository;
import dev.soft.assurance.repository.search.VehiculeSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Vehicule.
 */
@Service
@Transactional
public class VehiculeService {

    private final Logger log = LoggerFactory.getLogger(VehiculeService.class);
    
    @Inject
    private VehiculeRepository vehiculeRepository;

    @Inject
    private VehiculeSearchRepository vehiculeSearchRepository;

    /**
     * Save a vehicule.
     *
     * @param vehicule the entity to save
     * @return the persisted entity
     */
    public Vehicule save(Vehicule vehicule) {
        log.debug("Request to save Vehicule : {}", vehicule);
        Vehicule result = vehiculeRepository.save(vehicule);
        vehiculeSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the vehicules.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<Vehicule> findAll(Pageable pageable) {
        log.debug("Request to get all Vehicules");
        Page<Vehicule> result = vehiculeRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get one vehicule by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public Vehicule findOne(Long id) {
        log.debug("Request to get Vehicule : {}", id);
        Vehicule vehicule = vehiculeRepository.findOne(id);
        return vehicule;
    }

    /**
     *  Delete the  vehicule by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Vehicule : {}", id);
        vehiculeRepository.delete(id);
        vehiculeSearchRepository.delete(id);
    }

    /**
     * Search for the vehicule corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Vehicule> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Vehicules for query {}", query);
        Page<Vehicule> result = vehiculeSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
