package dev.soft.assurance.service;

import dev.soft.assurance.domain.NumeroAgence;
import dev.soft.assurance.repository.NumeroAgenceRepository;
import dev.soft.assurance.repository.search.NumeroAgenceSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing NumeroAgence.
 */
@Service
@Transactional
public class NumeroAgenceService {

    private final Logger log = LoggerFactory.getLogger(NumeroAgenceService.class);
    
    @Inject
    private NumeroAgenceRepository numeroAgenceRepository;

    @Inject
    private NumeroAgenceSearchRepository numeroAgenceSearchRepository;

    /**
     * Save a numeroAgence.
     *
     * @param numeroAgence the entity to save
     * @return the persisted entity
     */
    public NumeroAgence save(NumeroAgence numeroAgence) {
        log.debug("Request to save NumeroAgence : {}", numeroAgence);
        NumeroAgence result = numeroAgenceRepository.save(numeroAgence);
        numeroAgenceSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the numeroAgences.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<NumeroAgence> findAll(Pageable pageable) {
        log.debug("Request to get all NumeroAgences");
        Page<NumeroAgence> result = numeroAgenceRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get one numeroAgence by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public NumeroAgence findOne(Long id) {
        log.debug("Request to get NumeroAgence : {}", id);
        NumeroAgence numeroAgence = numeroAgenceRepository.findOne(id);
        return numeroAgence;
    }

    /**
     *  Delete the  numeroAgence by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete NumeroAgence : {}", id);
        numeroAgenceRepository.delete(id);
        numeroAgenceSearchRepository.delete(id);
    }

    /**
     * Search for the numeroAgence corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<NumeroAgence> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of NumeroAgences for query {}", query);
        Page<NumeroAgence> result = numeroAgenceSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
