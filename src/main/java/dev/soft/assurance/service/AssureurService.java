package dev.soft.assurance.service;

import dev.soft.assurance.domain.Assureur;
import dev.soft.assurance.repository.AssureurRepository;
import dev.soft.assurance.repository.search.AssureurSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Assureur.
 */
@Service
@Transactional
public class AssureurService {

    private final Logger log = LoggerFactory.getLogger(AssureurService.class);
    
    @Inject
    private AssureurRepository assureurRepository;

    @Inject
    private AssureurSearchRepository assureurSearchRepository;

    /**
     * Save a assureur.
     *
     * @param assureur the entity to save
     * @return the persisted entity
     */
    public Assureur save(Assureur assureur) {
        log.debug("Request to save Assureur : {}", assureur);
        Assureur result = assureurRepository.save(assureur);
        assureurSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the assureurs.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<Assureur> findAll(Pageable pageable) {
        log.debug("Request to get all Assureurs");
        Page<Assureur> result = assureurRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get one assureur by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public Assureur findOne(Long id) {
        log.debug("Request to get Assureur : {}", id);
        Assureur assureur = assureurRepository.findOne(id);
        return assureur;
    }

    /**
     *  Delete the  assureur by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Assureur : {}", id);
        assureurRepository.delete(id);
        assureurSearchRepository.delete(id);
    }

    /**
     * Search for the assureur corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Assureur> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Assureurs for query {}", query);
        Page<Assureur> result = assureurSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
