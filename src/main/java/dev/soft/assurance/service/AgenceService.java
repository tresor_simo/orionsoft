package dev.soft.assurance.service;

import dev.soft.assurance.domain.Agence;
import dev.soft.assurance.repository.AgenceRepository;
import dev.soft.assurance.repository.search.AgenceSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Agence.
 */
@Service
@Transactional
public class AgenceService {

    private final Logger log = LoggerFactory.getLogger(AgenceService.class);
    
    @Inject
    private AgenceRepository agenceRepository;

    @Inject
    private AgenceSearchRepository agenceSearchRepository;

    /**
     * Save a agence.
     *
     * @param agence the entity to save
     * @return the persisted entity
     */
    public Agence save(Agence agence) {
        log.debug("Request to save Agence : {}", agence);
        Agence result = agenceRepository.save(agence);
        agenceSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the agences.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<Agence> findAll(Pageable pageable) {
        log.debug("Request to get all Agences");
        Page<Agence> result = agenceRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get one agence by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public Agence findOne(Long id) {
        log.debug("Request to get Agence : {}", id);
        Agence agence = agenceRepository.findOne(id);
        return agence;
    }

    /**
     *  Delete the  agence by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Agence : {}", id);
        agenceRepository.delete(id);
        agenceSearchRepository.delete(id);
    }

    /**
     * Search for the agence corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Agence> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Agences for query {}", query);
        Page<Agence> result = agenceSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
