package dev.soft.assurance.service;

import dev.soft.assurance.domain.UserAssurance;
import dev.soft.assurance.repository.UserAssuranceRepository;
import dev.soft.assurance.repository.search.UserAssuranceSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing UserAssurance.
 */
@Service
@Transactional
public class UserAssuranceService {

    private final Logger log = LoggerFactory.getLogger(UserAssuranceService.class);
    
    @Inject
    private UserAssuranceRepository userAssuranceRepository;

    @Inject
    private UserAssuranceSearchRepository userAssuranceSearchRepository;

    /**
     * Save a userAssurance.
     *
     * @param userAssurance the entity to save
     * @return the persisted entity
     */
    public UserAssurance save(UserAssurance userAssurance) {
        log.debug("Request to save UserAssurance : {}", userAssurance);
        UserAssurance result = userAssuranceRepository.save(userAssurance);
        userAssuranceSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the userAssurances.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<UserAssurance> findAll(Pageable pageable) {
        log.debug("Request to get all UserAssurances");
        Page<UserAssurance> result = userAssuranceRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get one userAssurance by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public UserAssurance findOne(Long id) {
        log.debug("Request to get UserAssurance : {}", id);
        UserAssurance userAssurance = userAssuranceRepository.findOne(id);
        return userAssurance;
    }

    /**
     *  Delete the  userAssurance by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete UserAssurance : {}", id);
        userAssuranceRepository.delete(id);
        userAssuranceSearchRepository.delete(id);
    }

    /**
     * Search for the userAssurance corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<UserAssurance> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of UserAssurances for query {}", query);
        Page<UserAssurance> result = userAssuranceSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
