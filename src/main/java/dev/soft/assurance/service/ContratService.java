package dev.soft.assurance.service;

import dev.soft.assurance.domain.Contrat;
import dev.soft.assurance.domain.Vehicule;
import dev.soft.assurance.domain.Vignette;
import dev.soft.assurance.domain.Assure;
import dev.soft.assurance.domain.Paiement;
import dev.soft.assurance.domain.Setting;
import dev.soft.assurance.repository.ContratRepository;
import dev.soft.assurance.repository.PaiementRepository;
import dev.soft.assurance.repository.VignetteRepository;
import dev.soft.assurance.repository.SettingRepository;
import dev.soft.assurance.repository.search.ContratSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;
import java.time.ZonedDateTime;
import javax.sql.DataSource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import java.time.LocalDate;
import java.math.BigDecimal;





import javax.inject.Inject;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import java.util.*;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Contrat.
 */
@Service
@Transactional
public class ContratService {

  private static final String driverClassName = "com.zaxxer.hikari.HikariDataSource";
  private static final String url = "jdbc:mysql://51.255.170.248:3306/AssuranceServer?useUnicode=true&characterEncoding=utf8&useSSL=false";
  private static final String dbUsername = "root";
  private static final String dbPassword = "Hacking15";
  private static DataSource dataSource;



    private final Logger log = LoggerFactory.getLogger(ContratService.class);

    @Inject
    private ContratRepository contratRepository;

    @Inject
    private SettingRepository settingRepository;

    @Inject
    private PaiementRepository paiementRepository;

    @Inject
    private VignetteRepository vignetteRepository;

    @Inject
    private ContratSearchRepository contratSearchRepository;

    private String query;

    /**
     * Save a contrat.
     *
     * @param contrat the entity to save
     * @return the persisted entity
     */
    public Contrat save(Contrat contrat) {
        log.debug("Request to save Contrat : {}", contrat);
        contrat.setEtat(false);
        Contrat result = contratRepository.save(contrat);
        contratSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the contrats.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Contrat> findAll(Pageable pageable) {
        log.debug("Request to get all Contrats");
        Page<Contrat> result = contratRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get one contrat by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public Contrat findOne(Long id) {
        log.debug("Request to get Contrat : {}", id);
        Contrat contrat = contratRepository.findOne(id);
        return contrat;
    }

    /**
     *  Delete the  contrat by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Contrat : {}", id);
        contratRepository.delete(id);
        contratSearchRepository.delete(id);
    }

    /**
     * Search for the contrat corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Contrat> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Contrats for query {}", query);
        Page<Contrat> result = contratSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }



    @Transactional(readOnly = true)
    public List<Contrat> notsync() {
        List <Contrat> contratList = new ArrayList<Contrat>();
        List <Contrat> contrats = contratRepository.findAll();
        for(int i = 0; i < contrats.size(); i++) {
            if (!contrats.get(i).isEtat()) {
                contratList.add(contrats.get(i));
            }
        }
        return contratList;

    }

    public int bool(Boolean bool){
      if(bool){
        return 1;
      }else{
        return 0;
      }
    }


    @Transactional(readOnly = true)
    public List<Contrat> sync() {
        List <Contrat> contratList = new ArrayList<Contrat>();
        List <Contrat> contrats = contratRepository.findAll();
        for(int i = 0; i < contrats.size(); i++) {
            if (!contrats.get(i).isEtat()) {
                contratList.add(contrats.get(i));
            }
        }

        List <Paiement> paiementList = new ArrayList<Paiement>();
        List <Paiement> paiements = paiementRepository.findAll();
        for(int i = 0; i < paiements.size(); i++) {
            if (!paiements.get(i).isEtat()) {
                paiementList.add(paiements.get(i));
            }
        }

        List <Vignette> vignetteList = new ArrayList<Vignette>();
        List <Vignette> vignettes = vignetteRepository.findAll();
      /*  for(int i = 0; i < vignettes.size(); i++) {
            if(!vignettes.get(i).isEtat()) {
                vignetteList.add(vignettes.get(i));
            }
        }*/



        dataSource = getDataSource();
        // JdbcTemplate template = new JdbcTemplate(dataSource); // constructor
        JdbcTemplate template = new JdbcTemplate();
        template.setDataSource(dataSource);
        System.out.println(dataSource.getClass());
        LocalDate now = LocalDate.now();;
        for(int i =0; i < contratList.size(); i++){

          if(contratList.get(i).getVehicule().getDatePremMiseCirculation() == null){
            contratList.get(i).getVehicule().setDatePremMiseCirculation(now);
          }

          if(contratList.get(i).getVehicule().getDateDerniereVT() == null){
            contratList.get(i).getVehicule().setDateDerniereVT(now);
          }

          if(contratList.get(i).getVehicule().getValeurAccessoire() == null){
            contratList.get(i).getVehicule().setValeurAccessoire(BigDecimal.ZERO);
          }

          if(contratList.get(i).getVehicule().getValeurVenal() == null){
            contratList.get(i).getVehicule().setValeurVenal(BigDecimal.ZERO);
          }

          if(contratList.get(i).getVehicule().getValeurNeuve() == null){
            contratList.get(i).getVehicule().setValeurNeuve(BigDecimal.ZERO);
          }

          query = "insert into vehicule (immatriculation,marque,puissance,source,numero_chassi,date_prem_mise_circulation,nbre_place,poids,valeur_accessoire,valeur_venal,date_derniere_vt,valeur_neuve,categorie,created_by) values('" +
          contratList.get(i).getVehicule().getImmatriculation() + "','" + contratList.get(i).getVehicule().getMarque() +
          "','" + contratList.get(i).getVehicule().getPuissance() + "','" + contratList.get(i).getVehicule().getSource() + "','" + contratList.get(i).getVehicule().getNumeroChassi() + "','" + contratList.get(i).getVehicule().getDatePremMiseCirculation() +
          "','" + contratList.get(i).getVehicule().getNbrePlace() + "','" + contratList.get(i).getVehicule().getPoids() + "','" + contratList.get(i).getVehicule().getValeurAccessoire() +
          "','" + contratList.get(i).getVehicule().getValeurVenal() +"','" + contratList.get(i).getVehicule().getDateDerniereVT() +"','" + contratList.get(i).getVehicule().getValeurNeuve() + "','" + contratList.get(i).getVehicule().getCategorie() +
          "','" + contratList.get(i).getVehicule().getCreatedBy() + "')";
          template.execute(query);

          query = "insert into assure (nom,number,ville,adress,profession,created_by) values('" + contratList.get(i).getAssure().getNom() + "','" + contratList.get(i).getAssure().getNumber() +
          "','" + contratList.get(i).getAssure().getVille() + "','" + contratList.get(i).getAssure().getAdress() + "','" + contratList.get(i).getAssure().getProfession() + "','" + contratList.get(i).getAssure().getCreatedBy() + "')";
          template.execute(query);

          query = "insert into contrat (numero_police,date_effet,duree,souscripteur,conducteur,statut_socio,categorie_permis,prime_ttc,acompte,status,date_fin,prime_base,pourcentage,accessoire,cout_police,asac,carte_rose,numero_carte_rose,numero_attestation,nette,tva,agent_id,assureur_id,vehicule_id,zone_id,assure_id,agence_id,type_assurance_auto_id,created_by,is_dommage_collision,pourcent_dommage_collision,is_dommage_accident,pourcent_dommage_accident,is_vol_incendie,pourcent_vol_incendie,is_vol_electronic,pourcent_vol_electronic,annuel_vol_electronic) values('" + contratList.get(i).getNumeroPolice() +
          "','" + contratList.get(i).getDateEffet() +
          "','" + contratList.get(i).getDuree() + "','" + contratList.get(i).getSouscripteur() + "','" + contratList.get(i).getConducteur() + "','" + contratList.get(i).getStatutSocio() + "','" + contratList.get(i).getCategoriePermis() +
          "','" + contratList.get(i).getPrimeTTC() + "','" + contratList.get(i).getAcompte() + "','" + contratList.get(i).getStatus() + "','" + contratList.get(i).getDateFin() +
          "','" + contratList.get(i).getPrimeBase() +"','" + contratList.get(i).getPourcentage() + "','" + contratList.get(i).getAccessoire() + "','" + contratList.get(i).getCoutPolice() + "','" + contratList.get(i).getAsac() +
          "','" + contratList.get(i).getCarteRose() + "','" + contratList.get(i).getNumeroCarteRose() + "','" + contratList.get(i).getNumeroAttestation() + "','" + contratList.get(i).getNette() + "','" + contratList.get(i).getTva() +
          "','" + contratList.get(i).getAgent().getId() +"','" + contratList.get(i).getAssureur().getId() + "','" + contratList.get(i).getVehicule().getId() + "','" + contratList.get(i).getZone().getId() +
          "','" + contratList.get(i).getAssure().getId() +"','" + contratList.get(i).getAgence().getId() + "','" + contratList.get(i).getTypeAssuranceAuto().getId() + "','" + contratList.get(i).getCreatedBy()  +
          "'," + bool(contratList.get(i).isIsDommageCollision()) + ",'" + contratList.get(i).getPourcentDommageCollision() + "'," + bool(contratList.get(i).isIsDommageAccident()) + ",'" + contratList.get(i).getPourcentDommageAccident() +
          "'," + bool(contratList.get(i).isIsVolIncendie()) + ",'" + contratList.get(i).getPourcentVolIncendie() + "'," + bool(contratList.get(i).isIsVolElectronic()) + ",'" + contratList.get(i).getPourcentVolIncendie() +
          "','" + contratList.get(i).getAnnuelVolElectronic() + "')";
          template.execute(query);


          contratList.get(i).setEtat(true) ;
          contratRepository.save(contratList.get(i));

        }


        for(int i =0; i < paiementList.size(); i++){

          query = "insert into paiement (date_paiement,somme,mode_paiement,nature_paiement,pc,numero_avenant,user_assurance_id,contrat_id,created_by,created_date) values('" + paiementList.get(i).getDatePaiement() + "','" + paiementList.get(i).getSomme() +
          "','" + paiementList.get(i).getModePaiement() + "','" + paiementList.get(i).getNaturePaiement() + "','" + paiementList.get(i).getPc() + "','" + paiementList.get(i).getNumeroAvenant() +
          "','" + paiementList.get(i).getUserAssurance().getId() + "','" + paiementList.get(i).getContrat().getId() + "','" + paiementList.get(i).getCreatedBy() + "','" + paiementList.get(i).getCreatedDate() + "')";
          template.execute(query);

          paiementList.get(i).setEtat(true) ;
          paiementRepository.save(paiementList.get(i));

        }

        for(int i =0; i < vignetteList.size(); i++){
          query = "insert into vignette (dateEffet,dateFin,montant,vehicule_id,created_by,ceated_date) values('" + vignetteList.get(i).getDateEffet() + "','" + vignetteList.get(i).getDateFin() +
          "','" + vignetteList.get(i).getMontant() + "','" + vignetteList.get(i).getVehicule().getId() + "')";
          template.execute(query);
          vignetteList.get(i).setEtat(true) ;
          vignetteRepository.save(vignetteList.get(i));
        }


        return contratList;

    }

  public static DriverManagerDataSource getDataSource() {
      DriverManagerDataSource dataSource = new DriverManagerDataSource();
      dataSource.setDriverClassName(driverClassName);
      dataSource.setUrl(url);
      dataSource.setUsername(dbUsername);
      dataSource.setPassword(dbPassword);
      return dataSource;
  }




}
