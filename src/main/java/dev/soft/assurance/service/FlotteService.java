package dev.soft.assurance.service;

import dev.soft.assurance.domain.Flotte;
import dev.soft.assurance.repository.FlotteRepository;
import dev.soft.assurance.repository.search.FlotteSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Flotte.
 */
@Service
@Transactional
public class FlotteService {

    private final Logger log = LoggerFactory.getLogger(FlotteService.class);
    
    @Inject
    private FlotteRepository flotteRepository;

    @Inject
    private FlotteSearchRepository flotteSearchRepository;

    /**
     * Save a flotte.
     *
     * @param flotte the entity to save
     * @return the persisted entity
     */
    public Flotte save(Flotte flotte) {
        log.debug("Request to save Flotte : {}", flotte);
        Flotte result = flotteRepository.save(flotte);
        flotteSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the flottes.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<Flotte> findAll(Pageable pageable) {
        log.debug("Request to get all Flottes");
        Page<Flotte> result = flotteRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get one flotte by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public Flotte findOne(Long id) {
        log.debug("Request to get Flotte : {}", id);
        Flotte flotte = flotteRepository.findOne(id);
        return flotte;
    }

    /**
     *  Delete the  flotte by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Flotte : {}", id);
        flotteRepository.delete(id);
        flotteSearchRepository.delete(id);
    }

    /**
     * Search for the flotte corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Flotte> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Flottes for query {}", query);
        Page<Flotte> result = flotteSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
