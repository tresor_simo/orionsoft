package dev.soft.assurance.service;

import dev.soft.assurance.domain.Garanti;
import dev.soft.assurance.repository.GarantiRepository;
import dev.soft.assurance.repository.search.GarantiSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Garanti.
 */
@Service
@Transactional
public class GarantiService {

    private final Logger log = LoggerFactory.getLogger(GarantiService.class);
    
    @Inject
    private GarantiRepository garantiRepository;

    @Inject
    private GarantiSearchRepository garantiSearchRepository;

    /**
     * Save a garanti.
     *
     * @param garanti the entity to save
     * @return the persisted entity
     */
    public Garanti save(Garanti garanti) {
        log.debug("Request to save Garanti : {}", garanti);
        Garanti result = garantiRepository.save(garanti);
        garantiSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the garantis.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<Garanti> findAll(Pageable pageable) {
        log.debug("Request to get all Garantis");
        Page<Garanti> result = garantiRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get one garanti by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public Garanti findOne(Long id) {
        log.debug("Request to get Garanti : {}", id);
        Garanti garanti = garantiRepository.findOne(id);
        return garanti;
    }

    /**
     *  Delete the  garanti by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Garanti : {}", id);
        garantiRepository.delete(id);
        garantiSearchRepository.delete(id);
    }

    /**
     * Search for the garanti corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Garanti> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Garantis for query {}", query);
        Page<Garanti> result = garantiSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
