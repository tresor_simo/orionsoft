package dev.soft.assurance.web.rest;

import com.codahale.metrics.annotation.Timed;
import dev.soft.assurance.domain.NumeroAgence;
import dev.soft.assurance.service.NumeroAgenceService;
import dev.soft.assurance.web.rest.util.HeaderUtil;
import dev.soft.assurance.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing NumeroAgence.
 */
@RestController
@RequestMapping("/api")
public class NumeroAgenceResource {

    private final Logger log = LoggerFactory.getLogger(NumeroAgenceResource.class);
        
    @Inject
    private NumeroAgenceService numeroAgenceService;

    /**
     * POST  /numero-agences : Create a new numeroAgence.
     *
     * @param numeroAgence the numeroAgence to create
     * @return the ResponseEntity with status 201 (Created) and with body the new numeroAgence, or with status 400 (Bad Request) if the numeroAgence has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/numero-agences",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<NumeroAgence> createNumeroAgence(@RequestBody NumeroAgence numeroAgence) throws URISyntaxException {
        log.debug("REST request to save NumeroAgence : {}", numeroAgence);
        if (numeroAgence.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("numeroAgence", "idexists", "A new numeroAgence cannot already have an ID")).body(null);
        }
        NumeroAgence result = numeroAgenceService.save(numeroAgence);
        return ResponseEntity.created(new URI("/api/numero-agences/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("numeroAgence", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /numero-agences : Updates an existing numeroAgence.
     *
     * @param numeroAgence the numeroAgence to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated numeroAgence,
     * or with status 400 (Bad Request) if the numeroAgence is not valid,
     * or with status 500 (Internal Server Error) if the numeroAgence couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/numero-agences",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<NumeroAgence> updateNumeroAgence(@RequestBody NumeroAgence numeroAgence) throws URISyntaxException {
        log.debug("REST request to update NumeroAgence : {}", numeroAgence);
        if (numeroAgence.getId() == null) {
            return createNumeroAgence(numeroAgence);
        }
        NumeroAgence result = numeroAgenceService.save(numeroAgence);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("numeroAgence", numeroAgence.getId().toString()))
            .body(result);
    }

    /**
     * GET  /numero-agences : get all the numeroAgences.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of numeroAgences in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/numero-agences",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<NumeroAgence>> getAllNumeroAgences(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of NumeroAgences");
        Page<NumeroAgence> page = numeroAgenceService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/numero-agences");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /numero-agences/:id : get the "id" numeroAgence.
     *
     * @param id the id of the numeroAgence to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the numeroAgence, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/numero-agences/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<NumeroAgence> getNumeroAgence(@PathVariable Long id) {
        log.debug("REST request to get NumeroAgence : {}", id);
        NumeroAgence numeroAgence = numeroAgenceService.findOne(id);
        return Optional.ofNullable(numeroAgence)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /numero-agences/:id : delete the "id" numeroAgence.
     *
     * @param id the id of the numeroAgence to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/numero-agences/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteNumeroAgence(@PathVariable Long id) {
        log.debug("REST request to delete NumeroAgence : {}", id);
        numeroAgenceService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("numeroAgence", id.toString())).build();
    }

    /**
     * SEARCH  /_search/numero-agences?query=:query : search for the numeroAgence corresponding
     * to the query.
     *
     * @param query the query of the numeroAgence search 
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/_search/numero-agences",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<NumeroAgence>> searchNumeroAgences(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of NumeroAgences for query {}", query);
        Page<NumeroAgence> page = numeroAgenceService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/numero-agences");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
