package dev.soft.assurance.web.rest;

import com.codahale.metrics.annotation.Timed;
import dev.soft.assurance.domain.Vignette;
import dev.soft.assurance.service.VignetteService;
import dev.soft.assurance.web.rest.util.HeaderUtil;
import dev.soft.assurance.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Vignette.
 */
@RestController
@RequestMapping("/api")
public class VignetteResource {

    private final Logger log = LoggerFactory.getLogger(VignetteResource.class);
        
    @Inject
    private VignetteService vignetteService;

    /**
     * POST  /vignettes : Create a new vignette.
     *
     * @param vignette the vignette to create
     * @return the ResponseEntity with status 201 (Created) and with body the new vignette, or with status 400 (Bad Request) if the vignette has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/vignettes",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Vignette> createVignette(@RequestBody Vignette vignette) throws URISyntaxException {
        log.debug("REST request to save Vignette : {}", vignette);
        if (vignette.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("vignette", "idexists", "A new vignette cannot already have an ID")).body(null);
        }
        Vignette result = vignetteService.save(vignette);
        return ResponseEntity.created(new URI("/api/vignettes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("vignette", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /vignettes : Updates an existing vignette.
     *
     * @param vignette the vignette to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated vignette,
     * or with status 400 (Bad Request) if the vignette is not valid,
     * or with status 500 (Internal Server Error) if the vignette couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/vignettes",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Vignette> updateVignette(@RequestBody Vignette vignette) throws URISyntaxException {
        log.debug("REST request to update Vignette : {}", vignette);
        if (vignette.getId() == null) {
            return createVignette(vignette);
        }
        Vignette result = vignetteService.save(vignette);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("vignette", vignette.getId().toString()))
            .body(result);
    }

    /**
     * GET  /vignettes : get all the vignettes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of vignettes in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/vignettes",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Vignette>> getAllVignettes(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Vignettes");
        Page<Vignette> page = vignetteService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/vignettes");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /vignettes/:id : get the "id" vignette.
     *
     * @param id the id of the vignette to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the vignette, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/vignettes/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Vignette> getVignette(@PathVariable Long id) {
        log.debug("REST request to get Vignette : {}", id);
        Vignette vignette = vignetteService.findOne(id);
        return Optional.ofNullable(vignette)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /vignettes/:id : delete the "id" vignette.
     *
     * @param id the id of the vignette to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/vignettes/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteVignette(@PathVariable Long id) {
        log.debug("REST request to delete Vignette : {}", id);
        vignetteService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("vignette", id.toString())).build();
    }

    /**
     * SEARCH  /_search/vignettes?query=:query : search for the vignette corresponding
     * to the query.
     *
     * @param query the query of the vignette search 
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/_search/vignettes",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Vignette>> searchVignettes(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of Vignettes for query {}", query);
        Page<Vignette> page = vignetteService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/vignettes");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
