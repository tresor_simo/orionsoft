package dev.soft.assurance.web.rest;

import com.codahale.metrics.annotation.Timed;
import dev.soft.assurance.domain.Assure;
import dev.soft.assurance.service.AssureService;
import dev.soft.assurance.web.rest.util.HeaderUtil;
import dev.soft.assurance.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Assure.
 */
@RestController
@RequestMapping("/api")
public class AssureResource {

    private final Logger log = LoggerFactory.getLogger(AssureResource.class);
        
    @Inject
    private AssureService assureService;

    /**
     * POST  /assures : Create a new assure.
     *
     * @param assure the assure to create
     * @return the ResponseEntity with status 201 (Created) and with body the new assure, or with status 400 (Bad Request) if the assure has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/assures",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Assure> createAssure(@RequestBody Assure assure) throws URISyntaxException {
        log.debug("REST request to save Assure : {}", assure);
        if (assure.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("assure", "idexists", "A new assure cannot already have an ID")).body(null);
        }
        Assure result = assureService.save(assure);
        return ResponseEntity.created(new URI("/api/assures/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("assure", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /assures : Updates an existing assure.
     *
     * @param assure the assure to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated assure,
     * or with status 400 (Bad Request) if the assure is not valid,
     * or with status 500 (Internal Server Error) if the assure couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/assures",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Assure> updateAssure(@RequestBody Assure assure) throws URISyntaxException {
        log.debug("REST request to update Assure : {}", assure);
        if (assure.getId() == null) {
            return createAssure(assure);
        }
        Assure result = assureService.save(assure);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("assure", assure.getId().toString()))
            .body(result);
    }

    /**
     * GET  /assures : get all the assures.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of assures in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/assures",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Assure>> getAllAssures(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Assures");
        Page<Assure> page = assureService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/assures");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /assures/:id : get the "id" assure.
     *
     * @param id the id of the assure to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the assure, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/assures/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Assure> getAssure(@PathVariable Long id) {
        log.debug("REST request to get Assure : {}", id);
        Assure assure = assureService.findOne(id);
        return Optional.ofNullable(assure)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /assures/:id : delete the "id" assure.
     *
     * @param id the id of the assure to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/assures/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteAssure(@PathVariable Long id) {
        log.debug("REST request to delete Assure : {}", id);
        assureService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("assure", id.toString())).build();
    }

    /**
     * SEARCH  /_search/assures?query=:query : search for the assure corresponding
     * to the query.
     *
     * @param query the query of the assure search 
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/_search/assures",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Assure>> searchAssures(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of Assures for query {}", query);
        Page<Assure> page = assureService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/assures");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
