package dev.soft.assurance.web.rest;

import com.codahale.metrics.annotation.Timed;
import dev.soft.assurance.domain.Assureur;
import dev.soft.assurance.service.AssureurService;
import dev.soft.assurance.web.rest.util.HeaderUtil;
import dev.soft.assurance.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Assureur.
 */
@RestController
@RequestMapping("/api")
public class AssureurResource {

    private final Logger log = LoggerFactory.getLogger(AssureurResource.class);
        
    @Inject
    private AssureurService assureurService;

    /**
     * POST  /assureurs : Create a new assureur.
     *
     * @param assureur the assureur to create
     * @return the ResponseEntity with status 201 (Created) and with body the new assureur, or with status 400 (Bad Request) if the assureur has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/assureurs",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Assureur> createAssureur(@RequestBody Assureur assureur) throws URISyntaxException {
        log.debug("REST request to save Assureur : {}", assureur);
        if (assureur.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("assureur", "idexists", "A new assureur cannot already have an ID")).body(null);
        }
        Assureur result = assureurService.save(assureur);
        return ResponseEntity.created(new URI("/api/assureurs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("assureur", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /assureurs : Updates an existing assureur.
     *
     * @param assureur the assureur to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated assureur,
     * or with status 400 (Bad Request) if the assureur is not valid,
     * or with status 500 (Internal Server Error) if the assureur couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/assureurs",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Assureur> updateAssureur(@RequestBody Assureur assureur) throws URISyntaxException {
        log.debug("REST request to update Assureur : {}", assureur);
        if (assureur.getId() == null) {
            return createAssureur(assureur);
        }
        Assureur result = assureurService.save(assureur);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("assureur", assureur.getId().toString()))
            .body(result);
    }

    /**
     * GET  /assureurs : get all the assureurs.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of assureurs in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/assureurs",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Assureur>> getAllAssureurs(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Assureurs");
        Page<Assureur> page = assureurService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/assureurs");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /assureurs/:id : get the "id" assureur.
     *
     * @param id the id of the assureur to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the assureur, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/assureurs/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Assureur> getAssureur(@PathVariable Long id) {
        log.debug("REST request to get Assureur : {}", id);
        Assureur assureur = assureurService.findOne(id);
        return Optional.ofNullable(assureur)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /assureurs/:id : delete the "id" assureur.
     *
     * @param id the id of the assureur to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/assureurs/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteAssureur(@PathVariable Long id) {
        log.debug("REST request to delete Assureur : {}", id);
        assureurService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("assureur", id.toString())).build();
    }

    /**
     * SEARCH  /_search/assureurs?query=:query : search for the assureur corresponding
     * to the query.
     *
     * @param query the query of the assureur search 
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/_search/assureurs",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Assureur>> searchAssureurs(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of Assureurs for query {}", query);
        Page<Assureur> page = assureurService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/assureurs");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
