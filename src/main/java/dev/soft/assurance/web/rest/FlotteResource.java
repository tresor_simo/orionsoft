package dev.soft.assurance.web.rest;

import com.codahale.metrics.annotation.Timed;
import dev.soft.assurance.domain.Flotte;
import dev.soft.assurance.service.FlotteService;
import dev.soft.assurance.web.rest.util.HeaderUtil;
import dev.soft.assurance.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Flotte.
 */
@RestController
@RequestMapping("/api")
public class FlotteResource {

    private final Logger log = LoggerFactory.getLogger(FlotteResource.class);
        
    @Inject
    private FlotteService flotteService;

    /**
     * POST  /flottes : Create a new flotte.
     *
     * @param flotte the flotte to create
     * @return the ResponseEntity with status 201 (Created) and with body the new flotte, or with status 400 (Bad Request) if the flotte has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/flottes",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Flotte> createFlotte(@RequestBody Flotte flotte) throws URISyntaxException {
        log.debug("REST request to save Flotte : {}", flotte);
        if (flotte.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("flotte", "idexists", "A new flotte cannot already have an ID")).body(null);
        }
        Flotte result = flotteService.save(flotte);
        return ResponseEntity.created(new URI("/api/flottes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("flotte", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /flottes : Updates an existing flotte.
     *
     * @param flotte the flotte to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated flotte,
     * or with status 400 (Bad Request) if the flotte is not valid,
     * or with status 500 (Internal Server Error) if the flotte couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/flottes",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Flotte> updateFlotte(@RequestBody Flotte flotte) throws URISyntaxException {
        log.debug("REST request to update Flotte : {}", flotte);
        if (flotte.getId() == null) {
            return createFlotte(flotte);
        }
        Flotte result = flotteService.save(flotte);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("flotte", flotte.getId().toString()))
            .body(result);
    }

    /**
     * GET  /flottes : get all the flottes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of flottes in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/flottes",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Flotte>> getAllFlottes(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Flottes");
        Page<Flotte> page = flotteService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/flottes");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /flottes/:id : get the "id" flotte.
     *
     * @param id the id of the flotte to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the flotte, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/flottes/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Flotte> getFlotte(@PathVariable Long id) {
        log.debug("REST request to get Flotte : {}", id);
        Flotte flotte = flotteService.findOne(id);
        return Optional.ofNullable(flotte)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /flottes/:id : delete the "id" flotte.
     *
     * @param id the id of the flotte to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/flottes/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteFlotte(@PathVariable Long id) {
        log.debug("REST request to delete Flotte : {}", id);
        flotteService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("flotte", id.toString())).build();
    }

    /**
     * SEARCH  /_search/flottes?query=:query : search for the flotte corresponding
     * to the query.
     *
     * @param query the query of the flotte search 
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/_search/flottes",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Flotte>> searchFlottes(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of Flottes for query {}", query);
        Page<Flotte> page = flotteService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/flottes");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
