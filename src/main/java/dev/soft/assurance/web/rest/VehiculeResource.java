package dev.soft.assurance.web.rest;

import com.codahale.metrics.annotation.Timed;
import dev.soft.assurance.domain.Vehicule;
import dev.soft.assurance.service.VehiculeService;
import dev.soft.assurance.web.rest.util.HeaderUtil;
import dev.soft.assurance.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Vehicule.
 */
@RestController
@RequestMapping("/api")
public class VehiculeResource {

    private final Logger log = LoggerFactory.getLogger(VehiculeResource.class);
        
    @Inject
    private VehiculeService vehiculeService;

    /**
     * POST  /vehicules : Create a new vehicule.
     *
     * @param vehicule the vehicule to create
     * @return the ResponseEntity with status 201 (Created) and with body the new vehicule, or with status 400 (Bad Request) if the vehicule has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/vehicules",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Vehicule> createVehicule(@RequestBody Vehicule vehicule) throws URISyntaxException {
        log.debug("REST request to save Vehicule : {}", vehicule);
        if (vehicule.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("vehicule", "idexists", "A new vehicule cannot already have an ID")).body(null);
        }
        Vehicule result = vehiculeService.save(vehicule);
        return ResponseEntity.created(new URI("/api/vehicules/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("vehicule", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /vehicules : Updates an existing vehicule.
     *
     * @param vehicule the vehicule to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated vehicule,
     * or with status 400 (Bad Request) if the vehicule is not valid,
     * or with status 500 (Internal Server Error) if the vehicule couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/vehicules",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Vehicule> updateVehicule(@RequestBody Vehicule vehicule) throws URISyntaxException {
        log.debug("REST request to update Vehicule : {}", vehicule);
        if (vehicule.getId() == null) {
            return createVehicule(vehicule);
        }
        Vehicule result = vehiculeService.save(vehicule);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("vehicule", vehicule.getId().toString()))
            .body(result);
    }

    /**
     * GET  /vehicules : get all the vehicules.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of vehicules in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/vehicules",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Vehicule>> getAllVehicules(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Vehicules");
        Page<Vehicule> page = vehiculeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/vehicules");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /vehicules/:id : get the "id" vehicule.
     *
     * @param id the id of the vehicule to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the vehicule, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/vehicules/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Vehicule> getVehicule(@PathVariable Long id) {
        log.debug("REST request to get Vehicule : {}", id);
        Vehicule vehicule = vehiculeService.findOne(id);
        return Optional.ofNullable(vehicule)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /vehicules/:id : delete the "id" vehicule.
     *
     * @param id the id of the vehicule to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/vehicules/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteVehicule(@PathVariable Long id) {
        log.debug("REST request to delete Vehicule : {}", id);
        vehiculeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("vehicule", id.toString())).build();
    }

    /**
     * SEARCH  /_search/vehicules?query=:query : search for the vehicule corresponding
     * to the query.
     *
     * @param query the query of the vehicule search 
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/_search/vehicules",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Vehicule>> searchVehicules(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of Vehicules for query {}", query);
        Page<Vehicule> page = vehiculeService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/vehicules");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
