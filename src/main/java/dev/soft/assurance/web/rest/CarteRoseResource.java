package dev.soft.assurance.web.rest;

import com.codahale.metrics.annotation.Timed;
import dev.soft.assurance.domain.CarteRose;
import dev.soft.assurance.service.CarteRoseService;
import dev.soft.assurance.web.rest.util.HeaderUtil;
import dev.soft.assurance.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing CarteRose.
 */
@RestController
@RequestMapping("/api")
public class CarteRoseResource {

    private final Logger log = LoggerFactory.getLogger(CarteRoseResource.class);
        
    @Inject
    private CarteRoseService carteRoseService;

    /**
     * POST  /carte-roses : Create a new carteRose.
     *
     * @param carteRose the carteRose to create
     * @return the ResponseEntity with status 201 (Created) and with body the new carteRose, or with status 400 (Bad Request) if the carteRose has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/carte-roses",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<CarteRose> createCarteRose(@RequestBody CarteRose carteRose) throws URISyntaxException {
        log.debug("REST request to save CarteRose : {}", carteRose);
        if (carteRose.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("carteRose", "idexists", "A new carteRose cannot already have an ID")).body(null);
        }
        CarteRose result = carteRoseService.save(carteRose);
        return ResponseEntity.created(new URI("/api/carte-roses/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("carteRose", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /carte-roses : Updates an existing carteRose.
     *
     * @param carteRose the carteRose to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated carteRose,
     * or with status 400 (Bad Request) if the carteRose is not valid,
     * or with status 500 (Internal Server Error) if the carteRose couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/carte-roses",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<CarteRose> updateCarteRose(@RequestBody CarteRose carteRose) throws URISyntaxException {
        log.debug("REST request to update CarteRose : {}", carteRose);
        if (carteRose.getId() == null) {
            return createCarteRose(carteRose);
        }
        CarteRose result = carteRoseService.save(carteRose);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("carteRose", carteRose.getId().toString()))
            .body(result);
    }

    /**
     * GET  /carte-roses : get all the carteRoses.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of carteRoses in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/carte-roses",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<CarteRose>> getAllCarteRoses(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of CarteRoses");
        Page<CarteRose> page = carteRoseService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/carte-roses");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /carte-roses/:id : get the "id" carteRose.
     *
     * @param id the id of the carteRose to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the carteRose, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/carte-roses/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<CarteRose> getCarteRose(@PathVariable Long id) {
        log.debug("REST request to get CarteRose : {}", id);
        CarteRose carteRose = carteRoseService.findOne(id);
        return Optional.ofNullable(carteRose)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /carte-roses/:id : delete the "id" carteRose.
     *
     * @param id the id of the carteRose to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/carte-roses/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteCarteRose(@PathVariable Long id) {
        log.debug("REST request to delete CarteRose : {}", id);
        carteRoseService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("carteRose", id.toString())).build();
    }

    /**
     * SEARCH  /_search/carte-roses?query=:query : search for the carteRose corresponding
     * to the query.
     *
     * @param query the query of the carteRose search 
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/_search/carte-roses",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<CarteRose>> searchCarteRoses(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of CarteRoses for query {}", query);
        Page<CarteRose> page = carteRoseService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/carte-roses");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
