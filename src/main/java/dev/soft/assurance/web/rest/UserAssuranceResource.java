package dev.soft.assurance.web.rest;

import com.codahale.metrics.annotation.Timed;
import dev.soft.assurance.domain.UserAssurance;
import dev.soft.assurance.service.UserAssuranceService;
import dev.soft.assurance.web.rest.util.HeaderUtil;
import dev.soft.assurance.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing UserAssurance.
 */
@RestController
@RequestMapping("/api")
public class UserAssuranceResource {

    private final Logger log = LoggerFactory.getLogger(UserAssuranceResource.class);
        
    @Inject
    private UserAssuranceService userAssuranceService;

    /**
     * POST  /user-assurances : Create a new userAssurance.
     *
     * @param userAssurance the userAssurance to create
     * @return the ResponseEntity with status 201 (Created) and with body the new userAssurance, or with status 400 (Bad Request) if the userAssurance has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/user-assurances",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<UserAssurance> createUserAssurance(@RequestBody UserAssurance userAssurance) throws URISyntaxException {
        log.debug("REST request to save UserAssurance : {}", userAssurance);
        if (userAssurance.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("userAssurance", "idexists", "A new userAssurance cannot already have an ID")).body(null);
        }
        UserAssurance result = userAssuranceService.save(userAssurance);
        return ResponseEntity.created(new URI("/api/user-assurances/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("userAssurance", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /user-assurances : Updates an existing userAssurance.
     *
     * @param userAssurance the userAssurance to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated userAssurance,
     * or with status 400 (Bad Request) if the userAssurance is not valid,
     * or with status 500 (Internal Server Error) if the userAssurance couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/user-assurances",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<UserAssurance> updateUserAssurance(@RequestBody UserAssurance userAssurance) throws URISyntaxException {
        log.debug("REST request to update UserAssurance : {}", userAssurance);
        if (userAssurance.getId() == null) {
            return createUserAssurance(userAssurance);
        }
        UserAssurance result = userAssuranceService.save(userAssurance);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("userAssurance", userAssurance.getId().toString()))
            .body(result);
    }

    /**
     * GET  /user-assurances : get all the userAssurances.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of userAssurances in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/user-assurances",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<UserAssurance>> getAllUserAssurances(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of UserAssurances");
        Page<UserAssurance> page = userAssuranceService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/user-assurances");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /user-assurances/:id : get the "id" userAssurance.
     *
     * @param id the id of the userAssurance to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the userAssurance, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/user-assurances/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<UserAssurance> getUserAssurance(@PathVariable Long id) {
        log.debug("REST request to get UserAssurance : {}", id);
        UserAssurance userAssurance = userAssuranceService.findOne(id);
        return Optional.ofNullable(userAssurance)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /user-assurances/:id : delete the "id" userAssurance.
     *
     * @param id the id of the userAssurance to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/user-assurances/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteUserAssurance(@PathVariable Long id) {
        log.debug("REST request to delete UserAssurance : {}", id);
        userAssuranceService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("userAssurance", id.toString())).build();
    }

    /**
     * SEARCH  /_search/user-assurances?query=:query : search for the userAssurance corresponding
     * to the query.
     *
     * @param query the query of the userAssurance search 
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/_search/user-assurances",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<UserAssurance>> searchUserAssurances(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of UserAssurances for query {}", query);
        Page<UserAssurance> page = userAssuranceService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/user-assurances");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
