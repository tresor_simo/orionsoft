package dev.soft.assurance.web.rest;

import com.codahale.metrics.annotation.Timed;
import dev.soft.assurance.domain.Zone;
import dev.soft.assurance.service.ZoneService;
import dev.soft.assurance.web.rest.util.HeaderUtil;
import dev.soft.assurance.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Zone.
 */
@RestController
@RequestMapping("/api")
public class ZoneResource {

    private final Logger log = LoggerFactory.getLogger(ZoneResource.class);
        
    @Inject
    private ZoneService zoneService;

    /**
     * POST  /zones : Create a new zone.
     *
     * @param zone the zone to create
     * @return the ResponseEntity with status 201 (Created) and with body the new zone, or with status 400 (Bad Request) if the zone has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/zones",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Zone> createZone(@RequestBody Zone zone) throws URISyntaxException {
        log.debug("REST request to save Zone : {}", zone);
        if (zone.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("zone", "idexists", "A new zone cannot already have an ID")).body(null);
        }
        Zone result = zoneService.save(zone);
        return ResponseEntity.created(new URI("/api/zones/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("zone", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /zones : Updates an existing zone.
     *
     * @param zone the zone to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated zone,
     * or with status 400 (Bad Request) if the zone is not valid,
     * or with status 500 (Internal Server Error) if the zone couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/zones",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Zone> updateZone(@RequestBody Zone zone) throws URISyntaxException {
        log.debug("REST request to update Zone : {}", zone);
        if (zone.getId() == null) {
            return createZone(zone);
        }
        Zone result = zoneService.save(zone);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("zone", zone.getId().toString()))
            .body(result);
    }

    /**
     * GET  /zones : get all the zones.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of zones in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/zones",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Zone>> getAllZones(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Zones");
        Page<Zone> page = zoneService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/zones");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /zones/:id : get the "id" zone.
     *
     * @param id the id of the zone to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the zone, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/zones/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Zone> getZone(@PathVariable Long id) {
        log.debug("REST request to get Zone : {}", id);
        Zone zone = zoneService.findOne(id);
        return Optional.ofNullable(zone)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /zones/:id : delete the "id" zone.
     *
     * @param id the id of the zone to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/zones/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteZone(@PathVariable Long id) {
        log.debug("REST request to delete Zone : {}", id);
        zoneService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("zone", id.toString())).build();
    }

    /**
     * SEARCH  /_search/zones?query=:query : search for the zone corresponding
     * to the query.
     *
     * @param query the query of the zone search 
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/_search/zones",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Zone>> searchZones(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of Zones for query {}", query);
        Page<Zone> page = zoneService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/zones");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
