package dev.soft.assurance.web.rest;

import com.codahale.metrics.annotation.Timed;
import dev.soft.assurance.domain.GarantiContrat;
import dev.soft.assurance.service.GarantiContratService;
import dev.soft.assurance.web.rest.util.HeaderUtil;
import dev.soft.assurance.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing GarantiContrat.
 */
@RestController
@RequestMapping("/api")
public class GarantiContratResource {

    private final Logger log = LoggerFactory.getLogger(GarantiContratResource.class);
        
    @Inject
    private GarantiContratService garantiContratService;

    /**
     * POST  /garanti-contrats : Create a new garantiContrat.
     *
     * @param garantiContrat the garantiContrat to create
     * @return the ResponseEntity with status 201 (Created) and with body the new garantiContrat, or with status 400 (Bad Request) if the garantiContrat has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/garanti-contrats",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<GarantiContrat> createGarantiContrat(@RequestBody GarantiContrat garantiContrat) throws URISyntaxException {
        log.debug("REST request to save GarantiContrat : {}", garantiContrat);
        if (garantiContrat.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("garantiContrat", "idexists", "A new garantiContrat cannot already have an ID")).body(null);
        }
        GarantiContrat result = garantiContratService.save(garantiContrat);
        return ResponseEntity.created(new URI("/api/garanti-contrats/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("garantiContrat", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /garanti-contrats : Updates an existing garantiContrat.
     *
     * @param garantiContrat the garantiContrat to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated garantiContrat,
     * or with status 400 (Bad Request) if the garantiContrat is not valid,
     * or with status 500 (Internal Server Error) if the garantiContrat couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/garanti-contrats",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<GarantiContrat> updateGarantiContrat(@RequestBody GarantiContrat garantiContrat) throws URISyntaxException {
        log.debug("REST request to update GarantiContrat : {}", garantiContrat);
        if (garantiContrat.getId() == null) {
            return createGarantiContrat(garantiContrat);
        }
        GarantiContrat result = garantiContratService.save(garantiContrat);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("garantiContrat", garantiContrat.getId().toString()))
            .body(result);
    }

    /**
     * GET  /garanti-contrats : get all the garantiContrats.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of garantiContrats in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/garanti-contrats",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<GarantiContrat>> getAllGarantiContrats(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of GarantiContrats");
        Page<GarantiContrat> page = garantiContratService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/garanti-contrats");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /garanti-contrats/:id : get the "id" garantiContrat.
     *
     * @param id the id of the garantiContrat to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the garantiContrat, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/garanti-contrats/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<GarantiContrat> getGarantiContrat(@PathVariable Long id) {
        log.debug("REST request to get GarantiContrat : {}", id);
        GarantiContrat garantiContrat = garantiContratService.findOne(id);
        return Optional.ofNullable(garantiContrat)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /garanti-contrats/:id : delete the "id" garantiContrat.
     *
     * @param id the id of the garantiContrat to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/garanti-contrats/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteGarantiContrat(@PathVariable Long id) {
        log.debug("REST request to delete GarantiContrat : {}", id);
        garantiContratService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("garantiContrat", id.toString())).build();
    }

    /**
     * SEARCH  /_search/garanti-contrats?query=:query : search for the garantiContrat corresponding
     * to the query.
     *
     * @param query the query of the garantiContrat search 
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/_search/garanti-contrats",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<GarantiContrat>> searchGarantiContrats(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of GarantiContrats for query {}", query);
        Page<GarantiContrat> page = garantiContratService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/garanti-contrats");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
