package dev.soft.assurance.web.rest;

import com.codahale.metrics.annotation.Timed;
import dev.soft.assurance.domain.TypeAssuranceAuto;
import dev.soft.assurance.service.TypeAssuranceAutoService;
import dev.soft.assurance.web.rest.util.HeaderUtil;
import dev.soft.assurance.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing TypeAssuranceAuto.
 */
@RestController
@RequestMapping("/api")
public class TypeAssuranceAutoResource {

    private final Logger log = LoggerFactory.getLogger(TypeAssuranceAutoResource.class);
        
    @Inject
    private TypeAssuranceAutoService typeAssuranceAutoService;

    /**
     * POST  /type-assurance-autos : Create a new typeAssuranceAuto.
     *
     * @param typeAssuranceAuto the typeAssuranceAuto to create
     * @return the ResponseEntity with status 201 (Created) and with body the new typeAssuranceAuto, or with status 400 (Bad Request) if the typeAssuranceAuto has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/type-assurance-autos",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TypeAssuranceAuto> createTypeAssuranceAuto(@RequestBody TypeAssuranceAuto typeAssuranceAuto) throws URISyntaxException {
        log.debug("REST request to save TypeAssuranceAuto : {}", typeAssuranceAuto);
        if (typeAssuranceAuto.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("typeAssuranceAuto", "idexists", "A new typeAssuranceAuto cannot already have an ID")).body(null);
        }
        TypeAssuranceAuto result = typeAssuranceAutoService.save(typeAssuranceAuto);
        return ResponseEntity.created(new URI("/api/type-assurance-autos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("typeAssuranceAuto", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /type-assurance-autos : Updates an existing typeAssuranceAuto.
     *
     * @param typeAssuranceAuto the typeAssuranceAuto to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated typeAssuranceAuto,
     * or with status 400 (Bad Request) if the typeAssuranceAuto is not valid,
     * or with status 500 (Internal Server Error) if the typeAssuranceAuto couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/type-assurance-autos",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TypeAssuranceAuto> updateTypeAssuranceAuto(@RequestBody TypeAssuranceAuto typeAssuranceAuto) throws URISyntaxException {
        log.debug("REST request to update TypeAssuranceAuto : {}", typeAssuranceAuto);
        if (typeAssuranceAuto.getId() == null) {
            return createTypeAssuranceAuto(typeAssuranceAuto);
        }
        TypeAssuranceAuto result = typeAssuranceAutoService.save(typeAssuranceAuto);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("typeAssuranceAuto", typeAssuranceAuto.getId().toString()))
            .body(result);
    }

    /**
     * GET  /type-assurance-autos : get all the typeAssuranceAutos.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of typeAssuranceAutos in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/type-assurance-autos",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<TypeAssuranceAuto>> getAllTypeAssuranceAutos(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of TypeAssuranceAutos");
        Page<TypeAssuranceAuto> page = typeAssuranceAutoService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/type-assurance-autos");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /type-assurance-autos/:id : get the "id" typeAssuranceAuto.
     *
     * @param id the id of the typeAssuranceAuto to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the typeAssuranceAuto, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/type-assurance-autos/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<TypeAssuranceAuto> getTypeAssuranceAuto(@PathVariable Long id) {
        log.debug("REST request to get TypeAssuranceAuto : {}", id);
        TypeAssuranceAuto typeAssuranceAuto = typeAssuranceAutoService.findOne(id);
        return Optional.ofNullable(typeAssuranceAuto)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /type-assurance-autos/:id : delete the "id" typeAssuranceAuto.
     *
     * @param id the id of the typeAssuranceAuto to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/type-assurance-autos/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteTypeAssuranceAuto(@PathVariable Long id) {
        log.debug("REST request to delete TypeAssuranceAuto : {}", id);
        typeAssuranceAutoService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("typeAssuranceAuto", id.toString())).build();
    }

    /**
     * SEARCH  /_search/type-assurance-autos?query=:query : search for the typeAssuranceAuto corresponding
     * to the query.
     *
     * @param query the query of the typeAssuranceAuto search 
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/_search/type-assurance-autos",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<TypeAssuranceAuto>> searchTypeAssuranceAutos(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of TypeAssuranceAutos for query {}", query);
        Page<TypeAssuranceAuto> page = typeAssuranceAutoService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/type-assurance-autos");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
