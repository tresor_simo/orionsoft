/**
 * View Models used by Spring MVC REST controllers.
 */
package dev.soft.assurance.web.rest.vm;
