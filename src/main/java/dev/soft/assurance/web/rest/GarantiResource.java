package dev.soft.assurance.web.rest;

import com.codahale.metrics.annotation.Timed;
import dev.soft.assurance.domain.Garanti;
import dev.soft.assurance.service.GarantiService;
import dev.soft.assurance.web.rest.util.HeaderUtil;
import dev.soft.assurance.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Garanti.
 */
@RestController
@RequestMapping("/api")
public class GarantiResource {

    private final Logger log = LoggerFactory.getLogger(GarantiResource.class);
        
    @Inject
    private GarantiService garantiService;

    /**
     * POST  /garantis : Create a new garanti.
     *
     * @param garanti the garanti to create
     * @return the ResponseEntity with status 201 (Created) and with body the new garanti, or with status 400 (Bad Request) if the garanti has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/garantis",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Garanti> createGaranti(@RequestBody Garanti garanti) throws URISyntaxException {
        log.debug("REST request to save Garanti : {}", garanti);
        if (garanti.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("garanti", "idexists", "A new garanti cannot already have an ID")).body(null);
        }
        Garanti result = garantiService.save(garanti);
        return ResponseEntity.created(new URI("/api/garantis/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("garanti", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /garantis : Updates an existing garanti.
     *
     * @param garanti the garanti to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated garanti,
     * or with status 400 (Bad Request) if the garanti is not valid,
     * or with status 500 (Internal Server Error) if the garanti couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/garantis",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Garanti> updateGaranti(@RequestBody Garanti garanti) throws URISyntaxException {
        log.debug("REST request to update Garanti : {}", garanti);
        if (garanti.getId() == null) {
            return createGaranti(garanti);
        }
        Garanti result = garantiService.save(garanti);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("garanti", garanti.getId().toString()))
            .body(result);
    }

    /**
     * GET  /garantis : get all the garantis.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of garantis in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/garantis",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Garanti>> getAllGarantis(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Garantis");
        Page<Garanti> page = garantiService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/garantis");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /garantis/:id : get the "id" garanti.
     *
     * @param id the id of the garanti to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the garanti, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/garantis/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Garanti> getGaranti(@PathVariable Long id) {
        log.debug("REST request to get Garanti : {}", id);
        Garanti garanti = garantiService.findOne(id);
        return Optional.ofNullable(garanti)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /garantis/:id : delete the "id" garanti.
     *
     * @param id the id of the garanti to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/garantis/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteGaranti(@PathVariable Long id) {
        log.debug("REST request to delete Garanti : {}", id);
        garantiService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("garanti", id.toString())).build();
    }

    /**
     * SEARCH  /_search/garantis?query=:query : search for the garanti corresponding
     * to the query.
     *
     * @param query the query of the garanti search 
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/_search/garantis",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Garanti>> searchGarantis(@RequestParam String query, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of Garantis for query {}", query);
        Page<Garanti> page = garantiService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/garantis");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
