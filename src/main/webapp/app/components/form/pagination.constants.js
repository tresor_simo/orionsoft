(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
