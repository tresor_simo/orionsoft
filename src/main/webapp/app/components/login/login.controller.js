(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .controller('LoginController', LoginController);

    LoginController.$inject = ['$rootScope', '$state', '$timeout', 'Auth', '$uibModalInstance','UserAssurance'];

    function LoginController ($rootScope, $state, $timeout, Auth, $uibModalInstance,UserAssurance) {
        var vm = this;

        var storage = window.localStorage;

        vm.authenticationError = false;
        vm.cancel = cancel;
        vm.credentials = {};
        vm.login = login;
        vm.password = null;
        vm.register = register;
        vm.rememberMe = true;
        vm.requestResetPassword = requestResetPassword;
        vm.username = null;
        vm.userAssurances = UserAssurance.query();
        console.log(vm.userAssurances);

        $timeout(function (){angular.element('#username').focus();});

        function cancel () {
            vm.credentials = {
                username: null,
                password: null,
                rememberMe: true
            };
            vm.authenticationError = false;
            $uibModalInstance.dismiss('cancel');
        }

        function login (event) {
            event.preventDefault();
            var connu = false;
             for(var i = 0 ; i< vm.userAssurances.length; i++){
                 if(vm.userAssurances[i].user.login == vm.username){
                      storage.setItem("userAssurance",JSON.stringify(vm.userAssurances[i]));
                      connu = true;
                 }
                 console.log("User Assurance", $rootScope.userAssurance);

              }

            if(connu||vm.username=="admin"){

                Auth.login({
                    username: vm.username,
                    password: vm.password,
                    rememberMe: vm.rememberMe
                }).then(function () {

                vm.authenticationError = false;
                $uibModalInstance.close();
                if ($state.current.name === 'register' || $state.current.name === 'activate' ||
                $state.current.name === 'finishReset' || $state.current.name === 'requestReset') {
                $state.go('home');
                }

                $rootScope.$broadcast('authenticationSuccess');

                // previousState was set in the authExpiredInterceptor before being redirected to login modal.
                // since login is succesful, go to stored previousState and clear previousState
                if (Auth.getPreviousState()) {
                   var previousState = Auth.getPreviousState();
                   Auth.resetPreviousState();
                   $state.go(previousState.name, previousState.params);
                }
                }).catch(function () {
                   vm.authenticationError = true;
                });

            }else{

                  vm.authenticationError = true;

            }



        }

        function register () {
            $uibModalInstance.dismiss('cancel');
            $state.go('register');
        }

        function requestResetPassword () {
            $uibModalInstance.dismiss('cancel');
            $state.go('requestReset');
        }
    }
})();
