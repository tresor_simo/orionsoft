(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .controller('HomeController', HomeController);

    HomeController.$inject = ['$scope', 'Principal', 'LoginService', '$state','Setting','$http'];

    function HomeController ($scope, Principal, LoginService, $state,Setting,$http) {
        var vm = this;

        var ip = "";
        vm.notsync = [];
        vm.account = null;
        vm.isAuthenticated = null;
        vm.login = LoginService.open;
        vm.register = register;
        $scope.$on('authenticationSuccess', function() {
            getAccount();
        });

        getAccount();

        function getAccount() {
            Principal.identity().then(function(account) {
                vm.account = account;
                vm.isAuthenticated = Principal.isAuthenticated;
            });
        }
        function register () {
            $state.go('register');
        }



        $http.get("http://localhost:8080/api/notsync/contrats").success(function (result) {

            vm.notsync = result;
            console.log("Mon Resultat",result);
            vm.sync = vm.notsync.length;

        })
         .error(function (error) {
              console.log("Error",error);
         });

         vm.synchro = function(){
             console.log("Synchronisation avec le serveur");
             $http.get("http://localhost:8080/api/synchronize").success(function (result) {

                 alert("Synchronisation réussi");

             })

              .error(function (error) {
                   console.log("Error : " , error);
                   alert("Synchronisation echec " + error.message);
              });

         }


    }
})();
