(function() {
    'use strict';
    angular
        .module('orionSoftApp')
        .factory('Garanti', Garanti);

    Garanti.$inject = ['$resource'];

    function Garanti ($resource) {
        var resourceUrl =  'api/garantis/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
