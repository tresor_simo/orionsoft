(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .factory('GarantiSearch', GarantiSearch);

    GarantiSearch.$inject = ['$resource'];

    function GarantiSearch($resource) {
        var resourceUrl =  'api/_search/garantis/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
