(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('garanti', {
            parent: 'entity',
            url: '/garanti',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'orionSoftApp.garanti.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/garanti/garantis.html',
                    controller: 'GarantiController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('garanti');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('garanti-detail', {
            parent: 'entity',
            url: '/garanti/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'orionSoftApp.garanti.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/garanti/garanti-detail.html',
                    controller: 'GarantiDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('garanti');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Garanti', function($stateParams, Garanti) {
                    return Garanti.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'garanti',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('garanti-detail.edit', {
            parent: 'garanti-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/garanti/garanti-dialog.html',
                    controller: 'GarantiDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Garanti', function(Garanti) {
                            return Garanti.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('garanti.new', {
            parent: 'garanti',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/garanti/garanti-dialog.html',
                    controller: 'GarantiDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                nom: null,
                                capitaux: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('garanti', null, { reload: 'garanti' });
                }, function() {
                    $state.go('garanti');
                });
            }]
        })
        .state('garanti.edit', {
            parent: 'garanti',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/garanti/garanti-dialog.html',
                    controller: 'GarantiDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Garanti', function(Garanti) {
                            return Garanti.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('garanti', null, { reload: 'garanti' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('garanti.delete', {
            parent: 'garanti',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/garanti/garanti-delete-dialog.html',
                    controller: 'GarantiDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Garanti', function(Garanti) {
                            return Garanti.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('garanti', null, { reload: 'garanti' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
