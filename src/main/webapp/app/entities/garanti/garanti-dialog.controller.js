(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .controller('GarantiDialogController', GarantiDialogController);

    GarantiDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Garanti'];

    function GarantiDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Garanti) {
        var vm = this;

        vm.garanti = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.garanti.id !== null) {
                Garanti.update(vm.garanti, onSaveSuccess, onSaveError);
            } else {
                Garanti.save(vm.garanti, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('orionSoftApp:garantiUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
