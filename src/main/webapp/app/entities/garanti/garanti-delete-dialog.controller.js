(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .controller('GarantiDeleteController',GarantiDeleteController);

    GarantiDeleteController.$inject = ['$uibModalInstance', 'entity', 'Garanti'];

    function GarantiDeleteController($uibModalInstance, entity, Garanti) {
        var vm = this;

        vm.garanti = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Garanti.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
