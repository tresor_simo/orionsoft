(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .controller('GarantiDetailController', GarantiDetailController);

    GarantiDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Garanti'];

    function GarantiDetailController($scope, $rootScope, $stateParams, previousState, entity, Garanti) {
        var vm = this;

        vm.garanti = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('orionSoftApp:garantiUpdate', function(event, result) {
            vm.garanti = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
