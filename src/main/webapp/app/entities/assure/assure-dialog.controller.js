(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .controller('AssureDialogController', AssureDialogController);

    AssureDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Assure'];

    function AssureDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Assure) {
        var vm = this;

        vm.assure = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.assure.id !== null) {
                Assure.update(vm.assure, onSaveSuccess, onSaveError);
            } else {
                Assure.save(vm.assure, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('orionSoftApp:assureUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
