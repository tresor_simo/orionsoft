(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('assure', {
            parent: 'entity',
            url: '/assure',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'orionSoftApp.assure.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/assure/assures.html',
                    controller: 'AssureController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('assure');
                    $translatePartialLoader.addPart('vILLE');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('assure-detail', {
            parent: 'entity',
            url: '/assure/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'orionSoftApp.assure.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/assure/assure-detail.html',
                    controller: 'AssureDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('assure');
                    $translatePartialLoader.addPart('vILLE');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Assure', function($stateParams, Assure) {
                    return Assure.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'assure',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('assure-detail.edit', {
            parent: 'assure-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/assure/assure-dialog.html',
                    controller: 'AssureDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Assure', function(Assure) {
                            return Assure.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('assure.new', {
            parent: 'assure',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/assure/assure-dialog.html',
                    controller: 'AssureDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                nom: null,
                                number: null,
                                ville: null,
                                adress: null,
                                profession: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('assure', null, { reload: 'assure' });
                }, function() {
                    $state.go('assure');
                });
            }]
        })
        .state('assure.edit', {
            parent: 'assure',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/assure/assure-dialog.html',
                    controller: 'AssureDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Assure', function(Assure) {
                            return Assure.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('assure', null, { reload: 'assure' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('assure.delete', {
            parent: 'assure',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/assure/assure-delete-dialog.html',
                    controller: 'AssureDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Assure', function(Assure) {
                            return Assure.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('assure', null, { reload: 'assure' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
