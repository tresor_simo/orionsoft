(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .factory('AssureSearch', AssureSearch);

    AssureSearch.$inject = ['$resource'];

    function AssureSearch($resource) {
        var resourceUrl =  'api/_search/assures/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
