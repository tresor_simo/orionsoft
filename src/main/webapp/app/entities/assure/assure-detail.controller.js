(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .controller('AssureDetailController', AssureDetailController);

    AssureDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Assure'];

    function AssureDetailController($scope, $rootScope, $stateParams, previousState, entity, Assure) {
        var vm = this;

        vm.assure = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('orionSoftApp:assureUpdate', function(event, result) {
            vm.assure = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
