(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .factory('NumeroAgenceSearch', NumeroAgenceSearch);

    NumeroAgenceSearch.$inject = ['$resource'];

    function NumeroAgenceSearch($resource) {
        var resourceUrl =  'api/_search/numero-agences/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
