(function() {
    'use strict';
    angular
        .module('orionSoftApp')
        .factory('NumeroAgence', NumeroAgence);

    NumeroAgence.$inject = ['$resource'];

    function NumeroAgence ($resource) {
        var resourceUrl =  'api/numero-agences/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
