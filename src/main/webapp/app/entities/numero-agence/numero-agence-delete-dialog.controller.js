(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .controller('NumeroAgenceDeleteController',NumeroAgenceDeleteController);

    NumeroAgenceDeleteController.$inject = ['$uibModalInstance', 'entity', 'NumeroAgence'];

    function NumeroAgenceDeleteController($uibModalInstance, entity, NumeroAgence) {
        var vm = this;

        vm.numeroAgence = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            NumeroAgence.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
