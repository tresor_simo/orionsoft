(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .controller('NumeroAgenceDetailController', NumeroAgenceDetailController);

    NumeroAgenceDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'NumeroAgence', 'Agence'];

    function NumeroAgenceDetailController($scope, $rootScope, $stateParams, previousState, entity, NumeroAgence, Agence) {
        var vm = this;

        vm.numeroAgence = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('orionSoftApp:numeroAgenceUpdate', function(event, result) {
            vm.numeroAgence = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
