(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .controller('NumeroAgenceDialogController', NumeroAgenceDialogController);

    NumeroAgenceDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'NumeroAgence', 'Agence'];

    function NumeroAgenceDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, NumeroAgence, Agence) {
        var vm = this;

        vm.numeroAgence = entity;
        vm.clear = clear;
        vm.save = save;
        vm.agences = Agence.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.numeroAgence.id !== null) {
                NumeroAgence.update(vm.numeroAgence, onSaveSuccess, onSaveError);
            } else {
                NumeroAgence.save(vm.numeroAgence, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('orionSoftApp:numeroAgenceUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
