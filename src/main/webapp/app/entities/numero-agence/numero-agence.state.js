(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('numero-agence', {
            parent: 'entity',
            url: '/numero-agence',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'orionSoftApp.numeroAgence.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/numero-agence/numero-agences.html',
                    controller: 'NumeroAgenceController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('numeroAgence');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('numero-agence-detail', {
            parent: 'entity',
            url: '/numero-agence/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'orionSoftApp.numeroAgence.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/numero-agence/numero-agence-detail.html',
                    controller: 'NumeroAgenceDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('numeroAgence');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'NumeroAgence', function($stateParams, NumeroAgence) {
                    return NumeroAgence.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'numero-agence',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('numero-agence-detail.edit', {
            parent: 'numero-agence-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/numero-agence/numero-agence-dialog.html',
                    controller: 'NumeroAgenceDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['NumeroAgence', function(NumeroAgence) {
                            return NumeroAgence.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('numero-agence.new', {
            parent: 'numero-agence',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/numero-agence/numero-agence-dialog.html',
                    controller: 'NumeroAgenceDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                number: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('numero-agence', null, { reload: 'numero-agence' });
                }, function() {
                    $state.go('numero-agence');
                });
            }]
        })
        .state('numero-agence.edit', {
            parent: 'numero-agence',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/numero-agence/numero-agence-dialog.html',
                    controller: 'NumeroAgenceDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['NumeroAgence', function(NumeroAgence) {
                            return NumeroAgence.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('numero-agence', null, { reload: 'numero-agence' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('numero-agence.delete', {
            parent: 'numero-agence',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/numero-agence/numero-agence-delete-dialog.html',
                    controller: 'NumeroAgenceDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['NumeroAgence', function(NumeroAgence) {
                            return NumeroAgence.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('numero-agence', null, { reload: 'numero-agence' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
