(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .controller('CarteRoseDeleteController',CarteRoseDeleteController);

    CarteRoseDeleteController.$inject = ['$uibModalInstance', 'entity', 'CarteRose'];

    function CarteRoseDeleteController($uibModalInstance, entity, CarteRose) {
        var vm = this;

        vm.carteRose = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            CarteRose.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
