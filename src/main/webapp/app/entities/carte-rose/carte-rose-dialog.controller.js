(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .controller('CarteRoseDialogController', CarteRoseDialogController);

    CarteRoseDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'CarteRose'];

    function CarteRoseDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, CarteRose) {
        var vm = this;

        vm.carteRose = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.carteRose.id !== null) {
                CarteRose.update(vm.carteRose, onSaveSuccess, onSaveError);
            } else {
                CarteRose.save(vm.carteRose, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('orionSoftApp:carteRoseUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.dateEffet = false;
        vm.datePickerOpenStatus.dateFin = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
