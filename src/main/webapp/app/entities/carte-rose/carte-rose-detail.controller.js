(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .controller('CarteRoseDetailController', CarteRoseDetailController);

    CarteRoseDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'CarteRose'];

    function CarteRoseDetailController($scope, $rootScope, $stateParams, previousState, entity, CarteRose) {
        var vm = this;

        vm.carteRose = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('orionSoftApp:carteRoseUpdate', function(event, result) {
            vm.carteRose = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
