(function() {
    'use strict';
    angular
        .module('orionSoftApp')
        .factory('CarteRose', CarteRose);

    CarteRose.$inject = ['$resource', 'DateUtils'];

    function CarteRose ($resource, DateUtils) {
        var resourceUrl =  'api/carte-roses/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.dateEffet = DateUtils.convertLocalDateFromServer(data.dateEffet);
                        data.dateFin = DateUtils.convertLocalDateFromServer(data.dateFin);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    data.dateEffet = DateUtils.convertLocalDateToServer(data.dateEffet);
                    data.dateFin = DateUtils.convertLocalDateToServer(data.dateFin);
                    return angular.toJson(data);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    data.dateEffet = DateUtils.convertLocalDateToServer(data.dateEffet);
                    data.dateFin = DateUtils.convertLocalDateToServer(data.dateFin);
                    return angular.toJson(data);
                }
            }
        });
    }
})();
