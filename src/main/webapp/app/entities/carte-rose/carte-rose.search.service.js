(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .factory('CarteRoseSearch', CarteRoseSearch);

    CarteRoseSearch.$inject = ['$resource'];

    function CarteRoseSearch($resource) {
        var resourceUrl =  'api/_search/carte-roses/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
