(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('carte-rose', {
            parent: 'entity',
            url: '/carte-rose',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'orionSoftApp.carteRose.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/carte-rose/carte-roses.html',
                    controller: 'CarteRoseController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('carteRose');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('carte-rose-detail', {
            parent: 'entity',
            url: '/carte-rose/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'orionSoftApp.carteRose.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/carte-rose/carte-rose-detail.html',
                    controller: 'CarteRoseDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('carteRose');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'CarteRose', function($stateParams, CarteRose) {
                    return CarteRose.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'carte-rose',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('carte-rose-detail.edit', {
            parent: 'carte-rose-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/carte-rose/carte-rose-dialog.html',
                    controller: 'CarteRoseDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['CarteRose', function(CarteRose) {
                            return CarteRose.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('carte-rose.new', {
            parent: 'carte-rose',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/carte-rose/carte-rose-dialog.html',
                    controller: 'CarteRoseDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                nomAssure: null,
                                immatriculation: null,
                                marque: null,
                                assureur: null,
                                bureauEmetteur: null,
                                police: null,
                                dateEffet: null,
                                dateFin: null,
                                categoryVehicule: null,
                                numeroChassis: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('carte-rose', null, { reload: 'carte-rose' });
                }, function() {
                    $state.go('carte-rose');
                });
            }]
        })
        .state('carte-rose.edit', {
            parent: 'carte-rose',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/carte-rose/carte-rose-dialog.html',
                    controller: 'CarteRoseDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['CarteRose', function(CarteRose) {
                            return CarteRose.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('carte-rose', null, { reload: 'carte-rose' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('carte-rose.delete', {
            parent: 'carte-rose',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/carte-rose/carte-rose-delete-dialog.html',
                    controller: 'CarteRoseDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['CarteRose', function(CarteRose) {
                            return CarteRose.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('carte-rose', null, { reload: 'carte-rose' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
