(function() {
    'use strict';
    angular
        .module('orionSoftApp')
        .factory('UserAssurance', UserAssurance);

    UserAssurance.$inject = ['$resource'];

    function UserAssurance ($resource) {
        var resourceUrl =  'api/user-assurances/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
