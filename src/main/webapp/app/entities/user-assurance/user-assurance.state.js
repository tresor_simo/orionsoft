(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('user-assurance', {
            parent: 'entity',
            url: '/user-assurance',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'orionSoftApp.userAssurance.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/user-assurance/user-assurances.html',
                    controller: 'UserAssuranceController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('userAssurance');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('user-assurance-detail', {
            parent: 'entity',
            url: '/user-assurance/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'orionSoftApp.userAssurance.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/user-assurance/user-assurance-detail.html',
                    controller: 'UserAssuranceDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('userAssurance');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'UserAssurance', function($stateParams, UserAssurance) {
                    return UserAssurance.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'user-assurance',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('user-assurance-detail.edit', {
            parent: 'user-assurance-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-assurance/user-assurance-dialog.html',
                    controller: 'UserAssuranceDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['UserAssurance', function(UserAssurance) {
                            return UserAssurance.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('user-assurance.new', {
            parent: 'user-assurance',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-assurance/user-assurance-dialog.html',
                    controller: 'UserAssuranceDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                number: null,
                                email: null,
                                nom: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('user-assurance', null, { reload: 'user-assurance' });
                }, function() {
                    $state.go('user-assurance');
                });
            }]
        })
        .state('user-assurance.edit', {
            parent: 'user-assurance',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-assurance/user-assurance-dialog.html',
                    controller: 'UserAssuranceDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['UserAssurance', function(UserAssurance) {
                            return UserAssurance.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('user-assurance', null, { reload: 'user-assurance' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('user-assurance.delete', {
            parent: 'user-assurance',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-assurance/user-assurance-delete-dialog.html',
                    controller: 'UserAssuranceDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['UserAssurance', function(UserAssurance) {
                            return UserAssurance.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('user-assurance', null, { reload: 'user-assurance' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
