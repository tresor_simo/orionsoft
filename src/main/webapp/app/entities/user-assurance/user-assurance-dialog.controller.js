(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .controller('UserAssuranceDialogController', UserAssuranceDialogController);

    UserAssuranceDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'UserAssurance', 'User', 'Agence'];

    function UserAssuranceDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, UserAssurance, User, Agence) {
        var vm = this;

        vm.userAssurance = entity;
        vm.clear = clear;
        vm.save = save;
        vm.users = User.query();
        vm.agences = Agence.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.userAssurance.id !== null) {
                UserAssurance.update(vm.userAssurance, onSaveSuccess, onSaveError);
            } else {
                UserAssurance.save(vm.userAssurance, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('orionSoftApp:userAssuranceUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
