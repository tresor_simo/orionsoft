(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .factory('UserAssuranceSearch', UserAssuranceSearch);

    UserAssuranceSearch.$inject = ['$resource'];

    function UserAssuranceSearch($resource) {
        var resourceUrl =  'api/_search/user-assurances/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
