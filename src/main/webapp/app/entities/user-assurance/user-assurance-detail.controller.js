(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .controller('UserAssuranceDetailController', UserAssuranceDetailController);

    UserAssuranceDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'UserAssurance', 'User', 'Agence'];

    function UserAssuranceDetailController($scope, $rootScope, $stateParams, previousState, entity, UserAssurance, User, Agence) {
        var vm = this;

        vm.userAssurance = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('orionSoftApp:userAssuranceUpdate', function(event, result) {
            vm.userAssurance = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
