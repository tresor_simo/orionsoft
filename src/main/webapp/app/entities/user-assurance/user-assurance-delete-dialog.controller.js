(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .controller('UserAssuranceDeleteController',UserAssuranceDeleteController);

    UserAssuranceDeleteController.$inject = ['$uibModalInstance', 'entity', 'UserAssurance'];

    function UserAssuranceDeleteController($uibModalInstance, entity, UserAssurance) {
        var vm = this;

        vm.userAssurance = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            UserAssurance.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
