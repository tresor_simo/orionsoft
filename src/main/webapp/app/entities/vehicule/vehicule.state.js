(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('vehicule', {
            parent: 'entity',
            url: '/vehicule',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'orionSoftApp.vehicule.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/vehicule/vehicules.html',
                    controller: 'VehiculeController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('vehicule');
                    $translatePartialLoader.addPart('sOURCE_ENERGIE');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('vehicule-detail', {
            parent: 'entity',
            url: '/vehicule/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'orionSoftApp.vehicule.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/vehicule/vehicule-detail.html',
                    controller: 'VehiculeDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('vehicule');
                    $translatePartialLoader.addPart('sOURCE_ENERGIE');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Vehicule', function($stateParams, Vehicule) {
                    return Vehicule.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'vehicule',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('vehicule-detail.edit', {
            parent: 'vehicule-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/vehicule/vehicule-dialog.html',
                    controller: 'VehiculeDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Vehicule', function(Vehicule) {
                            return Vehicule.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('vehicule.new', {
            parent: 'vehicule',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/vehicule/vehicule-dialog.html',
                    controller: 'VehiculeDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                immatriculation: null,
                                marque: null,
                                puissance: null,
                                source: null,
                                numeroChassi: null,
                                datePremMiseCirculation: null,
                                nbrePlace: null,
                                poids: null,
                                valeurAccessoire: null,
                                valeurVenal: null,
                                dateDerniereVT: null,
                                valeurNeuve: null,
                                categorie: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('vehicule', null, { reload: 'vehicule' });
                }, function() {
                    $state.go('vehicule');
                });
            }]
        })
        .state('vehicule.edit', {
            parent: 'vehicule',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/vehicule/vehicule-dialog.html',
                    controller: 'VehiculeDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Vehicule', function(Vehicule) {
                            return Vehicule.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('vehicule', null, { reload: 'vehicule' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('vehicule.delete', {
            parent: 'vehicule',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/vehicule/vehicule-delete-dialog.html',
                    controller: 'VehiculeDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Vehicule', function(Vehicule) {
                            return Vehicule.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('vehicule', null, { reload: 'vehicule' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
