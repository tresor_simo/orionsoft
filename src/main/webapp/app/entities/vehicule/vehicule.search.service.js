(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .factory('VehiculeSearch', VehiculeSearch);

    VehiculeSearch.$inject = ['$resource'];

    function VehiculeSearch($resource) {
        var resourceUrl =  'api/_search/vehicules/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
