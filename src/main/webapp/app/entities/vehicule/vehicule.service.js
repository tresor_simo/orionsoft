(function() {
    'use strict';
    angular
        .module('orionSoftApp')
        .factory('Vehicule', Vehicule);

    Vehicule.$inject = ['$resource', 'DateUtils'];

    function Vehicule ($resource, DateUtils) {
        var resourceUrl =  'api/vehicules/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.datePremMiseCirculation = DateUtils.convertLocalDateFromServer(data.datePremMiseCirculation);
                        data.dateDerniereVT = DateUtils.convertLocalDateFromServer(data.dateDerniereVT);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    data.datePremMiseCirculation = DateUtils.convertLocalDateToServer(data.datePremMiseCirculation);
                    data.dateDerniereVT = DateUtils.convertLocalDateToServer(data.dateDerniereVT);
                    return angular.toJson(data);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    data.datePremMiseCirculation = DateUtils.convertLocalDateToServer(data.datePremMiseCirculation);
                    data.dateDerniereVT = DateUtils.convertLocalDateToServer(data.dateDerniereVT);
                    return angular.toJson(data);
                }
            }
        });
    }
})();
