(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .controller('VehiculeDetailController', VehiculeDetailController);

    VehiculeDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Vehicule'];

    function VehiculeDetailController($scope, $rootScope, $stateParams, previousState, entity, Vehicule) {
        var vm = this;

        vm.vehicule = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('orionSoftApp:vehiculeUpdate', function(event, result) {
            vm.vehicule = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
