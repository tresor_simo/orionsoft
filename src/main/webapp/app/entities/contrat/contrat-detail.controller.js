(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .controller('ContratDetailController', ContratDetailController);

    ContratDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Contrat', 'Agent', 'Assureur', 'Vehicule', 'Zone', 'Assure', 'TypeAssuranceAuto','$timeout','Paiement','Vignette','PaiementExtra','VignetteExtra'];

    function ContratDetailController($scope, $rootScope, $stateParams, previousState, entity, Contrat, Agent, Assureur, Vehicule, Zone, Assure, TypeAssuranceAuto,$timeout,Paiement,Vignette,PaiementExtra,VignetteExtra) {
        var vm = this;

        vm.contrat = entity;
        vm.previousState = previousState.name;
        vm.reverse = false;
        PaiementExtra.getPaiementsByContrat(vm.contrat.id).then(function(result){

               vm.paiements = result;
               for(var i =0; i < result.length; i++){
                  console.log("Paiements",result[i]);
                  if(result[i].contrat.id == $stateParams.id){
                      vm.pay = vm.pay + result[i].somme;
                  }

                }
                console.log("Paiements",result);

        });
        vm.pay = 0;
        vm.today = new Date(vm.contrat.createdDate);
        vm.printContrat = printContrat;
        console.log("primeTTC",vm.contrat.primeTTC);
        vm.showOption = true;
         var storage = window.localStorage;
        vm.userAssurance = JSON.parse(storage.getItem("userAssurance"));
        console.log("CONTRAT",vm.contrat);
        vm.isVignette = false;
         VignetteExtra.getVignettesByVehicule(vm.contrat.vehicule.id).then(function(result){
              console.log("SEARCH VIGNETTE",result);
             for(var i =0; i<result.length; i++){
                 var today = new Date();
                 var dateFin = new Date(result[i].dateFin);
                 if((result[i].vehicule.id == vm.contrat.vehicule.id) && (dateFin > today) ){
                      console.log("FOUND VIGNETTE",result[i]);
                      vm.vignette = result[i];
                      var date1 = new Date();
                      var date2 = new Date(vm.vignette.dateEffet);
                      console.log('Date1',date1);
                      console.log('Date2',date2);
                      if(date1.getDate() == date2.getDate()){

                      }else{
                           vm.isVignette = true;
                      }

                 }
             }

         });




        function printContrat(){

           //PrintElem('contrat');
           vm.showOption = false;

           setTimeout(function(){
                window.print();
           }, 3000);


        }

        $scope.printVerso = function(elementId){
            var getMyFrame = document.getElementById(elementId);
            getMyFrame.focus();
            getMyFrame.contentWindow.print();
        }



        var unsubscribe = $rootScope.$on('orionSoftApp:contratUpdate', function(event, result) {
            vm.contrat = result;
        });
        $scope.$on('$destroy', unsubscribe);

        changeValue();

        function PrintElem(elem)
            {
              var mywindow = window.open('', 'PRINT', 'height=900,width=600');


                mywindow.document.write('<html><head><title>' + document.title  + '</title>');

                mywindow.document.write('</head><body >');
                mywindow.document.write(document.getElementById(elem).innerHTML);
                mywindow.document.write('</body></html>');

                mywindow.document.close(); // necessary for IE >= 10
                mywindow.focus(); // necessary for IE >= 10*/

                mywindow.print();
                mywindow.close();

                return true;

         }

         function onSaveSuccess (result) {
                     for(var i =0; i < result.length; i++){
                        console.log("Paiements",result[i]);
                        if(result[i].contrat.id == $stateParams.id){
                            vm.pay = vm.pay + result[i].somme;
                        }

                      }
                           console.log("Paiements",result);
          }

          function onSaveError () {
                   // vm.isSaving = false;
          }



        function changeValue(){


                $timeout(function() {

                  if(vm.contrat.typeAssuranceAuto.nom != 'ORION FIRST'){
                    var garanti1 = parseInt(document.getElementById("garanti1").innerText);

                     console.log(garanti1);

                     var garanti2 = parseInt(document.getElementById("garanti2").innerText);

                     console.log(garanti2);

                     var garanti3 = parseInt(document.getElementById("garanti3").innerText);

                     console.log(garanti3);

                     var garanti4 = parseInt(document.getElementById("garanti4").innerText);

                                  console.log(garanti4);

                     var garanti5 = parseInt(document.getElementById("garanti5").innerText);

                                  console.log(garanti5);

                     var garanti6 = parseInt(document.getElementById("garanti6").innerText);

                                  console.log(garanti6);

                     var garanti7 = parseInt(document.getElementById("garanti7").innerText);

                                  console.log(garanti7);

                     var garanti8 = parseInt(document.getElementById("garanti8").innerText);

                                  console.log(garanti8);

                     var garanti9 = parseInt(document.getElementById("garanti9").innerText);

                                  console.log(garanti9);

                     var garanti10 = parseInt(document.getElementById("garanti10").innerText);

                                  console.log(garanti10);

                     var garanti11 = parseInt(document.getElementById("garanti11").innerText);


                     console.log(garanti11);

                     var primenet = document.getElementById("primeNet");

                     var accessoire = document.getElementById("accessoire");

                     var coutPolice = document.getElementById("coutPolice");

                     var primeHT = document.getElementById("primeHT");

                     var tva = document.getElementById("tva");

                     var asac = document.getElementById("asac");

                     var carteRose = document.getElementById("carteRose");

                     var primeTTC = document.getElementById("primeTTC");

                     primenet.innerText = garanti1 + garanti2 + garanti3 + garanti4 + garanti5 + garanti6 + garanti7 + garanti8 + garanti9 + garanti10 + garanti11;

                     primeHT.innerText = parseInt(primenet.innerText) + parseInt(accessoire.innerText) + parseInt(coutPolice.innerText);

                     tva.innerText = (parseInt(primeHT.innerText) + parseInt(asac.innerText)) * 0.1925;

                     primeTTC.innerText = parseInt(primeHT.innerText) + parseInt(tva.innerText) + parseInt(carteRose.innerText) + parseInt(asac.innerText);

                     vm.contrat.acompte = 0;
                  }else{
                    var garanti1 = parseInt(document.getElementById("garanti1").innerText);

                     console.log(garanti1);


                     var garanti4 = parseInt(document.getElementById("garanti4").innerText);

                                  console.log(garanti4);


                     var garanti7 = parseInt(document.getElementById("garanti7").innerText);

                                  console.log(garanti7);


                     var garanti9 = parseInt(document.getElementById("garanti9").innerText);

                                  console.log(garanti9);


                     var garanti11 = parseInt(document.getElementById("garanti11").innerText);


                     console.log(garanti11);

                     var primenet = document.getElementById("primeNet");

                     var primebrute = document.getElementById("primeBrute");

                     var reduction = document.getElementById("reduction");

                     var accessoire = document.getElementById("accessoire");

                     var coutPolice = document.getElementById("coutPolice");

                     var primeHT = document.getElementById("primeHT");

                     var tva = document.getElementById("tva");

                     var asac = document.getElementById("asac");

                     var carteRose = document.getElementById("carteRose");

                     var primeTTC = document.getElementById("primeTTC");

                     primebrute.innerText = garanti1 +  garanti4 + garanti7 +  garanti9 +  garanti11;

                      reduction.innerText = parseInt(primebrute.innerText) * 20/100;

                      primenet.innerText = parseInt(primebrute.innerText) - parseInt(reduction.innerText);

                     primeHT.innerText = parseInt(primenet.innerText) + parseInt(accessoire.innerText) + parseInt(coutPolice.innerText);

                     tva.innerText = (parseInt(primeHT.innerText) + parseInt(asac.innerText)) * 0.1925;

                     primeTTC.innerText = parseInt(primeHT.innerText) + parseInt(tva.innerText) + parseInt(carteRose.innerText) + parseInt(asac.innerText);

                     vm.contrat.acompte = 0;
                  }


                  //   vm.contrat.primeTTC = parseInt(primeTTC.innerText);

                },1000)
                       }
    }
})();
