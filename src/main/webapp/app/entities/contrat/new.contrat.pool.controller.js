(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .controller('NewContratPoolController', NewContratPoolController);

    NewContratPoolController.$inject = ['$timeout', '$scope', '$stateParams', 'Vehicule','Agent','Assureur','Zone','TypeAssuranceAuto','Contrat','Assure','Vignette'];

    function NewContratPoolController ($timeout, $scope, $stateParams, Vehicule,Agent,Assureur,Zone,TypeAssuranceAuto,Contrat,Assure,Vignette) {
        var vm = this;

       // vm.vehicule = entity;
        vm.vehicule = {};
        vm.contrat = {};
        vm.vignette = {};
        vm.clear = clear;
        vm.save = save;
        vm.contrat.primeBase = 0;
        vm.contrat.bonus = 0;
        vm.contrat.surplus = 0;
        vm.contrat.pourcentage = 0;
        vm.vehicule.valeurNeuve = 0;
        vm.contrat.annuelVolElectronic = 0;
        vm.contrat.annuelAvanceSecour = 0;
        vm.indAcc = 6600;
        vm.iptDecesAnnuel = 0;
        vm.iptInvalidAnnuel = 0;
        vm.iptFMPHAnnuel = 0;
        vm.vehicule.nbrePlace = 5;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.agents = Agent.query();
        vm.assureurs = Assureur.query();
        vm.zones = Zone.query();
        vm.typeassuranceautos = TypeAssuranceAuto.query();
        vm.calculDuree = calculDuree;
        vm.contrat.dateFin = new Date();
        vm.contrat.dateEffet = new Date();
        calculDuree(vm.contrat.dateEffet,vm.contrat.dateFin);
        vm.changeValue = changeValue;
        vm.calculVignetteAuto = calculVignetteAuto;
        vm.contrat.accessoire = 2500;
        vm.contrat.coutPolice = 0;
        vm.contrat.asac = 250;
        vm.contrat.carteRose = 1000;
        vm.calculE =  calculE;
        vm.contrat.isDommageCollision = false;
        vm.contrat.isDommageAccident = false;
        vm.contrat.isVolIncendie = false;
        vm.contrat.isVolElectronic = false;
        vm.contrat.isAvanceSecour = false;
        vm.contrat.isBriseGlace = false;
        vm.contrat.isRecourDefence = true;
        vm.contrat.isIndAcc = true;
       TypeAssuranceAuto.get({id : 1},function(result){
           console.log("FOUND TYPE ASSURANCE")
           vm.contrat.typeAssuranceAuto = result;
       });
        var storage = window.localStorage;
        vm.userAssurance = JSON.parse(storage.getItem("userAssurance"));
         vm.contrat.pourcentDommageCollision = 3;
        vm.contrat.pourcentDommageAccident = 8.5;
        vm.contrat.pourcentVolIncendie = 3;
        vm.contrat.pourcentVolElectronic = 0;
        vm.contrat.pourcentAvanceSecour = 0;
        vm.contrat.pourcentAssistance = 1.75;
        vm.contrat.pourcentBriseGlace = 1.5;
        vm.contrat.pourcentRecourDefense = 2.5;
        vm.configure = configure;
        vm.vignette.montant = 0;

        vm.changePourcent = true;




        console.log("TYPE ASSURANCE",vm.contrat.typeassuranceautos)


        changeValue();

         function configure(){

            if(vm.changePourcent){
               vm.changePourcent = false;
            }else{
               vm.changePourcent = true;
            }
        }

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
           // $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.vehicule.id !== null) {
                Vehicule.update(vm.vehicule, onSaveSuccess, onSaveError);
            } else {
                Vehicule.save(vm.vehicule, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('orionSoftApp:vehiculeUpdate', result);
         //   $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.datePremMiseCirculation = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }

        function calculE (){

            var date = new Date(vm.contrat.dateEffet);
            vm.contrat.dateFin = new Date(date.setTime( date.getTime() + vm.contrat.duree * 86400000 ));
            vm.contrat.pourcentage = vm.contrat.duree * 100 / 360 ;


        }

        function calculDuree(date1,date2){
           vm.contrat.duree =  dateDiff(date1,date2);



        }

        function dateDiff(date1, date2){
            var diff = {}                           // Initialisation du retour
            var tmp = date2 - date1;

            tmp = Math.floor(tmp/1000);             // Nombre de secondes entre les 2 dates
            diff.sec = tmp % 60;                    // Extraction du nombre de secondes

            tmp = Math.floor((tmp-diff.sec)/60);    // Nombre de minutes (partie entière)
            diff.min = tmp % 60;                    // Extraction du nombre de minutes

            tmp = Math.floor((tmp-diff.min)/60);    // Nombre d'heures (entières)
            diff.hour = tmp % 24;                   // Extraction du nombre d'heures

            tmp = Math.floor((tmp-diff.hour)/24);   // Nombre de jours restants
            diff.day = tmp;

            return diff.day;
        }

        function changeValue(){


        $timeout(function() {
            var garanti1 = parseInt(document.getElementById("garanti1").innerText);

             console.log(garanti1);

             var garanti2 = parseInt(document.getElementById("garanti2").innerText);

             console.log(garanti2);

             var garanti3 = parseInt(document.getElementById("garanti3").innerText);

             console.log(garanti3);

             var garanti4 = parseInt(document.getElementById("garanti4").innerText);

                          console.log(garanti4);

             var garanti5 = parseInt(document.getElementById("garanti5").innerText);

                          console.log(garanti5);

             var garanti6 = parseInt(document.getElementById("garanti6").innerText);

                          console.log(garanti6);

             var garanti7 = parseInt(document.getElementById("garanti7").innerText);

                          console.log(garanti7);

             var garanti8 = parseInt(document.getElementById("garanti8").innerText);

                          console.log(garanti8);

             var garanti9 = parseInt(document.getElementById("garanti9").innerText);

                          console.log(garanti9);

             var garanti10 = parseInt(document.getElementById("garanti10").innerText);

                          console.log(garanti10);

             var garanti11 = parseInt(document.getElementById("garanti11").innerText);


                          console.log(garanti11);

             var primenet = document.getElementById("primeNet");

             var accessoire = document.getElementById("accessoire");

             var coutPolice = document.getElementById("coutPolice");

             var primeHT = document.getElementById("primeHT");

             var tva = document.getElementById("tva");

             var asac = document.getElementById("asac");

             var carteRose = document.getElementById("carteRose");

             var vignette = document.getElementById("vignette");

             var primeTTC = document.getElementById("primeTTC");

             primenet.innerText = garanti1 + garanti2 + garanti3 + garanti4 + garanti5 + garanti6 + garanti7 + garanti8 + garanti9 + garanti10 + garanti11;

             vm.contrat.nette = parseInt(primenet.innerText);

             primeHT.innerText = parseInt(primenet.innerText) + parseInt(accessoire.value) + parseInt(coutPolice.value);

             tva.innerText = (parseInt(primeHT.innerText) + parseInt(asac.value)) * 0.1925;

             vm.contrat.tva = parseInt(tva.innerText);

             primeTTC.innerText = parseInt(primeHT.innerText) + parseInt(tva.innerText) + parseInt(carteRose.value) + parseInt(asac.value) + parseInt(vignette.innerText);

             vm.contrat.acompte = 0;

             vm.contrat.primeTTC = parseInt(primeTTC.innerText);

        },1000)
               }



            function calculVignetteAuto(){

                     if(vm.contrat.vehicule.puissance == 1){

                       vm.vignette.montant = 2000;

                     }

                     if(vm.contrat.vehicule.puissance >= 2 && vm.contrat.vehicule.puissance<=7){

                       vm.vignette.montant = 15000;
                     }

                     if(vm.contrat.vehicule.puissance >= 8 && vm.contrat.vehicule.puissance<=13){

                         vm.vignette.montant = 25000;
                     }

                     if(vm.contrat.vehicule.puissance >= 14 && vm.contrat.vehicule.puissance <= 20){

                         vm.vignette.montant = 50000;
                     }

                     if (vm.contrat.vehicule.puissance>20){
                        vm.vignette.montant = 100000;
                     }

                     changeValue();

                }


         function save () {
                     vm.isSaving = true;
                   /*  if (vm.contrat.id !== null) {
                         Contrat.update(vm.contrat, onSaveSuccess, onSaveError);
                     } else {
                         Contrat.save(vm.contrat, onSaveSuccess, onSaveError);
                     }*/
                     Assure.save(vm.assure,function(assure){

                        Vehicule.save(vm.vehicule,function(vehicule){


                         vm.vignette.vehicule = vehicule;
                         vm.vignette.dateEffet = new Date();
                         var date = new Date();
                          vm.vignette.dateFin = new Date(date.setTime( date.getTime() + 364 * 86400000 ));
                         Vignette.save(vm.vignette,function(vignette){

                             console.log("save vignette");
                         })

                          vm.contrat.assure = assure;

                          vm.contrat.vehicule = vehicule;

                          vm.contrat.status = "NOUVEAU";

                          vm.contrat.avenant = 1;

                          vm.contrat.agence = vm.userAssurance.agence;

                          Contrat.save(vm.contrat,function(contrat){

                            window.location.href = "#/contrat/" + contrat.id;

                          })

                        })

                     })
                 }


                 function onSaveSuccess (result) {
                     $scope.$emit('orionSoftApp:contratUpdate', result);
                     $uibModalInstance.close(result);
                     vm.isSaving = false;
                 }

                 function onSaveError () {
                     vm.isSaving = false;
                 }




    }
})();
