(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .factory('ContratSearch', ContratSearch);

    ContratSearch.$inject = ['$resource'];

    function ContratSearch($resource) {
        var resourceUrl =  'api/_search/contrats/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
