(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider

            .state('echeance', {
                      parent: 'entity',
                      url: '/echeance',
                      data: {
                          authorities: ['ROLE_USER'],
                          pageTitle: 'orionSoftApp.contrat.home.title'
                      },
                      views: {
                          'content@': {
                              templateUrl: 'app/entities/contrat/echeance.html',
                              controller: 'EcheanceController',
                              controllerAs: 'vm'
                          }
                      },
                      resolve: {
                          translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                              $translatePartialLoader.addPart('contrat');
                              $translatePartialLoader.addPart('pERMIS');
                              $translatePartialLoader.addPart('sTATUS_CONTRAT');
                              $translatePartialLoader.addPart('sOURCE_ENERGIE');
                              $translatePartialLoader.addPart('global');
                              return $translate.refresh();
                          }]
                      }
                  })


                  .state('dossier', {
                            parent: 'entity',
                            url: '/dossier',
                            data: {
                                authorities: ['ROLE_USER'],
                                pageTitle: 'orionSoftApp.contrat.home.title'
                            },
                            views: {
                                'content@': {
                                    templateUrl: 'app/entities/contrat/dossier.html',
                                    controller: 'DossierController',
                                    controllerAs: 'vm'
                                }
                            },
                            resolve: {
                                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                                    $translatePartialLoader.addPart('contrat');
                                    $translatePartialLoader.addPart('pERMIS');
                                    $translatePartialLoader.addPart('sTATUS_CONTRAT');
                                    $translatePartialLoader.addPart('sOURCE_ENERGIE');
                                    $translatePartialLoader.addPart('global');
                                    return $translate.refresh();
                                }]
                            }
                        })

          .state('renew-contrat', {
            parent: 'entity',
            url: '/renew/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'orionSoftApp.contrat.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/contrat/renew.html',
                    controller: 'RenewController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('contrat');
                    $translatePartialLoader.addPart('pERMIS');
                    $translatePartialLoader.addPart('sTATUS_CONTRAT');
                    $translatePartialLoader.addPart('sOURCE_ENERGIE');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })

        .state('add-contrat', {
          parent: 'entity',
          url: '/addContrat/{id}',
          data: {
              authorities: ['ROLE_USER'],
              pageTitle: 'orionSoftApp.contrat.home.title'
          },
          views: {
              'content@': {
                  templateUrl: 'app/entities/contrat/new.contrat.flotte.html',
                  controller: 'NewContratFlotteController',
                  controllerAs: 'vm'
              }
          },
          resolve: {
              translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                  $translatePartialLoader.addPart('contrat');
                  $translatePartialLoader.addPart('pERMIS');
                  $translatePartialLoader.addPart('sTATUS_CONTRAT');
                  $translatePartialLoader.addPart('sOURCE_ENERGIE');
                  $translatePartialLoader.addPart('global');
                  return $translate.refresh();
              }]
          }
      })

         .state('bordereau-one', {
            parent: 'entity',
            url: '/BordereauOne/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'orionSoftApp.contrat.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/contrat/bordereau.one.html',
                    controller: 'BordereauController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('contrat');
                    $translatePartialLoader.addPart('pERMIS');
                    $translatePartialLoader.addPart('sTATUS_CONTRAT');
                    $translatePartialLoader.addPart('sOURCE_ENERGIE');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })


        .state('bordereau-two', {
            parent: 'entity',
            url: '/BordereauTwo/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'orionSoftApp.contrat.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/contrat/bordereau.two.html',
                    controller: 'BordereauTwoController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('contrat');
                    $translatePartialLoader.addPart('pERMIS');
                    $translatePartialLoader.addPart('sTATUS_CONTRAT');
                    $translatePartialLoader.addPart('sOURCE_ENERGIE');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })

         .state('choix-bordereau', {
            parent: 'entity',
            url: '/choixBordereau',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'orionSoftApp.contrat.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/contrat/choix.bordereau.html',
                    controller: 'ChoixBordereauController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('contrat');
                    $translatePartialLoader.addPart('pERMIS');
                    $translatePartialLoader.addPart('sTATUS_CONTRAT');
                    $translatePartialLoader.addPart('sOURCE_ENERGIE');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })



        .state('choix-contrat', {
            parent: 'entity',
            url: '/choixContrat',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'orionSoftApp.contrat.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/contrat/choix.contrat.html',
                    controller: 'ChoixContratController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('contrat');
                    $translatePartialLoader.addPart('pERMIS');
                    $translatePartialLoader.addPart('sTATUS_CONTRAT');
                    $translatePartialLoader.addPart('sOURCE_ENERGIE');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })

         .state('new-contrat', {
            parent: 'entity',
            url: '/newContrat',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'orionSoftApp.contrat.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/contrat/new.contrat.html',
                    controller: 'NewContratController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('contrat');
                    $translatePartialLoader.addPart('pERMIS');
                    $translatePartialLoader.addPart('sTATUS_CONTRAT');
                    $translatePartialLoader.addPart('sOURCE_ENERGIE');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })

        .state('orion-first', {
           parent: 'entity',
           url: '/orionFirst',
           data: {
               authorities: ['ROLE_USER'],
               pageTitle: 'orionSoftApp.contrat.home.title'
           },
           views: {
               'content@': {
                   templateUrl: 'app/entities/contrat/orionfirst.html',
                   controller: 'OrionFirstController',
                   controllerAs: 'vm'
               }
           },
           resolve: {
               translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                   $translatePartialLoader.addPart('contrat');
                   $translatePartialLoader.addPart('pERMIS');
                   $translatePartialLoader.addPart('sTATUS_CONTRAT');
                   $translatePartialLoader.addPart('sOURCE_ENERGIE');
                   $translatePartialLoader.addPart('global');
                   return $translate.refresh();
               }]
           }
       })



        .state('new-contrat-pool', {
                    parent: 'entity',
                    url: '/newContratPool',
                    data: {
                        authorities: ['ROLE_USER'],
                        pageTitle: 'orionSoftApp.contrat.home.title'
                    },
                    views: {
                        'content@': {
                            templateUrl: 'app/entities/contrat/new.contrat.pool.html',
                            controller: 'NewContratPoolController',
                            controllerAs: 'vm'
                        }
                    },
                    resolve: {
                        translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                            $translatePartialLoader.addPart('contrat');
                            $translatePartialLoader.addPart('pERMIS');
                            $translatePartialLoader.addPart('sTATUS_CONTRAT');
                            $translatePartialLoader.addPart('sOURCE_ENERGIE');
                            $translatePartialLoader.addPart('global');
                            return $translate.refresh();
                        }]
                    }
                })

        .state('contrat', {
            parent: 'entity',
            url: '/contrat',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'orionSoftApp.contrat.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/contrat/contrats.html',
                    controller: 'ContratController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('contrat');
                    $translatePartialLoader.addPart('pERMIS');
                    $translatePartialLoader.addPart('sTATUS_CONTRAT');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('contrat-detail', {
            parent: 'entity',
            url: '/contrat/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'orionSoftApp.contrat.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/contrat/contrat-detail.html',
                    controller: 'ContratDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('contrat');
                    $translatePartialLoader.addPart('pERMIS');
                    $translatePartialLoader.addPart('sTATUS_CONTRAT');
                    $translatePartialLoader.addPart('paiement');
                    $translatePartialLoader.addPart('pAYMENT_MODE');
                    $translatePartialLoader.addPart('nATURE_PAIEMENT');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Contrat', function($stateParams, Contrat) {
                    return Contrat.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'contrat',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('contrat-detail.edit', {
            parent: 'contrat-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/contrat/new.contrat.html',
                    controller: 'ContratDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Contrat', function(Contrat) {
                            return Contrat.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })

        .state('contrat.new', {
            parent: 'contrat',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/contrat/contrat-dialog.html',
                    controller: 'ContratDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                numeroPolice: null,
                                dateEffet: null,
                                duree: null,
                                souscripteur: null,
                                conducteur: null,
                                statutSocio: null,
                                categoriePermis: null,
                                primeTTC: null,
                                acompte: null,
                                status: null,
                                dateFin: null,
                                primeBase: null,
                                pourcentage: null,
                                accessoire: null,
                                coutPolice: null,
                                asac: null,
                                carteRose: null,
                                isDommageCollision: null,
                                pourcentDommageCollision: null,
                                isDommageAccident: null,
                                pourcentDommageAccident: null,
                                isVolIncendie: null,
                                pourcentVolIncendie: null,
                                isVolElectronic: null,
                                pourcentVolElectronic: null,
                                annuelVolElectronic: null,
                                isAvanceSecour: null,
                                pourcentAvanceSecour: null,
                                annuelAvanceSecour: null,
                                isAssistance: null,
                                pourcentAssistance: null,
                                isBriseGlace: null,
                                pourcentBriseGlace: null,
                                isRecourDefence: null,
                                pourcentRecourDefense: null,
                                isIndAcc: null,
                                annuelIndAcc: null,
                                isIptDeces: null,
                                isIptInvalid: null,
                                isIptFMPH: null,
                                iptDecesCapitaux: null,
                                iptInvalidCapitaux: null,
                                iptFMPHCapitaux: null,
                                iptDecesAnnuel: null,
                                iptInvalidAnnuel: null,
                                iptFMPHAnnuel: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('contrat', null, { reload: 'contrat' });
                }, function() {
                    $state.go('contrat');
                });
            }]
        })
        .state('contrat.edit', {
            parent: 'contrat',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/contrat/contrat-dialog.html',
                    controller: 'ContratDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Contrat', function(Contrat) {
                            return Contrat.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('contrat', null, { reload: 'contrat' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('contrat.delete', {
            parent: 'contrat',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/contrat/contrat-delete-dialog.html',
                    controller: 'ContratDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Contrat', function(Contrat) {
                            return Contrat.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('contrat', null, { reload: 'contrat' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
