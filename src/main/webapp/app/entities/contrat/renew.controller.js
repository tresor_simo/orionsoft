(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .controller('RenewController', RenewController);

    RenewController.$inject = ['$timeout', '$scope', '$stateParams', 'Vehicule','Agent','Assureur','Zone','TypeAssuranceAuto','Contrat','Assure','$rootScope','Vignette'];

    function RenewController ($timeout, $scope, $stateParams, Vehicule,Agent,Assureur,Zone,TypeAssuranceAuto,Contrat,Assure,$rootScope,Vignette) {
        var vm = this;

       // vm.vehicule = entity;
        vm.vehicule = {};
        vm.contrat = {};
        vm.vignette = {};
        vm.clear = clear;
        vm.save = save;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.agents = Agent.query();
        vm.assureurs = Assureur.query();
        vm.zones = Zone.query();
        vm.typeassuranceautos = TypeAssuranceAuto.query();
        vm.calculDuree = calculDuree;
        calculDuree(vm.contrat.dateEffet,vm.contrat.dateFin);
        vm.changeValue = changeValue;

        vm.calculE =  calculE;
        vm.contrat.isDommageCollision = false;
        vm.contrat.isDommageAccident = false;
        vm.contrat.isVolIncendie = false;
        vm.contrat.isVolElectronic = false;
        vm.contrat.isAvanceSecour = false;
        vm.contrat.isBriseGlace = false;
        vm.contrat.isRecourDefence = false;
        vm.contrat.isIndAcc = false;
        Contrat.queryOne({id : $stateParams.id},function(result){
            vm.contrat = result;
             vm.contrat.avenant = vm.contrat.avenant + 1;
             vm.contrat.dateEffet = new Date();
             vm.contrat.vehicule.datePremMiseCirculation = new Date(vm.contrat.vehicule.datePremMiseCirculation);
             vm.contrat.vehicule.dateDerniereVT = new Date(vm.contrat.vehicule.dateDerniereVT);
             vm.contrat.typeAssuranceAuto = result.typeAssuranceAuto;
             vm.contrat.id = null;
             vm.contrat.idPrev = $stateParams.id;
             console.log("New Contrat",vm.contrat);
             vm.isVignette = false;
             Vignette.query({},function(result){
                  console.log("SEARCH VIGNETTE",result);
                 for(var i =0; i<result.length; i++){
                     var today = new Date();
                     var dateFin = new Date(result[i].dateFin);
                     if((result[i].vehicule.id == vm.contrat.vehicule.id) && (dateFin > today) ){
                          console.log("FOUND VIGNETTE",result[i]);
                          vm.vignette = result[i];
                          vm.isVignette = true;
                     }
                 }

                 calculVignetteAuto();

             });
        });

         var storage = window.localStorage;
        vm.userAssurance = JSON.parse(storage.getItem("userAssurance"));
        vm.contrat.pourcentDommageCollision = 3;
        vm.contrat.pourcentDommageAccident = 8.5;
        vm.contrat.pourcentVolIncendie = 3;
        vm.contrat.pourcentVolElectronic = 0;
        vm.contrat.pourcentAvanceSecour = 0;
        vm.contrat.pourcentAssistance = 1.75;
        vm.contrat.pourcentBriseGlace = 1.5;
        vm.contrat.pourcentRecourDefense = 2.5;
        vm.configure = configure;
        vm.vignette.montant = 0;
        vm.changePourcent = true;


        changeValue();

        function configure(){

            if(vm.changePourcent){
               vm.changePourcent = false;
            }else{
               vm.changePourcent = true;
            }
        }

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
           // $uibModalInstance.dismiss('cancel');
        }

        function onSaveSuccess (result) {
            $scope.$emit('orionSoftApp:vehiculeUpdate', result);
         //   $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.datePremMiseCirculation = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }



         function calculE (){

                    var date = new Date(vm.contrat.dateEffet);
                    vm.contrat.dateFin = new Date(date.setTime( date.getTime() + vm.contrat.duree * 86400000 ));
                    vm.contrat.pourcentage = vm.contrat.duree * 100 / 360 ;


                }

        function calculDuree(date1,date2){
           vm.contrat.duree =  dateDiff(date1,date2);
        }

        function dateDiff(date1, date2){
            var diff = {}                           // Initialisation du retour
            var tmp = date2 - date1;

            tmp = Math.floor(tmp/1000);             // Nombre de secondes entre les 2 dates
            diff.sec = tmp % 60;                    // Extraction du nombre de secondes

            tmp = Math.floor((tmp-diff.sec)/60);    // Nombre de minutes (partie entière)
            diff.min = tmp % 60;                    // Extraction du nombre de minutes

            tmp = Math.floor((tmp-diff.min)/60);    // Nombre d'heures (entières)
            diff.hour = tmp % 24;                   // Extraction du nombre d'heures

            tmp = Math.floor((tmp-diff.hour)/24);   // Nombre de jours restants
            diff.day = tmp;

            return diff.day;
        }

        function changeValue(){


        $timeout(function() {
            var garanti1 = parseInt(document.getElementById("garanti1").innerText);

             console.log(garanti1);

             var garanti2 = parseInt(document.getElementById("garanti2").innerText);

             console.log(garanti2);

             var garanti3 = parseInt(document.getElementById("garanti3").innerText);

             console.log(garanti3);

             var garanti4 = parseInt(document.getElementById("garanti4").innerText);

                          console.log(garanti4);

             var garanti5 = parseInt(document.getElementById("garanti5").innerText);

                          console.log(garanti5);

             var garanti6 = parseInt(document.getElementById("garanti6").innerText);

                          console.log(garanti6);

             var garanti7 = parseInt(document.getElementById("garanti7").innerText);

                          console.log(garanti7);

             var garanti8 = parseInt(document.getElementById("garanti8").innerText);

                          console.log(garanti8);

             var garanti9 = parseInt(document.getElementById("garanti9").innerText);

                          console.log(garanti9);

             var garanti10 = parseInt(document.getElementById("garanti10").innerText);

                          console.log(garanti10);

             var garanti11 = parseInt(document.getElementById("garanti11").innerText);


                          console.log(garanti11);

             var primenet = document.getElementById("primeNet");

             var accessoire = document.getElementById("accessoire");

             var coutPolice = document.getElementById("coutPolice");

             var primeHT = document.getElementById("primeHT");

             var tva = document.getElementById("tva");

             var asac = document.getElementById("asac");

             var carteRose = document.getElementById("carteRose");

             var primeTTC = document.getElementById("primeTTC");

             primenet.innerText = garanti1 + garanti2 + garanti3 + garanti4 + garanti5 + garanti6 + garanti7 + garanti8 + garanti9 + garanti10 + garanti11;

             vm.contrat.nette = parseInt(primenet.innerText);

             primeHT.innerText = parseInt(primenet.innerText) + parseInt(accessoire.value) + parseInt(coutPolice.value);

             tva.innerText = (parseInt(primeHT.innerText) + parseInt(asac.value)) * 0.1925;

             vm.contrat.tva = parseInt(tva.innerText);

             primeTTC.innerText = parseInt(primeHT.innerText) + parseInt(tva.innerText) + parseInt(carteRose.value) + parseInt(asac.value);

             vm.contrat.acompte = 0;

             vm.contrat.primeTTC = parseInt(primeTTC.innerText) + vm.vignette.montant;

        },1000)
               }


         function calculVignetteAuto(){

               if(vm.contrat.vehicule.puissance == 1){

                 vm.vignette.montant = 2000;

               }

               if(vm.contrat.vehicule.puissance >= 2 && vm.contrat.vehicule.puissance<=7){

                 vm.vignette.montant = 15000;
               }

               if(vm.contrat.vehicule.puissance >= 8 && vm.contrat.vehicule.puissance<=13){

                   vm.vignette.montant = 25000;
               }

               if(vm.contrat.vehicule.puissance >= 14 && vm.contrat.vehicule.puissance <= 20){

                   vm.vignette.montant = 50000;
               }

               if (vm.contrat.vehicule.puissance>20){
                  vm.vignette.montant = 100000;
               }

               changeValue();

          }


         function save () {
                     vm.isSaving = true;
                   /*  if (vm.contrat.id !== null) {
                         Contrat.update(vm.contrat, onSaveSuccess, onSaveError);
                     } else {
                         Contrat.save(vm.contrat, onSaveSuccess, onSaveError);
                     }*/
                          vm.contrat.status = "NOUVEAU";
                          vm.contrat.agence = vm.userAssurance.agence;
                          Contrat.save(vm.contrat,function(contrat){
                              console.log("===SAUVEGARDE DU NOUVEAU CONTRAT===");
                              Contrat.queryOne({id : $stateParams.id},function(result){
                                   result.status = "RENOUVELE";
                                   result.idNext = contrat.id;
                                   console.log("ok");
                                   Contrat.update(result,function(resultat){
                                        console.log("===UPDATE DE LANCIEN CONTRAT===");
                                        if(!vm.isVignette){
                                            vm.vignette.dateEffet = new Date();
                                             var date = new Date();
                                             vm.vignette.dateFin = new Date(date.setTime( date.getTime() + 364 * 86400000 ));
                                             vm.vignette.vehicule = vm.contrat.vehicule;
                                             Vignette.save(vm.vignette,function(result){
                                                 window.location.href = "#/contrat/" + contrat.id;
                                             })
                                        }else{
                                           window.location.href = "#/contrat/" + contrat.id;
                                        }

                                   },function(error){
                                       console.log(error);
                                   })
                               });

                     })
                 }




    }
})();
