(function() {
    'use strict';
    angular
        .module('orionSoftApp')
        .factory('Contrat', Contrat);

    Contrat.$inject = ['$resource', 'DateUtils'];

    function Contrat ($resource, DateUtils) {
        var resourceUrl =  'api/contrats/:id';

        return $resource(resourceUrl, {}, {
            'queryOne':{method:'GET', isArray:false},
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.dateEffet = DateUtils.convertLocalDateFromServer(data.dateEffet);
                        data.dateFin = DateUtils.convertLocalDateFromServer(data.dateFin);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    data.dateEffet = DateUtils.convertLocalDateToServer(data.dateEffet);
                    data.dateFin = DateUtils.convertLocalDateToServer(data.dateFin);
                    return angular.toJson(data);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    data.dateEffet = DateUtils.convertLocalDateToServer(data.dateEffet);
                    data.dateFin = DateUtils.convertLocalDateToServer(data.dateFin);
                    return angular.toJson(data);
                }
            }
        });
    }
})();
