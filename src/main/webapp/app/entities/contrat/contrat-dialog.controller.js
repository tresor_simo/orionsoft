(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .controller('ContratDialogController', ContratDialogController);

    ContratDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Contrat', 'Agent', 'Assureur', 'Vehicule', 'Zone', 'Assure', 'TypeAssuranceAuto', 'Agence','$state'];

    function ContratDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Contrat, Agent, Assureur, Vehicule, Zone, Assure, TypeAssuranceAuto, Agence,$state) {
        var vm = this;

        vm.contrat = entity;
        vm.contrat.dateEffet = new Date(vm.contrat.dateEffet);
        vm.contrat.dateFin = new Date(vm.contrat.dateFin);
        vm.vignette = {};
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.agents = Agent.query();
        vm.assureurs = Assureur.query();
        vm.vehicules = Vehicule.query();
        vm.zones = Zone.query();
        vm.assures = Assure.query();
        vm.typeassuranceautos = TypeAssuranceAuto.query();
        vm.agences = Agence.query();
        vm.changeValue = changeValue;
        vm.calculVignetteAuto = calculVignetteAuto;
        vm.configure=configure;


        calculVignetteAuto();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        changeValue();


         function changeValue(){


        $timeout(function() {
            var garanti1 = parseInt(document.getElementById("garanti1").innerText);

             console.log(garanti1);

             var garanti2 = parseInt(document.getElementById("garanti2").innerText);

             console.log(garanti2);

             var garanti3 = parseInt(document.getElementById("garanti3").innerText);

             console.log(garanti3);

             var garanti4 = parseInt(document.getElementById("garanti4").innerText);

                          console.log(garanti4);

             var garanti5 = parseInt(document.getElementById("garanti5").innerText);

                          console.log(garanti5);

             var garanti6 = parseInt(document.getElementById("garanti6").innerText);

                          console.log(garanti6);

             var garanti7 = parseInt(document.getElementById("garanti7").innerText);

                          console.log(garanti7);

             var garanti8 = parseInt(document.getElementById("garanti8").innerText);

                          console.log(garanti8);

             var garanti9 = parseInt(document.getElementById("garanti9").innerText);

                          console.log(garanti9);

             var garanti10 = parseInt(document.getElementById("garanti10").innerText);

                          console.log(garanti10);

             var garanti11 = parseInt(document.getElementById("garanti11").innerText);


                          console.log(garanti11);

             var primenet = document.getElementById("primeNet");

             var accessoire = document.getElementById("accessoire");

             var vignette = document.getElementById("vignette");

             var coutPolice = document.getElementById("coutPolice");

             var primeHT = document.getElementById("primeHT");

             var tva = document.getElementById("tva");

             var asac = document.getElementById("asac");

             var carteRose = document.getElementById("carteRose");

             var primeTTC = document.getElementById("primeTTC");

             primenet.innerText = garanti1 + garanti2 + garanti3 + garanti4 + garanti5 + garanti6 + garanti7 + garanti8 + garanti9 + garanti10 + garanti11;

             vm.contrat.nette = parseInt(primenet.innerText);

             primeHT.innerText = parseInt(primenet.innerText) + parseInt(accessoire.value) + parseInt(coutPolice.value);

             tva.innerText = (parseInt(primeHT.innerText) + parseInt(asac.value)) * 0.1925;

             vm.contrat.tva = parseInt(tva.innerText);

             primeTTC.innerText = parseInt(primeHT.innerText) + parseInt(tva.innerText) + parseInt(carteRose.value) + parseInt(asac.value) + parseInt(vignette.innerText);

             vm.contrat.acompte = 0;

             vm.contrat.primeTTC = parseInt(primeTTC.innerText);

        },1000)
               }

           function configure(){

                      if(vm.changePourcent){
                         vm.changePourcent = false;
                      }else{
                         vm.changePourcent = true;
                      }
                  }



          function calculVignetteAuto(){

            if(vm.contrat.vehicule.puissance == 1){

              vm.vignette.montant = 2000;

            }

            if(vm.contrat.vehicule.puissance >= 2 && vm.contrat.vehicule.puissance<=7){

              vm.vignette.montant = 15000;
            }

            if(vm.contrat.vehicule.puissance >= 8 && vm.contrat.vehicule.puissance<=13){

                vm.vignette.montant = 25000;
            }

            if(vm.contrat.vehicule.puissance >= 14 && vm.contrat.vehicule.puissance <= 20){

                vm.vignette.montant = 50000;
            }

            if (vm.contrat.vehicule.puissance>20){
               vm.vignette.montant = 100000;
            }

            changeValue();

       }


        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

         function save () {
                             vm.isSaving = true;
                           /*  if (vm.contrat.id !== null) {
                                 Contrat.update(vm.contrat, onSaveSuccess, onSaveError);
                             } else {
                                 Contrat.save(vm.contrat, onSaveSuccess, onSaveError);
                             }*/
                             Assure.update(vm.contrat.assure,function(assure){

                                 console.log("OK ASSURE");

                                Vehicule.update(vm.contrat.vehicule,function(vehicule){

                                  console.log("OK VEHICULE");

                                  vm.contrat.assure = assure;
                                  vm.contrat.vehicule = vehicule;


                                  Contrat.update(vm.contrat,function(contrat){
                                       console.log("OK CONTRAT");
                                       $scope.$emit('orionSoftApp:contratUpdate', contrat);
                                        $uibModalInstance.close(contrat);
                                  })

                                })

                             })
                         }

        function onSaveSuccess (result) {
            $scope.$emit('orionSoftApp:contratUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.dateEffet = false;
        vm.datePickerOpenStatus.dateFin = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }



    }
})();
