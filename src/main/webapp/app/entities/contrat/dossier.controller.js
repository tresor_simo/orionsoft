(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .controller('DossierController', DossierController);

    DossierController.$inject = ['$scope', '$state','$stateParams', 'Contrat', 'ContratSearch', 'ParseLinks', 'AlertService','Assureur'];

    function DossierController ($scope, $state,$stateParams, Contrat, ContratSearch, ParseLinks, AlertService,Assureur) {
        var vm = this;

        vm.contrats = [];
        vm.loadPage = loadPage;
        vm.page = 0;
        vm.links = {
            last: 0
        };
        vm.predicate = 'id';
        vm.reset = reset;
        vm.reverse = true;
        vm.clear = clear;
        vm.loadAll = loadAll;
        vm.search = search;
        vm.change = change;
        vm.valider = valider;
        vm.print = PrintBordereau;

        vm.type = $stateParams.id;
        vm.assureurs = Assureur.query();
        var contrats = Contrat.query();

        var storage = window.localStorage;
        vm.userAssurance = JSON.parse(storage.getItem("userAssurance"));


        var date = new Date();




       vm.title = "Echéances";
       vm.today = new Date();



        loadAll();

        function change(ass){
            console.log("Contrats",contrats);
            vm.contrats = []
            for(var i =0; i< contrats.length; i++){
               console.log("Contrat1",contrats[i])
               if(contrats[i].assureur.id == ass.id){
                   console.log("found");
                   vm.contrats.push(contrats[i]);
               }

            }
        }

        $scope.print = function(elementId){
            var getMyFrame = document.getElementById(elementId);
            getMyFrame.focus();
            getMyFrame.contentWindow.print();
        }

        function valider(ass){

            console.log("Contrats",contrats);
            vm.contrats = []
            for(var i =0; i< contrats.length; i++){
                 console.log("Contrat1",contrats[i])
                 console.log("Begin",vm.dateBegin.getTime());
                 console.log("Fin", vm.dateFin.getTime());
                 var dateFin = new Date(contrats[i].dateFin);
                 console.log("date effet",dateFin.getTime());
                 if(ass){
                      if((contrats[i].assureur.id == ass.id) && (dateFin >= vm.dateBegin) && (dateFin <= vm.dateFin)){
                          console.log("found");
                          vm.contrats.push(contrats[i]);
                       }
                  }else{
                      if((dateFin >= vm.dateBegin) && (dateFin <= vm.dateFin)){
                           console.log("found");
                           vm.contrats.push(contrats[i]);
                      }

                  }

            }
        }

         function PrintBordereau()
            {
              var mywindow = window.open('', 'PRINT', 'height=900,width=600');


                mywindow.document.write('<html><head><title>' + document.title  + '</title>');

                mywindow.document.write('</head><body >');
                mywindow.document.write(document.getElementById('bordereau').innerHTML);
                mywindow.document.write('</body></html>');

                mywindow.document.close(); // necessary for IE >= 10
                mywindow.focus(); // necessary for IE >= 10*/

                mywindow.print();
                mywindow.close();

                return true;

         }

        function loadAll () {
            if (vm.currentSearch) {
                ContratSearch.query({
                    query: vm.currentSearch,
                    page: vm.page,
                    size: 20,
                    sort: sort()
                }, onSuccess, onError);
            } else {
                Contrat.query({
                    page: vm.page,
                    size: 20,
                    sort: sort()
                }, onSuccess, onError);
            }
            function sort() {
                var result = [vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')];
                if (vm.predicate !== 'id') {
                    result.push('id');
                }
                return result;
            }

            function onSuccess(data, headers) {
                vm.links = ParseLinks.parse(headers('link'));
                vm.totalItems = headers('X-Total-Count');
                for (var i = 0; i < data.length; i++) {
                    vm.contrats.push(data[i]);
                }
            }

            function onError(error) {
                AlertService.error(error.data.message);
            }
        }

        function reset () {
            vm.page = 0;
            vm.contrats = [];
            loadAll();
        }

        function loadPage(page) {
            vm.page = page;
            loadAll();
        }

        function clear () {
            vm.contrats = [];
            vm.links = null;
            vm.page = 0;
            vm.predicate = 'id';
            vm.reverse = true;
            vm.searchQuery = null;
            vm.currentSearch = null;
            vm.loadAll();
        }

        function search (searchQuery) {
            if (!searchQuery){
                return vm.clear();
            }
            vm.contrats = [];
            vm.links = null;
            vm.page = 0;
            vm.predicate = '_score';
            vm.reverse = false;
            vm.currentSearch = searchQuery;
            vm.loadAll();
        }
    }
})();
