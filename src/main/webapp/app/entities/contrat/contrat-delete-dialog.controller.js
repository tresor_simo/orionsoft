(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .controller('ContratDeleteController',ContratDeleteController);

    ContratDeleteController.$inject = ['$uibModalInstance', 'entity', 'Contrat'];

    function ContratDeleteController($uibModalInstance, entity, Contrat) {
        var vm = this;

        vm.contrat = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Contrat.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
