(function() {
    'use strict';
    angular
        .module('orionSoftApp')
        .factory('Setting', Setting);

    Setting.$inject = ['$resource', 'DateUtils'];

    function Setting ($resource, DateUtils) {
        var resourceUrl =  'api/settings/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.updatetime = DateUtils.convertDateTimeFromServer(data.updatetime);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
