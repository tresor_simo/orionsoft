(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .factory('AgenceSearch', AgenceSearch);

    AgenceSearch.$inject = ['$resource'];

    function AgenceSearch($resource) {
        var resourceUrl =  'api/_search/agences/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
