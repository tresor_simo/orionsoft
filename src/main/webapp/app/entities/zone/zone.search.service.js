(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .factory('ZoneSearch', ZoneSearch);

    ZoneSearch.$inject = ['$resource'];

    function ZoneSearch($resource) {
        var resourceUrl =  'api/_search/zones/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
