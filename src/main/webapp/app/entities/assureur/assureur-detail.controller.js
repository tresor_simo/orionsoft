(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .controller('AssureurDetailController', AssureurDetailController);

    AssureurDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Assureur'];

    function AssureurDetailController($scope, $rootScope, $stateParams, previousState, entity, Assureur) {
        var vm = this;

        vm.assureur = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('orionSoftApp:assureurUpdate', function(event, result) {
            vm.assureur = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
