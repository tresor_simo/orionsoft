(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .factory('AssureurSearch', AssureurSearch);

    AssureurSearch.$inject = ['$resource'];

    function AssureurSearch($resource) {
        var resourceUrl =  'api/_search/assureurs/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
