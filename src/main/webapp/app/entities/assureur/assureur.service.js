(function() {
    'use strict';
    angular
        .module('orionSoftApp')
        .factory('Assureur', Assureur);

    Assureur.$inject = ['$resource'];

    function Assureur ($resource) {
        var resourceUrl =  'api/assureurs/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
