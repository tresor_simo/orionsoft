(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('assureur', {
            parent: 'entity',
            url: '/assureur',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'orionSoftApp.assureur.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/assureur/assureurs.html',
                    controller: 'AssureurController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('assureur');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('assureur-detail', {
            parent: 'entity',
            url: '/assureur/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'orionSoftApp.assureur.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/assureur/assureur-detail.html',
                    controller: 'AssureurDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('assureur');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Assureur', function($stateParams, Assureur) {
                    return Assureur.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'assureur',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('assureur-detail.edit', {
            parent: 'assureur-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/assureur/assureur-dialog.html',
                    controller: 'AssureurDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Assureur', function(Assureur) {
                            return Assureur.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('assureur.new', {
            parent: 'assureur',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/assureur/assureur-dialog.html',
                    controller: 'AssureurDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                nom: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('assureur', null, { reload: 'assureur' });
                }, function() {
                    $state.go('assureur');
                });
            }]
        })
        .state('assureur.edit', {
            parent: 'assureur',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/assureur/assureur-dialog.html',
                    controller: 'AssureurDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Assureur', function(Assureur) {
                            return Assureur.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('assureur', null, { reload: 'assureur' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('assureur.delete', {
            parent: 'assureur',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/assureur/assureur-delete-dialog.html',
                    controller: 'AssureurDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Assureur', function(Assureur) {
                            return Assureur.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('assureur', null, { reload: 'assureur' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
