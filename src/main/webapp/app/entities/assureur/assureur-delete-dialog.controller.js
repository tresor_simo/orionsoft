(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .controller('AssureurDeleteController',AssureurDeleteController);

    AssureurDeleteController.$inject = ['$uibModalInstance', 'entity', 'Assureur'];

    function AssureurDeleteController($uibModalInstance, entity, Assureur) {
        var vm = this;

        vm.assureur = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Assureur.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
