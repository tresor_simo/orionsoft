(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .controller('AssureurDialogController', AssureurDialogController);

    AssureurDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Assureur'];

    function AssureurDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Assureur) {
        var vm = this;

        vm.assureur = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.assureur.id !== null) {
                Assureur.update(vm.assureur, onSaveSuccess, onSaveError);
            } else {
                Assureur.save(vm.assureur, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('orionSoftApp:assureurUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
