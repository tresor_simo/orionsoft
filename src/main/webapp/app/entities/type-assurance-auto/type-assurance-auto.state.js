(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('type-assurance-auto', {
            parent: 'entity',
            url: '/type-assurance-auto',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'orionSoftApp.typeAssuranceAuto.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/type-assurance-auto/type-assurance-autos.html',
                    controller: 'TypeAssuranceAutoController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('typeAssuranceAuto');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('type-assurance-auto-detail', {
            parent: 'entity',
            url: '/type-assurance-auto/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'orionSoftApp.typeAssuranceAuto.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/type-assurance-auto/type-assurance-auto-detail.html',
                    controller: 'TypeAssuranceAutoDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('typeAssuranceAuto');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'TypeAssuranceAuto', function($stateParams, TypeAssuranceAuto) {
                    return TypeAssuranceAuto.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'type-assurance-auto',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('type-assurance-auto-detail.edit', {
            parent: 'type-assurance-auto-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/type-assurance-auto/type-assurance-auto-dialog.html',
                    controller: 'TypeAssuranceAutoDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['TypeAssuranceAuto', function(TypeAssuranceAuto) {
                            return TypeAssuranceAuto.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('type-assurance-auto.new', {
            parent: 'type-assurance-auto',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/type-assurance-auto/type-assurance-auto-dialog.html',
                    controller: 'TypeAssuranceAutoDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                nom: null,
                                nomPro: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('type-assurance-auto', null, { reload: 'type-assurance-auto' });
                }, function() {
                    $state.go('type-assurance-auto');
                });
            }]
        })
        .state('type-assurance-auto.edit', {
            parent: 'type-assurance-auto',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/type-assurance-auto/type-assurance-auto-dialog.html',
                    controller: 'TypeAssuranceAutoDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['TypeAssuranceAuto', function(TypeAssuranceAuto) {
                            return TypeAssuranceAuto.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('type-assurance-auto', null, { reload: 'type-assurance-auto' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('type-assurance-auto.delete', {
            parent: 'type-assurance-auto',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/type-assurance-auto/type-assurance-auto-delete-dialog.html',
                    controller: 'TypeAssuranceAutoDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['TypeAssuranceAuto', function(TypeAssuranceAuto) {
                            return TypeAssuranceAuto.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('type-assurance-auto', null, { reload: 'type-assurance-auto' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
