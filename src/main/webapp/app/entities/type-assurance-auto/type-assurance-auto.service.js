(function() {
    'use strict';
    angular
        .module('orionSoftApp')
        .factory('TypeAssuranceAuto', TypeAssuranceAuto);

    TypeAssuranceAuto.$inject = ['$resource'];

    function TypeAssuranceAuto ($resource) {
        var resourceUrl =  'api/type-assurance-autos/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
