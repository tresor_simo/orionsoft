(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .controller('TypeAssuranceAutoDeleteController',TypeAssuranceAutoDeleteController);

    TypeAssuranceAutoDeleteController.$inject = ['$uibModalInstance', 'entity', 'TypeAssuranceAuto'];

    function TypeAssuranceAutoDeleteController($uibModalInstance, entity, TypeAssuranceAuto) {
        var vm = this;

        vm.typeAssuranceAuto = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            TypeAssuranceAuto.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
