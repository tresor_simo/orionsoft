(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .controller('TypeAssuranceAutoDialogController', TypeAssuranceAutoDialogController);

    TypeAssuranceAutoDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'TypeAssuranceAuto'];

    function TypeAssuranceAutoDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, TypeAssuranceAuto) {
        var vm = this;

        vm.typeAssuranceAuto = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.typeAssuranceAuto.id !== null) {
                TypeAssuranceAuto.update(vm.typeAssuranceAuto, onSaveSuccess, onSaveError);
            } else {
                TypeAssuranceAuto.save(vm.typeAssuranceAuto, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('orionSoftApp:typeAssuranceAutoUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
