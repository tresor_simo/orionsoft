(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .controller('TypeAssuranceAutoDetailController', TypeAssuranceAutoDetailController);

    TypeAssuranceAutoDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'TypeAssuranceAuto'];

    function TypeAssuranceAutoDetailController($scope, $rootScope, $stateParams, previousState, entity, TypeAssuranceAuto) {
        var vm = this;

        vm.typeAssuranceAuto = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('orionSoftApp:typeAssuranceAutoUpdate', function(event, result) {
            vm.typeAssuranceAuto = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
