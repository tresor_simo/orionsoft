(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .factory('TypeAssuranceAutoSearch', TypeAssuranceAutoSearch);

    TypeAssuranceAutoSearch.$inject = ['$resource'];

    function TypeAssuranceAutoSearch($resource) {
        var resourceUrl =  'api/_search/type-assurance-autos/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
