(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .controller('NewPaiementController', NewPaiementController);

    NewPaiementController.$inject = ['$timeout', '$scope', '$stateParams', 'Paiement', 'UserAssurance', 'Contrat','Vignette','VignetteExtra'];

    function NewPaiementController ($timeout, $scope, $stateParams, Paiement, UserAssurance, Contrat,Vignette,VignetteExtra) {
        var vm = this;

        vm.paiement = {};
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.userassurances = UserAssurance.query();
        var storage = window.localStorage;
        vm.userAssurance = JSON.parse(storage.getItem("userAssurance"));
        Contrat.queryOne({id:$stateParams.id},function(result){
            vm.contrat = result;
            vm.paiements =  Paiement.query({},onSaveSuccess,onSaveError);
            Vignette.query({},function(result){
                 console.log("SEARCH VIGNETTE",result);
                for(var i =0; i<result.length; i++){
                    var today = new Date();
                    var dateFin = new Date(result[i].dateFin);
                    if((result[i].vehicule.id == vm.contrat.vehicule.id) && (dateFin > today) ){
                         console.log("FOUND VIGNETTE",result[i]);
                         vm.vignette = result[i];
                    }
                }

            });

            vm.isVignette = false;
             VignetteExtra.getVignettesByVehicule(vm.contrat.vehicule.id).then(function(result){
                  console.log("SEARCH VIGNETTE",result);
                 for(var i =0; i<result.length; i++){
                     var today = new Date();
                     var dateFin = new Date(result[i].dateFin);
                     if((result[i].vehicule.id == vm.contrat.vehicule.id) && (dateFin > today) ){
                          console.log("FOUND VIGNETTE",result[i]);
                          vm.vignette = result[i];
                          var date1 = new Date();
                          var date2 = new Date(vm.vignette.dateEffet);
                          console.log('Date1',date1);
                          console.log('Date2',date2);
                          if(date1.getDate() == date2.getDate()){

                          }else{
                               vm.isVignette = true;
                          }

                     }
                 }

             });

        });





        vm.today = new Date();
        vm.pay = 0;






        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
           // $uibModalInstance.dismiss('cancel');
        }

        function save () {

            vm.paiement.contrat = vm.contrat;
            vm.paiement.pc = vm.pc;
            vm.paiement.numeroAvenant = vm.contrat.avenant
            vm.paiement.datePaiement = new Date();
            console.log("Paiement",vm.paiement);
            vm.paiement.userAssurance = vm.userAssurance;
            Paiement.save(vm.paiement, success, error);

        }

        function onSaveSuccess (result) {
             vm.pc = result.length + 1;
             for(var i =0; i < result.length; i++){
                                console.log("Paiements",result[i]);

             if(result[i].contrat.id == $stateParams.id){

                                vm.pay = vm.pay + result[i].somme;

             }

              }
                   console.log("Paiements",result);
        }

        function onSaveError () {
           // vm.isSaving = false;
        }

        function success(result){
            window.location.href="#/contrat/" + $stateParams.id;
        }

        function error(){

        }

        vm.datePickerOpenStatus.datePaiement = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
