(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .factory('PaiementExtra', PaiementExtra);

    PaiementExtra.$inject = ['$http','$q'];

    function PaiementExtra($http,$q) {
        var resourceUrl =  'api/paiements/contrat/';

        return {
            getPaiementsByContrat:getPaiementsByContrat
        };

        function getPaiementsByContrat(contrat_id){
            var deferred =$q.defer();
            $http.get(resourceUrl + contrat_id)
                .success(function(response){
                   deferred.resolve(response);
                })
                .error(function(error){
                })
            return deferred.promise;

        }
    }
})();
