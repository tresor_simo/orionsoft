(function() {
    'use strict';
    angular
        .module('orionSoftApp')
        .factory('Paiement', Paiement);

    Paiement.$inject = ['$resource', 'DateUtils'];

    function Paiement ($resource, DateUtils) {
        var resourceUrl =  'api/paiements/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.datePaiement = DateUtils.convertDateTimeFromServer(data.datePaiement);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
