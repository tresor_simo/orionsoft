(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .controller('PaiementDetailController', PaiementDetailController);

    PaiementDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Paiement', 'UserAssurance', 'Contrat'];

    function PaiementDetailController($scope, $rootScope, $stateParams, previousState, entity, Paiement, UserAssurance, Contrat) {
        var vm = this;

        vm.paiement = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('orionSoftApp:paiementUpdate', function(event, result) {
            vm.paiement = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
