(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .controller('FlotteDialogController', FlotteDialogController);

    FlotteDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Flotte'];

    function FlotteDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Flotte) {
        var vm = this;

        vm.flotte = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.flotte.id !== null) {
                Flotte.update(vm.flotte, onSaveSuccess, onSaveError);
            } else {
                Flotte.save(vm.flotte, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('orionSoftApp:flotteUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
