(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .controller('FlotteDetailController', FlotteDetailController);

    FlotteDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Flotte','Contrat','VignetteExtra','$q'];

    function FlotteDetailController($scope, $rootScope, $stateParams, previousState, entity, Flotte, Contrat, VignetteExtra,$q) {
        var vm = this;

        vm.flotte = entity;
        vm.previousState = previousState.name;

        vm.contrats = [];
        vm.contratsFl = [];
        vm.contrat = {};

        vm.today = new Date();
        var storage = window.localStorage;
        vm.userAssurance = JSON.parse(storage.getItem("userAssurance"));


        var garanti1 = document.getElementById("garanti1");
        var g1 = 0;
        var garanti2 = document.getElementById("garanti2");
        var g2 = 0;
        var garanti3 = document.getElementById("garanti3");
        var g3 = 0;
        var garanti4 = document.getElementById("garanti4");
        var g4 = 0;
        var garanti5 = document.getElementById("garanti5");
        var g5 = 0;
        var garanti6 = document.getElementById("garanti6");
        var g6 = 0;
        var garanti7 = document.getElementById("garanti7");
        var g7 = 0;
        var garanti8 = document.getElementById("garanti8");
        var g8 = 0;
        var garanti9 = document.getElementById("garanti9");
        var g9 = 0;
        var garanti10 = document.getElementById("garanti10");
        var g10 = 0;
        var garanti11 = document.getElementById("garanti11");
        var g11 = 0;

        var vignette = document.getElementById("vignette");
        $scope.vign = 0;

        var primeBrute = document.getElementById("primebrute");
        var brute = 0;

        var reduction = document.getElementById("reduction");
        var primenet = document.getElementById("primenet");

        var accessoire = document.getElementById("accessoire");
        var acc = 0;

        var tva = document.getElementById("tva");

        var ttc = document.getElementById("ttc")



        Contrat.query(function(result){
           vm.contrats = result;
           for (var i = 0; i < vm.contrats.length; i++){
             if(vm.contrats[i].numeroPolice == vm.flotte.numeroPolice){
               vm.contratsFl.push(vm.contrats[i]);
               vm.contrat = vm.contrats[i];
               VignetteExtra.getVignettesByVehicule(vm.contrat.vehicule.id).then(function(result){
                    console.log("SEARCH VIGNETTE",result);
                     for(var i =0; i<result.length; i++){
                       var today = new Date();
                       var dateFin = new Date(result[i].dateFin);
                       if(dateFin > today ){
                            vm.contrat.vignette = result[i].montant;
                            console.log("VIGN AVANT",$scope.vign);
                            $scope.vign = $scope.vign + result[i].montant;
                            console.log("VIGN APRES",$scope.vign);
                            vignette.innerText = $scope.vign;
                       }
                     }
               });
               g1 = g1 + (vm.contrat.primeBase - (vm.contrat.bonus/100 * vm.contrat.primeBase) + (vm.contrat.surplus/100 * vm.contrat.primeBase)) * vm.contrat.pourcentage/100;
               g2 = g2 + (vm.contrat.vehicule.valeurNeuve * vm.contrat.pourcentDommageCollision/100) * vm.contrat.pourcentage / 100 ;
               g3 = g3 + (vm.contrat.vehicule.valeurNeuve * vm.contrat.pourcentDommageAccident/100) * vm.contrat.pourcentage / 100 ;
               g4 = g4 + (vm.contrat.vehicule.valeurNeuve * vm.contrat.pourcentVolIncendie/100) * vm.contrat.pourcentage / 100 ;
               g5 = g5 + vm.contrat.annuelVolElectronic * vm.contrat.pourcentage / 100 ;
               g6 = g6 + vm.contrat.annuelAvanceSecour * vm.contrat.pourcentage / 100 ;
               g7 = g7 + (vm.contrat.vehicule.valeurNeuve * vm.contrat.pourcentAssistance/100) * vm.contrat.pourcentage / 100 ;
               g8 = g8 + (vm.contrat.vehicule.valeurNeuve * vm.contrat.pourcentBriseGlace/100) * vm.contrat.pourcentage / 100 ;
               g9 = g9 + (vm.contrat.primeBase - (vm.contrat.bonus/100 * vm.contrat.primeBase) + (vm.contrat.surplus/100 * vm.contrat.primeBase)) * vm.contrat.pourcentRecourDefense/100 * vm.contrat.pourcentage /100 ;
               g10 = g10 + vm.contrat.annuelIndAcc * vm.contrat.pourcentage /100 ;
               g11 = g11 + ((vm.contrat.iptDecesAnnuel * vm.contrat.pourcentage /100) + (vm.contrat.iptInvalidAnnuel * vm.contrat.pourcentage /100) + (vm.contrat.iptFMPHAnnuel * vm.contrat.pourcentage /100)) * vm.contrat.vehicule.nbrePlace ;
               brute = brute + vm.contrat.primeTTC;
               acc = acc + vm.contrat.accessoire;
             }
           }


           garanti1.innerText= g1;
           garanti2.innerText = g2;
           garanti3.innerText = g3;
           garanti4.innerText= g4;
           garanti5.innerText = g5;
           garanti6.innerText = g6;
           garanti7.innerText= g7;
           garanti8.innerText = g8;
           garanti9.innerText = g9;
           garanti10.innerText= g10;
           garanti11.innerText = g11;

           primebrute.innerText = brute;
           $scope.primebrute = primebrute.innerText;
           reduction.innerText = parseInt(primebrute.innerText) * vm.flotte.nbre /100 ;
           $scope.reduction = reduction.innerText;
           primenet.innerText = parseInt(primebrute.innerText) - parseInt(reduction.innerText);
           $scope.primenet = primenet.innerText;
           accessoire.innerText = vm.flotte.accessoire;
           $scope.accessoire = accessoire.innerText;
           tva.innerText = parseInt(primenet.innerText) * 19.25 / 100;
           $scope.tva = tva.innerText;
           ttc.innerText = parseInt(primenet.innerText) + parseInt(tva.innerText) + parseInt(accessoire.innerText) +  $scope.vign ;
           $scope.ttc = ttc.innerText;

        })

        var unsubscribe = $rootScope.$on('orionSoftApp:flotteUpdate', function(event, result) {
            vm.flotte = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
