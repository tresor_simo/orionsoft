(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('flotte', {
            parent: 'entity',
            url: '/flotte',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'orionSoftApp.flotte.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/flotte/flottes.html',
                    controller: 'FlotteController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('flotte');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('flotte-detail', {
            parent: 'entity',
            url: '/flotte/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'orionSoftApp.flotte.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/flotte/flotte-detail.html',
                    controller: 'FlotteDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('flotte');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Flotte', function($stateParams, Flotte) {
                    return Flotte.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'flotte',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('flotte-detail.edit', {
            parent: 'flotte-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/flotte/flotte-dialog.html',
                    controller: 'FlotteDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Flotte', function(Flotte) {
                            return Flotte.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('flotte.new', {
            parent: 'flotte',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/flotte/flotte-dialog.html',
                    controller: 'FlotteDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                numeroPolice: null,
                                nbre: null,
                                accessoire: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('flotte', null, { reload: 'flotte' });
                }, function() {
                    $state.go('flotte');
                });
            }]
        })
        .state('flotte.edit', {
            parent: 'flotte',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/flotte/flotte-dialog.html',
                    controller: 'FlotteDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Flotte', function(Flotte) {
                            return Flotte.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('flotte', null, { reload: 'flotte' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('flotte.delete', {
            parent: 'flotte',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/flotte/flotte-delete-dialog.html',
                    controller: 'FlotteDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Flotte', function(Flotte) {
                            return Flotte.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('flotte', null, { reload: 'flotte' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
