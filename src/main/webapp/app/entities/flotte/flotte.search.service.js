(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .factory('FlotteSearch', FlotteSearch);

    FlotteSearch.$inject = ['$resource'];

    function FlotteSearch($resource) {
        var resourceUrl =  'api/_search/flottes/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
