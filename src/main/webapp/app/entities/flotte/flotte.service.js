(function() {
    'use strict';
    angular
        .module('orionSoftApp')
        .factory('Flotte', Flotte);

    Flotte.$inject = ['$resource'];

    function Flotte ($resource) {
        var resourceUrl =  'api/flottes/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
