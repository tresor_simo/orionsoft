(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .controller('FlotteDeleteController',FlotteDeleteController);

    FlotteDeleteController.$inject = ['$uibModalInstance', 'entity', 'Flotte'];

    function FlotteDeleteController($uibModalInstance, entity, Flotte) {
        var vm = this;

        vm.flotte = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Flotte.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
