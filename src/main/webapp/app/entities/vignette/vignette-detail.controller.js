(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .controller('VignetteDetailController', VignetteDetailController);

    VignetteDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Vignette', 'Vehicule'];

    function VignetteDetailController($scope, $rootScope, $stateParams, previousState, entity, Vignette, Vehicule) {
        var vm = this;

        vm.vignette = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('orionSoftApp:vignetteUpdate', function(event, result) {
            vm.vignette = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
