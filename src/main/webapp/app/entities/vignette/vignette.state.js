(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('vignette', {
            parent: 'entity',
            url: '/vignette',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'orionSoftApp.vignette.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/vignette/vignettes.html',
                    controller: 'VignetteController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('vignette');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('vignette-detail', {
            parent: 'entity',
            url: '/vignette/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'orionSoftApp.vignette.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/vignette/vignette-detail.html',
                    controller: 'VignetteDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('vignette');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Vignette', function($stateParams, Vignette) {
                    return Vignette.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'vignette',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('vignette-detail.edit', {
            parent: 'vignette-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/vignette/vignette-dialog.html',
                    controller: 'VignetteDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Vignette', function(Vignette) {
                            return Vignette.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('vignette.new', {
            parent: 'vignette',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/vignette/vignette-dialog.html',
                    controller: 'VignetteDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                dateEffet: null,
                                dateFin: null,
                                montant: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('vignette', null, { reload: 'vignette' });
                }, function() {
                    $state.go('vignette');
                });
            }]
        })
        .state('vignette.edit', {
            parent: 'vignette',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/vignette/vignette-dialog.html',
                    controller: 'VignetteDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Vignette', function(Vignette) {
                            return Vignette.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('vignette', null, { reload: 'vignette' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('vignette.delete', {
            parent: 'vignette',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/vignette/vignette-delete-dialog.html',
                    controller: 'VignetteDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Vignette', function(Vignette) {
                            return Vignette.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('vignette', null, { reload: 'vignette' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
