(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .controller('VignetteDeleteController',VignetteDeleteController);

    VignetteDeleteController.$inject = ['$uibModalInstance', 'entity', 'Vignette'];

    function VignetteDeleteController($uibModalInstance, entity, Vignette) {
        var vm = this;

        vm.vignette = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Vignette.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
