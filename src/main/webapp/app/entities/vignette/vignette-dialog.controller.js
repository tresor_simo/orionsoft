(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .controller('VignetteDialogController', VignetteDialogController);

    VignetteDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Vignette', 'Vehicule'];

    function VignetteDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Vignette, Vehicule) {
        var vm = this;

        vm.vignette = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.vehicules = Vehicule.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.vignette.id !== null) {
                Vignette.update(vm.vignette, onSaveSuccess, onSaveError);
            } else {
                Vignette.save(vm.vignette, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('orionSoftApp:vignetteUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.dateEffet = false;
        vm.datePickerOpenStatus.dateFin = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
