(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .factory('VignetteSearch', VignetteSearch);

    VignetteSearch.$inject = ['$resource'];

    function VignetteSearch($resource) {
        var resourceUrl =  'api/_search/vignettes/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
