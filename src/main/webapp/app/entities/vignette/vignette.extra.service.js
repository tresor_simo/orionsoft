(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .factory('VignetteExtra', VignetteExtra);

    VignetteExtra.$inject = ['$http','$q'];

    function VignetteExtra($http,$q) {
        var resourceUrl =  'api/vignettes/vehicule/';


        return {
            getVignettesByVehicule:getVignettesByVehicule
        };



        function getVignettesByVehicule(vehicule_id){
            var deferred =$q.defer();
            $http.get(resourceUrl + vehicule_id)
                .success(function(response){
                   console.log("=======================RESPONSE EXTRA====================",response);
                   deferred.resolve(response);
                })

                .error(function(error){

                })

            return deferred.promise;

        }




    }
})();
