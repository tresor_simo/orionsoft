(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .controller('GarantiContratDetailController', GarantiContratDetailController);

    GarantiContratDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'GarantiContrat', 'Contrat', 'Garanti'];

    function GarantiContratDetailController($scope, $rootScope, $stateParams, previousState, entity, GarantiContrat, Contrat, Garanti) {
        var vm = this;

        vm.garantiContrat = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('orionSoftApp:garantiContratUpdate', function(event, result) {
            vm.garantiContrat = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
