(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .controller('GarantiContratDeleteController',GarantiContratDeleteController);

    GarantiContratDeleteController.$inject = ['$uibModalInstance', 'entity', 'GarantiContrat'];

    function GarantiContratDeleteController($uibModalInstance, entity, GarantiContrat) {
        var vm = this;

        vm.garantiContrat = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            GarantiContrat.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
