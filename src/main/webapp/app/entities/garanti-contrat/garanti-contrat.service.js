(function() {
    'use strict';
    angular
        .module('orionSoftApp')
        .factory('GarantiContrat', GarantiContrat);

    GarantiContrat.$inject = ['$resource'];

    function GarantiContrat ($resource) {
        var resourceUrl =  'api/garanti-contrats/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
