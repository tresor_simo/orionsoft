(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .controller('GarantiContratDialogController', GarantiContratDialogController);

    GarantiContratDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'GarantiContrat', 'Contrat', 'Garanti'];

    function GarantiContratDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, GarantiContrat, Contrat, Garanti) {
        var vm = this;

        vm.garantiContrat = entity;
        vm.clear = clear;
        vm.save = save;
        vm.contrats = Contrat.query();
        vm.garantis = Garanti.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.garantiContrat.id !== null) {
                GarantiContrat.update(vm.garantiContrat, onSaveSuccess, onSaveError);
            } else {
                GarantiContrat.save(vm.garantiContrat, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('orionSoftApp:garantiContratUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
