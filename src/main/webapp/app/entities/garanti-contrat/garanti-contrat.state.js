(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('garanti-contrat', {
            parent: 'entity',
            url: '/garanti-contrat',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'orionSoftApp.garantiContrat.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/garanti-contrat/garanti-contrats.html',
                    controller: 'GarantiContratController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('garantiContrat');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('garanti-contrat-detail', {
            parent: 'entity',
            url: '/garanti-contrat/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'orionSoftApp.garantiContrat.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/garanti-contrat/garanti-contrat-detail.html',
                    controller: 'GarantiContratDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('garantiContrat');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'GarantiContrat', function($stateParams, GarantiContrat) {
                    return GarantiContrat.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'garanti-contrat',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('garanti-contrat-detail.edit', {
            parent: 'garanti-contrat-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/garanti-contrat/garanti-contrat-dialog.html',
                    controller: 'GarantiContratDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['GarantiContrat', function(GarantiContrat) {
                            return GarantiContrat.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('garanti-contrat.new', {
            parent: 'garanti-contrat',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/garanti-contrat/garanti-contrat-dialog.html',
                    controller: 'GarantiContratDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                annuel: null,
                                pourcentage: null,
                                nette: null,
                                bonus: null,
                                surplus: null,
                                nettefinal: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('garanti-contrat', null, { reload: 'garanti-contrat' });
                }, function() {
                    $state.go('garanti-contrat');
                });
            }]
        })
        .state('garanti-contrat.edit', {
            parent: 'garanti-contrat',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/garanti-contrat/garanti-contrat-dialog.html',
                    controller: 'GarantiContratDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['GarantiContrat', function(GarantiContrat) {
                            return GarantiContrat.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('garanti-contrat', null, { reload: 'garanti-contrat' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('garanti-contrat.delete', {
            parent: 'garanti-contrat',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/garanti-contrat/garanti-contrat-delete-dialog.html',
                    controller: 'GarantiContratDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['GarantiContrat', function(GarantiContrat) {
                            return GarantiContrat.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('garanti-contrat', null, { reload: 'garanti-contrat' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
