(function() {
    'use strict';

    angular
        .module('orionSoftApp')
        .factory('GarantiContratSearch', GarantiContratSearch);

    GarantiContratSearch.$inject = ['$resource'];

    function GarantiContratSearch($resource) {
        var resourceUrl =  'api/_search/garanti-contrats/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
