'use strict';

describe('Controller Tests', function() {

    describe('UserAssurance Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockUserAssurance, MockUser, MockAgence;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockUserAssurance = jasmine.createSpy('MockUserAssurance');
            MockUser = jasmine.createSpy('MockUser');
            MockAgence = jasmine.createSpy('MockAgence');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'UserAssurance': MockUserAssurance,
                'User': MockUser,
                'Agence': MockAgence
            };
            createController = function() {
                $injector.get('$controller')("UserAssuranceDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'orionSoftApp:userAssuranceUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
