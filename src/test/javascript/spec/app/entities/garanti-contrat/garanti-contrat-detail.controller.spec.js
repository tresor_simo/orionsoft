'use strict';

describe('Controller Tests', function() {

    describe('GarantiContrat Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockGarantiContrat, MockContrat, MockGaranti;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockGarantiContrat = jasmine.createSpy('MockGarantiContrat');
            MockContrat = jasmine.createSpy('MockContrat');
            MockGaranti = jasmine.createSpy('MockGaranti');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'GarantiContrat': MockGarantiContrat,
                'Contrat': MockContrat,
                'Garanti': MockGaranti
            };
            createController = function() {
                $injector.get('$controller')("GarantiContratDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'orionSoftApp:garantiContratUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
