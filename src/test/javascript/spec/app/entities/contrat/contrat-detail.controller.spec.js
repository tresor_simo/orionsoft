'use strict';

describe('Controller Tests', function() {

    describe('Contrat Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockContrat, MockAgent, MockAssureur, MockVehicule, MockZone, MockAssure, MockTypeAssuranceAuto, MockAgence, MockFlotte;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockContrat = jasmine.createSpy('MockContrat');
            MockAgent = jasmine.createSpy('MockAgent');
            MockAssureur = jasmine.createSpy('MockAssureur');
            MockVehicule = jasmine.createSpy('MockVehicule');
            MockZone = jasmine.createSpy('MockZone');
            MockAssure = jasmine.createSpy('MockAssure');
            MockTypeAssuranceAuto = jasmine.createSpy('MockTypeAssuranceAuto');
            MockAgence = jasmine.createSpy('MockAgence');
            MockFlotte = jasmine.createSpy('MockFlotte');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'Contrat': MockContrat,
                'Agent': MockAgent,
                'Assureur': MockAssureur,
                'Vehicule': MockVehicule,
                'Zone': MockZone,
                'Assure': MockAssure,
                'TypeAssuranceAuto': MockTypeAssuranceAuto,
                'Agence': MockAgence,
                'Flotte': MockFlotte
            };
            createController = function() {
                $injector.get('$controller')("ContratDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'orionSoftApp:contratUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
