package dev.soft.assurance.web.rest;

import dev.soft.assurance.OrionSoftApp;
import dev.soft.assurance.domain.TypeAssuranceAuto;
import dev.soft.assurance.repository.TypeAssuranceAutoRepository;
import dev.soft.assurance.service.TypeAssuranceAutoService;
import dev.soft.assurance.repository.search.TypeAssuranceAutoSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the TypeAssuranceAutoResource REST controller.
 *
 * @see TypeAssuranceAutoResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OrionSoftApp.class)
public class TypeAssuranceAutoResourceIntTest {
    private static final String DEFAULT_NOM = "AAAAA";
    private static final String UPDATED_NOM = "BBBBB";
    private static final String DEFAULT_NOM_PRO = "AAAAA";
    private static final String UPDATED_NOM_PRO = "BBBBB";

    @Inject
    private TypeAssuranceAutoRepository typeAssuranceAutoRepository;

    @Inject
    private TypeAssuranceAutoService typeAssuranceAutoService;

    @Inject
    private TypeAssuranceAutoSearchRepository typeAssuranceAutoSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restTypeAssuranceAutoMockMvc;

    private TypeAssuranceAuto typeAssuranceAuto;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        TypeAssuranceAutoResource typeAssuranceAutoResource = new TypeAssuranceAutoResource();
        ReflectionTestUtils.setField(typeAssuranceAutoResource, "typeAssuranceAutoService", typeAssuranceAutoService);
        this.restTypeAssuranceAutoMockMvc = MockMvcBuilders.standaloneSetup(typeAssuranceAutoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TypeAssuranceAuto createEntity(EntityManager em) {
        TypeAssuranceAuto typeAssuranceAuto = new TypeAssuranceAuto();
        typeAssuranceAuto = new TypeAssuranceAuto()
                .nom(DEFAULT_NOM)
                .nomPro(DEFAULT_NOM_PRO);
        return typeAssuranceAuto;
    }

    @Before
    public void initTest() {
        typeAssuranceAutoSearchRepository.deleteAll();
        typeAssuranceAuto = createEntity(em);
    }

    @Test
    @Transactional
    public void createTypeAssuranceAuto() throws Exception {
        int databaseSizeBeforeCreate = typeAssuranceAutoRepository.findAll().size();

        // Create the TypeAssuranceAuto

        restTypeAssuranceAutoMockMvc.perform(post("/api/type-assurance-autos")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(typeAssuranceAuto)))
                .andExpect(status().isCreated());

        // Validate the TypeAssuranceAuto in the database
        List<TypeAssuranceAuto> typeAssuranceAutos = typeAssuranceAutoRepository.findAll();
        assertThat(typeAssuranceAutos).hasSize(databaseSizeBeforeCreate + 1);
        TypeAssuranceAuto testTypeAssuranceAuto = typeAssuranceAutos.get(typeAssuranceAutos.size() - 1);
        assertThat(testTypeAssuranceAuto.getNom()).isEqualTo(DEFAULT_NOM);
        assertThat(testTypeAssuranceAuto.getNomPro()).isEqualTo(DEFAULT_NOM_PRO);

        // Validate the TypeAssuranceAuto in ElasticSearch
        TypeAssuranceAuto typeAssuranceAutoEs = typeAssuranceAutoSearchRepository.findOne(testTypeAssuranceAuto.getId());
        assertThat(typeAssuranceAutoEs).isEqualToComparingFieldByField(testTypeAssuranceAuto);
    }

    @Test
    @Transactional
    public void getAllTypeAssuranceAutos() throws Exception {
        // Initialize the database
        typeAssuranceAutoRepository.saveAndFlush(typeAssuranceAuto);

        // Get all the typeAssuranceAutos
        restTypeAssuranceAutoMockMvc.perform(get("/api/type-assurance-autos?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(typeAssuranceAuto.getId().intValue())))
                .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM.toString())))
                .andExpect(jsonPath("$.[*].nomPro").value(hasItem(DEFAULT_NOM_PRO.toString())));
    }

    @Test
    @Transactional
    public void getTypeAssuranceAuto() throws Exception {
        // Initialize the database
        typeAssuranceAutoRepository.saveAndFlush(typeAssuranceAuto);

        // Get the typeAssuranceAuto
        restTypeAssuranceAutoMockMvc.perform(get("/api/type-assurance-autos/{id}", typeAssuranceAuto.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(typeAssuranceAuto.getId().intValue()))
            .andExpect(jsonPath("$.nom").value(DEFAULT_NOM.toString()))
            .andExpect(jsonPath("$.nomPro").value(DEFAULT_NOM_PRO.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingTypeAssuranceAuto() throws Exception {
        // Get the typeAssuranceAuto
        restTypeAssuranceAutoMockMvc.perform(get("/api/type-assurance-autos/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTypeAssuranceAuto() throws Exception {
        // Initialize the database
        typeAssuranceAutoService.save(typeAssuranceAuto);

        int databaseSizeBeforeUpdate = typeAssuranceAutoRepository.findAll().size();

        // Update the typeAssuranceAuto
        TypeAssuranceAuto updatedTypeAssuranceAuto = typeAssuranceAutoRepository.findOne(typeAssuranceAuto.getId());
        updatedTypeAssuranceAuto
                .nom(UPDATED_NOM)
                .nomPro(UPDATED_NOM_PRO);

        restTypeAssuranceAutoMockMvc.perform(put("/api/type-assurance-autos")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedTypeAssuranceAuto)))
                .andExpect(status().isOk());

        // Validate the TypeAssuranceAuto in the database
        List<TypeAssuranceAuto> typeAssuranceAutos = typeAssuranceAutoRepository.findAll();
        assertThat(typeAssuranceAutos).hasSize(databaseSizeBeforeUpdate);
        TypeAssuranceAuto testTypeAssuranceAuto = typeAssuranceAutos.get(typeAssuranceAutos.size() - 1);
        assertThat(testTypeAssuranceAuto.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testTypeAssuranceAuto.getNomPro()).isEqualTo(UPDATED_NOM_PRO);

        // Validate the TypeAssuranceAuto in ElasticSearch
        TypeAssuranceAuto typeAssuranceAutoEs = typeAssuranceAutoSearchRepository.findOne(testTypeAssuranceAuto.getId());
        assertThat(typeAssuranceAutoEs).isEqualToComparingFieldByField(testTypeAssuranceAuto);
    }

    @Test
    @Transactional
    public void deleteTypeAssuranceAuto() throws Exception {
        // Initialize the database
        typeAssuranceAutoService.save(typeAssuranceAuto);

        int databaseSizeBeforeDelete = typeAssuranceAutoRepository.findAll().size();

        // Get the typeAssuranceAuto
        restTypeAssuranceAutoMockMvc.perform(delete("/api/type-assurance-autos/{id}", typeAssuranceAuto.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean typeAssuranceAutoExistsInEs = typeAssuranceAutoSearchRepository.exists(typeAssuranceAuto.getId());
        assertThat(typeAssuranceAutoExistsInEs).isFalse();

        // Validate the database is empty
        List<TypeAssuranceAuto> typeAssuranceAutos = typeAssuranceAutoRepository.findAll();
        assertThat(typeAssuranceAutos).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchTypeAssuranceAuto() throws Exception {
        // Initialize the database
        typeAssuranceAutoService.save(typeAssuranceAuto);

        // Search the typeAssuranceAuto
        restTypeAssuranceAutoMockMvc.perform(get("/api/_search/type-assurance-autos?query=id:" + typeAssuranceAuto.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(typeAssuranceAuto.getId().intValue())))
            .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM.toString())))
            .andExpect(jsonPath("$.[*].nomPro").value(hasItem(DEFAULT_NOM_PRO.toString())));
    }
}
