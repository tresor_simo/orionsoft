package dev.soft.assurance.web.rest;

import dev.soft.assurance.OrionSoftApp;
import dev.soft.assurance.domain.Zone;
import dev.soft.assurance.repository.ZoneRepository;
import dev.soft.assurance.service.ZoneService;
import dev.soft.assurance.repository.search.ZoneSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ZoneResource REST controller.
 *
 * @see ZoneResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OrionSoftApp.class)
public class ZoneResourceIntTest {
    private static final String DEFAULT_NOM = "AAAAA";
    private static final String UPDATED_NOM = "BBBBB";

    @Inject
    private ZoneRepository zoneRepository;

    @Inject
    private ZoneService zoneService;

    @Inject
    private ZoneSearchRepository zoneSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restZoneMockMvc;

    private Zone zone;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ZoneResource zoneResource = new ZoneResource();
        ReflectionTestUtils.setField(zoneResource, "zoneService", zoneService);
        this.restZoneMockMvc = MockMvcBuilders.standaloneSetup(zoneResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Zone createEntity(EntityManager em) {
        Zone zone = new Zone();
        zone = new Zone()
                .nom(DEFAULT_NOM);
        return zone;
    }

    @Before
    public void initTest() {
        zoneSearchRepository.deleteAll();
        zone = createEntity(em);
    }

    @Test
    @Transactional
    public void createZone() throws Exception {
        int databaseSizeBeforeCreate = zoneRepository.findAll().size();

        // Create the Zone

        restZoneMockMvc.perform(post("/api/zones")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(zone)))
                .andExpect(status().isCreated());

        // Validate the Zone in the database
        List<Zone> zones = zoneRepository.findAll();
        assertThat(zones).hasSize(databaseSizeBeforeCreate + 1);
        Zone testZone = zones.get(zones.size() - 1);
        assertThat(testZone.getNom()).isEqualTo(DEFAULT_NOM);

        // Validate the Zone in ElasticSearch
        Zone zoneEs = zoneSearchRepository.findOne(testZone.getId());
        assertThat(zoneEs).isEqualToComparingFieldByField(testZone);
    }

    @Test
    @Transactional
    public void getAllZones() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        // Get all the zones
        restZoneMockMvc.perform(get("/api/zones?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(zone.getId().intValue())))
                .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM.toString())));
    }

    @Test
    @Transactional
    public void getZone() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        // Get the zone
        restZoneMockMvc.perform(get("/api/zones/{id}", zone.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(zone.getId().intValue()))
            .andExpect(jsonPath("$.nom").value(DEFAULT_NOM.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingZone() throws Exception {
        // Get the zone
        restZoneMockMvc.perform(get("/api/zones/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateZone() throws Exception {
        // Initialize the database
        zoneService.save(zone);

        int databaseSizeBeforeUpdate = zoneRepository.findAll().size();

        // Update the zone
        Zone updatedZone = zoneRepository.findOne(zone.getId());
        updatedZone
                .nom(UPDATED_NOM);

        restZoneMockMvc.perform(put("/api/zones")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedZone)))
                .andExpect(status().isOk());

        // Validate the Zone in the database
        List<Zone> zones = zoneRepository.findAll();
        assertThat(zones).hasSize(databaseSizeBeforeUpdate);
        Zone testZone = zones.get(zones.size() - 1);
        assertThat(testZone.getNom()).isEqualTo(UPDATED_NOM);

        // Validate the Zone in ElasticSearch
        Zone zoneEs = zoneSearchRepository.findOne(testZone.getId());
        assertThat(zoneEs).isEqualToComparingFieldByField(testZone);
    }

    @Test
    @Transactional
    public void deleteZone() throws Exception {
        // Initialize the database
        zoneService.save(zone);

        int databaseSizeBeforeDelete = zoneRepository.findAll().size();

        // Get the zone
        restZoneMockMvc.perform(delete("/api/zones/{id}", zone.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean zoneExistsInEs = zoneSearchRepository.exists(zone.getId());
        assertThat(zoneExistsInEs).isFalse();

        // Validate the database is empty
        List<Zone> zones = zoneRepository.findAll();
        assertThat(zones).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchZone() throws Exception {
        // Initialize the database
        zoneService.save(zone);

        // Search the zone
        restZoneMockMvc.perform(get("/api/_search/zones?query=id:" + zone.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(zone.getId().intValue())))
            .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM.toString())));
    }
}
