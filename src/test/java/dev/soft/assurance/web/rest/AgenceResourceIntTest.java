package dev.soft.assurance.web.rest;

import dev.soft.assurance.OrionSoftApp;
import dev.soft.assurance.domain.Agence;
import dev.soft.assurance.repository.AgenceRepository;
import dev.soft.assurance.service.AgenceService;
import dev.soft.assurance.repository.search.AgenceSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the AgenceResource REST controller.
 *
 * @see AgenceResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OrionSoftApp.class)
public class AgenceResourceIntTest {
    private static final String DEFAULT_NOM = "AAAAA";
    private static final String UPDATED_NOM = "BBBBB";
    private static final String DEFAULT_CONTACT = "AAAAA";
    private static final String UPDATED_CONTACT = "BBBBB";
    private static final String DEFAULT_LOCALITE = "AAAAA";
    private static final String UPDATED_LOCALITE = "BBBBB";
    private static final String DEFAULT_VILLE = "AAAAA";
    private static final String UPDATED_VILLE = "BBBBB";

    @Inject
    private AgenceRepository agenceRepository;

    @Inject
    private AgenceService agenceService;

    @Inject
    private AgenceSearchRepository agenceSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restAgenceMockMvc;

    private Agence agence;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        AgenceResource agenceResource = new AgenceResource();
        ReflectionTestUtils.setField(agenceResource, "agenceService", agenceService);
        this.restAgenceMockMvc = MockMvcBuilders.standaloneSetup(agenceResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Agence createEntity(EntityManager em) {
        Agence agence = new Agence();
        agence = new Agence()
                .nom(DEFAULT_NOM)
                .contact(DEFAULT_CONTACT)
                .localite(DEFAULT_LOCALITE)
                .ville(DEFAULT_VILLE);
        return agence;
    }

    @Before
    public void initTest() {
        agenceSearchRepository.deleteAll();
        agence = createEntity(em);
    }

    @Test
    @Transactional
    public void createAgence() throws Exception {
        int databaseSizeBeforeCreate = agenceRepository.findAll().size();

        // Create the Agence

        restAgenceMockMvc.perform(post("/api/agences")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(agence)))
                .andExpect(status().isCreated());

        // Validate the Agence in the database
        List<Agence> agences = agenceRepository.findAll();
        assertThat(agences).hasSize(databaseSizeBeforeCreate + 1);
        Agence testAgence = agences.get(agences.size() - 1);
        assertThat(testAgence.getNom()).isEqualTo(DEFAULT_NOM);
        assertThat(testAgence.getContact()).isEqualTo(DEFAULT_CONTACT);
        assertThat(testAgence.getLocalite()).isEqualTo(DEFAULT_LOCALITE);
        assertThat(testAgence.getVille()).isEqualTo(DEFAULT_VILLE);

        // Validate the Agence in ElasticSearch
        Agence agenceEs = agenceSearchRepository.findOne(testAgence.getId());
        assertThat(agenceEs).isEqualToComparingFieldByField(testAgence);
    }

    @Test
    @Transactional
    public void getAllAgences() throws Exception {
        // Initialize the database
        agenceRepository.saveAndFlush(agence);

        // Get all the agences
        restAgenceMockMvc.perform(get("/api/agences?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(agence.getId().intValue())))
                .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM.toString())))
                .andExpect(jsonPath("$.[*].contact").value(hasItem(DEFAULT_CONTACT.toString())))
                .andExpect(jsonPath("$.[*].localite").value(hasItem(DEFAULT_LOCALITE.toString())))
                .andExpect(jsonPath("$.[*].ville").value(hasItem(DEFAULT_VILLE.toString())));
    }

    @Test
    @Transactional
    public void getAgence() throws Exception {
        // Initialize the database
        agenceRepository.saveAndFlush(agence);

        // Get the agence
        restAgenceMockMvc.perform(get("/api/agences/{id}", agence.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(agence.getId().intValue()))
            .andExpect(jsonPath("$.nom").value(DEFAULT_NOM.toString()))
            .andExpect(jsonPath("$.contact").value(DEFAULT_CONTACT.toString()))
            .andExpect(jsonPath("$.localite").value(DEFAULT_LOCALITE.toString()))
            .andExpect(jsonPath("$.ville").value(DEFAULT_VILLE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingAgence() throws Exception {
        // Get the agence
        restAgenceMockMvc.perform(get("/api/agences/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAgence() throws Exception {
        // Initialize the database
        agenceService.save(agence);

        int databaseSizeBeforeUpdate = agenceRepository.findAll().size();

        // Update the agence
        Agence updatedAgence = agenceRepository.findOne(agence.getId());
        updatedAgence
                .nom(UPDATED_NOM)
                .contact(UPDATED_CONTACT)
                .localite(UPDATED_LOCALITE)
                .ville(UPDATED_VILLE);

        restAgenceMockMvc.perform(put("/api/agences")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedAgence)))
                .andExpect(status().isOk());

        // Validate the Agence in the database
        List<Agence> agences = agenceRepository.findAll();
        assertThat(agences).hasSize(databaseSizeBeforeUpdate);
        Agence testAgence = agences.get(agences.size() - 1);
        assertThat(testAgence.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testAgence.getContact()).isEqualTo(UPDATED_CONTACT);
        assertThat(testAgence.getLocalite()).isEqualTo(UPDATED_LOCALITE);
        assertThat(testAgence.getVille()).isEqualTo(UPDATED_VILLE);

        // Validate the Agence in ElasticSearch
        Agence agenceEs = agenceSearchRepository.findOne(testAgence.getId());
        assertThat(agenceEs).isEqualToComparingFieldByField(testAgence);
    }

    @Test
    @Transactional
    public void deleteAgence() throws Exception {
        // Initialize the database
        agenceService.save(agence);

        int databaseSizeBeforeDelete = agenceRepository.findAll().size();

        // Get the agence
        restAgenceMockMvc.perform(delete("/api/agences/{id}", agence.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean agenceExistsInEs = agenceSearchRepository.exists(agence.getId());
        assertThat(agenceExistsInEs).isFalse();

        // Validate the database is empty
        List<Agence> agences = agenceRepository.findAll();
        assertThat(agences).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchAgence() throws Exception {
        // Initialize the database
        agenceService.save(agence);

        // Search the agence
        restAgenceMockMvc.perform(get("/api/_search/agences?query=id:" + agence.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(agence.getId().intValue())))
            .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM.toString())))
            .andExpect(jsonPath("$.[*].contact").value(hasItem(DEFAULT_CONTACT.toString())))
            .andExpect(jsonPath("$.[*].localite").value(hasItem(DEFAULT_LOCALITE.toString())))
            .andExpect(jsonPath("$.[*].ville").value(hasItem(DEFAULT_VILLE.toString())));
    }
}
