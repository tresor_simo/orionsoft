package dev.soft.assurance.web.rest;

import dev.soft.assurance.OrionSoftApp;
import dev.soft.assurance.domain.Vignette;
import dev.soft.assurance.repository.VignetteRepository;
import dev.soft.assurance.service.VignetteService;
import dev.soft.assurance.repository.search.VignetteSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the VignetteResource REST controller.
 *
 * @see VignetteResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OrionSoftApp.class)
public class VignetteResourceIntTest {

    private static final LocalDate DEFAULT_DATE_EFFET = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_EFFET = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_DATE_FIN = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_FIN = LocalDate.now(ZoneId.systemDefault());

    private static final BigDecimal DEFAULT_MONTANT = new BigDecimal(1);
    private static final BigDecimal UPDATED_MONTANT = new BigDecimal(2);

    private static final Boolean DEFAULT_ETAT = false;
    private static final Boolean UPDATED_ETAT = true;

    @Inject
    private VignetteRepository vignetteRepository;

    @Inject
    private VignetteService vignetteService;

    @Inject
    private VignetteSearchRepository vignetteSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restVignetteMockMvc;

    private Vignette vignette;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        VignetteResource vignetteResource = new VignetteResource();
        ReflectionTestUtils.setField(vignetteResource, "vignetteService", vignetteService);
        this.restVignetteMockMvc = MockMvcBuilders.standaloneSetup(vignetteResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Vignette createEntity(EntityManager em) {
        Vignette vignette = new Vignette();
        vignette = new Vignette()
                .dateEffet(DEFAULT_DATE_EFFET)
                .dateFin(DEFAULT_DATE_FIN)
                .montant(DEFAULT_MONTANT)
                .etat(DEFAULT_ETAT);
        return vignette;
    }

    @Before
    public void initTest() {
        vignetteSearchRepository.deleteAll();
        vignette = createEntity(em);
    }

    @Test
    @Transactional
    public void createVignette() throws Exception {
        int databaseSizeBeforeCreate = vignetteRepository.findAll().size();

        // Create the Vignette

        restVignetteMockMvc.perform(post("/api/vignettes")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(vignette)))
                .andExpect(status().isCreated());

        // Validate the Vignette in the database
        List<Vignette> vignettes = vignetteRepository.findAll();
        assertThat(vignettes).hasSize(databaseSizeBeforeCreate + 1);
        Vignette testVignette = vignettes.get(vignettes.size() - 1);
        assertThat(testVignette.getDateEffet()).isEqualTo(DEFAULT_DATE_EFFET);
        assertThat(testVignette.getDateFin()).isEqualTo(DEFAULT_DATE_FIN);
        assertThat(testVignette.getMontant()).isEqualTo(DEFAULT_MONTANT);
        assertThat(testVignette.isEtat()).isEqualTo(DEFAULT_ETAT);

        // Validate the Vignette in ElasticSearch
        Vignette vignetteEs = vignetteSearchRepository.findOne(testVignette.getId());
        assertThat(vignetteEs).isEqualToComparingFieldByField(testVignette);
    }

    @Test
    @Transactional
    public void getAllVignettes() throws Exception {
        // Initialize the database
        vignetteRepository.saveAndFlush(vignette);

        // Get all the vignettes
        restVignetteMockMvc.perform(get("/api/vignettes?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(vignette.getId().intValue())))
                .andExpect(jsonPath("$.[*].dateEffet").value(hasItem(DEFAULT_DATE_EFFET.toString())))
                .andExpect(jsonPath("$.[*].dateFin").value(hasItem(DEFAULT_DATE_FIN.toString())))
                .andExpect(jsonPath("$.[*].montant").value(hasItem(DEFAULT_MONTANT.intValue())))
                .andExpect(jsonPath("$.[*].etat").value(hasItem(DEFAULT_ETAT.booleanValue())));
    }

    @Test
    @Transactional
    public void getVignette() throws Exception {
        // Initialize the database
        vignetteRepository.saveAndFlush(vignette);

        // Get the vignette
        restVignetteMockMvc.perform(get("/api/vignettes/{id}", vignette.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(vignette.getId().intValue()))
            .andExpect(jsonPath("$.dateEffet").value(DEFAULT_DATE_EFFET.toString()))
            .andExpect(jsonPath("$.dateFin").value(DEFAULT_DATE_FIN.toString()))
            .andExpect(jsonPath("$.montant").value(DEFAULT_MONTANT.intValue()))
            .andExpect(jsonPath("$.etat").value(DEFAULT_ETAT.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingVignette() throws Exception {
        // Get the vignette
        restVignetteMockMvc.perform(get("/api/vignettes/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateVignette() throws Exception {
        // Initialize the database
        vignetteService.save(vignette);

        int databaseSizeBeforeUpdate = vignetteRepository.findAll().size();

        // Update the vignette
        Vignette updatedVignette = vignetteRepository.findOne(vignette.getId());
        updatedVignette
                .dateEffet(UPDATED_DATE_EFFET)
                .dateFin(UPDATED_DATE_FIN)
                .montant(UPDATED_MONTANT)
                .etat(UPDATED_ETAT);

        restVignetteMockMvc.perform(put("/api/vignettes")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedVignette)))
                .andExpect(status().isOk());

        // Validate the Vignette in the database
        List<Vignette> vignettes = vignetteRepository.findAll();
        assertThat(vignettes).hasSize(databaseSizeBeforeUpdate);
        Vignette testVignette = vignettes.get(vignettes.size() - 1);
        assertThat(testVignette.getDateEffet()).isEqualTo(UPDATED_DATE_EFFET);
        assertThat(testVignette.getDateFin()).isEqualTo(UPDATED_DATE_FIN);
        assertThat(testVignette.getMontant()).isEqualTo(UPDATED_MONTANT);
        assertThat(testVignette.isEtat()).isEqualTo(UPDATED_ETAT);

        // Validate the Vignette in ElasticSearch
        Vignette vignetteEs = vignetteSearchRepository.findOne(testVignette.getId());
        assertThat(vignetteEs).isEqualToComparingFieldByField(testVignette);
    }

    @Test
    @Transactional
    public void deleteVignette() throws Exception {
        // Initialize the database
        vignetteService.save(vignette);

        int databaseSizeBeforeDelete = vignetteRepository.findAll().size();

        // Get the vignette
        restVignetteMockMvc.perform(delete("/api/vignettes/{id}", vignette.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean vignetteExistsInEs = vignetteSearchRepository.exists(vignette.getId());
        assertThat(vignetteExistsInEs).isFalse();

        // Validate the database is empty
        List<Vignette> vignettes = vignetteRepository.findAll();
        assertThat(vignettes).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchVignette() throws Exception {
        // Initialize the database
        vignetteService.save(vignette);

        // Search the vignette
        restVignetteMockMvc.perform(get("/api/_search/vignettes?query=id:" + vignette.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(vignette.getId().intValue())))
            .andExpect(jsonPath("$.[*].dateEffet").value(hasItem(DEFAULT_DATE_EFFET.toString())))
            .andExpect(jsonPath("$.[*].dateFin").value(hasItem(DEFAULT_DATE_FIN.toString())))
            .andExpect(jsonPath("$.[*].montant").value(hasItem(DEFAULT_MONTANT.intValue())))
            .andExpect(jsonPath("$.[*].etat").value(hasItem(DEFAULT_ETAT.booleanValue())));
    }
}
