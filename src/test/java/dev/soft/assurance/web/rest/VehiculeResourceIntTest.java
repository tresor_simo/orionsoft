package dev.soft.assurance.web.rest;

import dev.soft.assurance.OrionSoftApp;
import dev.soft.assurance.domain.Vehicule;
import dev.soft.assurance.repository.VehiculeRepository;
import dev.soft.assurance.service.VehiculeService;
import dev.soft.assurance.repository.search.VehiculeSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import dev.soft.assurance.domain.enumeration.SOURCE_ENERGIE;
/**
 * Test class for the VehiculeResource REST controller.
 *
 * @see VehiculeResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OrionSoftApp.class)
public class VehiculeResourceIntTest {
    private static final String DEFAULT_IMMATRICULATION = "AAAAA";
    private static final String UPDATED_IMMATRICULATION = "BBBBB";
    private static final String DEFAULT_MARQUE = "AAAAA";
    private static final String UPDATED_MARQUE = "BBBBB";

    private static final BigDecimal DEFAULT_PUISSANCE = new BigDecimal(1);
    private static final BigDecimal UPDATED_PUISSANCE = new BigDecimal(2);

    private static final SOURCE_ENERGIE DEFAULT_SOURCE = SOURCE_ENERGIE.DIESEL;
    private static final SOURCE_ENERGIE UPDATED_SOURCE = SOURCE_ENERGIE.ESSENCE;
    private static final String DEFAULT_NUMERO_CHASSI = "AAAAA";
    private static final String UPDATED_NUMERO_CHASSI = "BBBBB";

    private static final LocalDate DEFAULT_DATE_PREM_MISE_CIRCULATION = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_PREM_MISE_CIRCULATION = LocalDate.now(ZoneId.systemDefault());

    private static final BigDecimal DEFAULT_NBRE_PLACE = new BigDecimal(1);
    private static final BigDecimal UPDATED_NBRE_PLACE = new BigDecimal(2);

    private static final BigDecimal DEFAULT_POIDS = new BigDecimal(1);
    private static final BigDecimal UPDATED_POIDS = new BigDecimal(2);

    private static final BigDecimal DEFAULT_VALEUR_ACCESSOIRE = new BigDecimal(1);
    private static final BigDecimal UPDATED_VALEUR_ACCESSOIRE = new BigDecimal(2);

    private static final BigDecimal DEFAULT_VALEUR_VENAL = new BigDecimal(1);
    private static final BigDecimal UPDATED_VALEUR_VENAL = new BigDecimal(2);

    private static final LocalDate DEFAULT_DATE_DERNIERE_VT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_DERNIERE_VT = LocalDate.now(ZoneId.systemDefault());

    private static final BigDecimal DEFAULT_VALEUR_NEUVE = new BigDecimal(1);
    private static final BigDecimal UPDATED_VALEUR_NEUVE = new BigDecimal(2);
    private static final String DEFAULT_CATEGORIE = "AAAAA";
    private static final String UPDATED_CATEGORIE = "BBBBB";

    @Inject
    private VehiculeRepository vehiculeRepository;

    @Inject
    private VehiculeService vehiculeService;

    @Inject
    private VehiculeSearchRepository vehiculeSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restVehiculeMockMvc;

    private Vehicule vehicule;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        VehiculeResource vehiculeResource = new VehiculeResource();
        ReflectionTestUtils.setField(vehiculeResource, "vehiculeService", vehiculeService);
        this.restVehiculeMockMvc = MockMvcBuilders.standaloneSetup(vehiculeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Vehicule createEntity(EntityManager em) {
        Vehicule vehicule = new Vehicule();
        vehicule = new Vehicule()
                .immatriculation(DEFAULT_IMMATRICULATION)
                .marque(DEFAULT_MARQUE)
                .puissance(DEFAULT_PUISSANCE)
                .source(DEFAULT_SOURCE)
                .numeroChassi(DEFAULT_NUMERO_CHASSI)
                .datePremMiseCirculation(DEFAULT_DATE_PREM_MISE_CIRCULATION)
                .nbrePlace(DEFAULT_NBRE_PLACE)
                .poids(DEFAULT_POIDS)
                .valeurAccessoire(DEFAULT_VALEUR_ACCESSOIRE)
                .valeurVenal(DEFAULT_VALEUR_VENAL)
                .dateDerniereVT(DEFAULT_DATE_DERNIERE_VT)
                .valeurNeuve(DEFAULT_VALEUR_NEUVE)
                .categorie(DEFAULT_CATEGORIE);
        return vehicule;
    }

    @Before
    public void initTest() {
        vehiculeSearchRepository.deleteAll();
        vehicule = createEntity(em);
    }

    @Test
    @Transactional
    public void createVehicule() throws Exception {
        int databaseSizeBeforeCreate = vehiculeRepository.findAll().size();

        // Create the Vehicule

        restVehiculeMockMvc.perform(post("/api/vehicules")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(vehicule)))
                .andExpect(status().isCreated());

        // Validate the Vehicule in the database
        List<Vehicule> vehicules = vehiculeRepository.findAll();
        assertThat(vehicules).hasSize(databaseSizeBeforeCreate + 1);
        Vehicule testVehicule = vehicules.get(vehicules.size() - 1);
        assertThat(testVehicule.getImmatriculation()).isEqualTo(DEFAULT_IMMATRICULATION);
        assertThat(testVehicule.getMarque()).isEqualTo(DEFAULT_MARQUE);
        assertThat(testVehicule.getPuissance()).isEqualTo(DEFAULT_PUISSANCE);
        assertThat(testVehicule.getSource()).isEqualTo(DEFAULT_SOURCE);
        assertThat(testVehicule.getNumeroChassi()).isEqualTo(DEFAULT_NUMERO_CHASSI);
        assertThat(testVehicule.getDatePremMiseCirculation()).isEqualTo(DEFAULT_DATE_PREM_MISE_CIRCULATION);
        assertThat(testVehicule.getNbrePlace()).isEqualTo(DEFAULT_NBRE_PLACE);
        assertThat(testVehicule.getPoids()).isEqualTo(DEFAULT_POIDS);
        assertThat(testVehicule.getValeurAccessoire()).isEqualTo(DEFAULT_VALEUR_ACCESSOIRE);
        assertThat(testVehicule.getValeurVenal()).isEqualTo(DEFAULT_VALEUR_VENAL);
        assertThat(testVehicule.getDateDerniereVT()).isEqualTo(DEFAULT_DATE_DERNIERE_VT);
        assertThat(testVehicule.getValeurNeuve()).isEqualTo(DEFAULT_VALEUR_NEUVE);
        assertThat(testVehicule.getCategorie()).isEqualTo(DEFAULT_CATEGORIE);

        // Validate the Vehicule in ElasticSearch
        Vehicule vehiculeEs = vehiculeSearchRepository.findOne(testVehicule.getId());
        assertThat(vehiculeEs).isEqualToComparingFieldByField(testVehicule);
    }

    @Test
    @Transactional
    public void getAllVehicules() throws Exception {
        // Initialize the database
        vehiculeRepository.saveAndFlush(vehicule);

        // Get all the vehicules
        restVehiculeMockMvc.perform(get("/api/vehicules?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(vehicule.getId().intValue())))
                .andExpect(jsonPath("$.[*].immatriculation").value(hasItem(DEFAULT_IMMATRICULATION.toString())))
                .andExpect(jsonPath("$.[*].marque").value(hasItem(DEFAULT_MARQUE.toString())))
                .andExpect(jsonPath("$.[*].puissance").value(hasItem(DEFAULT_PUISSANCE.intValue())))
                .andExpect(jsonPath("$.[*].source").value(hasItem(DEFAULT_SOURCE.toString())))
                .andExpect(jsonPath("$.[*].numeroChassi").value(hasItem(DEFAULT_NUMERO_CHASSI.toString())))
                .andExpect(jsonPath("$.[*].datePremMiseCirculation").value(hasItem(DEFAULT_DATE_PREM_MISE_CIRCULATION.toString())))
                .andExpect(jsonPath("$.[*].nbrePlace").value(hasItem(DEFAULT_NBRE_PLACE.intValue())))
                .andExpect(jsonPath("$.[*].poids").value(hasItem(DEFAULT_POIDS.intValue())))
                .andExpect(jsonPath("$.[*].valeurAccessoire").value(hasItem(DEFAULT_VALEUR_ACCESSOIRE.intValue())))
                .andExpect(jsonPath("$.[*].valeurVenal").value(hasItem(DEFAULT_VALEUR_VENAL.intValue())))
                .andExpect(jsonPath("$.[*].dateDerniereVT").value(hasItem(DEFAULT_DATE_DERNIERE_VT.toString())))
                .andExpect(jsonPath("$.[*].valeurNeuve").value(hasItem(DEFAULT_VALEUR_NEUVE.intValue())))
                .andExpect(jsonPath("$.[*].categorie").value(hasItem(DEFAULT_CATEGORIE.toString())));
    }

    @Test
    @Transactional
    public void getVehicule() throws Exception {
        // Initialize the database
        vehiculeRepository.saveAndFlush(vehicule);

        // Get the vehicule
        restVehiculeMockMvc.perform(get("/api/vehicules/{id}", vehicule.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(vehicule.getId().intValue()))
            .andExpect(jsonPath("$.immatriculation").value(DEFAULT_IMMATRICULATION.toString()))
            .andExpect(jsonPath("$.marque").value(DEFAULT_MARQUE.toString()))
            .andExpect(jsonPath("$.puissance").value(DEFAULT_PUISSANCE.intValue()))
            .andExpect(jsonPath("$.source").value(DEFAULT_SOURCE.toString()))
            .andExpect(jsonPath("$.numeroChassi").value(DEFAULT_NUMERO_CHASSI.toString()))
            .andExpect(jsonPath("$.datePremMiseCirculation").value(DEFAULT_DATE_PREM_MISE_CIRCULATION.toString()))
            .andExpect(jsonPath("$.nbrePlace").value(DEFAULT_NBRE_PLACE.intValue()))
            .andExpect(jsonPath("$.poids").value(DEFAULT_POIDS.intValue()))
            .andExpect(jsonPath("$.valeurAccessoire").value(DEFAULT_VALEUR_ACCESSOIRE.intValue()))
            .andExpect(jsonPath("$.valeurVenal").value(DEFAULT_VALEUR_VENAL.intValue()))
            .andExpect(jsonPath("$.dateDerniereVT").value(DEFAULT_DATE_DERNIERE_VT.toString()))
            .andExpect(jsonPath("$.valeurNeuve").value(DEFAULT_VALEUR_NEUVE.intValue()))
            .andExpect(jsonPath("$.categorie").value(DEFAULT_CATEGORIE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingVehicule() throws Exception {
        // Get the vehicule
        restVehiculeMockMvc.perform(get("/api/vehicules/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateVehicule() throws Exception {
        // Initialize the database
        vehiculeService.save(vehicule);

        int databaseSizeBeforeUpdate = vehiculeRepository.findAll().size();

        // Update the vehicule
        Vehicule updatedVehicule = vehiculeRepository.findOne(vehicule.getId());
        updatedVehicule
                .immatriculation(UPDATED_IMMATRICULATION)
                .marque(UPDATED_MARQUE)
                .puissance(UPDATED_PUISSANCE)
                .source(UPDATED_SOURCE)
                .numeroChassi(UPDATED_NUMERO_CHASSI)
                .datePremMiseCirculation(UPDATED_DATE_PREM_MISE_CIRCULATION)
                .nbrePlace(UPDATED_NBRE_PLACE)
                .poids(UPDATED_POIDS)
                .valeurAccessoire(UPDATED_VALEUR_ACCESSOIRE)
                .valeurVenal(UPDATED_VALEUR_VENAL)
                .dateDerniereVT(UPDATED_DATE_DERNIERE_VT)
                .valeurNeuve(UPDATED_VALEUR_NEUVE)
                .categorie(UPDATED_CATEGORIE);

        restVehiculeMockMvc.perform(put("/api/vehicules")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedVehicule)))
                .andExpect(status().isOk());

        // Validate the Vehicule in the database
        List<Vehicule> vehicules = vehiculeRepository.findAll();
        assertThat(vehicules).hasSize(databaseSizeBeforeUpdate);
        Vehicule testVehicule = vehicules.get(vehicules.size() - 1);
        assertThat(testVehicule.getImmatriculation()).isEqualTo(UPDATED_IMMATRICULATION);
        assertThat(testVehicule.getMarque()).isEqualTo(UPDATED_MARQUE);
        assertThat(testVehicule.getPuissance()).isEqualTo(UPDATED_PUISSANCE);
        assertThat(testVehicule.getSource()).isEqualTo(UPDATED_SOURCE);
        assertThat(testVehicule.getNumeroChassi()).isEqualTo(UPDATED_NUMERO_CHASSI);
        assertThat(testVehicule.getDatePremMiseCirculation()).isEqualTo(UPDATED_DATE_PREM_MISE_CIRCULATION);
        assertThat(testVehicule.getNbrePlace()).isEqualTo(UPDATED_NBRE_PLACE);
        assertThat(testVehicule.getPoids()).isEqualTo(UPDATED_POIDS);
        assertThat(testVehicule.getValeurAccessoire()).isEqualTo(UPDATED_VALEUR_ACCESSOIRE);
        assertThat(testVehicule.getValeurVenal()).isEqualTo(UPDATED_VALEUR_VENAL);
        assertThat(testVehicule.getDateDerniereVT()).isEqualTo(UPDATED_DATE_DERNIERE_VT);
        assertThat(testVehicule.getValeurNeuve()).isEqualTo(UPDATED_VALEUR_NEUVE);
        assertThat(testVehicule.getCategorie()).isEqualTo(UPDATED_CATEGORIE);

        // Validate the Vehicule in ElasticSearch
        Vehicule vehiculeEs = vehiculeSearchRepository.findOne(testVehicule.getId());
        assertThat(vehiculeEs).isEqualToComparingFieldByField(testVehicule);
    }

    @Test
    @Transactional
    public void deleteVehicule() throws Exception {
        // Initialize the database
        vehiculeService.save(vehicule);

        int databaseSizeBeforeDelete = vehiculeRepository.findAll().size();

        // Get the vehicule
        restVehiculeMockMvc.perform(delete("/api/vehicules/{id}", vehicule.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean vehiculeExistsInEs = vehiculeSearchRepository.exists(vehicule.getId());
        assertThat(vehiculeExistsInEs).isFalse();

        // Validate the database is empty
        List<Vehicule> vehicules = vehiculeRepository.findAll();
        assertThat(vehicules).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchVehicule() throws Exception {
        // Initialize the database
        vehiculeService.save(vehicule);

        // Search the vehicule
        restVehiculeMockMvc.perform(get("/api/_search/vehicules?query=id:" + vehicule.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(vehicule.getId().intValue())))
            .andExpect(jsonPath("$.[*].immatriculation").value(hasItem(DEFAULT_IMMATRICULATION.toString())))
            .andExpect(jsonPath("$.[*].marque").value(hasItem(DEFAULT_MARQUE.toString())))
            .andExpect(jsonPath("$.[*].puissance").value(hasItem(DEFAULT_PUISSANCE.intValue())))
            .andExpect(jsonPath("$.[*].source").value(hasItem(DEFAULT_SOURCE.toString())))
            .andExpect(jsonPath("$.[*].numeroChassi").value(hasItem(DEFAULT_NUMERO_CHASSI.toString())))
            .andExpect(jsonPath("$.[*].datePremMiseCirculation").value(hasItem(DEFAULT_DATE_PREM_MISE_CIRCULATION.toString())))
            .andExpect(jsonPath("$.[*].nbrePlace").value(hasItem(DEFAULT_NBRE_PLACE.intValue())))
            .andExpect(jsonPath("$.[*].poids").value(hasItem(DEFAULT_POIDS.intValue())))
            .andExpect(jsonPath("$.[*].valeurAccessoire").value(hasItem(DEFAULT_VALEUR_ACCESSOIRE.intValue())))
            .andExpect(jsonPath("$.[*].valeurVenal").value(hasItem(DEFAULT_VALEUR_VENAL.intValue())))
            .andExpect(jsonPath("$.[*].dateDerniereVT").value(hasItem(DEFAULT_DATE_DERNIERE_VT.toString())))
            .andExpect(jsonPath("$.[*].valeurNeuve").value(hasItem(DEFAULT_VALEUR_NEUVE.intValue())))
            .andExpect(jsonPath("$.[*].categorie").value(hasItem(DEFAULT_CATEGORIE.toString())));
    }
}
