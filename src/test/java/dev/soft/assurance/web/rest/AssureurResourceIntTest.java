package dev.soft.assurance.web.rest;

import dev.soft.assurance.OrionSoftApp;
import dev.soft.assurance.domain.Assureur;
import dev.soft.assurance.repository.AssureurRepository;
import dev.soft.assurance.service.AssureurService;
import dev.soft.assurance.repository.search.AssureurSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the AssureurResource REST controller.
 *
 * @see AssureurResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OrionSoftApp.class)
public class AssureurResourceIntTest {
    private static final String DEFAULT_NOM = "AAAAA";
    private static final String UPDATED_NOM = "BBBBB";

    @Inject
    private AssureurRepository assureurRepository;

    @Inject
    private AssureurService assureurService;

    @Inject
    private AssureurSearchRepository assureurSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restAssureurMockMvc;

    private Assureur assureur;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        AssureurResource assureurResource = new AssureurResource();
        ReflectionTestUtils.setField(assureurResource, "assureurService", assureurService);
        this.restAssureurMockMvc = MockMvcBuilders.standaloneSetup(assureurResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Assureur createEntity(EntityManager em) {
        Assureur assureur = new Assureur();
        assureur = new Assureur()
                .nom(DEFAULT_NOM);
        return assureur;
    }

    @Before
    public void initTest() {
        assureurSearchRepository.deleteAll();
        assureur = createEntity(em);
    }

    @Test
    @Transactional
    public void createAssureur() throws Exception {
        int databaseSizeBeforeCreate = assureurRepository.findAll().size();

        // Create the Assureur

        restAssureurMockMvc.perform(post("/api/assureurs")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(assureur)))
                .andExpect(status().isCreated());

        // Validate the Assureur in the database
        List<Assureur> assureurs = assureurRepository.findAll();
        assertThat(assureurs).hasSize(databaseSizeBeforeCreate + 1);
        Assureur testAssureur = assureurs.get(assureurs.size() - 1);
        assertThat(testAssureur.getNom()).isEqualTo(DEFAULT_NOM);

        // Validate the Assureur in ElasticSearch
        Assureur assureurEs = assureurSearchRepository.findOne(testAssureur.getId());
        assertThat(assureurEs).isEqualToComparingFieldByField(testAssureur);
    }

    @Test
    @Transactional
    public void getAllAssureurs() throws Exception {
        // Initialize the database
        assureurRepository.saveAndFlush(assureur);

        // Get all the assureurs
        restAssureurMockMvc.perform(get("/api/assureurs?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(assureur.getId().intValue())))
                .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM.toString())));
    }

    @Test
    @Transactional
    public void getAssureur() throws Exception {
        // Initialize the database
        assureurRepository.saveAndFlush(assureur);

        // Get the assureur
        restAssureurMockMvc.perform(get("/api/assureurs/{id}", assureur.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(assureur.getId().intValue()))
            .andExpect(jsonPath("$.nom").value(DEFAULT_NOM.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingAssureur() throws Exception {
        // Get the assureur
        restAssureurMockMvc.perform(get("/api/assureurs/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAssureur() throws Exception {
        // Initialize the database
        assureurService.save(assureur);

        int databaseSizeBeforeUpdate = assureurRepository.findAll().size();

        // Update the assureur
        Assureur updatedAssureur = assureurRepository.findOne(assureur.getId());
        updatedAssureur
                .nom(UPDATED_NOM);

        restAssureurMockMvc.perform(put("/api/assureurs")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedAssureur)))
                .andExpect(status().isOk());

        // Validate the Assureur in the database
        List<Assureur> assureurs = assureurRepository.findAll();
        assertThat(assureurs).hasSize(databaseSizeBeforeUpdate);
        Assureur testAssureur = assureurs.get(assureurs.size() - 1);
        assertThat(testAssureur.getNom()).isEqualTo(UPDATED_NOM);

        // Validate the Assureur in ElasticSearch
        Assureur assureurEs = assureurSearchRepository.findOne(testAssureur.getId());
        assertThat(assureurEs).isEqualToComparingFieldByField(testAssureur);
    }

    @Test
    @Transactional
    public void deleteAssureur() throws Exception {
        // Initialize the database
        assureurService.save(assureur);

        int databaseSizeBeforeDelete = assureurRepository.findAll().size();

        // Get the assureur
        restAssureurMockMvc.perform(delete("/api/assureurs/{id}", assureur.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean assureurExistsInEs = assureurSearchRepository.exists(assureur.getId());
        assertThat(assureurExistsInEs).isFalse();

        // Validate the database is empty
        List<Assureur> assureurs = assureurRepository.findAll();
        assertThat(assureurs).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchAssureur() throws Exception {
        // Initialize the database
        assureurService.save(assureur);

        // Search the assureur
        restAssureurMockMvc.perform(get("/api/_search/assureurs?query=id:" + assureur.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(assureur.getId().intValue())))
            .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM.toString())));
    }
}
