package dev.soft.assurance.web.rest;

import dev.soft.assurance.OrionSoftApp;
import dev.soft.assurance.domain.GarantiContrat;
import dev.soft.assurance.repository.GarantiContratRepository;
import dev.soft.assurance.service.GarantiContratService;
import dev.soft.assurance.repository.search.GarantiContratSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the GarantiContratResource REST controller.
 *
 * @see GarantiContratResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OrionSoftApp.class)
public class GarantiContratResourceIntTest {

    private static final BigDecimal DEFAULT_ANNUEL = new BigDecimal(1);
    private static final BigDecimal UPDATED_ANNUEL = new BigDecimal(2);

    private static final BigDecimal DEFAULT_POURCENTAGE = new BigDecimal(1);
    private static final BigDecimal UPDATED_POURCENTAGE = new BigDecimal(2);

    private static final BigDecimal DEFAULT_NETTE = new BigDecimal(1);
    private static final BigDecimal UPDATED_NETTE = new BigDecimal(2);

    private static final BigDecimal DEFAULT_BONUS = new BigDecimal(1);
    private static final BigDecimal UPDATED_BONUS = new BigDecimal(2);

    private static final BigDecimal DEFAULT_SURPLUS = new BigDecimal(1);
    private static final BigDecimal UPDATED_SURPLUS = new BigDecimal(2);

    private static final BigDecimal DEFAULT_NETTEFINAL = new BigDecimal(1);
    private static final BigDecimal UPDATED_NETTEFINAL = new BigDecimal(2);

    @Inject
    private GarantiContratRepository garantiContratRepository;

    @Inject
    private GarantiContratService garantiContratService;

    @Inject
    private GarantiContratSearchRepository garantiContratSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restGarantiContratMockMvc;

    private GarantiContrat garantiContrat;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        GarantiContratResource garantiContratResource = new GarantiContratResource();
        ReflectionTestUtils.setField(garantiContratResource, "garantiContratService", garantiContratService);
        this.restGarantiContratMockMvc = MockMvcBuilders.standaloneSetup(garantiContratResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static GarantiContrat createEntity(EntityManager em) {
        GarantiContrat garantiContrat = new GarantiContrat();
        garantiContrat = new GarantiContrat()
                .annuel(DEFAULT_ANNUEL)
                .pourcentage(DEFAULT_POURCENTAGE)
                .nette(DEFAULT_NETTE)
                .bonus(DEFAULT_BONUS)
                .surplus(DEFAULT_SURPLUS)
                .nettefinal(DEFAULT_NETTEFINAL);
        return garantiContrat;
    }

    @Before
    public void initTest() {
        garantiContratSearchRepository.deleteAll();
        garantiContrat = createEntity(em);
    }

    @Test
    @Transactional
    public void createGarantiContrat() throws Exception {
        int databaseSizeBeforeCreate = garantiContratRepository.findAll().size();

        // Create the GarantiContrat

        restGarantiContratMockMvc.perform(post("/api/garanti-contrats")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(garantiContrat)))
                .andExpect(status().isCreated());

        // Validate the GarantiContrat in the database
        List<GarantiContrat> garantiContrats = garantiContratRepository.findAll();
        assertThat(garantiContrats).hasSize(databaseSizeBeforeCreate + 1);
        GarantiContrat testGarantiContrat = garantiContrats.get(garantiContrats.size() - 1);
        assertThat(testGarantiContrat.getAnnuel()).isEqualTo(DEFAULT_ANNUEL);
        assertThat(testGarantiContrat.getPourcentage()).isEqualTo(DEFAULT_POURCENTAGE);
        assertThat(testGarantiContrat.getNette()).isEqualTo(DEFAULT_NETTE);
        assertThat(testGarantiContrat.getBonus()).isEqualTo(DEFAULT_BONUS);
        assertThat(testGarantiContrat.getSurplus()).isEqualTo(DEFAULT_SURPLUS);
        assertThat(testGarantiContrat.getNettefinal()).isEqualTo(DEFAULT_NETTEFINAL);

        // Validate the GarantiContrat in ElasticSearch
        GarantiContrat garantiContratEs = garantiContratSearchRepository.findOne(testGarantiContrat.getId());
        assertThat(garantiContratEs).isEqualToComparingFieldByField(testGarantiContrat);
    }

    @Test
    @Transactional
    public void getAllGarantiContrats() throws Exception {
        // Initialize the database
        garantiContratRepository.saveAndFlush(garantiContrat);

        // Get all the garantiContrats
        restGarantiContratMockMvc.perform(get("/api/garanti-contrats?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(garantiContrat.getId().intValue())))
                .andExpect(jsonPath("$.[*].annuel").value(hasItem(DEFAULT_ANNUEL.intValue())))
                .andExpect(jsonPath("$.[*].pourcentage").value(hasItem(DEFAULT_POURCENTAGE.intValue())))
                .andExpect(jsonPath("$.[*].nette").value(hasItem(DEFAULT_NETTE.intValue())))
                .andExpect(jsonPath("$.[*].bonus").value(hasItem(DEFAULT_BONUS.intValue())))
                .andExpect(jsonPath("$.[*].surplus").value(hasItem(DEFAULT_SURPLUS.intValue())))
                .andExpect(jsonPath("$.[*].nettefinal").value(hasItem(DEFAULT_NETTEFINAL.intValue())));
    }

    @Test
    @Transactional
    public void getGarantiContrat() throws Exception {
        // Initialize the database
        garantiContratRepository.saveAndFlush(garantiContrat);

        // Get the garantiContrat
        restGarantiContratMockMvc.perform(get("/api/garanti-contrats/{id}", garantiContrat.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(garantiContrat.getId().intValue()))
            .andExpect(jsonPath("$.annuel").value(DEFAULT_ANNUEL.intValue()))
            .andExpect(jsonPath("$.pourcentage").value(DEFAULT_POURCENTAGE.intValue()))
            .andExpect(jsonPath("$.nette").value(DEFAULT_NETTE.intValue()))
            .andExpect(jsonPath("$.bonus").value(DEFAULT_BONUS.intValue()))
            .andExpect(jsonPath("$.surplus").value(DEFAULT_SURPLUS.intValue()))
            .andExpect(jsonPath("$.nettefinal").value(DEFAULT_NETTEFINAL.intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingGarantiContrat() throws Exception {
        // Get the garantiContrat
        restGarantiContratMockMvc.perform(get("/api/garanti-contrats/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateGarantiContrat() throws Exception {
        // Initialize the database
        garantiContratService.save(garantiContrat);

        int databaseSizeBeforeUpdate = garantiContratRepository.findAll().size();

        // Update the garantiContrat
        GarantiContrat updatedGarantiContrat = garantiContratRepository.findOne(garantiContrat.getId());
        updatedGarantiContrat
                .annuel(UPDATED_ANNUEL)
                .pourcentage(UPDATED_POURCENTAGE)
                .nette(UPDATED_NETTE)
                .bonus(UPDATED_BONUS)
                .surplus(UPDATED_SURPLUS)
                .nettefinal(UPDATED_NETTEFINAL);

        restGarantiContratMockMvc.perform(put("/api/garanti-contrats")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedGarantiContrat)))
                .andExpect(status().isOk());

        // Validate the GarantiContrat in the database
        List<GarantiContrat> garantiContrats = garantiContratRepository.findAll();
        assertThat(garantiContrats).hasSize(databaseSizeBeforeUpdate);
        GarantiContrat testGarantiContrat = garantiContrats.get(garantiContrats.size() - 1);
        assertThat(testGarantiContrat.getAnnuel()).isEqualTo(UPDATED_ANNUEL);
        assertThat(testGarantiContrat.getPourcentage()).isEqualTo(UPDATED_POURCENTAGE);
        assertThat(testGarantiContrat.getNette()).isEqualTo(UPDATED_NETTE);
        assertThat(testGarantiContrat.getBonus()).isEqualTo(UPDATED_BONUS);
        assertThat(testGarantiContrat.getSurplus()).isEqualTo(UPDATED_SURPLUS);
        assertThat(testGarantiContrat.getNettefinal()).isEqualTo(UPDATED_NETTEFINAL);

        // Validate the GarantiContrat in ElasticSearch
        GarantiContrat garantiContratEs = garantiContratSearchRepository.findOne(testGarantiContrat.getId());
        assertThat(garantiContratEs).isEqualToComparingFieldByField(testGarantiContrat);
    }

    @Test
    @Transactional
    public void deleteGarantiContrat() throws Exception {
        // Initialize the database
        garantiContratService.save(garantiContrat);

        int databaseSizeBeforeDelete = garantiContratRepository.findAll().size();

        // Get the garantiContrat
        restGarantiContratMockMvc.perform(delete("/api/garanti-contrats/{id}", garantiContrat.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean garantiContratExistsInEs = garantiContratSearchRepository.exists(garantiContrat.getId());
        assertThat(garantiContratExistsInEs).isFalse();

        // Validate the database is empty
        List<GarantiContrat> garantiContrats = garantiContratRepository.findAll();
        assertThat(garantiContrats).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchGarantiContrat() throws Exception {
        // Initialize the database
        garantiContratService.save(garantiContrat);

        // Search the garantiContrat
        restGarantiContratMockMvc.perform(get("/api/_search/garanti-contrats?query=id:" + garantiContrat.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(garantiContrat.getId().intValue())))
            .andExpect(jsonPath("$.[*].annuel").value(hasItem(DEFAULT_ANNUEL.intValue())))
            .andExpect(jsonPath("$.[*].pourcentage").value(hasItem(DEFAULT_POURCENTAGE.intValue())))
            .andExpect(jsonPath("$.[*].nette").value(hasItem(DEFAULT_NETTE.intValue())))
            .andExpect(jsonPath("$.[*].bonus").value(hasItem(DEFAULT_BONUS.intValue())))
            .andExpect(jsonPath("$.[*].surplus").value(hasItem(DEFAULT_SURPLUS.intValue())))
            .andExpect(jsonPath("$.[*].nettefinal").value(hasItem(DEFAULT_NETTEFINAL.intValue())));
    }
}
