package dev.soft.assurance.web.rest;

import dev.soft.assurance.OrionSoftApp;
import dev.soft.assurance.domain.Assure;
import dev.soft.assurance.repository.AssureRepository;
import dev.soft.assurance.service.AssureService;
import dev.soft.assurance.repository.search.AssureSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import dev.soft.assurance.domain.enumeration.VILLE;
/**
 * Test class for the AssureResource REST controller.
 *
 * @see AssureResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OrionSoftApp.class)
public class AssureResourceIntTest {
    private static final String DEFAULT_NOM = "AAAAA";
    private static final String UPDATED_NOM = "BBBBB";
    private static final String DEFAULT_NUMBER = "AAAAA";
    private static final String UPDATED_NUMBER = "BBBBB";

    private static final VILLE DEFAULT_VILLE = VILLE.YAOUNDE;
    private static final VILLE UPDATED_VILLE = VILLE.DOUALA;
    private static final String DEFAULT_ADRESS = "AAAAA";
    private static final String UPDATED_ADRESS = "BBBBB";
    private static final String DEFAULT_PROFESSION = "AAAAA";
    private static final String UPDATED_PROFESSION = "BBBBB";

    @Inject
    private AssureRepository assureRepository;

    @Inject
    private AssureService assureService;

    @Inject
    private AssureSearchRepository assureSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restAssureMockMvc;

    private Assure assure;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        AssureResource assureResource = new AssureResource();
        ReflectionTestUtils.setField(assureResource, "assureService", assureService);
        this.restAssureMockMvc = MockMvcBuilders.standaloneSetup(assureResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Assure createEntity(EntityManager em) {
        Assure assure = new Assure();
        assure = new Assure()
                .nom(DEFAULT_NOM)
                .number(DEFAULT_NUMBER)
                .ville(DEFAULT_VILLE)
                .adress(DEFAULT_ADRESS)
                .profession(DEFAULT_PROFESSION);
        return assure;
    }

    @Before
    public void initTest() {
        assureSearchRepository.deleteAll();
        assure = createEntity(em);
    }

    @Test
    @Transactional
    public void createAssure() throws Exception {
        int databaseSizeBeforeCreate = assureRepository.findAll().size();

        // Create the Assure

        restAssureMockMvc.perform(post("/api/assures")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(assure)))
                .andExpect(status().isCreated());

        // Validate the Assure in the database
        List<Assure> assures = assureRepository.findAll();
        assertThat(assures).hasSize(databaseSizeBeforeCreate + 1);
        Assure testAssure = assures.get(assures.size() - 1);
        assertThat(testAssure.getNom()).isEqualTo(DEFAULT_NOM);
        assertThat(testAssure.getNumber()).isEqualTo(DEFAULT_NUMBER);
        assertThat(testAssure.getVille()).isEqualTo(DEFAULT_VILLE);
        assertThat(testAssure.getAdress()).isEqualTo(DEFAULT_ADRESS);
        assertThat(testAssure.getProfession()).isEqualTo(DEFAULT_PROFESSION);

        // Validate the Assure in ElasticSearch
        Assure assureEs = assureSearchRepository.findOne(testAssure.getId());
        assertThat(assureEs).isEqualToComparingFieldByField(testAssure);
    }

    @Test
    @Transactional
    public void getAllAssures() throws Exception {
        // Initialize the database
        assureRepository.saveAndFlush(assure);

        // Get all the assures
        restAssureMockMvc.perform(get("/api/assures?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(assure.getId().intValue())))
                .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM.toString())))
                .andExpect(jsonPath("$.[*].number").value(hasItem(DEFAULT_NUMBER.toString())))
                .andExpect(jsonPath("$.[*].ville").value(hasItem(DEFAULT_VILLE.toString())))
                .andExpect(jsonPath("$.[*].adress").value(hasItem(DEFAULT_ADRESS.toString())))
                .andExpect(jsonPath("$.[*].profession").value(hasItem(DEFAULT_PROFESSION.toString())));
    }

    @Test
    @Transactional
    public void getAssure() throws Exception {
        // Initialize the database
        assureRepository.saveAndFlush(assure);

        // Get the assure
        restAssureMockMvc.perform(get("/api/assures/{id}", assure.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(assure.getId().intValue()))
            .andExpect(jsonPath("$.nom").value(DEFAULT_NOM.toString()))
            .andExpect(jsonPath("$.number").value(DEFAULT_NUMBER.toString()))
            .andExpect(jsonPath("$.ville").value(DEFAULT_VILLE.toString()))
            .andExpect(jsonPath("$.adress").value(DEFAULT_ADRESS.toString()))
            .andExpect(jsonPath("$.profession").value(DEFAULT_PROFESSION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingAssure() throws Exception {
        // Get the assure
        restAssureMockMvc.perform(get("/api/assures/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAssure() throws Exception {
        // Initialize the database
        assureService.save(assure);

        int databaseSizeBeforeUpdate = assureRepository.findAll().size();

        // Update the assure
        Assure updatedAssure = assureRepository.findOne(assure.getId());
        updatedAssure
                .nom(UPDATED_NOM)
                .number(UPDATED_NUMBER)
                .ville(UPDATED_VILLE)
                .adress(UPDATED_ADRESS)
                .profession(UPDATED_PROFESSION);

        restAssureMockMvc.perform(put("/api/assures")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedAssure)))
                .andExpect(status().isOk());

        // Validate the Assure in the database
        List<Assure> assures = assureRepository.findAll();
        assertThat(assures).hasSize(databaseSizeBeforeUpdate);
        Assure testAssure = assures.get(assures.size() - 1);
        assertThat(testAssure.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testAssure.getNumber()).isEqualTo(UPDATED_NUMBER);
        assertThat(testAssure.getVille()).isEqualTo(UPDATED_VILLE);
        assertThat(testAssure.getAdress()).isEqualTo(UPDATED_ADRESS);
        assertThat(testAssure.getProfession()).isEqualTo(UPDATED_PROFESSION);

        // Validate the Assure in ElasticSearch
        Assure assureEs = assureSearchRepository.findOne(testAssure.getId());
        assertThat(assureEs).isEqualToComparingFieldByField(testAssure);
    }

    @Test
    @Transactional
    public void deleteAssure() throws Exception {
        // Initialize the database
        assureService.save(assure);

        int databaseSizeBeforeDelete = assureRepository.findAll().size();

        // Get the assure
        restAssureMockMvc.perform(delete("/api/assures/{id}", assure.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean assureExistsInEs = assureSearchRepository.exists(assure.getId());
        assertThat(assureExistsInEs).isFalse();

        // Validate the database is empty
        List<Assure> assures = assureRepository.findAll();
        assertThat(assures).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchAssure() throws Exception {
        // Initialize the database
        assureService.save(assure);

        // Search the assure
        restAssureMockMvc.perform(get("/api/_search/assures?query=id:" + assure.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(assure.getId().intValue())))
            .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM.toString())))
            .andExpect(jsonPath("$.[*].number").value(hasItem(DEFAULT_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].ville").value(hasItem(DEFAULT_VILLE.toString())))
            .andExpect(jsonPath("$.[*].adress").value(hasItem(DEFAULT_ADRESS.toString())))
            .andExpect(jsonPath("$.[*].profession").value(hasItem(DEFAULT_PROFESSION.toString())));
    }
}
