package dev.soft.assurance.web.rest;

import dev.soft.assurance.OrionSoftApp;
import dev.soft.assurance.domain.CarteRose;
import dev.soft.assurance.repository.CarteRoseRepository;
import dev.soft.assurance.service.CarteRoseService;
import dev.soft.assurance.repository.search.CarteRoseSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the CarteRoseResource REST controller.
 *
 * @see CarteRoseResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OrionSoftApp.class)
public class CarteRoseResourceIntTest {
    private static final String DEFAULT_NOM_ASSURE = "AAAAA";
    private static final String UPDATED_NOM_ASSURE = "BBBBB";
    private static final String DEFAULT_IMMATRICULATION = "AAAAA";
    private static final String UPDATED_IMMATRICULATION = "BBBBB";
    private static final String DEFAULT_MARQUE = "AAAAA";
    private static final String UPDATED_MARQUE = "BBBBB";
    private static final String DEFAULT_ASSUREUR = "AAAAA";
    private static final String UPDATED_ASSUREUR = "BBBBB";
    private static final String DEFAULT_BUREAU_EMETTEUR = "AAAAA";
    private static final String UPDATED_BUREAU_EMETTEUR = "BBBBB";
    private static final String DEFAULT_POLICE = "AAAAA";
    private static final String UPDATED_POLICE = "BBBBB";

    private static final LocalDate DEFAULT_DATE_EFFET = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_EFFET = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_DATE_FIN = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_FIN = LocalDate.now(ZoneId.systemDefault());
    private static final String DEFAULT_CATEGORY_VEHICULE = "AAAAA";
    private static final String UPDATED_CATEGORY_VEHICULE = "BBBBB";
    private static final String DEFAULT_NUMERO_CHASSIS = "AAAAA";
    private static final String UPDATED_NUMERO_CHASSIS = "BBBBB";

    @Inject
    private CarteRoseRepository carteRoseRepository;

    @Inject
    private CarteRoseService carteRoseService;

    @Inject
    private CarteRoseSearchRepository carteRoseSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restCarteRoseMockMvc;

    private CarteRose carteRose;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        CarteRoseResource carteRoseResource = new CarteRoseResource();
        ReflectionTestUtils.setField(carteRoseResource, "carteRoseService", carteRoseService);
        this.restCarteRoseMockMvc = MockMvcBuilders.standaloneSetup(carteRoseResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CarteRose createEntity(EntityManager em) {
        CarteRose carteRose = new CarteRose();
        carteRose = new CarteRose()
                .nomAssure(DEFAULT_NOM_ASSURE)
                .immatriculation(DEFAULT_IMMATRICULATION)
                .marque(DEFAULT_MARQUE)
                .assureur(DEFAULT_ASSUREUR)
                .bureauEmetteur(DEFAULT_BUREAU_EMETTEUR)
                .police(DEFAULT_POLICE)
                .dateEffet(DEFAULT_DATE_EFFET)
                .dateFin(DEFAULT_DATE_FIN)
                .categoryVehicule(DEFAULT_CATEGORY_VEHICULE)
                .numeroChassis(DEFAULT_NUMERO_CHASSIS);
        return carteRose;
    }

    @Before
    public void initTest() {
        carteRoseSearchRepository.deleteAll();
        carteRose = createEntity(em);
    }

    @Test
    @Transactional
    public void createCarteRose() throws Exception {
        int databaseSizeBeforeCreate = carteRoseRepository.findAll().size();

        // Create the CarteRose

        restCarteRoseMockMvc.perform(post("/api/carte-roses")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(carteRose)))
                .andExpect(status().isCreated());

        // Validate the CarteRose in the database
        List<CarteRose> carteRoses = carteRoseRepository.findAll();
        assertThat(carteRoses).hasSize(databaseSizeBeforeCreate + 1);
        CarteRose testCarteRose = carteRoses.get(carteRoses.size() - 1);
        assertThat(testCarteRose.getNomAssure()).isEqualTo(DEFAULT_NOM_ASSURE);
        assertThat(testCarteRose.getImmatriculation()).isEqualTo(DEFAULT_IMMATRICULATION);
        assertThat(testCarteRose.getMarque()).isEqualTo(DEFAULT_MARQUE);
        assertThat(testCarteRose.getAssureur()).isEqualTo(DEFAULT_ASSUREUR);
        assertThat(testCarteRose.getBureauEmetteur()).isEqualTo(DEFAULT_BUREAU_EMETTEUR);
        assertThat(testCarteRose.getPolice()).isEqualTo(DEFAULT_POLICE);
        assertThat(testCarteRose.getDateEffet()).isEqualTo(DEFAULT_DATE_EFFET);
        assertThat(testCarteRose.getDateFin()).isEqualTo(DEFAULT_DATE_FIN);
        assertThat(testCarteRose.getCategoryVehicule()).isEqualTo(DEFAULT_CATEGORY_VEHICULE);
        assertThat(testCarteRose.getNumeroChassis()).isEqualTo(DEFAULT_NUMERO_CHASSIS);

        // Validate the CarteRose in ElasticSearch
        CarteRose carteRoseEs = carteRoseSearchRepository.findOne(testCarteRose.getId());
        assertThat(carteRoseEs).isEqualToComparingFieldByField(testCarteRose);
    }

    @Test
    @Transactional
    public void getAllCarteRoses() throws Exception {
        // Initialize the database
        carteRoseRepository.saveAndFlush(carteRose);

        // Get all the carteRoses
        restCarteRoseMockMvc.perform(get("/api/carte-roses?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(carteRose.getId().intValue())))
                .andExpect(jsonPath("$.[*].nomAssure").value(hasItem(DEFAULT_NOM_ASSURE.toString())))
                .andExpect(jsonPath("$.[*].immatriculation").value(hasItem(DEFAULT_IMMATRICULATION.toString())))
                .andExpect(jsonPath("$.[*].marque").value(hasItem(DEFAULT_MARQUE.toString())))
                .andExpect(jsonPath("$.[*].assureur").value(hasItem(DEFAULT_ASSUREUR.toString())))
                .andExpect(jsonPath("$.[*].bureauEmetteur").value(hasItem(DEFAULT_BUREAU_EMETTEUR.toString())))
                .andExpect(jsonPath("$.[*].police").value(hasItem(DEFAULT_POLICE.toString())))
                .andExpect(jsonPath("$.[*].dateEffet").value(hasItem(DEFAULT_DATE_EFFET.toString())))
                .andExpect(jsonPath("$.[*].dateFin").value(hasItem(DEFAULT_DATE_FIN.toString())))
                .andExpect(jsonPath("$.[*].categoryVehicule").value(hasItem(DEFAULT_CATEGORY_VEHICULE.toString())))
                .andExpect(jsonPath("$.[*].numeroChassis").value(hasItem(DEFAULT_NUMERO_CHASSIS.toString())));
    }

    @Test
    @Transactional
    public void getCarteRose() throws Exception {
        // Initialize the database
        carteRoseRepository.saveAndFlush(carteRose);

        // Get the carteRose
        restCarteRoseMockMvc.perform(get("/api/carte-roses/{id}", carteRose.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(carteRose.getId().intValue()))
            .andExpect(jsonPath("$.nomAssure").value(DEFAULT_NOM_ASSURE.toString()))
            .andExpect(jsonPath("$.immatriculation").value(DEFAULT_IMMATRICULATION.toString()))
            .andExpect(jsonPath("$.marque").value(DEFAULT_MARQUE.toString()))
            .andExpect(jsonPath("$.assureur").value(DEFAULT_ASSUREUR.toString()))
            .andExpect(jsonPath("$.bureauEmetteur").value(DEFAULT_BUREAU_EMETTEUR.toString()))
            .andExpect(jsonPath("$.police").value(DEFAULT_POLICE.toString()))
            .andExpect(jsonPath("$.dateEffet").value(DEFAULT_DATE_EFFET.toString()))
            .andExpect(jsonPath("$.dateFin").value(DEFAULT_DATE_FIN.toString()))
            .andExpect(jsonPath("$.categoryVehicule").value(DEFAULT_CATEGORY_VEHICULE.toString()))
            .andExpect(jsonPath("$.numeroChassis").value(DEFAULT_NUMERO_CHASSIS.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingCarteRose() throws Exception {
        // Get the carteRose
        restCarteRoseMockMvc.perform(get("/api/carte-roses/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCarteRose() throws Exception {
        // Initialize the database
        carteRoseService.save(carteRose);

        int databaseSizeBeforeUpdate = carteRoseRepository.findAll().size();

        // Update the carteRose
        CarteRose updatedCarteRose = carteRoseRepository.findOne(carteRose.getId());
        updatedCarteRose
                .nomAssure(UPDATED_NOM_ASSURE)
                .immatriculation(UPDATED_IMMATRICULATION)
                .marque(UPDATED_MARQUE)
                .assureur(UPDATED_ASSUREUR)
                .bureauEmetteur(UPDATED_BUREAU_EMETTEUR)
                .police(UPDATED_POLICE)
                .dateEffet(UPDATED_DATE_EFFET)
                .dateFin(UPDATED_DATE_FIN)
                .categoryVehicule(UPDATED_CATEGORY_VEHICULE)
                .numeroChassis(UPDATED_NUMERO_CHASSIS);

        restCarteRoseMockMvc.perform(put("/api/carte-roses")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedCarteRose)))
                .andExpect(status().isOk());

        // Validate the CarteRose in the database
        List<CarteRose> carteRoses = carteRoseRepository.findAll();
        assertThat(carteRoses).hasSize(databaseSizeBeforeUpdate);
        CarteRose testCarteRose = carteRoses.get(carteRoses.size() - 1);
        assertThat(testCarteRose.getNomAssure()).isEqualTo(UPDATED_NOM_ASSURE);
        assertThat(testCarteRose.getImmatriculation()).isEqualTo(UPDATED_IMMATRICULATION);
        assertThat(testCarteRose.getMarque()).isEqualTo(UPDATED_MARQUE);
        assertThat(testCarteRose.getAssureur()).isEqualTo(UPDATED_ASSUREUR);
        assertThat(testCarteRose.getBureauEmetteur()).isEqualTo(UPDATED_BUREAU_EMETTEUR);
        assertThat(testCarteRose.getPolice()).isEqualTo(UPDATED_POLICE);
        assertThat(testCarteRose.getDateEffet()).isEqualTo(UPDATED_DATE_EFFET);
        assertThat(testCarteRose.getDateFin()).isEqualTo(UPDATED_DATE_FIN);
        assertThat(testCarteRose.getCategoryVehicule()).isEqualTo(UPDATED_CATEGORY_VEHICULE);
        assertThat(testCarteRose.getNumeroChassis()).isEqualTo(UPDATED_NUMERO_CHASSIS);

        // Validate the CarteRose in ElasticSearch
        CarteRose carteRoseEs = carteRoseSearchRepository.findOne(testCarteRose.getId());
        assertThat(carteRoseEs).isEqualToComparingFieldByField(testCarteRose);
    }

    @Test
    @Transactional
    public void deleteCarteRose() throws Exception {
        // Initialize the database
        carteRoseService.save(carteRose);

        int databaseSizeBeforeDelete = carteRoseRepository.findAll().size();

        // Get the carteRose
        restCarteRoseMockMvc.perform(delete("/api/carte-roses/{id}", carteRose.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean carteRoseExistsInEs = carteRoseSearchRepository.exists(carteRose.getId());
        assertThat(carteRoseExistsInEs).isFalse();

        // Validate the database is empty
        List<CarteRose> carteRoses = carteRoseRepository.findAll();
        assertThat(carteRoses).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchCarteRose() throws Exception {
        // Initialize the database
        carteRoseService.save(carteRose);

        // Search the carteRose
        restCarteRoseMockMvc.perform(get("/api/_search/carte-roses?query=id:" + carteRose.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(carteRose.getId().intValue())))
            .andExpect(jsonPath("$.[*].nomAssure").value(hasItem(DEFAULT_NOM_ASSURE.toString())))
            .andExpect(jsonPath("$.[*].immatriculation").value(hasItem(DEFAULT_IMMATRICULATION.toString())))
            .andExpect(jsonPath("$.[*].marque").value(hasItem(DEFAULT_MARQUE.toString())))
            .andExpect(jsonPath("$.[*].assureur").value(hasItem(DEFAULT_ASSUREUR.toString())))
            .andExpect(jsonPath("$.[*].bureauEmetteur").value(hasItem(DEFAULT_BUREAU_EMETTEUR.toString())))
            .andExpect(jsonPath("$.[*].police").value(hasItem(DEFAULT_POLICE.toString())))
            .andExpect(jsonPath("$.[*].dateEffet").value(hasItem(DEFAULT_DATE_EFFET.toString())))
            .andExpect(jsonPath("$.[*].dateFin").value(hasItem(DEFAULT_DATE_FIN.toString())))
            .andExpect(jsonPath("$.[*].categoryVehicule").value(hasItem(DEFAULT_CATEGORY_VEHICULE.toString())))
            .andExpect(jsonPath("$.[*].numeroChassis").value(hasItem(DEFAULT_NUMERO_CHASSIS.toString())));
    }
}
