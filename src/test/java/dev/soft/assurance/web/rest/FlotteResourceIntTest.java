package dev.soft.assurance.web.rest;

import dev.soft.assurance.OrionSoftApp;
import dev.soft.assurance.domain.Flotte;
import dev.soft.assurance.repository.FlotteRepository;
import dev.soft.assurance.service.FlotteService;
import dev.soft.assurance.repository.search.FlotteSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the FlotteResource REST controller.
 *
 * @see FlotteResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OrionSoftApp.class)
public class FlotteResourceIntTest {
    private static final String DEFAULT_NUMERO_POLICE = "AAAAA";
    private static final String UPDATED_NUMERO_POLICE = "BBBBB";

    private static final Integer DEFAULT_NBRE = 1;
    private static final Integer UPDATED_NBRE = 2;

    private static final BigDecimal DEFAULT_ACCESSOIRE = new BigDecimal(1);
    private static final BigDecimal UPDATED_ACCESSOIRE = new BigDecimal(2);

    private static final Boolean DEFAULT_ETAT = false;
    private static final Boolean UPDATED_ETAT = true;

    @Inject
    private FlotteRepository flotteRepository;

    @Inject
    private FlotteService flotteService;

    @Inject
    private FlotteSearchRepository flotteSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restFlotteMockMvc;

    private Flotte flotte;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        FlotteResource flotteResource = new FlotteResource();
        ReflectionTestUtils.setField(flotteResource, "flotteService", flotteService);
        this.restFlotteMockMvc = MockMvcBuilders.standaloneSetup(flotteResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Flotte createEntity(EntityManager em) {
        Flotte flotte = new Flotte();
        flotte = new Flotte()
                .numeroPolice(DEFAULT_NUMERO_POLICE)
                .nbre(DEFAULT_NBRE)
                .accessoire(DEFAULT_ACCESSOIRE)
                .etat(DEFAULT_ETAT);
        return flotte;
    }

    @Before
    public void initTest() {
        flotteSearchRepository.deleteAll();
        flotte = createEntity(em);
    }

    @Test
    @Transactional
    public void createFlotte() throws Exception {
        int databaseSizeBeforeCreate = flotteRepository.findAll().size();

        // Create the Flotte

        restFlotteMockMvc.perform(post("/api/flottes")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(flotte)))
                .andExpect(status().isCreated());

        // Validate the Flotte in the database
        List<Flotte> flottes = flotteRepository.findAll();
        assertThat(flottes).hasSize(databaseSizeBeforeCreate + 1);
        Flotte testFlotte = flottes.get(flottes.size() - 1);
        assertThat(testFlotte.getNumeroPolice()).isEqualTo(DEFAULT_NUMERO_POLICE);
        assertThat(testFlotte.getNbre()).isEqualTo(DEFAULT_NBRE);
        assertThat(testFlotte.getAccessoire()).isEqualTo(DEFAULT_ACCESSOIRE);
        assertThat(testFlotte.isEtat()).isEqualTo(DEFAULT_ETAT);

        // Validate the Flotte in ElasticSearch
        Flotte flotteEs = flotteSearchRepository.findOne(testFlotte.getId());
        assertThat(flotteEs).isEqualToComparingFieldByField(testFlotte);
    }

    @Test
    @Transactional
    public void getAllFlottes() throws Exception {
        // Initialize the database
        flotteRepository.saveAndFlush(flotte);

        // Get all the flottes
        restFlotteMockMvc.perform(get("/api/flottes?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(flotte.getId().intValue())))
                .andExpect(jsonPath("$.[*].numeroPolice").value(hasItem(DEFAULT_NUMERO_POLICE.toString())))
                .andExpect(jsonPath("$.[*].nbre").value(hasItem(DEFAULT_NBRE)))
                .andExpect(jsonPath("$.[*].accessoire").value(hasItem(DEFAULT_ACCESSOIRE.intValue())))
                .andExpect(jsonPath("$.[*].etat").value(hasItem(DEFAULT_ETAT.booleanValue())));
    }

    @Test
    @Transactional
    public void getFlotte() throws Exception {
        // Initialize the database
        flotteRepository.saveAndFlush(flotte);

        // Get the flotte
        restFlotteMockMvc.perform(get("/api/flottes/{id}", flotte.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(flotte.getId().intValue()))
            .andExpect(jsonPath("$.numeroPolice").value(DEFAULT_NUMERO_POLICE.toString()))
            .andExpect(jsonPath("$.nbre").value(DEFAULT_NBRE))
            .andExpect(jsonPath("$.accessoire").value(DEFAULT_ACCESSOIRE.intValue()))
            .andExpect(jsonPath("$.etat").value(DEFAULT_ETAT.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingFlotte() throws Exception {
        // Get the flotte
        restFlotteMockMvc.perform(get("/api/flottes/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFlotte() throws Exception {
        // Initialize the database
        flotteService.save(flotte);

        int databaseSizeBeforeUpdate = flotteRepository.findAll().size();

        // Update the flotte
        Flotte updatedFlotte = flotteRepository.findOne(flotte.getId());
        updatedFlotte
                .numeroPolice(UPDATED_NUMERO_POLICE)
                .nbre(UPDATED_NBRE)
                .accessoire(UPDATED_ACCESSOIRE)
                .etat(UPDATED_ETAT);

        restFlotteMockMvc.perform(put("/api/flottes")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedFlotte)))
                .andExpect(status().isOk());

        // Validate the Flotte in the database
        List<Flotte> flottes = flotteRepository.findAll();
        assertThat(flottes).hasSize(databaseSizeBeforeUpdate);
        Flotte testFlotte = flottes.get(flottes.size() - 1);
        assertThat(testFlotte.getNumeroPolice()).isEqualTo(UPDATED_NUMERO_POLICE);
        assertThat(testFlotte.getNbre()).isEqualTo(UPDATED_NBRE);
        assertThat(testFlotte.getAccessoire()).isEqualTo(UPDATED_ACCESSOIRE);
        assertThat(testFlotte.isEtat()).isEqualTo(UPDATED_ETAT);

        // Validate the Flotte in ElasticSearch
        Flotte flotteEs = flotteSearchRepository.findOne(testFlotte.getId());
        assertThat(flotteEs).isEqualToComparingFieldByField(testFlotte);
    }

    @Test
    @Transactional
    public void deleteFlotte() throws Exception {
        // Initialize the database
        flotteService.save(flotte);

        int databaseSizeBeforeDelete = flotteRepository.findAll().size();

        // Get the flotte
        restFlotteMockMvc.perform(delete("/api/flottes/{id}", flotte.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean flotteExistsInEs = flotteSearchRepository.exists(flotte.getId());
        assertThat(flotteExistsInEs).isFalse();

        // Validate the database is empty
        List<Flotte> flottes = flotteRepository.findAll();
        assertThat(flottes).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchFlotte() throws Exception {
        // Initialize the database
        flotteService.save(flotte);

        // Search the flotte
        restFlotteMockMvc.perform(get("/api/_search/flottes?query=id:" + flotte.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(flotte.getId().intValue())))
            .andExpect(jsonPath("$.[*].numeroPolice").value(hasItem(DEFAULT_NUMERO_POLICE.toString())))
            .andExpect(jsonPath("$.[*].nbre").value(hasItem(DEFAULT_NBRE)))
            .andExpect(jsonPath("$.[*].accessoire").value(hasItem(DEFAULT_ACCESSOIRE.intValue())))
            .andExpect(jsonPath("$.[*].etat").value(hasItem(DEFAULT_ETAT.booleanValue())));
    }
}
