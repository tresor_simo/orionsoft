package dev.soft.assurance.web.rest;

import dev.soft.assurance.OrionSoftApp;
import dev.soft.assurance.domain.Contrat;
import dev.soft.assurance.repository.ContratRepository;
import dev.soft.assurance.service.ContratService;
import dev.soft.assurance.repository.search.ContratSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import dev.soft.assurance.domain.enumeration.PERMIS;
import dev.soft.assurance.domain.enumeration.STATUS_CONTRAT;
/**
 * Test class for the ContratResource REST controller.
 *
 * @see ContratResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OrionSoftApp.class)
public class ContratResourceIntTest {
    private static final String DEFAULT_NUMERO_POLICE = "AAAAA";
    private static final String UPDATED_NUMERO_POLICE = "BBBBB";

    private static final LocalDate DEFAULT_DATE_EFFET = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_EFFET = LocalDate.now(ZoneId.systemDefault());

    private static final BigDecimal DEFAULT_DUREE = new BigDecimal(1);
    private static final BigDecimal UPDATED_DUREE = new BigDecimal(2);
    private static final String DEFAULT_SOUSCRIPTEUR = "AAAAA";
    private static final String UPDATED_SOUSCRIPTEUR = "BBBBB";
    private static final String DEFAULT_CONDUCTEUR = "AAAAA";
    private static final String UPDATED_CONDUCTEUR = "BBBBB";
    private static final String DEFAULT_STATUT_SOCIO = "AAAAA";
    private static final String UPDATED_STATUT_SOCIO = "BBBBB";

    private static final PERMIS DEFAULT_CATEGORIE_PERMIS = PERMIS.A;
    private static final PERMIS UPDATED_CATEGORIE_PERMIS = PERMIS.B;

    private static final BigDecimal DEFAULT_PRIME_TTC = new BigDecimal(1);
    private static final BigDecimal UPDATED_PRIME_TTC = new BigDecimal(2);

    private static final BigDecimal DEFAULT_ACOMPTE = new BigDecimal(1);
    private static final BigDecimal UPDATED_ACOMPTE = new BigDecimal(2);

    private static final STATUS_CONTRAT DEFAULT_STATUS = STATUS_CONTRAT.NOUVEAU;
    private static final STATUS_CONTRAT UPDATED_STATUS = STATUS_CONTRAT.RENOUVELE;

    private static final LocalDate DEFAULT_DATE_FIN = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_FIN = LocalDate.now(ZoneId.systemDefault());

    private static final BigDecimal DEFAULT_PRIME_BASE = new BigDecimal(1);
    private static final BigDecimal UPDATED_PRIME_BASE = new BigDecimal(2);

    private static final BigDecimal DEFAULT_POURCENTAGE = new BigDecimal(1);
    private static final BigDecimal UPDATED_POURCENTAGE = new BigDecimal(2);

    private static final BigDecimal DEFAULT_ACCESSOIRE = new BigDecimal(1);
    private static final BigDecimal UPDATED_ACCESSOIRE = new BigDecimal(2);

    private static final BigDecimal DEFAULT_COUT_POLICE = new BigDecimal(1);
    private static final BigDecimal UPDATED_COUT_POLICE = new BigDecimal(2);

    private static final BigDecimal DEFAULT_ASAC = new BigDecimal(1);
    private static final BigDecimal UPDATED_ASAC = new BigDecimal(2);

    private static final BigDecimal DEFAULT_CARTE_ROSE = new BigDecimal(1);
    private static final BigDecimal UPDATED_CARTE_ROSE = new BigDecimal(2);

    private static final Boolean DEFAULT_IS_DOMMAGE_COLLISION = false;
    private static final Boolean UPDATED_IS_DOMMAGE_COLLISION = true;

    private static final BigDecimal DEFAULT_POURCENT_DOMMAGE_COLLISION = new BigDecimal(1);
    private static final BigDecimal UPDATED_POURCENT_DOMMAGE_COLLISION = new BigDecimal(2);

    private static final Boolean DEFAULT_IS_DOMMAGE_ACCIDENT = false;
    private static final Boolean UPDATED_IS_DOMMAGE_ACCIDENT = true;

    private static final BigDecimal DEFAULT_POURCENT_DOMMAGE_ACCIDENT = new BigDecimal(1);
    private static final BigDecimal UPDATED_POURCENT_DOMMAGE_ACCIDENT = new BigDecimal(2);

    private static final Boolean DEFAULT_IS_VOL_INCENDIE = false;
    private static final Boolean UPDATED_IS_VOL_INCENDIE = true;

    private static final BigDecimal DEFAULT_POURCENT_VOL_INCENDIE = new BigDecimal(1);
    private static final BigDecimal UPDATED_POURCENT_VOL_INCENDIE = new BigDecimal(2);

    private static final Boolean DEFAULT_IS_VOL_ELECTRONIC = false;
    private static final Boolean UPDATED_IS_VOL_ELECTRONIC = true;

    private static final BigDecimal DEFAULT_POURCENT_VOL_ELECTRONIC = new BigDecimal(1);
    private static final BigDecimal UPDATED_POURCENT_VOL_ELECTRONIC = new BigDecimal(2);

    private static final BigDecimal DEFAULT_ANNUEL_VOL_ELECTRONIC = new BigDecimal(1);
    private static final BigDecimal UPDATED_ANNUEL_VOL_ELECTRONIC = new BigDecimal(2);

    private static final Boolean DEFAULT_IS_AVANCE_SECOUR = false;
    private static final Boolean UPDATED_IS_AVANCE_SECOUR = true;

    private static final BigDecimal DEFAULT_POURCENT_AVANCE_SECOUR = new BigDecimal(1);
    private static final BigDecimal UPDATED_POURCENT_AVANCE_SECOUR = new BigDecimal(2);

    private static final BigDecimal DEFAULT_ANNUEL_AVANCE_SECOUR = new BigDecimal(1);
    private static final BigDecimal UPDATED_ANNUEL_AVANCE_SECOUR = new BigDecimal(2);

    private static final Boolean DEFAULT_IS_ASSISTANCE = false;
    private static final Boolean UPDATED_IS_ASSISTANCE = true;

    private static final BigDecimal DEFAULT_POURCENT_ASSISTANCE = new BigDecimal(1);
    private static final BigDecimal UPDATED_POURCENT_ASSISTANCE = new BigDecimal(2);

    private static final Boolean DEFAULT_IS_BRISE_GLACE = false;
    private static final Boolean UPDATED_IS_BRISE_GLACE = true;

    private static final BigDecimal DEFAULT_POURCENT_BRISE_GLACE = new BigDecimal(1);
    private static final BigDecimal UPDATED_POURCENT_BRISE_GLACE = new BigDecimal(2);

    private static final Boolean DEFAULT_IS_RECOUR_DEFENCE = false;
    private static final Boolean UPDATED_IS_RECOUR_DEFENCE = true;

    private static final BigDecimal DEFAULT_POURCENT_RECOUR_DEFENSE = new BigDecimal(1);
    private static final BigDecimal UPDATED_POURCENT_RECOUR_DEFENSE = new BigDecimal(2);

    private static final Boolean DEFAULT_IS_IND_ACC = false;
    private static final Boolean UPDATED_IS_IND_ACC = true;

    private static final BigDecimal DEFAULT_ANNUEL_IND_ACC = new BigDecimal(1);
    private static final BigDecimal UPDATED_ANNUEL_IND_ACC = new BigDecimal(2);

    private static final Boolean DEFAULT_IS_IPT_INVALID = false;
    private static final Boolean UPDATED_IS_IPT_INVALID = true;

    private static final Boolean DEFAULT_IS_IPT_FMPH = false;
    private static final Boolean UPDATED_IS_IPT_FMPH = true;

    private static final BigDecimal DEFAULT_IPT_DECES_CAPITAUX = new BigDecimal(1);
    private static final BigDecimal UPDATED_IPT_DECES_CAPITAUX = new BigDecimal(2);

    private static final BigDecimal DEFAULT_IPT_INVALID_CAPITAUX = new BigDecimal(1);
    private static final BigDecimal UPDATED_IPT_INVALID_CAPITAUX = new BigDecimal(2);

    private static final BigDecimal DEFAULT_IPT_FMPH_CAPITAUX = new BigDecimal(1);
    private static final BigDecimal UPDATED_IPT_FMPH_CAPITAUX = new BigDecimal(2);

    private static final BigDecimal DEFAULT_IPT_DECES_ANNUEL = new BigDecimal(1);
    private static final BigDecimal UPDATED_IPT_DECES_ANNUEL = new BigDecimal(2);

    private static final BigDecimal DEFAULT_IPT_INVALID_ANNUEL = new BigDecimal(1);
    private static final BigDecimal UPDATED_IPT_INVALID_ANNUEL = new BigDecimal(2);

    private static final BigDecimal DEFAULT_IPT_FMPH_ANNUEL = new BigDecimal(1);
    private static final BigDecimal UPDATED_IPT_FMPH_ANNUEL = new BigDecimal(2);

    private static final BigDecimal DEFAULT_BONUS = new BigDecimal(1);
    private static final BigDecimal UPDATED_BONUS = new BigDecimal(2);

    private static final BigDecimal DEFAULT_SURPLUS = new BigDecimal(1);
    private static final BigDecimal UPDATED_SURPLUS = new BigDecimal(2);
    private static final String DEFAULT_NUMERO_CARTE_ROSE = "AAAAA";
    private static final String UPDATED_NUMERO_CARTE_ROSE = "BBBBB";
    private static final String DEFAULT_NUMERO_ATTESTATION = "AAAAA";
    private static final String UPDATED_NUMERO_ATTESTATION = "BBBBB";

    private static final BigDecimal DEFAULT_AVENANT = new BigDecimal(1);
    private static final BigDecimal UPDATED_AVENANT = new BigDecimal(2);

    private static final BigDecimal DEFAULT_ID_PREV = new BigDecimal(1);
    private static final BigDecimal UPDATED_ID_PREV = new BigDecimal(2);

    private static final BigDecimal DEFAULT_ID_NEXT = new BigDecimal(1);
    private static final BigDecimal UPDATED_ID_NEXT = new BigDecimal(2);

    private static final BigDecimal DEFAULT_NETTE = new BigDecimal(1);
    private static final BigDecimal UPDATED_NETTE = new BigDecimal(2);

    private static final BigDecimal DEFAULT_TVA = new BigDecimal(1);
    private static final BigDecimal UPDATED_TVA = new BigDecimal(2);

    private static final Boolean DEFAULT_ETAT = false;
    private static final Boolean UPDATED_ETAT = true;

    @Inject
    private ContratRepository contratRepository;

    @Inject
    private ContratService contratService;

    @Inject
    private ContratSearchRepository contratSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restContratMockMvc;

    private Contrat contrat;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ContratResource contratResource = new ContratResource();
        ReflectionTestUtils.setField(contratResource, "contratService", contratService);
        this.restContratMockMvc = MockMvcBuilders.standaloneSetup(contratResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Contrat createEntity(EntityManager em) {
        Contrat contrat = new Contrat();
        contrat = new Contrat()
                .numeroPolice(DEFAULT_NUMERO_POLICE)
                .dateEffet(DEFAULT_DATE_EFFET)
                .duree(DEFAULT_DUREE)
                .souscripteur(DEFAULT_SOUSCRIPTEUR)
                .conducteur(DEFAULT_CONDUCTEUR)
                .statutSocio(DEFAULT_STATUT_SOCIO)
                .categoriePermis(DEFAULT_CATEGORIE_PERMIS)
                .primeTTC(DEFAULT_PRIME_TTC)
                .acompte(DEFAULT_ACOMPTE)
                .status(DEFAULT_STATUS)
                .dateFin(DEFAULT_DATE_FIN)
                .primeBase(DEFAULT_PRIME_BASE)
                .pourcentage(DEFAULT_POURCENTAGE)
                .accessoire(DEFAULT_ACCESSOIRE)
                .coutPolice(DEFAULT_COUT_POLICE)
                .asac(DEFAULT_ASAC)
                .carteRose(DEFAULT_CARTE_ROSE)
                .isDommageCollision(DEFAULT_IS_DOMMAGE_COLLISION)
                .pourcentDommageCollision(DEFAULT_POURCENT_DOMMAGE_COLLISION)
                .isDommageAccident(DEFAULT_IS_DOMMAGE_ACCIDENT)
                .pourcentDommageAccident(DEFAULT_POURCENT_DOMMAGE_ACCIDENT)
                .isVolIncendie(DEFAULT_IS_VOL_INCENDIE)
                .pourcentVolIncendie(DEFAULT_POURCENT_VOL_INCENDIE)
                .isVolElectronic(DEFAULT_IS_VOL_ELECTRONIC)
                .pourcentVolElectronic(DEFAULT_POURCENT_VOL_ELECTRONIC)
                .annuelVolElectronic(DEFAULT_ANNUEL_VOL_ELECTRONIC)
                .isAvanceSecour(DEFAULT_IS_AVANCE_SECOUR)
                .pourcentAvanceSecour(DEFAULT_POURCENT_AVANCE_SECOUR)
                .annuelAvanceSecour(DEFAULT_ANNUEL_AVANCE_SECOUR)
                .isAssistance(DEFAULT_IS_ASSISTANCE)
                .pourcentAssistance(DEFAULT_POURCENT_ASSISTANCE)
                .isBriseGlace(DEFAULT_IS_BRISE_GLACE)
                .pourcentBriseGlace(DEFAULT_POURCENT_BRISE_GLACE)
                .isRecourDefence(DEFAULT_IS_RECOUR_DEFENCE)
                .pourcentRecourDefense(DEFAULT_POURCENT_RECOUR_DEFENSE)
                .isIndAcc(DEFAULT_IS_IND_ACC)
                .annuelIndAcc(DEFAULT_ANNUEL_IND_ACC)
                .isIptInvalid(DEFAULT_IS_IPT_INVALID)
                .isIptFMPH(DEFAULT_IS_IPT_FMPH)
                .iptDecesCapitaux(DEFAULT_IPT_DECES_CAPITAUX)
                .iptInvalidCapitaux(DEFAULT_IPT_INVALID_CAPITAUX)
                .iptFMPHCapitaux(DEFAULT_IPT_FMPH_CAPITAUX)
                .iptDecesAnnuel(DEFAULT_IPT_DECES_ANNUEL)
                .iptInvalidAnnuel(DEFAULT_IPT_INVALID_ANNUEL)
                .iptFMPHAnnuel(DEFAULT_IPT_FMPH_ANNUEL)
                .bonus(DEFAULT_BONUS)
                .surplus(DEFAULT_SURPLUS)
                .numeroCarteRose(DEFAULT_NUMERO_CARTE_ROSE)
                .numeroAttestation(DEFAULT_NUMERO_ATTESTATION)
                .avenant(DEFAULT_AVENANT)
                .idPrev(DEFAULT_ID_PREV)
                .idNext(DEFAULT_ID_NEXT)
                .nette(DEFAULT_NETTE)
                .tva(DEFAULT_TVA)
                .etat(DEFAULT_ETAT);
        return contrat;
    }

    @Before
    public void initTest() {
        contratSearchRepository.deleteAll();
        contrat = createEntity(em);
    }

    @Test
    @Transactional
    public void createContrat() throws Exception {
        int databaseSizeBeforeCreate = contratRepository.findAll().size();

        // Create the Contrat

        restContratMockMvc.perform(post("/api/contrats")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(contrat)))
                .andExpect(status().isCreated());

        // Validate the Contrat in the database
        List<Contrat> contrats = contratRepository.findAll();
        assertThat(contrats).hasSize(databaseSizeBeforeCreate + 1);
        Contrat testContrat = contrats.get(contrats.size() - 1);
        assertThat(testContrat.getNumeroPolice()).isEqualTo(DEFAULT_NUMERO_POLICE);
        assertThat(testContrat.getDateEffet()).isEqualTo(DEFAULT_DATE_EFFET);
        assertThat(testContrat.getDuree()).isEqualTo(DEFAULT_DUREE);
        assertThat(testContrat.getSouscripteur()).isEqualTo(DEFAULT_SOUSCRIPTEUR);
        assertThat(testContrat.getConducteur()).isEqualTo(DEFAULT_CONDUCTEUR);
        assertThat(testContrat.getStatutSocio()).isEqualTo(DEFAULT_STATUT_SOCIO);
        assertThat(testContrat.getCategoriePermis()).isEqualTo(DEFAULT_CATEGORIE_PERMIS);
        assertThat(testContrat.getPrimeTTC()).isEqualTo(DEFAULT_PRIME_TTC);
        assertThat(testContrat.getAcompte()).isEqualTo(DEFAULT_ACOMPTE);
        assertThat(testContrat.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testContrat.getDateFin()).isEqualTo(DEFAULT_DATE_FIN);
        assertThat(testContrat.getPrimeBase()).isEqualTo(DEFAULT_PRIME_BASE);
        assertThat(testContrat.getPourcentage()).isEqualTo(DEFAULT_POURCENTAGE);
        assertThat(testContrat.getAccessoire()).isEqualTo(DEFAULT_ACCESSOIRE);
        assertThat(testContrat.getCoutPolice()).isEqualTo(DEFAULT_COUT_POLICE);
        assertThat(testContrat.getAsac()).isEqualTo(DEFAULT_ASAC);
        assertThat(testContrat.getCarteRose()).isEqualTo(DEFAULT_CARTE_ROSE);
        assertThat(testContrat.isIsDommageCollision()).isEqualTo(DEFAULT_IS_DOMMAGE_COLLISION);
        assertThat(testContrat.getPourcentDommageCollision()).isEqualTo(DEFAULT_POURCENT_DOMMAGE_COLLISION);
        assertThat(testContrat.isIsDommageAccident()).isEqualTo(DEFAULT_IS_DOMMAGE_ACCIDENT);
        assertThat(testContrat.getPourcentDommageAccident()).isEqualTo(DEFAULT_POURCENT_DOMMAGE_ACCIDENT);
        assertThat(testContrat.isIsVolIncendie()).isEqualTo(DEFAULT_IS_VOL_INCENDIE);
        assertThat(testContrat.getPourcentVolIncendie()).isEqualTo(DEFAULT_POURCENT_VOL_INCENDIE);
        assertThat(testContrat.isIsVolElectronic()).isEqualTo(DEFAULT_IS_VOL_ELECTRONIC);
        assertThat(testContrat.getPourcentVolElectronic()).isEqualTo(DEFAULT_POURCENT_VOL_ELECTRONIC);
        assertThat(testContrat.getAnnuelVolElectronic()).isEqualTo(DEFAULT_ANNUEL_VOL_ELECTRONIC);
        assertThat(testContrat.isIsAvanceSecour()).isEqualTo(DEFAULT_IS_AVANCE_SECOUR);
        assertThat(testContrat.getPourcentAvanceSecour()).isEqualTo(DEFAULT_POURCENT_AVANCE_SECOUR);
        assertThat(testContrat.getAnnuelAvanceSecour()).isEqualTo(DEFAULT_ANNUEL_AVANCE_SECOUR);
        assertThat(testContrat.isIsAssistance()).isEqualTo(DEFAULT_IS_ASSISTANCE);
        assertThat(testContrat.getPourcentAssistance()).isEqualTo(DEFAULT_POURCENT_ASSISTANCE);
        assertThat(testContrat.isIsBriseGlace()).isEqualTo(DEFAULT_IS_BRISE_GLACE);
        assertThat(testContrat.getPourcentBriseGlace()).isEqualTo(DEFAULT_POURCENT_BRISE_GLACE);
        assertThat(testContrat.isIsRecourDefence()).isEqualTo(DEFAULT_IS_RECOUR_DEFENCE);
        assertThat(testContrat.getPourcentRecourDefense()).isEqualTo(DEFAULT_POURCENT_RECOUR_DEFENSE);
        assertThat(testContrat.isIsIndAcc()).isEqualTo(DEFAULT_IS_IND_ACC);
        assertThat(testContrat.getAnnuelIndAcc()).isEqualTo(DEFAULT_ANNUEL_IND_ACC);
        assertThat(testContrat.isIsIptInvalid()).isEqualTo(DEFAULT_IS_IPT_INVALID);
        assertThat(testContrat.isIsIptFMPH()).isEqualTo(DEFAULT_IS_IPT_FMPH);
        assertThat(testContrat.getIptDecesCapitaux()).isEqualTo(DEFAULT_IPT_DECES_CAPITAUX);
        assertThat(testContrat.getIptInvalidCapitaux()).isEqualTo(DEFAULT_IPT_INVALID_CAPITAUX);
        assertThat(testContrat.getIptFMPHCapitaux()).isEqualTo(DEFAULT_IPT_FMPH_CAPITAUX);
        assertThat(testContrat.getIptDecesAnnuel()).isEqualTo(DEFAULT_IPT_DECES_ANNUEL);
        assertThat(testContrat.getIptInvalidAnnuel()).isEqualTo(DEFAULT_IPT_INVALID_ANNUEL);
        assertThat(testContrat.getIptFMPHAnnuel()).isEqualTo(DEFAULT_IPT_FMPH_ANNUEL);
        assertThat(testContrat.getBonus()).isEqualTo(DEFAULT_BONUS);
        assertThat(testContrat.getSurplus()).isEqualTo(DEFAULT_SURPLUS);
        assertThat(testContrat.getNumeroCarteRose()).isEqualTo(DEFAULT_NUMERO_CARTE_ROSE);
        assertThat(testContrat.getNumeroAttestation()).isEqualTo(DEFAULT_NUMERO_ATTESTATION);
        assertThat(testContrat.getAvenant()).isEqualTo(DEFAULT_AVENANT);
        assertThat(testContrat.getIdPrev()).isEqualTo(DEFAULT_ID_PREV);
        assertThat(testContrat.getIdNext()).isEqualTo(DEFAULT_ID_NEXT);
        assertThat(testContrat.getNette()).isEqualTo(DEFAULT_NETTE);
        assertThat(testContrat.getTva()).isEqualTo(DEFAULT_TVA);
        assertThat(testContrat.isEtat()).isEqualTo(DEFAULT_ETAT);

        // Validate the Contrat in ElasticSearch
        Contrat contratEs = contratSearchRepository.findOne(testContrat.getId());
        assertThat(contratEs).isEqualToComparingFieldByField(testContrat);
    }

    @Test
    @Transactional
    public void getAllContrats() throws Exception {
        // Initialize the database
        contratRepository.saveAndFlush(contrat);

        // Get all the contrats
        restContratMockMvc.perform(get("/api/contrats?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(contrat.getId().intValue())))
                .andExpect(jsonPath("$.[*].numeroPolice").value(hasItem(DEFAULT_NUMERO_POLICE.toString())))
                .andExpect(jsonPath("$.[*].dateEffet").value(hasItem(DEFAULT_DATE_EFFET.toString())))
                .andExpect(jsonPath("$.[*].duree").value(hasItem(DEFAULT_DUREE.intValue())))
                .andExpect(jsonPath("$.[*].souscripteur").value(hasItem(DEFAULT_SOUSCRIPTEUR.toString())))
                .andExpect(jsonPath("$.[*].conducteur").value(hasItem(DEFAULT_CONDUCTEUR.toString())))
                .andExpect(jsonPath("$.[*].statutSocio").value(hasItem(DEFAULT_STATUT_SOCIO.toString())))
                .andExpect(jsonPath("$.[*].categoriePermis").value(hasItem(DEFAULT_CATEGORIE_PERMIS.toString())))
                .andExpect(jsonPath("$.[*].primeTTC").value(hasItem(DEFAULT_PRIME_TTC.intValue())))
                .andExpect(jsonPath("$.[*].acompte").value(hasItem(DEFAULT_ACOMPTE.intValue())))
                .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
                .andExpect(jsonPath("$.[*].dateFin").value(hasItem(DEFAULT_DATE_FIN.toString())))
                .andExpect(jsonPath("$.[*].primeBase").value(hasItem(DEFAULT_PRIME_BASE.intValue())))
                .andExpect(jsonPath("$.[*].pourcentage").value(hasItem(DEFAULT_POURCENTAGE.intValue())))
                .andExpect(jsonPath("$.[*].accessoire").value(hasItem(DEFAULT_ACCESSOIRE.intValue())))
                .andExpect(jsonPath("$.[*].coutPolice").value(hasItem(DEFAULT_COUT_POLICE.intValue())))
                .andExpect(jsonPath("$.[*].asac").value(hasItem(DEFAULT_ASAC.intValue())))
                .andExpect(jsonPath("$.[*].carteRose").value(hasItem(DEFAULT_CARTE_ROSE.intValue())))
                .andExpect(jsonPath("$.[*].isDommageCollision").value(hasItem(DEFAULT_IS_DOMMAGE_COLLISION.booleanValue())))
                .andExpect(jsonPath("$.[*].pourcentDommageCollision").value(hasItem(DEFAULT_POURCENT_DOMMAGE_COLLISION.intValue())))
                .andExpect(jsonPath("$.[*].isDommageAccident").value(hasItem(DEFAULT_IS_DOMMAGE_ACCIDENT.booleanValue())))
                .andExpect(jsonPath("$.[*].pourcentDommageAccident").value(hasItem(DEFAULT_POURCENT_DOMMAGE_ACCIDENT.intValue())))
                .andExpect(jsonPath("$.[*].isVolIncendie").value(hasItem(DEFAULT_IS_VOL_INCENDIE.booleanValue())))
                .andExpect(jsonPath("$.[*].pourcentVolIncendie").value(hasItem(DEFAULT_POURCENT_VOL_INCENDIE.intValue())))
                .andExpect(jsonPath("$.[*].isVolElectronic").value(hasItem(DEFAULT_IS_VOL_ELECTRONIC.booleanValue())))
                .andExpect(jsonPath("$.[*].pourcentVolElectronic").value(hasItem(DEFAULT_POURCENT_VOL_ELECTRONIC.intValue())))
                .andExpect(jsonPath("$.[*].annuelVolElectronic").value(hasItem(DEFAULT_ANNUEL_VOL_ELECTRONIC.intValue())))
                .andExpect(jsonPath("$.[*].isAvanceSecour").value(hasItem(DEFAULT_IS_AVANCE_SECOUR.booleanValue())))
                .andExpect(jsonPath("$.[*].pourcentAvanceSecour").value(hasItem(DEFAULT_POURCENT_AVANCE_SECOUR.intValue())))
                .andExpect(jsonPath("$.[*].annuelAvanceSecour").value(hasItem(DEFAULT_ANNUEL_AVANCE_SECOUR.intValue())))
                .andExpect(jsonPath("$.[*].isAssistance").value(hasItem(DEFAULT_IS_ASSISTANCE.booleanValue())))
                .andExpect(jsonPath("$.[*].pourcentAssistance").value(hasItem(DEFAULT_POURCENT_ASSISTANCE.intValue())))
                .andExpect(jsonPath("$.[*].isBriseGlace").value(hasItem(DEFAULT_IS_BRISE_GLACE.booleanValue())))
                .andExpect(jsonPath("$.[*].pourcentBriseGlace").value(hasItem(DEFAULT_POURCENT_BRISE_GLACE.intValue())))
                .andExpect(jsonPath("$.[*].isRecourDefence").value(hasItem(DEFAULT_IS_RECOUR_DEFENCE.booleanValue())))
                .andExpect(jsonPath("$.[*].pourcentRecourDefense").value(hasItem(DEFAULT_POURCENT_RECOUR_DEFENSE.intValue())))
                .andExpect(jsonPath("$.[*].isIndAcc").value(hasItem(DEFAULT_IS_IND_ACC.booleanValue())))
                .andExpect(jsonPath("$.[*].annuelIndAcc").value(hasItem(DEFAULT_ANNUEL_IND_ACC.intValue())))
                .andExpect(jsonPath("$.[*].isIptInvalid").value(hasItem(DEFAULT_IS_IPT_INVALID.booleanValue())))
                .andExpect(jsonPath("$.[*].isIptFMPH").value(hasItem(DEFAULT_IS_IPT_FMPH.booleanValue())))
                .andExpect(jsonPath("$.[*].iptDecesCapitaux").value(hasItem(DEFAULT_IPT_DECES_CAPITAUX.intValue())))
                .andExpect(jsonPath("$.[*].iptInvalidCapitaux").value(hasItem(DEFAULT_IPT_INVALID_CAPITAUX.intValue())))
                .andExpect(jsonPath("$.[*].iptFMPHCapitaux").value(hasItem(DEFAULT_IPT_FMPH_CAPITAUX.intValue())))
                .andExpect(jsonPath("$.[*].iptDecesAnnuel").value(hasItem(DEFAULT_IPT_DECES_ANNUEL.intValue())))
                .andExpect(jsonPath("$.[*].iptInvalidAnnuel").value(hasItem(DEFAULT_IPT_INVALID_ANNUEL.intValue())))
                .andExpect(jsonPath("$.[*].iptFMPHAnnuel").value(hasItem(DEFAULT_IPT_FMPH_ANNUEL.intValue())))
                .andExpect(jsonPath("$.[*].bonus").value(hasItem(DEFAULT_BONUS.intValue())))
                .andExpect(jsonPath("$.[*].surplus").value(hasItem(DEFAULT_SURPLUS.intValue())))
                .andExpect(jsonPath("$.[*].numeroCarteRose").value(hasItem(DEFAULT_NUMERO_CARTE_ROSE.toString())))
                .andExpect(jsonPath("$.[*].numeroAttestation").value(hasItem(DEFAULT_NUMERO_ATTESTATION.toString())))
                .andExpect(jsonPath("$.[*].avenant").value(hasItem(DEFAULT_AVENANT.intValue())))
                .andExpect(jsonPath("$.[*].idPrev").value(hasItem(DEFAULT_ID_PREV.intValue())))
                .andExpect(jsonPath("$.[*].idNext").value(hasItem(DEFAULT_ID_NEXT.intValue())))
                .andExpect(jsonPath("$.[*].nette").value(hasItem(DEFAULT_NETTE.intValue())))
                .andExpect(jsonPath("$.[*].tva").value(hasItem(DEFAULT_TVA.intValue())))
                .andExpect(jsonPath("$.[*].etat").value(hasItem(DEFAULT_ETAT.booleanValue())));
    }

    @Test
    @Transactional
    public void getContrat() throws Exception {
        // Initialize the database
        contratRepository.saveAndFlush(contrat);

        // Get the contrat
        restContratMockMvc.perform(get("/api/contrats/{id}", contrat.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(contrat.getId().intValue()))
            .andExpect(jsonPath("$.numeroPolice").value(DEFAULT_NUMERO_POLICE.toString()))
            .andExpect(jsonPath("$.dateEffet").value(DEFAULT_DATE_EFFET.toString()))
            .andExpect(jsonPath("$.duree").value(DEFAULT_DUREE.intValue()))
            .andExpect(jsonPath("$.souscripteur").value(DEFAULT_SOUSCRIPTEUR.toString()))
            .andExpect(jsonPath("$.conducteur").value(DEFAULT_CONDUCTEUR.toString()))
            .andExpect(jsonPath("$.statutSocio").value(DEFAULT_STATUT_SOCIO.toString()))
            .andExpect(jsonPath("$.categoriePermis").value(DEFAULT_CATEGORIE_PERMIS.toString()))
            .andExpect(jsonPath("$.primeTTC").value(DEFAULT_PRIME_TTC.intValue()))
            .andExpect(jsonPath("$.acompte").value(DEFAULT_ACOMPTE.intValue()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.dateFin").value(DEFAULT_DATE_FIN.toString()))
            .andExpect(jsonPath("$.primeBase").value(DEFAULT_PRIME_BASE.intValue()))
            .andExpect(jsonPath("$.pourcentage").value(DEFAULT_POURCENTAGE.intValue()))
            .andExpect(jsonPath("$.accessoire").value(DEFAULT_ACCESSOIRE.intValue()))
            .andExpect(jsonPath("$.coutPolice").value(DEFAULT_COUT_POLICE.intValue()))
            .andExpect(jsonPath("$.asac").value(DEFAULT_ASAC.intValue()))
            .andExpect(jsonPath("$.carteRose").value(DEFAULT_CARTE_ROSE.intValue()))
            .andExpect(jsonPath("$.isDommageCollision").value(DEFAULT_IS_DOMMAGE_COLLISION.booleanValue()))
            .andExpect(jsonPath("$.pourcentDommageCollision").value(DEFAULT_POURCENT_DOMMAGE_COLLISION.intValue()))
            .andExpect(jsonPath("$.isDommageAccident").value(DEFAULT_IS_DOMMAGE_ACCIDENT.booleanValue()))
            .andExpect(jsonPath("$.pourcentDommageAccident").value(DEFAULT_POURCENT_DOMMAGE_ACCIDENT.intValue()))
            .andExpect(jsonPath("$.isVolIncendie").value(DEFAULT_IS_VOL_INCENDIE.booleanValue()))
            .andExpect(jsonPath("$.pourcentVolIncendie").value(DEFAULT_POURCENT_VOL_INCENDIE.intValue()))
            .andExpect(jsonPath("$.isVolElectronic").value(DEFAULT_IS_VOL_ELECTRONIC.booleanValue()))
            .andExpect(jsonPath("$.pourcentVolElectronic").value(DEFAULT_POURCENT_VOL_ELECTRONIC.intValue()))
            .andExpect(jsonPath("$.annuelVolElectronic").value(DEFAULT_ANNUEL_VOL_ELECTRONIC.intValue()))
            .andExpect(jsonPath("$.isAvanceSecour").value(DEFAULT_IS_AVANCE_SECOUR.booleanValue()))
            .andExpect(jsonPath("$.pourcentAvanceSecour").value(DEFAULT_POURCENT_AVANCE_SECOUR.intValue()))
            .andExpect(jsonPath("$.annuelAvanceSecour").value(DEFAULT_ANNUEL_AVANCE_SECOUR.intValue()))
            .andExpect(jsonPath("$.isAssistance").value(DEFAULT_IS_ASSISTANCE.booleanValue()))
            .andExpect(jsonPath("$.pourcentAssistance").value(DEFAULT_POURCENT_ASSISTANCE.intValue()))
            .andExpect(jsonPath("$.isBriseGlace").value(DEFAULT_IS_BRISE_GLACE.booleanValue()))
            .andExpect(jsonPath("$.pourcentBriseGlace").value(DEFAULT_POURCENT_BRISE_GLACE.intValue()))
            .andExpect(jsonPath("$.isRecourDefence").value(DEFAULT_IS_RECOUR_DEFENCE.booleanValue()))
            .andExpect(jsonPath("$.pourcentRecourDefense").value(DEFAULT_POURCENT_RECOUR_DEFENSE.intValue()))
            .andExpect(jsonPath("$.isIndAcc").value(DEFAULT_IS_IND_ACC.booleanValue()))
            .andExpect(jsonPath("$.annuelIndAcc").value(DEFAULT_ANNUEL_IND_ACC.intValue()))
            .andExpect(jsonPath("$.isIptInvalid").value(DEFAULT_IS_IPT_INVALID.booleanValue()))
            .andExpect(jsonPath("$.isIptFMPH").value(DEFAULT_IS_IPT_FMPH.booleanValue()))
            .andExpect(jsonPath("$.iptDecesCapitaux").value(DEFAULT_IPT_DECES_CAPITAUX.intValue()))
            .andExpect(jsonPath("$.iptInvalidCapitaux").value(DEFAULT_IPT_INVALID_CAPITAUX.intValue()))
            .andExpect(jsonPath("$.iptFMPHCapitaux").value(DEFAULT_IPT_FMPH_CAPITAUX.intValue()))
            .andExpect(jsonPath("$.iptDecesAnnuel").value(DEFAULT_IPT_DECES_ANNUEL.intValue()))
            .andExpect(jsonPath("$.iptInvalidAnnuel").value(DEFAULT_IPT_INVALID_ANNUEL.intValue()))
            .andExpect(jsonPath("$.iptFMPHAnnuel").value(DEFAULT_IPT_FMPH_ANNUEL.intValue()))
            .andExpect(jsonPath("$.bonus").value(DEFAULT_BONUS.intValue()))
            .andExpect(jsonPath("$.surplus").value(DEFAULT_SURPLUS.intValue()))
            .andExpect(jsonPath("$.numeroCarteRose").value(DEFAULT_NUMERO_CARTE_ROSE.toString()))
            .andExpect(jsonPath("$.numeroAttestation").value(DEFAULT_NUMERO_ATTESTATION.toString()))
            .andExpect(jsonPath("$.avenant").value(DEFAULT_AVENANT.intValue()))
            .andExpect(jsonPath("$.idPrev").value(DEFAULT_ID_PREV.intValue()))
            .andExpect(jsonPath("$.idNext").value(DEFAULT_ID_NEXT.intValue()))
            .andExpect(jsonPath("$.nette").value(DEFAULT_NETTE.intValue()))
            .andExpect(jsonPath("$.tva").value(DEFAULT_TVA.intValue()))
            .andExpect(jsonPath("$.etat").value(DEFAULT_ETAT.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingContrat() throws Exception {
        // Get the contrat
        restContratMockMvc.perform(get("/api/contrats/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateContrat() throws Exception {
        // Initialize the database
        contratService.save(contrat);

        int databaseSizeBeforeUpdate = contratRepository.findAll().size();

        // Update the contrat
        Contrat updatedContrat = contratRepository.findOne(contrat.getId());
        updatedContrat
                .numeroPolice(UPDATED_NUMERO_POLICE)
                .dateEffet(UPDATED_DATE_EFFET)
                .duree(UPDATED_DUREE)
                .souscripteur(UPDATED_SOUSCRIPTEUR)
                .conducteur(UPDATED_CONDUCTEUR)
                .statutSocio(UPDATED_STATUT_SOCIO)
                .categoriePermis(UPDATED_CATEGORIE_PERMIS)
                .primeTTC(UPDATED_PRIME_TTC)
                .acompte(UPDATED_ACOMPTE)
                .status(UPDATED_STATUS)
                .dateFin(UPDATED_DATE_FIN)
                .primeBase(UPDATED_PRIME_BASE)
                .pourcentage(UPDATED_POURCENTAGE)
                .accessoire(UPDATED_ACCESSOIRE)
                .coutPolice(UPDATED_COUT_POLICE)
                .asac(UPDATED_ASAC)
                .carteRose(UPDATED_CARTE_ROSE)
                .isDommageCollision(UPDATED_IS_DOMMAGE_COLLISION)
                .pourcentDommageCollision(UPDATED_POURCENT_DOMMAGE_COLLISION)
                .isDommageAccident(UPDATED_IS_DOMMAGE_ACCIDENT)
                .pourcentDommageAccident(UPDATED_POURCENT_DOMMAGE_ACCIDENT)
                .isVolIncendie(UPDATED_IS_VOL_INCENDIE)
                .pourcentVolIncendie(UPDATED_POURCENT_VOL_INCENDIE)
                .isVolElectronic(UPDATED_IS_VOL_ELECTRONIC)
                .pourcentVolElectronic(UPDATED_POURCENT_VOL_ELECTRONIC)
                .annuelVolElectronic(UPDATED_ANNUEL_VOL_ELECTRONIC)
                .isAvanceSecour(UPDATED_IS_AVANCE_SECOUR)
                .pourcentAvanceSecour(UPDATED_POURCENT_AVANCE_SECOUR)
                .annuelAvanceSecour(UPDATED_ANNUEL_AVANCE_SECOUR)
                .isAssistance(UPDATED_IS_ASSISTANCE)
                .pourcentAssistance(UPDATED_POURCENT_ASSISTANCE)
                .isBriseGlace(UPDATED_IS_BRISE_GLACE)
                .pourcentBriseGlace(UPDATED_POURCENT_BRISE_GLACE)
                .isRecourDefence(UPDATED_IS_RECOUR_DEFENCE)
                .pourcentRecourDefense(UPDATED_POURCENT_RECOUR_DEFENSE)
                .isIndAcc(UPDATED_IS_IND_ACC)
                .annuelIndAcc(UPDATED_ANNUEL_IND_ACC)
                .isIptInvalid(UPDATED_IS_IPT_INVALID)
                .isIptFMPH(UPDATED_IS_IPT_FMPH)
                .iptDecesCapitaux(UPDATED_IPT_DECES_CAPITAUX)
                .iptInvalidCapitaux(UPDATED_IPT_INVALID_CAPITAUX)
                .iptFMPHCapitaux(UPDATED_IPT_FMPH_CAPITAUX)
                .iptDecesAnnuel(UPDATED_IPT_DECES_ANNUEL)
                .iptInvalidAnnuel(UPDATED_IPT_INVALID_ANNUEL)
                .iptFMPHAnnuel(UPDATED_IPT_FMPH_ANNUEL)
                .bonus(UPDATED_BONUS)
                .surplus(UPDATED_SURPLUS)
                .numeroCarteRose(UPDATED_NUMERO_CARTE_ROSE)
                .numeroAttestation(UPDATED_NUMERO_ATTESTATION)
                .avenant(UPDATED_AVENANT)
                .idPrev(UPDATED_ID_PREV)
                .idNext(UPDATED_ID_NEXT)
                .nette(UPDATED_NETTE)
                .tva(UPDATED_TVA)
                .etat(UPDATED_ETAT);

        restContratMockMvc.perform(put("/api/contrats")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedContrat)))
                .andExpect(status().isOk());

        // Validate the Contrat in the database
        List<Contrat> contrats = contratRepository.findAll();
        assertThat(contrats).hasSize(databaseSizeBeforeUpdate);
        Contrat testContrat = contrats.get(contrats.size() - 1);
        assertThat(testContrat.getNumeroPolice()).isEqualTo(UPDATED_NUMERO_POLICE);
        assertThat(testContrat.getDateEffet()).isEqualTo(UPDATED_DATE_EFFET);
        assertThat(testContrat.getDuree()).isEqualTo(UPDATED_DUREE);
        assertThat(testContrat.getSouscripteur()).isEqualTo(UPDATED_SOUSCRIPTEUR);
        assertThat(testContrat.getConducteur()).isEqualTo(UPDATED_CONDUCTEUR);
        assertThat(testContrat.getStatutSocio()).isEqualTo(UPDATED_STATUT_SOCIO);
        assertThat(testContrat.getCategoriePermis()).isEqualTo(UPDATED_CATEGORIE_PERMIS);
        assertThat(testContrat.getPrimeTTC()).isEqualTo(UPDATED_PRIME_TTC);
        assertThat(testContrat.getAcompte()).isEqualTo(UPDATED_ACOMPTE);
        assertThat(testContrat.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testContrat.getDateFin()).isEqualTo(UPDATED_DATE_FIN);
        assertThat(testContrat.getPrimeBase()).isEqualTo(UPDATED_PRIME_BASE);
        assertThat(testContrat.getPourcentage()).isEqualTo(UPDATED_POURCENTAGE);
        assertThat(testContrat.getAccessoire()).isEqualTo(UPDATED_ACCESSOIRE);
        assertThat(testContrat.getCoutPolice()).isEqualTo(UPDATED_COUT_POLICE);
        assertThat(testContrat.getAsac()).isEqualTo(UPDATED_ASAC);
        assertThat(testContrat.getCarteRose()).isEqualTo(UPDATED_CARTE_ROSE);
        assertThat(testContrat.isIsDommageCollision()).isEqualTo(UPDATED_IS_DOMMAGE_COLLISION);
        assertThat(testContrat.getPourcentDommageCollision()).isEqualTo(UPDATED_POURCENT_DOMMAGE_COLLISION);
        assertThat(testContrat.isIsDommageAccident()).isEqualTo(UPDATED_IS_DOMMAGE_ACCIDENT);
        assertThat(testContrat.getPourcentDommageAccident()).isEqualTo(UPDATED_POURCENT_DOMMAGE_ACCIDENT);
        assertThat(testContrat.isIsVolIncendie()).isEqualTo(UPDATED_IS_VOL_INCENDIE);
        assertThat(testContrat.getPourcentVolIncendie()).isEqualTo(UPDATED_POURCENT_VOL_INCENDIE);
        assertThat(testContrat.isIsVolElectronic()).isEqualTo(UPDATED_IS_VOL_ELECTRONIC);
        assertThat(testContrat.getPourcentVolElectronic()).isEqualTo(UPDATED_POURCENT_VOL_ELECTRONIC);
        assertThat(testContrat.getAnnuelVolElectronic()).isEqualTo(UPDATED_ANNUEL_VOL_ELECTRONIC);
        assertThat(testContrat.isIsAvanceSecour()).isEqualTo(UPDATED_IS_AVANCE_SECOUR);
        assertThat(testContrat.getPourcentAvanceSecour()).isEqualTo(UPDATED_POURCENT_AVANCE_SECOUR);
        assertThat(testContrat.getAnnuelAvanceSecour()).isEqualTo(UPDATED_ANNUEL_AVANCE_SECOUR);
        assertThat(testContrat.isIsAssistance()).isEqualTo(UPDATED_IS_ASSISTANCE);
        assertThat(testContrat.getPourcentAssistance()).isEqualTo(UPDATED_POURCENT_ASSISTANCE);
        assertThat(testContrat.isIsBriseGlace()).isEqualTo(UPDATED_IS_BRISE_GLACE);
        assertThat(testContrat.getPourcentBriseGlace()).isEqualTo(UPDATED_POURCENT_BRISE_GLACE);
        assertThat(testContrat.isIsRecourDefence()).isEqualTo(UPDATED_IS_RECOUR_DEFENCE);
        assertThat(testContrat.getPourcentRecourDefense()).isEqualTo(UPDATED_POURCENT_RECOUR_DEFENSE);
        assertThat(testContrat.isIsIndAcc()).isEqualTo(UPDATED_IS_IND_ACC);
        assertThat(testContrat.getAnnuelIndAcc()).isEqualTo(UPDATED_ANNUEL_IND_ACC);
        assertThat(testContrat.isIsIptInvalid()).isEqualTo(UPDATED_IS_IPT_INVALID);
        assertThat(testContrat.isIsIptFMPH()).isEqualTo(UPDATED_IS_IPT_FMPH);
        assertThat(testContrat.getIptDecesCapitaux()).isEqualTo(UPDATED_IPT_DECES_CAPITAUX);
        assertThat(testContrat.getIptInvalidCapitaux()).isEqualTo(UPDATED_IPT_INVALID_CAPITAUX);
        assertThat(testContrat.getIptFMPHCapitaux()).isEqualTo(UPDATED_IPT_FMPH_CAPITAUX);
        assertThat(testContrat.getIptDecesAnnuel()).isEqualTo(UPDATED_IPT_DECES_ANNUEL);
        assertThat(testContrat.getIptInvalidAnnuel()).isEqualTo(UPDATED_IPT_INVALID_ANNUEL);
        assertThat(testContrat.getIptFMPHAnnuel()).isEqualTo(UPDATED_IPT_FMPH_ANNUEL);
        assertThat(testContrat.getBonus()).isEqualTo(UPDATED_BONUS);
        assertThat(testContrat.getSurplus()).isEqualTo(UPDATED_SURPLUS);
        assertThat(testContrat.getNumeroCarteRose()).isEqualTo(UPDATED_NUMERO_CARTE_ROSE);
        assertThat(testContrat.getNumeroAttestation()).isEqualTo(UPDATED_NUMERO_ATTESTATION);
        assertThat(testContrat.getAvenant()).isEqualTo(UPDATED_AVENANT);
        assertThat(testContrat.getIdPrev()).isEqualTo(UPDATED_ID_PREV);
        assertThat(testContrat.getIdNext()).isEqualTo(UPDATED_ID_NEXT);
        assertThat(testContrat.getNette()).isEqualTo(UPDATED_NETTE);
        assertThat(testContrat.getTva()).isEqualTo(UPDATED_TVA);
        assertThat(testContrat.isEtat()).isEqualTo(UPDATED_ETAT);

        // Validate the Contrat in ElasticSearch
        Contrat contratEs = contratSearchRepository.findOne(testContrat.getId());
        assertThat(contratEs).isEqualToComparingFieldByField(testContrat);
    }

    @Test
    @Transactional
    public void deleteContrat() throws Exception {
        // Initialize the database
        contratService.save(contrat);

        int databaseSizeBeforeDelete = contratRepository.findAll().size();

        // Get the contrat
        restContratMockMvc.perform(delete("/api/contrats/{id}", contrat.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean contratExistsInEs = contratSearchRepository.exists(contrat.getId());
        assertThat(contratExistsInEs).isFalse();

        // Validate the database is empty
        List<Contrat> contrats = contratRepository.findAll();
        assertThat(contrats).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchContrat() throws Exception {
        // Initialize the database
        contratService.save(contrat);

        // Search the contrat
        restContratMockMvc.perform(get("/api/_search/contrats?query=id:" + contrat.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(contrat.getId().intValue())))
            .andExpect(jsonPath("$.[*].numeroPolice").value(hasItem(DEFAULT_NUMERO_POLICE.toString())))
            .andExpect(jsonPath("$.[*].dateEffet").value(hasItem(DEFAULT_DATE_EFFET.toString())))
            .andExpect(jsonPath("$.[*].duree").value(hasItem(DEFAULT_DUREE.intValue())))
            .andExpect(jsonPath("$.[*].souscripteur").value(hasItem(DEFAULT_SOUSCRIPTEUR.toString())))
            .andExpect(jsonPath("$.[*].conducteur").value(hasItem(DEFAULT_CONDUCTEUR.toString())))
            .andExpect(jsonPath("$.[*].statutSocio").value(hasItem(DEFAULT_STATUT_SOCIO.toString())))
            .andExpect(jsonPath("$.[*].categoriePermis").value(hasItem(DEFAULT_CATEGORIE_PERMIS.toString())))
            .andExpect(jsonPath("$.[*].primeTTC").value(hasItem(DEFAULT_PRIME_TTC.intValue())))
            .andExpect(jsonPath("$.[*].acompte").value(hasItem(DEFAULT_ACOMPTE.intValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].dateFin").value(hasItem(DEFAULT_DATE_FIN.toString())))
            .andExpect(jsonPath("$.[*].primeBase").value(hasItem(DEFAULT_PRIME_BASE.intValue())))
            .andExpect(jsonPath("$.[*].pourcentage").value(hasItem(DEFAULT_POURCENTAGE.intValue())))
            .andExpect(jsonPath("$.[*].accessoire").value(hasItem(DEFAULT_ACCESSOIRE.intValue())))
            .andExpect(jsonPath("$.[*].coutPolice").value(hasItem(DEFAULT_COUT_POLICE.intValue())))
            .andExpect(jsonPath("$.[*].asac").value(hasItem(DEFAULT_ASAC.intValue())))
            .andExpect(jsonPath("$.[*].carteRose").value(hasItem(DEFAULT_CARTE_ROSE.intValue())))
            .andExpect(jsonPath("$.[*].isDommageCollision").value(hasItem(DEFAULT_IS_DOMMAGE_COLLISION.booleanValue())))
            .andExpect(jsonPath("$.[*].pourcentDommageCollision").value(hasItem(DEFAULT_POURCENT_DOMMAGE_COLLISION.intValue())))
            .andExpect(jsonPath("$.[*].isDommageAccident").value(hasItem(DEFAULT_IS_DOMMAGE_ACCIDENT.booleanValue())))
            .andExpect(jsonPath("$.[*].pourcentDommageAccident").value(hasItem(DEFAULT_POURCENT_DOMMAGE_ACCIDENT.intValue())))
            .andExpect(jsonPath("$.[*].isVolIncendie").value(hasItem(DEFAULT_IS_VOL_INCENDIE.booleanValue())))
            .andExpect(jsonPath("$.[*].pourcentVolIncendie").value(hasItem(DEFAULT_POURCENT_VOL_INCENDIE.intValue())))
            .andExpect(jsonPath("$.[*].isVolElectronic").value(hasItem(DEFAULT_IS_VOL_ELECTRONIC.booleanValue())))
            .andExpect(jsonPath("$.[*].pourcentVolElectronic").value(hasItem(DEFAULT_POURCENT_VOL_ELECTRONIC.intValue())))
            .andExpect(jsonPath("$.[*].annuelVolElectronic").value(hasItem(DEFAULT_ANNUEL_VOL_ELECTRONIC.intValue())))
            .andExpect(jsonPath("$.[*].isAvanceSecour").value(hasItem(DEFAULT_IS_AVANCE_SECOUR.booleanValue())))
            .andExpect(jsonPath("$.[*].pourcentAvanceSecour").value(hasItem(DEFAULT_POURCENT_AVANCE_SECOUR.intValue())))
            .andExpect(jsonPath("$.[*].annuelAvanceSecour").value(hasItem(DEFAULT_ANNUEL_AVANCE_SECOUR.intValue())))
            .andExpect(jsonPath("$.[*].isAssistance").value(hasItem(DEFAULT_IS_ASSISTANCE.booleanValue())))
            .andExpect(jsonPath("$.[*].pourcentAssistance").value(hasItem(DEFAULT_POURCENT_ASSISTANCE.intValue())))
            .andExpect(jsonPath("$.[*].isBriseGlace").value(hasItem(DEFAULT_IS_BRISE_GLACE.booleanValue())))
            .andExpect(jsonPath("$.[*].pourcentBriseGlace").value(hasItem(DEFAULT_POURCENT_BRISE_GLACE.intValue())))
            .andExpect(jsonPath("$.[*].isRecourDefence").value(hasItem(DEFAULT_IS_RECOUR_DEFENCE.booleanValue())))
            .andExpect(jsonPath("$.[*].pourcentRecourDefense").value(hasItem(DEFAULT_POURCENT_RECOUR_DEFENSE.intValue())))
            .andExpect(jsonPath("$.[*].isIndAcc").value(hasItem(DEFAULT_IS_IND_ACC.booleanValue())))
            .andExpect(jsonPath("$.[*].annuelIndAcc").value(hasItem(DEFAULT_ANNUEL_IND_ACC.intValue())))
            .andExpect(jsonPath("$.[*].isIptInvalid").value(hasItem(DEFAULT_IS_IPT_INVALID.booleanValue())))
            .andExpect(jsonPath("$.[*].isIptFMPH").value(hasItem(DEFAULT_IS_IPT_FMPH.booleanValue())))
            .andExpect(jsonPath("$.[*].iptDecesCapitaux").value(hasItem(DEFAULT_IPT_DECES_CAPITAUX.intValue())))
            .andExpect(jsonPath("$.[*].iptInvalidCapitaux").value(hasItem(DEFAULT_IPT_INVALID_CAPITAUX.intValue())))
            .andExpect(jsonPath("$.[*].iptFMPHCapitaux").value(hasItem(DEFAULT_IPT_FMPH_CAPITAUX.intValue())))
            .andExpect(jsonPath("$.[*].iptDecesAnnuel").value(hasItem(DEFAULT_IPT_DECES_ANNUEL.intValue())))
            .andExpect(jsonPath("$.[*].iptInvalidAnnuel").value(hasItem(DEFAULT_IPT_INVALID_ANNUEL.intValue())))
            .andExpect(jsonPath("$.[*].iptFMPHAnnuel").value(hasItem(DEFAULT_IPT_FMPH_ANNUEL.intValue())))
            .andExpect(jsonPath("$.[*].bonus").value(hasItem(DEFAULT_BONUS.intValue())))
            .andExpect(jsonPath("$.[*].surplus").value(hasItem(DEFAULT_SURPLUS.intValue())))
            .andExpect(jsonPath("$.[*].numeroCarteRose").value(hasItem(DEFAULT_NUMERO_CARTE_ROSE.toString())))
            .andExpect(jsonPath("$.[*].numeroAttestation").value(hasItem(DEFAULT_NUMERO_ATTESTATION.toString())))
            .andExpect(jsonPath("$.[*].avenant").value(hasItem(DEFAULT_AVENANT.intValue())))
            .andExpect(jsonPath("$.[*].idPrev").value(hasItem(DEFAULT_ID_PREV.intValue())))
            .andExpect(jsonPath("$.[*].idNext").value(hasItem(DEFAULT_ID_NEXT.intValue())))
            .andExpect(jsonPath("$.[*].nette").value(hasItem(DEFAULT_NETTE.intValue())))
            .andExpect(jsonPath("$.[*].tva").value(hasItem(DEFAULT_TVA.intValue())))
            .andExpect(jsonPath("$.[*].etat").value(hasItem(DEFAULT_ETAT.booleanValue())));
    }
}
