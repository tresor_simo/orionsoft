package dev.soft.assurance.web.rest;

import dev.soft.assurance.OrionSoftApp;
import dev.soft.assurance.domain.UserAssurance;
import dev.soft.assurance.repository.UserAssuranceRepository;
import dev.soft.assurance.service.UserAssuranceService;
import dev.soft.assurance.repository.search.UserAssuranceSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the UserAssuranceResource REST controller.
 *
 * @see UserAssuranceResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OrionSoftApp.class)
public class UserAssuranceResourceIntTest {
    private static final String DEFAULT_NUMBER = "AAAAA";
    private static final String UPDATED_NUMBER = "BBBBB";
    private static final String DEFAULT_EMAIL = "AAAAA";
    private static final String UPDATED_EMAIL = "BBBBB";
    private static final String DEFAULT_NOM = "AAAAA";
    private static final String UPDATED_NOM = "BBBBB";

    @Inject
    private UserAssuranceRepository userAssuranceRepository;

    @Inject
    private UserAssuranceService userAssuranceService;

    @Inject
    private UserAssuranceSearchRepository userAssuranceSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restUserAssuranceMockMvc;

    private UserAssurance userAssurance;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        UserAssuranceResource userAssuranceResource = new UserAssuranceResource();
        ReflectionTestUtils.setField(userAssuranceResource, "userAssuranceService", userAssuranceService);
        this.restUserAssuranceMockMvc = MockMvcBuilders.standaloneSetup(userAssuranceResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserAssurance createEntity(EntityManager em) {
        UserAssurance userAssurance = new UserAssurance();
        userAssurance = new UserAssurance()
                .number(DEFAULT_NUMBER)
                .email(DEFAULT_EMAIL)
                .nom(DEFAULT_NOM);
        return userAssurance;
    }

    @Before
    public void initTest() {
        userAssuranceSearchRepository.deleteAll();
        userAssurance = createEntity(em);
    }

    @Test
    @Transactional
    public void createUserAssurance() throws Exception {
        int databaseSizeBeforeCreate = userAssuranceRepository.findAll().size();

        // Create the UserAssurance

        restUserAssuranceMockMvc.perform(post("/api/user-assurances")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(userAssurance)))
                .andExpect(status().isCreated());

        // Validate the UserAssurance in the database
        List<UserAssurance> userAssurances = userAssuranceRepository.findAll();
        assertThat(userAssurances).hasSize(databaseSizeBeforeCreate + 1);
        UserAssurance testUserAssurance = userAssurances.get(userAssurances.size() - 1);
        assertThat(testUserAssurance.getNumber()).isEqualTo(DEFAULT_NUMBER);
        assertThat(testUserAssurance.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testUserAssurance.getNom()).isEqualTo(DEFAULT_NOM);

        // Validate the UserAssurance in ElasticSearch
        UserAssurance userAssuranceEs = userAssuranceSearchRepository.findOne(testUserAssurance.getId());
        assertThat(userAssuranceEs).isEqualToComparingFieldByField(testUserAssurance);
    }

    @Test
    @Transactional
    public void getAllUserAssurances() throws Exception {
        // Initialize the database
        userAssuranceRepository.saveAndFlush(userAssurance);

        // Get all the userAssurances
        restUserAssuranceMockMvc.perform(get("/api/user-assurances?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(userAssurance.getId().intValue())))
                .andExpect(jsonPath("$.[*].number").value(hasItem(DEFAULT_NUMBER.toString())))
                .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())))
                .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM.toString())));
    }

    @Test
    @Transactional
    public void getUserAssurance() throws Exception {
        // Initialize the database
        userAssuranceRepository.saveAndFlush(userAssurance);

        // Get the userAssurance
        restUserAssuranceMockMvc.perform(get("/api/user-assurances/{id}", userAssurance.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(userAssurance.getId().intValue()))
            .andExpect(jsonPath("$.number").value(DEFAULT_NUMBER.toString()))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL.toString()))
            .andExpect(jsonPath("$.nom").value(DEFAULT_NOM.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingUserAssurance() throws Exception {
        // Get the userAssurance
        restUserAssuranceMockMvc.perform(get("/api/user-assurances/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUserAssurance() throws Exception {
        // Initialize the database
        userAssuranceService.save(userAssurance);

        int databaseSizeBeforeUpdate = userAssuranceRepository.findAll().size();

        // Update the userAssurance
        UserAssurance updatedUserAssurance = userAssuranceRepository.findOne(userAssurance.getId());
        updatedUserAssurance
                .number(UPDATED_NUMBER)
                .email(UPDATED_EMAIL)
                .nom(UPDATED_NOM);

        restUserAssuranceMockMvc.perform(put("/api/user-assurances")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedUserAssurance)))
                .andExpect(status().isOk());

        // Validate the UserAssurance in the database
        List<UserAssurance> userAssurances = userAssuranceRepository.findAll();
        assertThat(userAssurances).hasSize(databaseSizeBeforeUpdate);
        UserAssurance testUserAssurance = userAssurances.get(userAssurances.size() - 1);
        assertThat(testUserAssurance.getNumber()).isEqualTo(UPDATED_NUMBER);
        assertThat(testUserAssurance.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testUserAssurance.getNom()).isEqualTo(UPDATED_NOM);

        // Validate the UserAssurance in ElasticSearch
        UserAssurance userAssuranceEs = userAssuranceSearchRepository.findOne(testUserAssurance.getId());
        assertThat(userAssuranceEs).isEqualToComparingFieldByField(testUserAssurance);
    }

    @Test
    @Transactional
    public void deleteUserAssurance() throws Exception {
        // Initialize the database
        userAssuranceService.save(userAssurance);

        int databaseSizeBeforeDelete = userAssuranceRepository.findAll().size();

        // Get the userAssurance
        restUserAssuranceMockMvc.perform(delete("/api/user-assurances/{id}", userAssurance.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean userAssuranceExistsInEs = userAssuranceSearchRepository.exists(userAssurance.getId());
        assertThat(userAssuranceExistsInEs).isFalse();

        // Validate the database is empty
        List<UserAssurance> userAssurances = userAssuranceRepository.findAll();
        assertThat(userAssurances).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchUserAssurance() throws Exception {
        // Initialize the database
        userAssuranceService.save(userAssurance);

        // Search the userAssurance
        restUserAssuranceMockMvc.perform(get("/api/_search/user-assurances?query=id:" + userAssurance.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userAssurance.getId().intValue())))
            .andExpect(jsonPath("$.[*].number").value(hasItem(DEFAULT_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())))
            .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM.toString())));
    }
}
