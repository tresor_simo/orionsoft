package dev.soft.assurance.web.rest;

import dev.soft.assurance.OrionSoftApp;
import dev.soft.assurance.domain.Garanti;
import dev.soft.assurance.repository.GarantiRepository;
import dev.soft.assurance.service.GarantiService;
import dev.soft.assurance.repository.search.GarantiSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the GarantiResource REST controller.
 *
 * @see GarantiResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OrionSoftApp.class)
public class GarantiResourceIntTest {
    private static final String DEFAULT_NOM = "AAAAA";
    private static final String UPDATED_NOM = "BBBBB";
    private static final String DEFAULT_CAPITAUX = "AAAAA";
    private static final String UPDATED_CAPITAUX = "BBBBB";

    @Inject
    private GarantiRepository garantiRepository;

    @Inject
    private GarantiService garantiService;

    @Inject
    private GarantiSearchRepository garantiSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restGarantiMockMvc;

    private Garanti garanti;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        GarantiResource garantiResource = new GarantiResource();
        ReflectionTestUtils.setField(garantiResource, "garantiService", garantiService);
        this.restGarantiMockMvc = MockMvcBuilders.standaloneSetup(garantiResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Garanti createEntity(EntityManager em) {
        Garanti garanti = new Garanti();
        garanti = new Garanti()
                .nom(DEFAULT_NOM)
                .capitaux(DEFAULT_CAPITAUX);
        return garanti;
    }

    @Before
    public void initTest() {
        garantiSearchRepository.deleteAll();
        garanti = createEntity(em);
    }

    @Test
    @Transactional
    public void createGaranti() throws Exception {
        int databaseSizeBeforeCreate = garantiRepository.findAll().size();

        // Create the Garanti

        restGarantiMockMvc.perform(post("/api/garantis")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(garanti)))
                .andExpect(status().isCreated());

        // Validate the Garanti in the database
        List<Garanti> garantis = garantiRepository.findAll();
        assertThat(garantis).hasSize(databaseSizeBeforeCreate + 1);
        Garanti testGaranti = garantis.get(garantis.size() - 1);
        assertThat(testGaranti.getNom()).isEqualTo(DEFAULT_NOM);
        assertThat(testGaranti.getCapitaux()).isEqualTo(DEFAULT_CAPITAUX);

        // Validate the Garanti in ElasticSearch
        Garanti garantiEs = garantiSearchRepository.findOne(testGaranti.getId());
        assertThat(garantiEs).isEqualToComparingFieldByField(testGaranti);
    }

    @Test
    @Transactional
    public void getAllGarantis() throws Exception {
        // Initialize the database
        garantiRepository.saveAndFlush(garanti);

        // Get all the garantis
        restGarantiMockMvc.perform(get("/api/garantis?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(garanti.getId().intValue())))
                .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM.toString())))
                .andExpect(jsonPath("$.[*].capitaux").value(hasItem(DEFAULT_CAPITAUX.toString())));
    }

    @Test
    @Transactional
    public void getGaranti() throws Exception {
        // Initialize the database
        garantiRepository.saveAndFlush(garanti);

        // Get the garanti
        restGarantiMockMvc.perform(get("/api/garantis/{id}", garanti.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(garanti.getId().intValue()))
            .andExpect(jsonPath("$.nom").value(DEFAULT_NOM.toString()))
            .andExpect(jsonPath("$.capitaux").value(DEFAULT_CAPITAUX.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingGaranti() throws Exception {
        // Get the garanti
        restGarantiMockMvc.perform(get("/api/garantis/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateGaranti() throws Exception {
        // Initialize the database
        garantiService.save(garanti);

        int databaseSizeBeforeUpdate = garantiRepository.findAll().size();

        // Update the garanti
        Garanti updatedGaranti = garantiRepository.findOne(garanti.getId());
        updatedGaranti
                .nom(UPDATED_NOM)
                .capitaux(UPDATED_CAPITAUX);

        restGarantiMockMvc.perform(put("/api/garantis")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedGaranti)))
                .andExpect(status().isOk());

        // Validate the Garanti in the database
        List<Garanti> garantis = garantiRepository.findAll();
        assertThat(garantis).hasSize(databaseSizeBeforeUpdate);
        Garanti testGaranti = garantis.get(garantis.size() - 1);
        assertThat(testGaranti.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testGaranti.getCapitaux()).isEqualTo(UPDATED_CAPITAUX);

        // Validate the Garanti in ElasticSearch
        Garanti garantiEs = garantiSearchRepository.findOne(testGaranti.getId());
        assertThat(garantiEs).isEqualToComparingFieldByField(testGaranti);
    }

    @Test
    @Transactional
    public void deleteGaranti() throws Exception {
        // Initialize the database
        garantiService.save(garanti);

        int databaseSizeBeforeDelete = garantiRepository.findAll().size();

        // Get the garanti
        restGarantiMockMvc.perform(delete("/api/garantis/{id}", garanti.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean garantiExistsInEs = garantiSearchRepository.exists(garanti.getId());
        assertThat(garantiExistsInEs).isFalse();

        // Validate the database is empty
        List<Garanti> garantis = garantiRepository.findAll();
        assertThat(garantis).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchGaranti() throws Exception {
        // Initialize the database
        garantiService.save(garanti);

        // Search the garanti
        restGarantiMockMvc.perform(get("/api/_search/garantis?query=id:" + garanti.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(garanti.getId().intValue())))
            .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM.toString())))
            .andExpect(jsonPath("$.[*].capitaux").value(hasItem(DEFAULT_CAPITAUX.toString())));
    }
}
