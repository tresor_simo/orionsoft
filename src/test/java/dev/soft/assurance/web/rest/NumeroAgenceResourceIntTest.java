package dev.soft.assurance.web.rest;

import dev.soft.assurance.OrionSoftApp;
import dev.soft.assurance.domain.NumeroAgence;
import dev.soft.assurance.repository.NumeroAgenceRepository;
import dev.soft.assurance.service.NumeroAgenceService;
import dev.soft.assurance.repository.search.NumeroAgenceSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the NumeroAgenceResource REST controller.
 *
 * @see NumeroAgenceResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OrionSoftApp.class)
public class NumeroAgenceResourceIntTest {
    private static final String DEFAULT_NUMBER = "AAAAA";
    private static final String UPDATED_NUMBER = "BBBBB";

    @Inject
    private NumeroAgenceRepository numeroAgenceRepository;

    @Inject
    private NumeroAgenceService numeroAgenceService;

    @Inject
    private NumeroAgenceSearchRepository numeroAgenceSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restNumeroAgenceMockMvc;

    private NumeroAgence numeroAgence;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        NumeroAgenceResource numeroAgenceResource = new NumeroAgenceResource();
        ReflectionTestUtils.setField(numeroAgenceResource, "numeroAgenceService", numeroAgenceService);
        this.restNumeroAgenceMockMvc = MockMvcBuilders.standaloneSetup(numeroAgenceResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static NumeroAgence createEntity(EntityManager em) {
        NumeroAgence numeroAgence = new NumeroAgence();
        numeroAgence = new NumeroAgence()
                .number(DEFAULT_NUMBER);
        return numeroAgence;
    }

    @Before
    public void initTest() {
        numeroAgenceSearchRepository.deleteAll();
        numeroAgence = createEntity(em);
    }

    @Test
    @Transactional
    public void createNumeroAgence() throws Exception {
        int databaseSizeBeforeCreate = numeroAgenceRepository.findAll().size();

        // Create the NumeroAgence

        restNumeroAgenceMockMvc.perform(post("/api/numero-agences")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(numeroAgence)))
                .andExpect(status().isCreated());

        // Validate the NumeroAgence in the database
        List<NumeroAgence> numeroAgences = numeroAgenceRepository.findAll();
        assertThat(numeroAgences).hasSize(databaseSizeBeforeCreate + 1);
        NumeroAgence testNumeroAgence = numeroAgences.get(numeroAgences.size() - 1);
        assertThat(testNumeroAgence.getNumber()).isEqualTo(DEFAULT_NUMBER);

        // Validate the NumeroAgence in ElasticSearch
        NumeroAgence numeroAgenceEs = numeroAgenceSearchRepository.findOne(testNumeroAgence.getId());
        assertThat(numeroAgenceEs).isEqualToComparingFieldByField(testNumeroAgence);
    }

    @Test
    @Transactional
    public void getAllNumeroAgences() throws Exception {
        // Initialize the database
        numeroAgenceRepository.saveAndFlush(numeroAgence);

        // Get all the numeroAgences
        restNumeroAgenceMockMvc.perform(get("/api/numero-agences?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(numeroAgence.getId().intValue())))
                .andExpect(jsonPath("$.[*].number").value(hasItem(DEFAULT_NUMBER.toString())));
    }

    @Test
    @Transactional
    public void getNumeroAgence() throws Exception {
        // Initialize the database
        numeroAgenceRepository.saveAndFlush(numeroAgence);

        // Get the numeroAgence
        restNumeroAgenceMockMvc.perform(get("/api/numero-agences/{id}", numeroAgence.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(numeroAgence.getId().intValue()))
            .andExpect(jsonPath("$.number").value(DEFAULT_NUMBER.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingNumeroAgence() throws Exception {
        // Get the numeroAgence
        restNumeroAgenceMockMvc.perform(get("/api/numero-agences/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateNumeroAgence() throws Exception {
        // Initialize the database
        numeroAgenceService.save(numeroAgence);

        int databaseSizeBeforeUpdate = numeroAgenceRepository.findAll().size();

        // Update the numeroAgence
        NumeroAgence updatedNumeroAgence = numeroAgenceRepository.findOne(numeroAgence.getId());
        updatedNumeroAgence
                .number(UPDATED_NUMBER);

        restNumeroAgenceMockMvc.perform(put("/api/numero-agences")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedNumeroAgence)))
                .andExpect(status().isOk());

        // Validate the NumeroAgence in the database
        List<NumeroAgence> numeroAgences = numeroAgenceRepository.findAll();
        assertThat(numeroAgences).hasSize(databaseSizeBeforeUpdate);
        NumeroAgence testNumeroAgence = numeroAgences.get(numeroAgences.size() - 1);
        assertThat(testNumeroAgence.getNumber()).isEqualTo(UPDATED_NUMBER);

        // Validate the NumeroAgence in ElasticSearch
        NumeroAgence numeroAgenceEs = numeroAgenceSearchRepository.findOne(testNumeroAgence.getId());
        assertThat(numeroAgenceEs).isEqualToComparingFieldByField(testNumeroAgence);
    }

    @Test
    @Transactional
    public void deleteNumeroAgence() throws Exception {
        // Initialize the database
        numeroAgenceService.save(numeroAgence);

        int databaseSizeBeforeDelete = numeroAgenceRepository.findAll().size();

        // Get the numeroAgence
        restNumeroAgenceMockMvc.perform(delete("/api/numero-agences/{id}", numeroAgence.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean numeroAgenceExistsInEs = numeroAgenceSearchRepository.exists(numeroAgence.getId());
        assertThat(numeroAgenceExistsInEs).isFalse();

        // Validate the database is empty
        List<NumeroAgence> numeroAgences = numeroAgenceRepository.findAll();
        assertThat(numeroAgences).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchNumeroAgence() throws Exception {
        // Initialize the database
        numeroAgenceService.save(numeroAgence);

        // Search the numeroAgence
        restNumeroAgenceMockMvc.perform(get("/api/_search/numero-agences?query=id:" + numeroAgence.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(numeroAgence.getId().intValue())))
            .andExpect(jsonPath("$.[*].number").value(hasItem(DEFAULT_NUMBER.toString())));
    }
}
