package dev.soft.assurance.web.rest;

import dev.soft.assurance.OrionSoftApp;
import dev.soft.assurance.domain.Paiement;
import dev.soft.assurance.repository.PaiementRepository;
import dev.soft.assurance.service.PaiementService;
import dev.soft.assurance.repository.search.PaiementSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import dev.soft.assurance.domain.enumeration.PAYMENT_MODE;
import dev.soft.assurance.domain.enumeration.NATURE_PAIEMENT;
/**
 * Test class for the PaiementResource REST controller.
 *
 * @see PaiementResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OrionSoftApp.class)
public class PaiementResourceIntTest {
    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").withZone(ZoneId.of("Z"));

    private static final ZonedDateTime DEFAULT_DATE_PAIEMENT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_DATE_PAIEMENT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_DATE_PAIEMENT_STR = dateTimeFormatter.format(DEFAULT_DATE_PAIEMENT);

    private static final BigDecimal DEFAULT_SOMME = new BigDecimal(1);
    private static final BigDecimal UPDATED_SOMME = new BigDecimal(2);

    private static final PAYMENT_MODE DEFAULT_MODE_PAIEMENT = PAYMENT_MODE.ESPECE;
    private static final PAYMENT_MODE UPDATED_MODE_PAIEMENT = PAYMENT_MODE.CHEQUE;

    private static final NATURE_PAIEMENT DEFAULT_NATURE_PAIEMENT = NATURE_PAIEMENT.SOLDE;
    private static final NATURE_PAIEMENT UPDATED_NATURE_PAIEMENT = NATURE_PAIEMENT.ACOMPTE;
    private static final String DEFAULT_PC = "AAAAA";
    private static final String UPDATED_PC = "BBBBB";
    private static final String DEFAULT_NUMERO_AVENANT = "AAAAA";
    private static final String UPDATED_NUMERO_AVENANT = "BBBBB";

    private static final Boolean DEFAULT_ETAT = false;
    private static final Boolean UPDATED_ETAT = true;

    @Inject
    private PaiementRepository paiementRepository;

    @Inject
    private PaiementService paiementService;

    @Inject
    private PaiementSearchRepository paiementSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restPaiementMockMvc;

    private Paiement paiement;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PaiementResource paiementResource = new PaiementResource();
        ReflectionTestUtils.setField(paiementResource, "paiementService", paiementService);
        this.restPaiementMockMvc = MockMvcBuilders.standaloneSetup(paiementResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Paiement createEntity(EntityManager em) {
        Paiement paiement = new Paiement();
        paiement = new Paiement()
                .datePaiement(DEFAULT_DATE_PAIEMENT)
                .somme(DEFAULT_SOMME)
                .modePaiement(DEFAULT_MODE_PAIEMENT)
                .naturePaiement(DEFAULT_NATURE_PAIEMENT)
                .pc(DEFAULT_PC)
                .numeroAvenant(DEFAULT_NUMERO_AVENANT)
                .etat(DEFAULT_ETAT);
        return paiement;
    }

    @Before
    public void initTest() {
        paiementSearchRepository.deleteAll();
        paiement = createEntity(em);
    }

    @Test
    @Transactional
    public void createPaiement() throws Exception {
        int databaseSizeBeforeCreate = paiementRepository.findAll().size();

        // Create the Paiement

        restPaiementMockMvc.perform(post("/api/paiements")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(paiement)))
                .andExpect(status().isCreated());

        // Validate the Paiement in the database
        List<Paiement> paiements = paiementRepository.findAll();
        assertThat(paiements).hasSize(databaseSizeBeforeCreate + 1);
        Paiement testPaiement = paiements.get(paiements.size() - 1);
        assertThat(testPaiement.getDatePaiement()).isEqualTo(DEFAULT_DATE_PAIEMENT);
        assertThat(testPaiement.getSomme()).isEqualTo(DEFAULT_SOMME);
        assertThat(testPaiement.getModePaiement()).isEqualTo(DEFAULT_MODE_PAIEMENT);
        assertThat(testPaiement.getNaturePaiement()).isEqualTo(DEFAULT_NATURE_PAIEMENT);
        assertThat(testPaiement.getPc()).isEqualTo(DEFAULT_PC);
        assertThat(testPaiement.getNumeroAvenant()).isEqualTo(DEFAULT_NUMERO_AVENANT);
        assertThat(testPaiement.isEtat()).isEqualTo(DEFAULT_ETAT);

        // Validate the Paiement in ElasticSearch
        Paiement paiementEs = paiementSearchRepository.findOne(testPaiement.getId());
        assertThat(paiementEs).isEqualToComparingFieldByField(testPaiement);
    }

    @Test
    @Transactional
    public void getAllPaiements() throws Exception {
        // Initialize the database
        paiementRepository.saveAndFlush(paiement);

        // Get all the paiements
        restPaiementMockMvc.perform(get("/api/paiements?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(paiement.getId().intValue())))
                .andExpect(jsonPath("$.[*].datePaiement").value(hasItem(DEFAULT_DATE_PAIEMENT_STR)))
                .andExpect(jsonPath("$.[*].somme").value(hasItem(DEFAULT_SOMME.intValue())))
                .andExpect(jsonPath("$.[*].modePaiement").value(hasItem(DEFAULT_MODE_PAIEMENT.toString())))
                .andExpect(jsonPath("$.[*].naturePaiement").value(hasItem(DEFAULT_NATURE_PAIEMENT.toString())))
                .andExpect(jsonPath("$.[*].pc").value(hasItem(DEFAULT_PC.toString())))
                .andExpect(jsonPath("$.[*].numeroAvenant").value(hasItem(DEFAULT_NUMERO_AVENANT.toString())))
                .andExpect(jsonPath("$.[*].etat").value(hasItem(DEFAULT_ETAT.booleanValue())));
    }

    @Test
    @Transactional
    public void getPaiement() throws Exception {
        // Initialize the database
        paiementRepository.saveAndFlush(paiement);

        // Get the paiement
        restPaiementMockMvc.perform(get("/api/paiements/{id}", paiement.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(paiement.getId().intValue()))
            .andExpect(jsonPath("$.datePaiement").value(DEFAULT_DATE_PAIEMENT_STR))
            .andExpect(jsonPath("$.somme").value(DEFAULT_SOMME.intValue()))
            .andExpect(jsonPath("$.modePaiement").value(DEFAULT_MODE_PAIEMENT.toString()))
            .andExpect(jsonPath("$.naturePaiement").value(DEFAULT_NATURE_PAIEMENT.toString()))
            .andExpect(jsonPath("$.pc").value(DEFAULT_PC.toString()))
            .andExpect(jsonPath("$.numeroAvenant").value(DEFAULT_NUMERO_AVENANT.toString()))
            .andExpect(jsonPath("$.etat").value(DEFAULT_ETAT.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingPaiement() throws Exception {
        // Get the paiement
        restPaiementMockMvc.perform(get("/api/paiements/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePaiement() throws Exception {
        // Initialize the database
        paiementService.save(paiement);

        int databaseSizeBeforeUpdate = paiementRepository.findAll().size();

        // Update the paiement
        Paiement updatedPaiement = paiementRepository.findOne(paiement.getId());
        updatedPaiement
                .datePaiement(UPDATED_DATE_PAIEMENT)
                .somme(UPDATED_SOMME)
                .modePaiement(UPDATED_MODE_PAIEMENT)
                .naturePaiement(UPDATED_NATURE_PAIEMENT)
                .pc(UPDATED_PC)
                .numeroAvenant(UPDATED_NUMERO_AVENANT)
                .etat(UPDATED_ETAT);

        restPaiementMockMvc.perform(put("/api/paiements")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedPaiement)))
                .andExpect(status().isOk());

        // Validate the Paiement in the database
        List<Paiement> paiements = paiementRepository.findAll();
        assertThat(paiements).hasSize(databaseSizeBeforeUpdate);
        Paiement testPaiement = paiements.get(paiements.size() - 1);
        assertThat(testPaiement.getDatePaiement()).isEqualTo(UPDATED_DATE_PAIEMENT);
        assertThat(testPaiement.getSomme()).isEqualTo(UPDATED_SOMME);
        assertThat(testPaiement.getModePaiement()).isEqualTo(UPDATED_MODE_PAIEMENT);
        assertThat(testPaiement.getNaturePaiement()).isEqualTo(UPDATED_NATURE_PAIEMENT);
        assertThat(testPaiement.getPc()).isEqualTo(UPDATED_PC);
        assertThat(testPaiement.getNumeroAvenant()).isEqualTo(UPDATED_NUMERO_AVENANT);
        assertThat(testPaiement.isEtat()).isEqualTo(UPDATED_ETAT);

        // Validate the Paiement in ElasticSearch
        Paiement paiementEs = paiementSearchRepository.findOne(testPaiement.getId());
        assertThat(paiementEs).isEqualToComparingFieldByField(testPaiement);
    }

    @Test
    @Transactional
    public void deletePaiement() throws Exception {
        // Initialize the database
        paiementService.save(paiement);

        int databaseSizeBeforeDelete = paiementRepository.findAll().size();

        // Get the paiement
        restPaiementMockMvc.perform(delete("/api/paiements/{id}", paiement.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate ElasticSearch is empty
        boolean paiementExistsInEs = paiementSearchRepository.exists(paiement.getId());
        assertThat(paiementExistsInEs).isFalse();

        // Validate the database is empty
        List<Paiement> paiements = paiementRepository.findAll();
        assertThat(paiements).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchPaiement() throws Exception {
        // Initialize the database
        paiementService.save(paiement);

        // Search the paiement
        restPaiementMockMvc.perform(get("/api/_search/paiements?query=id:" + paiement.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(paiement.getId().intValue())))
            .andExpect(jsonPath("$.[*].datePaiement").value(hasItem(DEFAULT_DATE_PAIEMENT_STR)))
            .andExpect(jsonPath("$.[*].somme").value(hasItem(DEFAULT_SOMME.intValue())))
            .andExpect(jsonPath("$.[*].modePaiement").value(hasItem(DEFAULT_MODE_PAIEMENT.toString())))
            .andExpect(jsonPath("$.[*].naturePaiement").value(hasItem(DEFAULT_NATURE_PAIEMENT.toString())))
            .andExpect(jsonPath("$.[*].pc").value(hasItem(DEFAULT_PC.toString())))
            .andExpect(jsonPath("$.[*].numeroAvenant").value(hasItem(DEFAULT_NUMERO_AVENANT.toString())))
            .andExpect(jsonPath("$.[*].etat").value(hasItem(DEFAULT_ETAT.booleanValue())));
    }
}
