import _root_.io.gatling.core.scenario.Simulation
import ch.qos.logback.classic.{Level, LoggerContext}
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import org.slf4j.LoggerFactory

import scala.concurrent.duration._

/**
 * Performance test for the Contrat entity.
 */
class ContratGatlingTest extends Simulation {

    val context: LoggerContext = LoggerFactory.getILoggerFactory.asInstanceOf[LoggerContext]
    // Log all HTTP requests
    //context.getLogger("io.gatling.http").setLevel(Level.valueOf("TRACE"))
    // Log failed HTTP requests
    //context.getLogger("io.gatling.http").setLevel(Level.valueOf("DEBUG"))

    val baseURL = Option(System.getProperty("baseURL")) getOrElse """http://127.0.0.1:8080"""

    val httpConf = http
        .baseURL(baseURL)
        .inferHtmlResources()
        .acceptHeader("*/*")
        .acceptEncodingHeader("gzip, deflate")
        .acceptLanguageHeader("fr,fr-fr;q=0.8,en-us;q=0.5,en;q=0.3")
        .connection("keep-alive")
        .userAgentHeader("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.10; rv:33.0) Gecko/20100101 Firefox/33.0")

    val headers_http = Map(
        "Accept" -> """application/json"""
    )

    val headers_http_authenticated = Map(
        "Accept" -> """application/json""",
        "X-CSRF-TOKEN" -> "${csrf_token}"
    )

    val scn = scenario("Test the Contrat entity")
        .exec(http("First unauthenticated request")
        .get("/api/account")
        .headers(headers_http)
        .check(status.is(401))
        .check(headerRegex("Set-Cookie", "CSRF-TOKEN=(.*);[P,p]ath=/").saveAs("csrf_token"))).exitHereIfFailed
        .pause(10)
        .exec(http("Authentication")
        .post("/api/authentication")
        .headers(headers_http_authenticated)
        .formParam("j_username", "admin")
        .formParam("j_password", "admin")
        .formParam("remember-me", "true")
        .formParam("submit", "Login")).exitHereIfFailed
        .pause(1)
        .exec(http("Authenticated request")
        .get("/api/account")
        .headers(headers_http_authenticated)
        .check(status.is(200))
        .check(headerRegex("Set-Cookie", "CSRF-TOKEN=(.*);[P,p]ath=/").saveAs("csrf_token")))
        .pause(10)
        .repeat(2) {
            exec(http("Get all contrats")
            .get("/api/contrats")
            .headers(headers_http_authenticated)
            .check(status.is(200)))
            .pause(10 seconds, 20 seconds)
            .exec(http("Create new contrat")
            .post("/api/contrats")
            .headers(headers_http_authenticated)
            .body(StringBody("""{"id":null, "numeroPolice":"SAMPLE_TEXT", "dateEffet":"2020-01-01T00:00:00.000Z", "duree":null, "souscripteur":"SAMPLE_TEXT", "conducteur":"SAMPLE_TEXT", "statutSocio":"SAMPLE_TEXT", "categoriePermis":null, "primeTTC":null, "acompte":null, "status":null, "dateFin":"2020-01-01T00:00:00.000Z", "primeBase":null, "pourcentage":null, "accessoire":null, "coutPolice":null, "asac":null, "carteRose":null, "isDommageCollision":null, "pourcentDommageCollision":null, "isDommageAccident":null, "pourcentDommageAccident":null, "isVolIncendie":null, "pourcentVolIncendie":null, "isVolElectronic":null, "pourcentVolElectronic":null, "annuelVolElectronic":null, "isAvanceSecour":null, "pourcentAvanceSecour":null, "annuelAvanceSecour":null, "isAssistance":null, "pourcentAssistance":null, "isBriseGlace":null, "pourcentBriseGlace":null, "isRecourDefence":null, "pourcentRecourDefense":null, "isIndAcc":null, "annuelIndAcc":null, "isIptInvalid":null, "isIptFMPH":null, "iptDecesCapitaux":null, "iptInvalidCapitaux":null, "iptFMPHCapitaux":null, "iptDecesAnnuel":null, "iptInvalidAnnuel":null, "iptFMPHAnnuel":null, "bonus":null, "surplus":null, "numeroCarteRose":"SAMPLE_TEXT", "numeroAttestation":"SAMPLE_TEXT", "avenant":null, "idPrev":null, "idNext":null, "nette":null, "tva":null, "etat":null}""")).asJSON
            .check(status.is(201))
            .check(headerRegex("Location", "(.*)").saveAs("new_contrat_url"))).exitHereIfFailed
            .pause(10)
            .repeat(5) {
                exec(http("Get created contrat")
                .get("${new_contrat_url}")
                .headers(headers_http_authenticated))
                .pause(10)
            }
            .exec(http("Delete created contrat")
            .delete("${new_contrat_url}")
            .headers(headers_http_authenticated))
            .pause(10)
        }

    val users = scenario("Users").exec(scn)

    setUp(
        users.inject(rampUsers(100) over (1 minutes))
    ).protocols(httpConf)
}
